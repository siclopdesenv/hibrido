// Modules to control application life and create native browser window
const {app, BrowserWindow,globalShortcut} = require('electron')
const path = require('path')
const electron = require('electron')
const fs = require('fs');

var Helper = require('./controller/Helper');
Helper.verificaExistencia('siclop', path.join('c:/'), 'diretorio');
Helper.verificaExistencia('hibrido', path.join('c:/siclop'), 'diretorio');
Helper.verificaExistencia('impressoes', path.join('c:/siclop/hibrido/'), 'diretorio');
Helper.verificaExistencia('cfghibrido.txt', path.join('c:/siclop/hibrido/'), 'arquivo');
Helper.verificaExistencia('cfghibridobd.txt', path.join('c:/siclop/hibrido/'), 'arquivo');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    minWidth: 900,
    minHeight: 820,
    webPreferences: {
      //esta primeira linha se ainda estiver ai previne um problema ao buildar
      webSecurity: false,
      webviewTag: true,
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration:true
    },
    autoHideMenuBar: true
  });

  mainWindow.maximize();

  globalShortcut.register('f5', function() {
    mainWindow.reload()
  });

  /*globalShortcut.register('CommandOrControl+SHIFT+K+C+T', function() {
    mainWindow.webContents.openDevTools();
  });*/

  // and load the index.html of the app.
  //mainWindow.loadFile(__dirname+'/view/html/menuPrincipal.html')
  //mainWindow.loadFile(__dirname+'/view/html/menuTradicional.html')
  mainWindow.loadURL(`file://${__dirname}/view/html/telaLogin.html`);
  //mainWindow.loadFile(__dirname+'/view/html/infoSis.html')

  mainWindow.on('closed', function () {
    mainWindow = null
  })

}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
});

app.on('activate', function () {
  if (mainWindow === null) createWindow()
});