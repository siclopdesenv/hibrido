var path                = require('path');
const Op                = require('sequelize').Op;
const sequelize         = require('sequelize');
const connection        = require(path.join(__dirname + '/../database/connection'));
const mCadPedido        = require(path.join(__dirname + '/../database/model/principais/mTB_PEDIDO.js'));
const mLogradouros      = require(path.join(__dirname + '/../database/model/principais/mTB_LOGRADOUROS.js'));
const mProdutos         = require(path.join(__dirname + '/../database/model/principais/mTB_PRODUTOS.js'));
const mItemPedido       = require(path.join(__dirname + '/../database/model/principais/mTB_ITEM_PED.js'));

mProdutos.init(connection);
mCadPedido.init(connection);
mLogradouros.init(connection);
mItemPedido.init(connection);

class Pedido{
	   
	static async insert(){
        return await mCadPedido.create({
            nr_pedido:          this.nr_pedido,
            cliente:            this.cliente,
            telefone:           this.telefone,
            qde_total_itens:    this.qde_total_itens,
            desconto:           this.desconto,
            acrescimo:          this.acrescimo,
            vl_total:           this.vl_total,
            frete:              this.frete,
            tx_serv:            this.tx_serv,
            nr_nfiscal:         this.nr_nfiscal,
            cpf_cnpj:           this.cpf_cnpj,
            dt_pedido:          this.dt_pedido,
            hr_pedido:          this.hr_pedido,
            supervisor:         this.supervisor,
            obs:                this.obs,
            id_infor_trm:       this.id_infor_trm,
            id_oper:            this.id_oper,
            id_logradouro:      this.id_logradouro,
            tipo_entrega:       this.tipo_entrega,
            numero_mesa:        this.numero_mesa,
            status_pedido:      this.status_pedido,
            num_endereco:       this.num_endereco,
            comp_endereco:      this.comp_endereco
        });
    }

    static async selectAll (){
        return await mCadPedido.findAll();
    }
    static async selectDentroData(inicial, final){
        return await mCadPedido.findAll({
            where:{
                dt_pedido : {
                    [Op.between]:[inicial, final]
                }
            }
              
        });
    }

    static async countAll (){
        return await mCadPedido.count();
    }
    
    static async select(id){
		const item = await mCadPedido.findOne({where: {id_pedido: id}});
		return item;
    }

    static async recuperaNrPedido(dataPedido){
        const item = await mCadPedido.findAll({
            attributes: ['nr_pedido'], 
            order: [['nr_pedido', 'DESC']],
            where:{dt_pedido : dataPedido}}

        );

        return item;
    }

    static update(){
        return mCadPedido.update(
            { 
                desconto:           this.desconto,
                acrescimo:          this.acrescimo,
                vl_total:           this.vl_total,
                frete:              this.frete,
                tx_serv:            this.tx_serv,
                obs:                this.obs,
                id_oper:            this.id_oper,
                status_pedido:      this.status_pedido,
                comp_endereco:      this.comp_endereco
            },
            {where: {id_pedido: this.id_pedido}}
        );
    }
    
    static atualizaStatusPedido(id, statusNovo){
        return mCadPedido.update(
            {
                status_pedido:statusNovo,
            },
            {where: {id_pedido: id}}
        );
    }

    static atualizaValorAcrescimo(id, acrescimo){
        return mCadPedido.update(
            {
                acrescimo:acrescimo,
            },
            {where: {id_pedido: id}}
        );
    }

    static atualizaValorDesconto(id, desconto){
        return mCadPedido.update(
            {
                desconto:desconto,
            },
            {where: {id_pedido: id}}
        );
    }

    static async buscaPedidoSelecionado(numMesa, tipoEntrega, statusMesa){
        return mCadPedido.findOne({
            where: {
                numero_mesa : numMesa,
                tipo_entrega: tipoEntrega,
                status_pedido: {
                    [Op.ne] : statusMesa
                }
            }
        })
    }

    static async atualizaValorPedidoAposAcrDesc(idPedido, valorTotal){
        return mCadPedido.update({
            vl_total: valorTotal
        },{
            where: {
                id_pedido:idPedido
            }
        });
    }

    static async delete(id){
        return await mCadPedido.destroy({
            where : {
                id_pedido : parseInt(id)
            }
        });
    }

    static async like (pesquisa){
        return await mCadPedido.findAll({
            where: {
                nr_pedido: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }
        });
    }

    static async buscaRelacoes(id){
        return 0;
    }

    static async selectJoinLogradouros(id = null){
        return await mCadPedido.findOne(
            {
                raw: true,
                include: [{
                    model: mLogradouros,
                    required: true,
                    atributes: ['rua']
                }],
                where:{
                    id_pedido : id
                }
            }
        );
    }

    static async selectJoinProdutos(id = null){
        mProduto.hasMany(mCadPedido, {foreignKey: 'id_produto'});
        mCadPedido.belongsTo(mProduto, {foreignKey: 'id_produto'});
        return await mCadPedido.findAll({include:[mProduto]});
    }
    
    static trocaRelacao(tipo){
        tipo === 'HASONE' ? mCadPedido.hasOne(mLogradouros, {foreignKey: 'id_logradouro'}) : tipo === 'BELONGSTO' ? mCadPedido.belongsTo(mLogradouros, {foreignKey: 'id_logradouro'}) : '';
    }
}



module.exports = Pedido;