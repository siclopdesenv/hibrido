var path = require('path');
const Op = require('sequelize').Op;
var swal = require('sweetalert');
const helperAuth    = require(path.join(__dirname + '/../helpers/helperDelete'));
var usuarioLogado      = JSON.parse(window.localStorage.getItem('usuario'));
const connection = require(path.join(__dirname + '/../database/connection'));
const mLogradouros = require(path.join(__dirname + '/../database/model/principais/mTB_LOGRADOUROS.js'));
mLogradouros.init(connection);
const mUF = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_UF.js'));
mUF.init(connection);
const mCidade = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_MUNICIPIOS.js'));
mCidade.init(connection);
const mTpLogr = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_TP_LOGR.js'));
mTpLogr.init(connection);

const cCadClientes = require(path.join(__dirname + '/cCadClientes'));
const cCadFuncionarios = require(path.join(__dirname + '/cCadFuncionarios'));
let resulSelect;
let objetosPromise;

class Enderecos {
    //static add(sub_cod, cod_prod, cod_barr, cod_original, nome_prod, vl_venda1, vl_venda2, vl_venda3, qde_estq, qde_min, contr_estq, corredor, prateleira, dt_valid, dt_cad, dt_alt, id_grupo, id_tpprod, id_umed, id_oper){

    static async insert() {
        return await mLogradouros.create({
            titulo: this.titulo,
            rua: this.rua,
            bairro: this.bairro,
            CEP: this.CEP,
            range: this.range,
            frete: this.frete,
            id_tsocial: this.id_tsocial,
            id_tplogr: this.id_tplogr,
            id_uf: this.id_uf,
            id_municipio: this.id_municipio
        });
    }

    static async massiveInsert(array) {
        return await mLogradouros.bulkCreate({
            titulo: this.titulo,
            rua: this.rua,
            bairro: this.bairro,
            CEP: this.CEP,
            range: this.range,
            frete: this.frete,
            id_tsocial: this.id_tsocial,
            id_tplogr: this.id_tplogr,
            id_uf: this.id_uf,
            id_municipio: this.id_municipio
        });
    }

    static async selectAll() {
        return await mLogradouros.findAll();
    }

    static async selectAllJoin() {
        mUF.hasMany(mLogradouros, { foreignKey: 'id_uf' });
        mLogradouros.belongsTo(mUF, { foreignKey: 'id_uf' });
        mCidade.hasMany(mLogradouros, { foreignKey: 'id_municipio' });
        mLogradouros.belongsTo(mCidade, { foreignKey: 'id_municipio' });
        mTpLogr.hasMany(mLogradouros, { foreignKey: 'id_tplogr' });
        mLogradouros.belongsTo(mTpLogr, { foreignKey: 'id_tplogr' });
        return await mLogradouros.findAll({ include: [mUF, mCidade, mTpLogr], order: ['titulo', 'rua'] });
    }

    static async countAll() {
        return await mLogradouros.count();
    }

    static async select(id) {
        mUF.hasMany(mLogradouros, { foreignKey: 'id_uf' });
        mLogradouros.belongsTo(mUF, { foreignKey: 'id_uf' });
        mCidade.hasMany(mLogradouros, { foreignKey: 'id_municipio' });
        mLogradouros.belongsTo(mCidade, { foreignKey: 'id_municipio' });
        mTpLogr.hasMany(mLogradouros, { foreignKey: 'id_tplogr' });
        mLogradouros.belongsTo(mTpLogr, { foreignKey: 'id_tplogr' });
        let item = await mLogradouros.findOne({ where: { id_logradouro: id }, include: [mUF, mCidade, mTpLogr] });
        return item;
    }

    static async selectPorCEP(cep) {
        mUF.hasMany(mLogradouros, { foreignKey: 'id_uf' });
        mLogradouros.belongsTo(mUF, { foreignKey: 'id_uf' });
        mCidade.hasMany(mLogradouros, { foreignKey: 'id_municipio' });
        mLogradouros.belongsTo(mCidade, { foreignKey: 'id_municipio' });
        mTpLogr.hasMany(mLogradouros, { foreignKey: 'id_tplogr' });
        mLogradouros.belongsTo(mTpLogr, { foreignKey: 'id_tplogr' });
        let item = await mLogradouros.findOne({ where: { CEP: cep }, include: [mUF, mCidade, mTpLogr] });
        return item;
    }

    static async selectPorBairro(nomeBairro) {
        let item = await mLogradouros.findAll(
            {
                where: {
                    bairro: {
                        [Op.like]: '%' + nomeBairro + '%'
                    } 
                }
            });
        return item;
    }

    static async recuperaCidade(id) {
        let item = await mCidade.findOne({ where: { id_municipio: id } });
        return item;
    }

    static async selectPorMunicipio(idMunicipio) {
        return await mLogradouros.findAll(
            {
                where:
                {
                    id_municipio: idMunicipio
                }
            }
        );
    }

    static async selectPorTipoLogr(idTpLogr) {
        return await mLogradouros.findAll(
            {
                where:
                {
                    id_tplogr: idTpLogr
                }
            }
        );
    }

    static async selectPorUF(idUF) {
        return await mLogradouros.findAll(
            {
                where:
                {
                    id_uf: idUF
                }
            }
        );
    }

    static async delete(id) {
        if(usuarioLogado.id_nivel !== 3) {
            const escolha = await swal({
                icon: 'warning',
                title: 'Ação não permitida',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Você não tem permissão para concluir excluir registros!',
                buttons: ['Inserir senha administrativa', 'Okay']
            });
            if(escolha) return 'NP';

            const retornoAutenticacao = await helperAuth.solicitaAutenticacaoADM();
            if(!retornoAutenticacao){
                return 'NP';
            }
        }
        return await mLogradouros.destroy({
            where: {
                id_logradouro: parseInt(id)
            }
        });
    }

    static update() {
        return mLogradouros.update(
            {
                titulo: this.titulo,
                rua: this.rua,
                bairro: this.bairro,
                CEP: this.CEP,
                range: this.range,
                frete: this.frete,
                id_tsocial: this.id_tsocial,
                id_tplogr: this.id_tplogr,
                id_uf: this.id_uf,
                id_municipio: this.id_municipio
            },
            { where: { id_logradouro: this.id_logradouro } }
        );
    }

    static updateValorTaxa(idLogradouro, novoValor) {
        return mLogradouros.update(
            {
                frete: novoValor
            },
            { where: { id_logradouro: idLogradouro } }
        );
    }

    static async like(pesquisa) {
        mUF.hasMany(mLogradouros, { foreignKey: 'id_uf' });
        mLogradouros.belongsTo(mUF, { foreignKey: 'id_uf' });

        mCidade.hasMany(mLogradouros, { foreignKey: 'id_municipio' });
        mLogradouros.belongsTo(mCidade, { foreignKey: 'id_municipio' });

        return await mLogradouros.findAll({
            where: {
                [Op.or]: [
                    {
                        titulo: {
                            [Op.like]: '%' + pesquisa + '%'
                        },
                    },
                    {
                        rua: {
                            [Op.like]: '%' + pesquisa + '%'
                        },
                    },
                    {
                        bairro: {
                            [Op.like]: '%' + pesquisa + '%'
                        },
                    }
                ]
                
            },

            include: [mUF, mCidade], order: ['titulo', 'rua']
        });
    }

    static async buscaRelacoes(id) {
        objetosPromise = await cCadClientes.selectPorLogradouro(id);
        resulSelect = objetosPromise.length;
        objetosPromise = await cCadFuncionarios.selectPorLogradouro(id);
        resulSelect += objetosPromise.length;
        return resulSelect;
    }

    static async countAll() {
        return await mLogradouros.count();
    }
}

module.exports = Enderecos;
