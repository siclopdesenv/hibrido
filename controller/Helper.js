var path = require('path');
const fs = require('fs');
let swal = require('sweetalert');

function dataAtualEua(onlyDate){
	var dataBrutaSis = new Date();
	if(onlyDate != true){
		var ss = String(dataBrutaSis.getSeconds()).padStart(2,'0');
		var min = String(dataBrutaSis.getMinutes()).padStart(2,'0');
		var hh = String(dataBrutaSis.getHours()).padStart(2,'0');
	}

	var dd = String(dataBrutaSis.getDate()).padStart(2, '0');
	var mm = String(dataBrutaSis.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = dataBrutaSis.getFullYear();
	
	if(onlyDate != true){
		var dataAtualSis =  yyyy + '-' + mm  + '-' + dd + ' ' + hh + ':' + min + ':' + ss;
	}else{
		var dataAtualSis =  yyyy + '-' + mm  + '-' + dd;
	}
	return dataAtualSis;
}

function dataAtualBr(){
	var dataBrutaSis = new Date();

	var ss = String(dataBrutaSis.getSeconds()).padStart(2,'0');
	var min = String(dataBrutaSis.getMinutes()).padStart(2,'0');
	var hh = String(dataBrutaSis.getHours()).padStart(2,'0');

	var dd = String(dataBrutaSis.getDate()).padStart(2, '0');
	var mm = String(dataBrutaSis.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = dataBrutaSis.getFullYear();

	var dataAtualSis =  dd + '-' + mm  + '-' + yyyy + ' ' + hh + ':' + min + ':' + ss;

	return dataAtualSis;
}

function formataData(data){
	let dataCompleta = String(data.getFullYear()).padStart(2,'0')+"-"+String(data.getMonth() + 1).padStart(2,'0')+"-"+String(data.getDate()).padStart(2,'0');
	return dataCompleta;
}

function converteDataBancoPDataCampo(data){
	return (data.getFullYear() + '-' + data.getMonth().toString().padStart(2,0) + '-' + data.getDate().toString().padStart(2,0));
}

function converteDataBancoPDataString(data){
	let arrData = data.split('-');
	return arrData[2] + '/' + arrData[1] + '/' + arrData[0];
}

function validaData(dataPassada){
	if(dataPassada == undefined){
		return true;
	}
	var strData = dataPassada;
	var partesData = strData.split("-");
	var data = new Date(partesData[0] , partesData[1] - 1, parseInt(partesData[2])+1+"");
	if(data > new Date()){
		return true;
	}else {
		return false;
	}
}

function verificaExistencia(nome, caminho, tipo){
	fs.existsSync(path.join(caminho, nome)) ? console.log("Existe " + path.join(caminho, nome)) : criaDiretorioOuArquivo(nome, caminho, tipo);
}

function criaDiretorioOuArquivo(nome, caminho, tipo) {
	if(tipo.toLowerCase() === 'diretorio'){
		fs.mkdirSync(path.join(caminho, nome));
	}else if(tipo.toLowerCase() === 'arquivo'){
		var data = '';
		nome === 'cfghibrido.txt' ? data = 'dbnaocriado' : nome === 'cfghibridobd.txt' ? data = 'root|123456|bdsiclop|localhost|3309' : null;
		fs.writeFile(path.join(caminho,nome), data,(err)=>{if(err) throw err});
	}
}

function phoneMask(){
	const e = event || window.event;

	var input = e.target;
	var value = (input && input.value) || null;

	if(!value) return;

	value = value.replace(/\D/g,'');

	console.log(value)

	if(value.length === 11){
		const ddd = value.substring(0,2);
		const nine = value.substring(2,3);
		const prefix = value.substring(3,7);
		const suffix = value.substring(7,11);

		const parts = [];

		if(ddd) parts.push('(' + ddd + ')');
		if(nine) parts.push(' ' + nine);
		if(prefix) parts.push(' ' + prefix);
		if (suffix) parts.push('-' + suffix);
		e.target.value = parts.join('');
	}else{
		const ddd = value.substring(0,2);
		const prefix = value.substring(2,6);
		const suffix = value.substring(6,10);

		const parts = [];

		if(ddd) parts.push('(' + ddd + ')');
		if(prefix) parts.push(' ' + prefix);
		if (suffix) parts.push('-' + suffix);
		e.target.value = parts.join('');
	}
}

function phoneMaskSemDDD(){
	const e = event || window.event;

	var input = e.target;
	var value = (input && input.value) || null;

	if(!value) return;

	value = value.replace(/\D/g,'');

	let prefix;
	let suffix;

	if(value.length < 9){
		prefix = value.substring(0,4);
		suffix = value.substring(4,8);	
	}else{
		prefix = value.substring(0,5);
		suffix = value.substring(5,9);
	}

	const parts = [];

	if(prefix) parts.push(prefix);
	if (suffix) parts.push('-' + suffix);
	e.target.value = parts.join('');
}

function maskCPF(){
	const e = event || window.event;

	var input = e.target;
	var value = (input && input.value) || null;

	if(!value) return;

	value = value.replace(/\D/g,'');

	if(value.length === 11) {
		if(!validaCPF(value)) swal({
			icon: 'error',
			title: 'CPF Inválido!',
			text: 'Tente novamente'
		});
	}

	let primeiro 	= value.substring(0,3);
	let segundo 	= value.substring(3,6);
	let terceiro 	= value.substring(6,9);
	let digito 		= value.substring(9,11);

	const parts = [];

	if (primeiro) parts.push(primeiro);
	if (segundo) parts.push('.' + segundo);
	if (terceiro) parts.push('.' + terceiro);
	if (digito) parts.push('-' + digito);
	e.target.value = parts.join('');
}

function maskCNPJ(){
	const e = event || window.event;

	var input = e.target;
	var value = (input && input.value) || null;

	if(!value) return;

	value = value.replace(/\D/g,'');

	if(value.length === 14) {
		if(!validaCNPJ(value)) swal({
			icon: 'error',
			title: 'CNPJ Inválido!',
			text: 'Tente novamente'
		});
	}

	let primeiro 	= value.substring(0,2);
	let segundo 	= value.substring(2,5);
	let terceiro 	= value.substring(5,8);
	let quarto 		= value.substring(8,12);
	let quinto 		= value.substring(12,14);

	const parts = [];

	if (primeiro) parts.push(primeiro);
	if (segundo) parts.push('.' + segundo);
	if (terceiro) parts.push('.' + terceiro);
	if (quarto) parts.push('/' + quarto);
	if (quinto) parts.push('-' + quinto);
	e.target.value = parts.join('');
}

function maskCPFCNPJ(){
	const e = event || window.event;

	var input = e.target;
	var value = (input && input.value) || null;

	if(!value) return;

	value = value.replace(/\D/g,'');

	if(value.length === 14) {
		if(!validaCNPJ(value)) swal({
			icon: 'error',
			title: 'CNPJ Inválido!',
			text: 'Tente novamente'
		});
	}

	if(value.length <= 11){
		let primeiro 	= value.substring(0,3);
		let segundo 	= value.substring(3,6);
		let terceiro 	= value.substring(6,9);
		let digito 		= value.substring(9,11);

		const parts = [];

		if (primeiro) parts.push(primeiro);
		if (segundo) parts.push('.' + segundo);
		if (terceiro) parts.push('.' + terceiro);
		if (digito) parts.push('-' + digito);
		e.target.value = parts.join('');
	}else{
		let primeiro 	= value.substring(0,2);
		let segundo 	= value.substring(2,5);
		let terceiro 	= value.substring(5,8);
		let quarto 		= value.substring(8,12);
		let quinto 		= value.substring(12,14);
	
		const parts = [];
	
		if (primeiro) parts.push(primeiro);
		if (segundo) parts.push('.' + segundo);
		if (terceiro) parts.push('.' + terceiro);
		if (quarto) parts.push('/' + quarto);
		if (quinto) parts.push('-' + quinto);
		e.target.value = parts.join('');
	}
}

function valorEmReal(valor){
	const opts = {style: 'currency', currency: 'BRL'};
	return valor.toLocaleString('pt-BR', opts);
}

function valorEmDollar(valor){
	const opts = {style: 'currency', currency: 'USD'};
	return valor.toLocaleString('en-US', opts);
}

String.prototype.reverse = function(){
	return this.split('').reverse().join(''); 
};

function removeZeroAEsquerda(valor = ''){
	let stringFinal = '';
	let posicaoComeco = 0;
	for(let i = 0; i < valor.length; i++){
		if(valor[i] === '0') posicaoComeco = i + 1;
		else {
			stringFinal = valor.substring(posicaoComeco, valor.length);
			break;
		}
	}
	return stringFinal;
}

function maskMoeda(campo, evento){
	var tecla = (!evento) ? window.event.keyCode : evento.which;
	var valor  =  campo.value.replace(/[^\d]+/gi,'');
	valor = removeZeroAEsquerda(valor).reverse();
	var resultado  = "";
	var mascara = "##.###.###,##".reverse();
	for (var x = 0, y = 0; x < mascara.length && y < valor.length;) {
		if (mascara.charAt(x) != '#') {
			resultado += mascara.charAt(x);
			x++;
		} else {
			resultado += valor.charAt(y);
			y++;
			x++;
		}
	}
	let resultadoFinal = resultado.reverse();
	resultadoFinal = resultadoFinal.length === 1 ?  '0,0' + resultadoFinal : (resultadoFinal.length === 2 ?  '0,' + resultadoFinal : resultadoFinal);
	campo.value = resultadoFinal;
}

function insereMascaraTelefone(value, comDDD){
	const ddd = value.substring(0,2);
	const nine = value.substring(2,3);
	const prefix = value.substring(3,7);
	const suffix = value.substring(7,11);

	const parts = [];

	if(ddd) parts.push('(' + ddd + ')');
	if(nine) parts.push(' ' + nine);
	if(prefix) parts.push(' ' + prefix);
	if (suffix) parts.push('-' + suffix);
	return parts.join('');
}

function validaCPF(cpf){
	var numeros, digitos, soma, i, resultado, digitos_iguais;
	digitos_iguais = 1;
	if (cpf.length < 11)
		return false;
	for (i = 0; i < cpf.length - 1; i++)
		if (cpf.charAt(i) != cpf.charAt(i + 1))
		{
			digitos_iguais = 0;
			break;
		}
		if (!digitos_iguais){
			numeros = cpf.substring(0,9);
			digitos = cpf.substring(9);
			soma = 0;
			for (i = 10; i > 1; i--)
				soma += numeros.charAt(10 - i) * i;
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0))
				return false;
			numeros = cpf.substring(0,10);
			soma = 0;
			for (i = 11; i > 1; i--)
				soma += numeros.charAt(11 - i) * i;
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1))
				return false;
			return true;
		}else
		return false;
}

function validaCNPJ(cnpj) {

		cnpj = cnpj.replace(/[^\d]+/g,'');

		if(cnpj == '') return false;

		if (cnpj.length != 14)
			return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
    	cnpj == "11111111111111" || 
    	cnpj == "22222222222222" || 
    	cnpj == "33333333333333" || 
    	cnpj == "44444444444444" || 
    	cnpj == "55555555555555" || 
    	cnpj == "66666666666666" || 
    	cnpj == "77777777777777" || 
    	cnpj == "88888888888888" || 
    	cnpj == "99999999999999")
    	return false;

    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
    	soma += numeros.charAt(tamanho - i) * pos--;
    	if (pos < 2)
    		pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
    	return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
    	soma += numeros.charAt(tamanho - i) * pos--;
    	if (pos < 2)
    		pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
    	return false;

    return true;
    
}

function horaParaMinuto(hora){
	return (parseInt(hora.split(':')[0] * 60)) + parseInt(hora.split(':')[1]);
}

function versaoSistema(){
	return "1.5.4";
}

function isEmail(email){
	var exclude=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
	if(email.search(exclude) != -1){return false;}
	else {return true;}
}

function verificaInternet(obrigatorio = false){
	//essa funcao verifica a conexao do sistema com a internet e se a acao que esta sendo
	//efetuada no momento precisar de internet entao o parametro "obrigatorio" deve ser 
	//passado, caso contrario a funcao apenas retornará true ou false
	if(!navigator.onLine) {
		if(obrigatorio){
			swal({
				icon: 'warning',
				title: "Sem Conexão com a Internet !!!",
				text: `Tente novamente mais tarde! 
				após o Okay, a página atualizará!`
			}).then(()=>{
				window.location.reload();
			})
		}else{
			return navigator.onLine;
		}
	}else{
		return navigator.onLine;
	}
}

function alertaCampoVazio(nomeCampo){
	swal({
		icon: 'warning',
		title: "Campo obrigatório!",
		text: "O campo "+nomeCampo+" está vazio."
	})
}

function mascaraNumInt(element){
	var clean = element.value.replace(/[^0-9,]*/g, '').replace(',', '');
	document.getElementById(element.id).value = clean;
}

function retiraAcento(string = ''){
	return string.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
}

function removeCaracEspeciais(stringOriginal = ''){
	return stringOriginal.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
}

function controlaTamanhoCampo(campo, limiteCampo){
	const valor = campo.value;
	if(valor.length > limiteCampo) campo.value = valor.substring(0, (valor.length - 1));
}

module.exports = {
	dataAtualEua: dataAtualEua,
	dataAtualBr: dataAtualBr,
	verificaExistencia: verificaExistencia,
	criaDiretorioOuArquivo: criaDiretorioOuArquivo,
	phoneMask: phoneMask,
	phoneMaskSemDDD: phoneMaskSemDDD,
	maskCPF: maskCPF,
	validaCPF: validaCPF,
	valorEmReal: valorEmReal,
	valorEmDollar: valorEmDollar,
	maskMoeda: maskMoeda,
	maskCNPJ: maskCNPJ,
	validaCNPJ: validaCNPJ,
	maskCPFCNPJ:maskCPFCNPJ,
	horaParaMinuto: horaParaMinuto,
	versaoSistema: versaoSistema,
	formataData: formataData,
	validaData: validaData,
	isEmail: isEmail,
	verificaInternet: verificaInternet,
	alertaCampoVazio: alertaCampoVazio,
	mascaraNumInt: mascaraNumInt,
	converteDataBancoPDataCampo: converteDataBancoPDataCampo,
	converteDataBancoPDataString: converteDataBancoPDataString,
	retiraAcento: retiraAcento,
	removeCaracEspeciais:removeCaracEspeciais,
	controlaTamanhoCampo: controlaTamanhoCampo
};