var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mHistFpgto    = require(path.join(__dirname + '/../database/model/principais/mTB_HIST_PGTO'));
const sequelize            = require('sequelize');
const Op            = require('sequelize').Op;

mHistFpgto.init(connection);

class cHistFpgto {

    static async insert(){
        return await mHistFpgto.create({
            nr_pedido: this.nr_pedido,
            vl: this.vl,
            qde_parcela: this.qde_parcela,
            troco: this.troco,
            dt_pedido: this.dt_pedido,
            hr_pedido: this.hr_pedido,
            id_fpgto: this.id_fpgto,
            dt_pgto: this.dt_pgto
        });
    }

    static async insertBulk(formasPagamento){
        return await mHistFpgto.bulkCreate(formasPagamento);
    }

    static select (){
        return mHistFpgto.findAll({
            where: {
                id_hist_fpgto: parseInt(this.id)
            }
        });
    }

    static async selectAll (){
        return await mHistFpgto.findAll();
    }

    static async selectValorSomadoDentroPeriodo(inicial, final){
        return await mHistFpgto.findAll({
            where:{
                dt_pgto : {
                    [Op.between]:[inicial, final]
                }
            }
        });
    }

    static async delete(){
        return await mHistFpgto.destroy({
            where : {
                id_hist_fpgto : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mHistFpgto.update(
            { descricao: this.descricao },
            {where: {id_hist_fpgto: this.id}}
        );
    }

}

module.exports = cHistFpgto;