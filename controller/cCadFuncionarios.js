var path = require('path');
const Op = require('sequelize').Op;
const connection = require(path.join(__dirname + '/../database/connection'));
const mCadPess = require(path.join(__dirname + '/../database/model/principais/mTB_CAD_FUNC.js'));
mCadPess.init(connection);

const mFuncoes = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_FUNCOES.js'));
mFuncoes.init(connection);
const mNivelUsu = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_NIVEL_USU.js'));
mNivelUsu.init(connection);

class Funcionarios{
    
    static async insert(){
        return await mCadPess.create({
            operador: this.operador,
            senha: this.senha,
            nome: this.nome,
            rg: this.rg,
            cpf: this.cpf,
            dt_adm: this.dt_adm,
            numero_casa: this.numero_casa,
            complemento: this.complemento,
            ddd: this.ddd,
            fone1: this.fone1,
            fone2: this.fone2,
            comissao: this.comissao,
            id_nivel: this.id_nivel,
            id_setor: this.id_setor,
            id_funcao: this.id_funcao,
            id_logradouro: this.id_logradouro
        });
    }

    static update(){
        return mCadPess.update(
            { 
            operador: this.operador,
            senha: this.senha,
            nome: this.nome,
            rg: this.rg,
            cpf: this.cpf,
            dt_adm: this.dt_adm,
            numero_casa: this.numero_casa,
            complemento: this.complemento,
            ddd: this.ddd,
            fone1: this.fone1,
            fone2: this.fone2,
            comissao: this.comissao,
            id_nivel: this.id_nivel,
            id_setor: this.id_setor,
            id_funcao: this.id_funcao,
            id_logradouro: this.id_logradouro },
            {where: {id_oper: this.id_oper}}
        );
    }   

    static async selectAll (){
        return await mCadPess.findAll();
    }

    static async countAll (){
        return await mCadPess.count();
    }

    static async select(id){
        event.preventDefault();
		const item = await mCadPess.findOne({where: {id_oper: id}});
		return item;
	}    

	static async delete(id){
        return await mCadPess.destroy({
            where : {
                id_oper : parseInt(id)
            }
        });
    }

    static async validaLogin(login, senha){
        return await mCadPess.findAll({
            where: {
                operador: login,
                senha : senha
            }
        })
    }

    static async recuperaFuncao(id){
        mFuncoes.hasMany(mCadPess, {foreignKey: 'id_funcao'})
        mCadPess.belongsTo(mFuncoes, {foreignKey: 'id_funcao'})
        return await mCadPess.findOne({where: {id_oper: id},include:[mFuncoes]});
    }

    static async recuperaNivel(id){
        mNivelUsu.hasMany(mCadPess, {foreignKey: 'id_nivel'})
        mCadPess.belongsTo(mNivelUsu, {foreignKey: 'id_nivel'})
        return await mCadPess.findOne({where: {id_oper: id},include:[mNivelUsu]});
    }

    static async like (pesquisa){
        return await mCadPess.findAll({
            where: {
                nome: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }
        });
    }

    static async selectPorLogradouro(id){
        return await mCadPess.findAll(
            {
                where:
                    {
                        id_logradouro: id
                    }  
            }
        );
    }
    
    static async buscaRelacoes(id){
        return 0;
    }
}

module.exports = Funcionarios