var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mFuncoes      = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_FUNCOES'));
const cCadFunc      = require(path.join(__dirname + '/cCadFunc'));
mFuncoes.init(connection);

class cFuncoes {

    static async insert(){
        
        return await mFuncoes.create({
            descricao : this.descricao
        });
    }

    static select(){
        mFuncoes.findAll({
            where: {
                id: this.id
        }
        })
    }

    static async selectAll (){
        return await mFuncoes.findAll();
    }

    static async delete(){
        return await mFuncoes.destroy({
            where : {
                id_funcao : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mFuncoes.update(
            { descricao: this.descricao },
            {where: {id_funcao: this.id}}
        );
    }

    static async buscaRelacoes(idFuncao){
        return await cCadFunc.buscaRelacoes(idFuncao);
    }

}

module.exports = cFuncoes;