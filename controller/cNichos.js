var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mNichos       = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_NICHOS'));
mNichos.init(connection);

class cNichos {
    
    static async insert(){
        return await mNichos.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mNichos.findAll();
    }

    static async delete(){
        return await mNichos.destroy({
            where : {
                id_nicho : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mNichos.update(
            { descricao: this.descricao },
            {where: {id_nicho: this.id}}
        );
    }

}

module.exports = cNichos;