var path = require('path');
const Op = require('sequelize').Op;
var swal = require('sweetalert');
var usuarioLogado      = JSON.parse(window.localStorage.getItem('usuario'));
const helperAuth    = require(path.join(__dirname + '/../helpers/helperDelete'));
const connection    = require(path.join(__dirname + '/../database/connection'));
const mCliente      = require(path.join(__dirname + '/../database/model/principais/mTB_CLIENTE.js'));
mCliente.init(connection);
const mLogradouros  = require(path.join(__dirname + '/../database/model/principais/mTB_LOGRADOUROS.js'));
mLogradouros.init(connection);
const mCidade       = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_MUNICIPIOS.js'));
mCidade.init(connection);

class Clientes {

    static async insert() {
        return await mCliente.create({
            cod_cli: this.cod_cli,
            nome_cli: this.nome_cli,
            apelido: this.apelido,
            num_res: this.num_res,
            compl_res: this.compl_res,
            num_com: this.num_com,
            compl_com: this.compl_com,
            referencia: this.referencia,
            credito: this.credito,
            desconto: this.desconto,
            dt_cad: this.dt_cad,
            dt_alt: this.dt_alt,
            dt_nasc: this.dt_nasc,
            id_situacao: this.id_situacao,
            id_fpgto: this.id_fpgto,
            id_logradouro: this.id_logradouro,
            id_oper: this.id_oper,
            ddd_cli: this.ddd_cli,
            fone1: this.fone1,
            fone2: this.fone2,
            fone3: this.fone3
        });
    }

    static async selectAll() {
        return await mCliente.findAll();
    }

    static async countAll() {
        return await mCliente.count();
    }

    static async select(id) {
        mLogradouros.hasMany(mCliente, { foreignKey: 'id_logradouro' })
        mCliente.belongsTo(mLogradouros, { foreignKey: 'id_logradouro' })
        const item = await mCliente.findOne({ where: { id_cli: id }, include: [mLogradouros] });
        return item;
    }
    static async selectCodMax() {
        const item = await mCliente.findAll({ order: ['cod_cli'] });
        return item;
    }

    static async selectOrderByNome() {
        return await mCliente.findAll({ order: ['nome_cli'] });
    }

    static async selectOrderByCod() {
        return await mCliente.findAll({ order: ['cod_cli'] });
    }

    static async selectOrderByTelefone() {
        return await mCliente.findAll({ order: ['fone1'] });
    }

    static async selectPorFpgto(idFpgto) {
        return await mCliente.findAll(
            {
                where:
                {
                    id_fpgto: idFpgto
                }
            }
        );
    }

    static async selectPorSituação(idSituacao) {
        return await mCliente.findAll(
            {
                where:
                {
                    id_situacao: idSituacao
                }
            }
        );
    }

    static update() {
        return mCliente.update(
            {
                cod_cli: this.cod_cli,
                nome_cli: this.nome_cli,
                apelido: this.apelido,
                num_res: this.num_res,
                compl_res: this.compl_res,
                num_com: this.num_com,
                compl_com: this.compl_com,
                referencia: this.referencia,
                credito: this.credito,
                desconto: this.desconto,
                dt_cad: this.dt_cad,
                dt_alt: this.dt_alt,
                dt_nasc: this.dt_nasc,
                id_situacao: this.id_situacao,
                id_fpgto: this.id_fpgto,
                id_logradouro: this.id_logradouro,
                id_oper: this.id_oper,
                ddd_cli: this.ddd_cli,
                fone1: this.fone1,
                fone2: this.fone2,
                fone3: this.fone3
            },
            { where: { id_cli: this.id_cli } }
        );
    }

    static async delete(id) {
        if(usuarioLogado.id_nivel !== 3) {
            const escolha = await swal({
                icon: 'warning',
                title: 'Ação não permitida',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Você não tem permissão para concluir excluir registros!',
                buttons: ['Inserir senha administrativa', 'Okay']
            });
            if(escolha) return 'NP';

            const retornoAutenticacao = await helperAuth.solicitaAutenticacaoADM();
            if(!retornoAutenticacao){
                return 'NP';
            }
        }
        return await mCliente.destroy({
            where: {
                id_cli: parseInt(id)
            }
        });
    }

    static async recuperaLogradouro(id) {
        mLogradouros.hasMany(mCliente, { foreignKey: 'id_logradouro' })
        mCliente.belongsTo(mLogradouros, { foreignKey: 'id_logradouro' })
        return await mCliente.findOne({ where: { id_cli: id }, include: [mLogradouros] });
    }

    static async recuperaTodosClientesByLogradouro() {
        mLogradouros.hasMany(mCliente, { foreignKey: 'id_logradouro' })
        mCliente.belongsTo(mLogradouros, { foreignKey: 'id_logradouro' })
        return await mCliente.findAll({ include: [mLogradouros] });
    }

    static async like(pesquisa) {
        mLogradouros.hasMany(mCliente, { foreignKey: 'id_logradouro' })
        mCliente.belongsTo(mLogradouros, { foreignKey: 'id_logradouro' })
        return await mCliente.findAll({
            include: [mLogradouros],
            where: {
                nome_cli: {
                    [Op.like]: '%' + pesquisa + '%'
                },
            }
        });
    }

    static async likeByPhone(pesquisa) {
        mLogradouros.hasMany(mCliente, { foreignKey: 'id_logradouro' })
        mCliente.belongsTo(mLogradouros, { foreignKey: 'id_logradouro' })
        return await mCliente.findAll({
            include: [mLogradouros],
            where: {
                [Op.or]: {
                    fone1: {
                        [Op.like]: '%' + pesquisa + '%'
                    },

                    fone2: {
                        [Op.like]: '%' + pesquisa + '%'
                    },
                }
            }
        });
    }


    static async likeLog(nomeLog) {
        mLogradouros.hasMany(mCliente, { foreignKey: 'id_logradouro' })
        mCliente.belongsTo(mLogradouros, { foreignKey: 'id_logradouro' })

        return await mCliente.findAll({
            include: [{
                model: mLogradouros,
                where: {
                    rua: {
                        [Op.like]: '%' + nomeLog + '%'
                    }
                }
            }]
        });

    }

    static async selectPorLogradouro(id) {
        return await mCliente.findAll(
            {
                where:
                {
                    id_logradouro: id
                }
            }
        );
    }

    static async buscaRelacoes(id) {
        return 0;
    }
}

module.exports = Clientes;
