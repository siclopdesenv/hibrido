var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mPensamento   = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_PENSAMENTO'));
mPensamento.init(connection);

class cPensamento {

    static async selectAll (){
        return await mPensamento.findAll();
    }

    static async select(id){
		const item = await mPensamento.findOne({where: {id_pensamento: id}});
		return item;
    }
}

module.exports = cPensamento;