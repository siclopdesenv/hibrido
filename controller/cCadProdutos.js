var path = require('path');
var swal = require('sweetalert');
var usuarioLogado      = JSON.parse(window.localStorage.getItem('usuario'));

const helperAuth    = require(path.join(__dirname + '/../helpers/helperDelete'));
const connection = require(path.join(__dirname + '/../database/connection'));
const mProdutos = require(path.join(__dirname + '/../database/model/principais/mTB_PRODUTOS.js'));
mProdutos.init(connection);
const mGrupos = require(path.join(__dirname + '/../database/model/principais/mTB_GRUPO.js'));
mGrupos.init(connection);
const mUnidMed = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_UNID_MED.js'));
mUnidMed.init(connection);
const Helper = require(path.join(__dirname + '/Helper.js'));
const Op = require('sequelize').Op;

class Produtos{

	static async insert(){
        return await mProdutos.create({
            cod_prod:   this.cod_prod,
            cod_original:this.cod_original,
            sub_cod:    this.sub_cod,
            cod_barr:   this.cod_barr,
            nome_prod:  this.nome_prod,
            descricao:  this.descricao,
            vl_venda1:  this.vl_venda1,
            vl_venda2:  this.vl_venda2,
            vl_venda3:  this.vl_venda3,
            vl_broto:   this.vl_broto,
            vl_media:   this.vl_media,
            vl_giga:    this.vl_giga,
            qde_estq:   this.qde_estq,
            qde_min:    this.qde_min,
            contr_estq: this.contr_estq,
            corredor:   this.corredor,
            prateleira: this.prateleira,
            dt_valid:   this.dt_valid,
            dt_cad:     this.dt_cad,
            dt_alt:     this.dt_alt,
            dia_semana: this.dia_semana,
            comissao:   this.comissao,
            ncm:        this.ncm,
            origem:     this.origem,
            cfop:       this.cfop,
            icms:       this.icms,
            csticms:    this.csticms,
            bcicms:     this.bcicms,
            pis:        this.pis,
            cstpis:     this.cstpis,
            bcpis:      this.bcpis,
            cofins:     this.cofins,
            cstcofins:  this.cstcofins,
            bccofins:   this.bccofins,
            porc_mun:   this.porc_mun,
            porc_est:   this.porc_est,
            porc_fed:   this.porc_fed,
            cest:       this.cest,
            id_grupo:   this.id_grupo,
            id_tpprod:  this.id_tpprod,
            id_umed:    this.id_umed,
            id_oper:    this.id_oper
        });
    }   

	static update(){
        return mProdutos.update(
            { 
            cod_prod:   this.cod_prod,
            cod_original:this.cod_original,
            sub_cod:   this.sub_cod,
            cod_barr:   this.cod_barr,
            nome_prod:  this.nome_prod,
            descricao:  this.descricao,
            vl_venda1:  this.vl_venda1,
            vl_venda2:  this.vl_venda2,
            vl_venda3:  this.vl_venda3,
            vl_broto:   this.vl_broto,
            vl_media:   this.vl_media,
            vl_giga:    this.vl_giga,
            qde_estq:   this.qde_estq,
            qde_min:    this.qde_min,
            contr_estq: this.contr_estq,
            corredor:   this.corredor,
            prateleira: this.prateleira,
            dt_valid:   this.dt_valid,
            dt_cad:     this.dt_cad,
            dt_alt:     this.dt_alt,
            dia_semana: this.dia_semana,
            comissao:   this.comissao,
            ncm:        this.ncm,
            origem:     this.origem,
            cfop:       this.cfop,
            icms:       this.icms,
            csticms:    this.csticms,
            bcicms:     this.bcicms,
            pis:        this.pis,
            cstpis:     this.cstpis,
            bcpis:      this.bcpis,
            cofins:     this.cofins,
            cstcofins:  this.cstcofins,
            bccofins:   this.bccofins,
            porc_mun:   this.porc_mun,
            porc_est:   this.porc_est,
            porc_fed:   this.porc_fed,
            cest:       this.cest,
            id_grupo:   this.id_grupo,
            id_tpprod:  this.id_tpprod,
            id_umed:    this.id_umed,
            id_oper:    this.id_oper},
            {where: {id_prod: this.id_prod}}
        );
    }

    static updateQtdEstoque(idProduto, novaQtd){
        return mProdutos.update(
            { 
            qde_estq:   novaQtd
            },
            {where: {id_prod: idProduto}}
        );
    }

	static async selectAll (){
        mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})
        return await mProdutos.findAll({include:[mGrupos], order:['cod_prod']});
    }

    static async countAll (){
        return await mProdutos.count();
    }

	static async select(id){
		const item = await mProdutos.findOne({where: {id_prod: id}});
		return item;
    }
	static async selectQtdItem(id){
		const item = await mProdutos.findOne({ attributes: ['contr_estq', 'qde_estq', 'qde_min'], where: {id_prod: id}});
		return item;
    }

    static async selectByCod(cod){
		const item = await mProdutos.findOne({where: {cod_prod: cod}});
		return item;
    }

    static async selectByCodBarras(codBarras){
        mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})
		const item = await mProdutos.findOne({include:[mGrupos], where: {cod_barr: codBarras}});
		return item;
    }
    
    static async selectCodMax(){
        const item = await mProdutos.findAll({order:['cod_prod']});
        return item;
    }

    static async selectOrderByNome(){
        mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})
        return await mProdutos.findAll({include: [mGrupos], order:['id_grupo', 'nome_prod']});
    }

    static async selectOrderByNomeQtdDisponivel(){
        mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})
        return await mProdutos.findAll({include: [mGrupos], order:['id_grupo', 'nome_prod'], where: {
            [Op.or]:{
                qde_estq : {[Op.gt]: 0},
                contr_estq: {[Op.not] : true}
            }
        } });// colocar clausula or para pegar se for pizza, se bem que no cadastro é obrigatorio o preenchimento
    }

    static async selectOrderByGrupo(){
        mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})
        return await mProdutos.findAll({include: [mGrupos], order:["Grupo.descricao"]});
    }

    static async selectOrderByCod(){
        return await mProdutos.findAll({order:['cod_prod']});
    }

    static async selectOrderByVlVenda1(){
        return await mProdutos.findAll({order:['vl_venda1']});
    }
    
    static async selectPorGrupo(idGrupo){
        return await mProdutos.findAll(
            {
                where:
                    {
                        id_grupo: idGrupo
                    }
            }
        );
    }

    static async selectPorUnidadeMedida(idUnidMed){
        return await mProdutos.findAll(
            {
                where:
                    {
                        id_umed: idUnidMed
                    }  
            }
        );
    }   

    static async selectPorTipoProd(idTipoProd){
        return await mProdutos.findAll(
            {
                where:
                    {
                        id_tpprod: idTipoProd
                    }  
            }
        );
    }

	static async recuperaGrupo(id){
		mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})
		return await mProdutos.findOne({where: {id_prod: id},include:[mGrupos]});
	}

    static async recuperaGrupoPedido(id){
		mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})
		return await mProdutos.findOne({where: {cod_prod: id},include:[mGrupos]});
	}

	static async recuperaUnidMed(id){
		mUnidMed.hasMany(mProdutos, {foreignKey: 'id_umed'})
		mProdutos.belongsTo(mUnidMed, {foreignKey: 'id_umed'})
		return await mProdutos.findOne({where: {id_prod: id},include:[mUnidMed]});
	}

	static async delete(id){
        if(usuarioLogado.id_nivel !== 3) {
            const escolha = await swal({
                icon: 'warning',
                title: 'Ação não permitida',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Você não tem permissão para concluir excluir registros!',
                buttons: ['Inserir senha administrativa', 'Okay']
            });
            if(escolha) return 'NP';

            const retornoAutenticacao = await helperAuth.solicitaAutenticacaoADM();
            if(!retornoAutenticacao){
                return 'NP';
            }
        }
        return await mProdutos.destroy({
            where : {
                id_prod : parseInt(id)
            }
        });
    }

    static async like (pesquisa){
        /*mGrupos.hasMany(mProdutos, {foreignKey: 'id_grupo'})
		mProdutos.belongsTo(mGrupos, {foreignKey: 'id_grupo'})*/

        return await mProdutos.findAll({
            where: {
                [Op.or]: {
                    nome_prod: {
                        [Op.like] : '%' + pesquisa + '%'
                    },
                    cod_prod: {
                        [Op.like] : '%' + pesquisa + '%' 
                    }
                }   
            },
                include:[mGrupos],
                order:['nome_prod']
            }              
        );
    }

    static async buscaRelacoes(id){
        return 0;
    }

}

module.exports = Produtos;