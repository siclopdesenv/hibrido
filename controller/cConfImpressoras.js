var path            = require('path');
var {exec}          = require('child_process');
var os              = require('os');
const Op            = require('sequelize').Op;
const connection    = require(path.join(__dirname + '/../database/connection'));
const mImpressora   = require(path.join(__dirname + '/../database/model/principais/mTB_IMPRESSORA.js'));
mImpressora.init(connection);
const cCadGrupos = require(path.join(__dirname + '/cCadGrupos'));

class cConfImpressoras{
    static async insert(){
        this.nomePC = this.nomePC.trim().toUpperCase();
        //verificando se a impressora está no mesmo computador ou não
        if(this.nomePC !== os.hostname().toUpperCase()) exec('net use lpt' + this.portaLPT + ' \\\\' + this.nomePC + '\\' + this.apelido + ' senha /User:usuario /y');
        else exec('net use lpt' + this.portaLPT + ' \\\\' + this.nomePC + '\\' + this.apelido);
        //BLOQUEAR O CARA DE PREENCHER O CAMPO DA IMPRESSORA COM UMA PORTA LPT JÁ EXISTENTE

        return await mImpressora.create({
            apelido:        this.apelido,
            nomePC:         this.nomePC,
            portaLPT:       this.portaLPT,
            obs_impressora: this.obs_impressora,
            ip_impressora:  this.ip_impressora,
            marca:          this.marca,
            modelo:         this.modelo,
            largura:        this.largura,
            salto:          this.salto
        });
    }

    static update(alterarLPT = false, antigaLPT){
        exec('net use lpt' + antigaLPT +' /d', (error,stdout,stderr)=>{
            if(error){
                console.log(error);
            }

            if(this.nomePC !== os.hostname().toUpperCase()) exec('net use lpt' + this.portaLPT + ' \\\\' + this.nomePC + '\\' + this.apelido + ' senha /User:usuario /y');
            else exec('net use lpt' + this.portaLPT + ' \\\\' + this.nomePC + '\\' + this.apelido);
        });
        
        
        
        mImpressora.update({
            apelido:        this.apelido,
            nomePC:         this.nomePC,
            portaLPT:       this.portaLPT,
            obs_impressora: this.obs_impressora,
            ip_impressora:  this.ip_impressora,
            marca:          this.marca,
            modelo:         this.modelo,
            largura:        this.largura,
            salto:          this.salto
        },{where: {id_impressora: this.id_impressora}});
    }

    static async selectAll (){
        return await mImpressora.findAll();
    }

    static async select(id){
        const item = await mImpressora.findOne({where: {id_impressora: id}});
        return item;
    }

    static async selectPorLPT(portaLPT){
        const item = await mImpressora.findOne({where: {portaLPT: portaLPT}});
        return item;
    }

    static async selectMaiorLPT(){
        const item = await mImpressora.findAll({order: ['portaLPT']});
        return item;
    }

    static async like (pesquisa){
        return await mImpressora.findAll({
            where: {
                apelido: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }
        });
    }

    static async delete(id){
        const objImp = await this.select(id);

        exec('net use lpt' + objImp.portaLPT +' /d');
        return await mImpressora.destroy({
            where : {
                id_impressora : parseInt(id)
            }
        });
    }

    static async sincronizaPortas(portaLPT, nomePC, apelido){
        exec('net use lpt' + portaLPT +' /d', (error,stdout,stderr)=>{
            if(error){
                console.log(error);
            }

            if(nomePC !== os.hostname().toUpperCase()) exec('net use lpt' + portaLPT + ' \\\\' + nomePC + '\\' + apelido + ' senha /User:usuario /y', trataErroNetUse);
            else exec('net use lpt' + portaLPT + ' \\\\' + nomePC + '\\' + apelido, trataErroNetUse);

            function trataErroNetUse(error,stdout,stderr){
                if(error){
                    swal({
                        icon: 'error',
                        title: 'Erro ao configurar impressora!',
                        text: 'Erro: ' + error
                    })
                }
            }
        });
    }
    
    static async buscaRelacoes(id){
        return await cCadGrupos.selectPorImpressora(id);
    } 

}

module.exports = cConfImpressoras;

