var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mSituacao     = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_SITUACAO'));
mSituacao.init(connection);

class cSituacao {

    static async insert(){
        return await mSituacao.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mSituacao.findAll();
    }

    static async delete(){
        return await mSituacao.destroy({
            where : {
                id_situacao : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mSituacao.update(
            { descricao: this.descricao },
            { where: {id_situacao: this.id}}
        );
    }


}

module.exports = cSituacao;