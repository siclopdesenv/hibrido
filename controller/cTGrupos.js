var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mTbtGrupos    = require(path.join(__dirname + '/../database/model/auxiliares/mTBT_GRUPOS'));
mTbtGrupos.init(connection);

class cUnidMed {

    static async selectAll (){
        return await mTbtGrupos.findAll();
    }

    static async select (id_nicho){
        return await mTbtGrupos.findAll({where:{id_nicho: id_nicho}});
    }
}

module.exports = cUnidMed;