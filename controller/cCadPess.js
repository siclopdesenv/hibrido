var path = require('path');

const connection = require(path.join(__dirname + '/../database/connection'));
const mCadPess = require(path.join(__dirname + '/../database/model/principais/mTB_CAD_FUNC'));
mCadPess.init(connection);

class Pess {

    static async selectAll (){
        return await mCadPess.findAll();
    }

    static async selectPorNivel(idNivel){
        return await mCadPess.findAll(
            {
                where:
                    {
                        id_nivel: idNivel
                    }
            }
        );
    }

    static async selectPorFuncao(idFuncao){
        return await mCadPess.findAll(
            {
                where:
                    {
                        id_funcao: idFuncao
                    }
            }
        );
    }

    static async selectPorSetor(idSetor){
        return await mCadPess.findAll(
            {
                where:
                    {
                        id_setor: idSetor
                    }
            }
        );
    }

    static async insert(operador, senha, nome, rg, cpf, dt_adm, numero_casa, complemento, ddd, fone1, fone2, comissao, id_nivel, id_setor, id_funcao, id_logradouro) {
        return await mCadPess.create({
            operador: operador,
            senha: senha,
            nome: nome,
            rg: rg,
            cpf: cpf,
            dt_adm: dt_adm,
            numero_casa: numero_casa,
            complemento: complemento,
            ddd: ddd,
            fone1: fone1,
            fone2: fone2,
            comissao: comissao,
            id_nivel: id_nivel,
            id_setor: id_setor,
            id_funcao: id_funcao,
            id_logradouro: id_logradouro
        });
    }

    static update(id_oper,operador, senha, nome, rg, cpf, dt_adm, numero_casa, complemento, ddd, fone1, fone2, comissao, id_nivel, id_setor, id_funcao, id_logradouro){
        
        return mCadPess.update(
            { 
            operador: operador,
            senha: senha,
            nome: nome,
            rg: rg,
            cpf: cpf,
            dt_adm: dt_adm,
            numero_casa: numero_casa,
            complemento: complemento,
            ddd: ddd,
            fone1: fone1,
            fone2: fone2,
            comissao: comissao,
            id_nivel: id_nivel,
            id_setor: id_setor,
            id_funcao: id_funcao,
            id_logradouro: id_logradouro },
            {where: {id_oper: id_oper}}
        );
    }   

    static async validaLogin(login, senha){
        let existeUsuario;
        let dadosUsuario;
        dadosUsuario = await mCadPess.findAll({
            where: {
                operador: login
            }
        })
        dadosUsuario.length > 0 ? existeUsuario = true : existeUsuario = false;
        dadosUsuario = await mCadPess.findAll({
            where: {
                operador: login,
                senha : senha
            }
        })
        let dados = [dadosUsuario,existeUsuario];
        return dados;
    }

}

module.exports = Pess;