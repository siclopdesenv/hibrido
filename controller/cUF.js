var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mUF           = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_UF'));
const CadEnderecos  = require(path.join(__dirname + '/cCadEnderecos'));
mUF.init(connection);

class cUF {

    static async insert(){
        return await mUF.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mUF.findAll();
    }
    
    static async select (id){
        return await mUF.findOne({where:{id_uf:id}});
    }

    static async delete(){
        return await mUF.destroy({
            where : {
                id_uf : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mUF.update(
            { descricao: this.descricao },
            {where: {id_uf: this.id}}
        );
    }

    static async buscaRelacoes(idUF){
        return await CadEnderecos.selectPorUF(idUF);
    }

}

module.exports = cUF;