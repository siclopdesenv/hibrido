var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mTpVenda      = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_TP_VENDA'));
mTpVenda.init(connection);

class cTpVenda {

    static async insert(){
        return mTpVenda.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mTpVenda.findAll();
    }

    static async delete(){
        return await mTpVenda.destroy({
            where : {
                id_tpvenda : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mTpVenda.update(
            { descricao: this.descricao },
            {where: {id_tpvenda: this.id}}
        );
    }

}

module.exports = cTpVenda;