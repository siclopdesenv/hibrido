var path = require('path');

const connection = require(path.join(__dirname + '/../database/connection'));
const mTerminal = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_TP_TRM'));
mTerminal.init(connection);

class Terminal {

    static async insert(){
        return await mTerminal.create({
            id_tpterminal : this.tipo_terminal,
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mTerminal.findAll();
    }

    static async delete(){
        return await mTerminal.destroy({
            where : {
                id_tpterminal : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mTerminal.update(
            { id_tpterminal: this.id_tpterminal, descricao: this.descricao },
            {where: {id_tpterminal: this.id_tpterminal}}
        );
    }

}

module.exports = Terminal;