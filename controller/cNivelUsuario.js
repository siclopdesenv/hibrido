var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mNivelUsu     = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_NIVEL_USU'));
const cCadFunc      = require(path.join(__dirname + '/cCadFunc'));
mNivelUsu.init(connection);

class cNivelUsu {

    static async insert(){
        return await mNivelUsu.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mNivelUsu.findAll();
    }

    static async delete(){
        return await mNivelUsu.destroy({
            where : {
                id_nivel : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mNivelUsu.update(
            { descricao: this.descricao },
            {where: {id_nivel: this.id}}
        );
    }

    static async buscaRelacoes(idNivel){
        return await cCadFunc.selectPorNivel(idNivel);
    }

}

module.exports = cNivelUsu;