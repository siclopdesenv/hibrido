var path = require('path');
const Helper = require(path.join(__dirname + '/Helper.js'));
const connection = require(path.join(__dirname + '/../database/connection'));
const mLogs = require(path.join(__dirname + '/../database/model/principais/mLogs.js'));
mLogs.init(connection);

    
class Logs{
    static async insert(){
    	let data = Helper.dataAtualEua();
        return await mLogs.create({
            descricao: this.descricao,
            data: data,
            hora: '545',
            id_oper: this.id_oper
        });
    }
}

module.exports = Logs;