var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mCatFiscalPadrao = require(path.join(__dirname + '/../database/model/auxiliares/mTBT_CAT_FISCAL_PADRAO.js'));
const Op            = require('sequelize').Op;

mCatFiscalPadrao.init(connection);

class cCatFiscalPadrao {

    static async insert(){
        return await mCatFiscalPadrao.create({
            descricao : this.descricao
        });
    }

    static select (id){
        return mCatFiscalPadrao.findAll({
            where: {
                id_cat_padrao: parseInt(id)
            }
        });
    }

    static selectPorDesc (){
        return mCatFiscalPadrao.findOne({
            where: {
                descricao: this.descricao
            }
        });
    }

    static selectDescLike(){
        return mCatFiscalPadrao.findOne({
            where: {
                descricao: {
                    [Op.like] : '%' + this.descricao + '%'
                }
            }
        })
    }

    static async selectAll (){
        return await mCatFiscalPadrao.findAll();
    }
}

module.exports = cCatFiscalPadrao;