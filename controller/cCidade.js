var path = require('path');

const connection = require(path.join(__dirname + '/../database/connection'));
const mCidade = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_CIDADE'));
const cCadEnderecos = require(path.join(__dirname + '/cCadEnderecos'));
mCidade.init(connection);

class Cidade {

    static async insert(){
        return await mCidade.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mCidade.findAll();
    }

    static async delete(){
        return await mCidade.destroy({
            where : {
                id_cidade : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mCidade.update(
            { descricao: this.descricao },
            {where: {id_cidade: this.id}}
        );
    }

    static async buscaRelacoes(idCidade){
        return await cCadEnderecos.selectPorCidade(idCidade);
    }
}

module.exports = Cidade;