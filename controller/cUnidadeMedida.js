var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mUnidMed      = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_UNID_MED'));
mUnidMed.init(connection);
const cCadProdutos   = require(path.join(__dirname + '/cCadProdutos'));

class cUnidMed {
    
    static async insert(){
        return await mUnidMed.create({
            descricao : this.descricao,
            sigla: this.sigla
        });
    }

    static async selectAll (){
        return await mUnidMed.findAll();
    }   

    static async delete(){
        return await mUnidMed.destroy({
            where : {
                id_unid_medida : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mUnidMed.update(
            {descricao: this.descricao,
            sigla: this.sigla},
            {where: {id_unid_medida: this.id}}
        );
    }

    static async buscaRelacoes(idUnidMed){
        return await cCadProdutos.selectPorUnidadeMedida(idUnidMed);
    }
}

module.exports = cUnidMed;