var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mINFORSIS     = require(path.join(__dirname + '/../database/model/principais/mINFORSIS.js'));
const mImpressora   = require(path.join(__dirname + '/../database/model/principais/mTB_IMPRESSORA.js'));
mINFORSIS.init(connection);
mImpressora.init(connection);

class cINFORSIS{
	//static add(sub_cod, cod_prod, cod_barr, cod_original, nome_prod, vl_venda1, vl_venda2, vl_venda3, qde_estq, qde_min, contr_estq, corredor, prateleira, dt_valid, dt_cad, dt_alt, id_grupo, id_tpprod, id_umed, id_oper){
	// static add(){
	// 	mINFOSIS.insert(this.cfg_nome, this.cfg_fone1, this.cfg_fone2, this.cfg_fone3, this.cfg_numero_local, this.cfg_complemento, this.cfg_cnpj, this.cfg_nr_serie, this.cfg_cod_lib, this.cfg_pzcombinada, this.cfg_formabroto, this.cfg_porc_broto, this.cfg_porc_media, this.cfg_porc_giga, this.cfg_frete_basico, this.cfg_taxa_servico, this.cfg_qde_cupom, this.cfg_separa_cupom, this.cfg_codigo_cupom, this.cfg_contr_pagto, this.cfg_contr_estq, this.cfg_promo_dia, this.cfg_modulo_movel, this.cfg_modulo_web, this.cfg_modulo_fiscal, this.cfg_contr_motoq, this.cfg_baixa_aqui, this.cfg_repete_item, this.cfg_separa_periodo, this.cfg_cod_barra, this.cfg_faz_delivery, this.cfg_id_logradouro, this.cfg_id_nicho, null);
	// }
	//static insert(cfg_nome, cfg_fone1, cfg_fone2, cfg_fone3, cfg_numero_local, cfg_complemento, cfg_cnpj, cfg_nr_serie, cfg_cod_lib, cfg_pzcombinada, cfg_formabroto, cfg_porc_broto, cfg_porc_media, cfg_porc_giga, cfg_frete_basico, cfg_taxa_servico, cfg_qde_cupom, cfg_separa_cupom, cfg_codigo_cupom, cfg_contr_pagto, cfg_contr_estq, cfg_promo_dia, cfg_modulo_movel, cfg_modulo_web, cfg_modulo_fiscal, cfg_contr_motoq, cfg_baixa_aqui, cfg_repete_item, cfg_separa_periodo, cfg_cod_barra, cfg_faz_delivery, cfg_id_logradouro, cfg_id_nicho, cfg_id_impressora) {
	//
	static async insert(){
        return await mINFORSIS.create({
            cfg_nome:               this.cfg_nome,
            cfg_nome_fantasia:      this.cfg_nome_fantasia,
            cfg_ddd:                this.cfg_ddd,
            cfg_fone1:              this.cfg_fone1,
            cfg_fone2:              this.cfg_fone2,
            cfg_fone3:              this.cfg_fone3,
            cfg_ie:                 this.cfg_ie,
            cfg_im:                 this.cfg_im,
            cfg_numero_local:       this.cfg_numero_local,
            cfg_complemento:        this.cfg_complemento,
            cfg_cnpj:               this.cfg_cnpj,
            email:                  this.email,
            rateio:                 this.rateio,
            cfg_nr_serie:           this.cfg_nr_serie,
            cfg_cod_lib:            this.cfg_cod_lib,
            cfg_pzcombinada:        this.cfg_pzcombinada,
            cfg_formabroto:         this.cfg_formabroto,
            cfg_porc_broto:         this.cfg_porc_broto,
            cfg_porc_media:         this.cfg_porc_media,
            cfg_porc_giga:          this.cfg_porc_giga,
            cfg_frete_basico:       this.cfg_frete_basico,
            cfg_taxa_servico:       this.cfg_taxa_servico,
            cfg_qde_cupom:          this.cfg_qde_cupom,
            cfg_separa_cupom:       this.cfg_separa_cupom,
            cfg_codigo_cupom:       this.cfg_codigo_cupom,
            cfg_preco_cupom:        this.cfg_preco_cupom,
            cfg_canhoto_cupom:      this.cfg_canhoto_cupom,
            cfg_contr_pagto:        this.cfg_contr_pagto,
            cfg_contr_estq:         this.cfg_contr_estq,
            cfg_promo_dia:          this.cfg_promo_dia,
            cfg_modulo_movel:       this.cfg_modulo_movel,
            cfg_modulo_web:         this.cfg_modulo_web,
            cfg_modulo_fiscal:      this.cfg_modulo_fiscal,
            cfg_contr_motoq:        this.cfg_contr_motoq,
            cfg_baixa_aqui:         this.cfg_baixa_aqui,
            cfg_repete_item:        this.cfg_repete_item,
            cfg_separa_periodo:     this.cfg_separa_periodo,
            cfg_cod_barra:          this.cfg_cod_barra,
            cfg_faz_delivery:       this.cfg_faz_delivery,
            cfg_ie:                 this.cfg_ie,
            cfg_im:                 this.cfg_im,
            rateio:                 this.rateio,
            email:                  this.email,
            qde_mesas:              this.qde_mesas,
            id_logradouro:          this.cfg_id_logradouro,
            id_nicho:               this.cfg_id_nicho,
            id_impressora:          this.id_impressora,
            id_regime_tributario:   this.id_regime_tributario,
            qde_mesas:              this.qde_mesas
        });
    }

    static async update(id){  
        return await mINFORSIS.update({ 
            cfg_nome:               this.cfg_nome,
            cfg_nome_fantasia:      this.cfg_nome_fantasia,
            cfg_ddd:                this.cfg_ddd,
            cfg_fone1:              this.cfg_fone1,
            cfg_fone2:              this.cfg_fone2,
            cfg_fone3:              this.cfg_fone3,
            cfg_ie:                 this.cfg_ie,
            cfg_im:                 this.cfg_im,
            cfg_numero_local:       this.cfg_numero_local,
            cfg_complemento:        this.cfg_complemento,
            cfg_cnpj:               this.cfg_cnpj,
            email:                  this.email,
            rateio:                 this.rateio,
            cfg_nr_serie:           this.cfg_nr_serie,
            cfg_cod_lib:            this.cfg_cod_lib,
            cfg_pzcombinada:        this.cfg_pzcombinada,
            cfg_formabroto:         this.cfg_formabroto,
            cfg_porc_broto:         this.cfg_porc_broto,
            cfg_porc_media:         this.cfg_porc_media,
            cfg_porc_giga:          this.cfg_porc_giga,
            cfg_frete_basico:       this.cfg_frete_basico,
            cfg_taxa_servico:       this.cfg_taxa_servico,
            cfg_qde_cupom:          this.cfg_qde_cupom,
            cfg_separa_cupom:       this.cfg_separa_cupom,
            cfg_codigo_cupom:       this.cfg_codigo_cupom,
            cfg_preco_cupom:        this.cfg_preco_cupom,
            cfg_canhoto_cupom:      this.cfg_canhoto_cupom,
            cfg_contr_pagto:        this.cfg_contr_pagto,
            cfg_contr_estq:         this.cfg_contr_estq,
            cfg_promo_dia:          this.cfg_promo_dia,
            cfg_modulo_movel:       this.cfg_modulo_movel,
            cfg_modulo_web:         this.cfg_modulo_web,
            cfg_modulo_fiscal:      this.cfg_modulo_fiscal,
            cfg_contr_motoq:        this.cfg_contr_motoq,
            cfg_baixa_aqui:         this.cfg_baixa_aqui,
            cfg_repete_item:        this.cfg_repete_item,
            cfg_separa_periodo:     this.cfg_separa_periodo,
            cfg_cod_barra:          this.cfg_cod_barra,
            cfg_faz_delivery:       this.cfg_faz_delivery,
            cfg_ie:                 this.cfg_ie,
            cfg_im:                 this.cfg_im,
            rateio:                 this.rateio,
            email:                  this.email,
            qde_mesas:              this.qde_mesas,
            id_logradouro:          this.cfg_id_logradouro,
            id_nicho:               this.cfg_id_nicho,
            id_impressora:          this.id_impressora,
            id_regime_tributario:   this.id_regime_tributario},
            {where: {id: id}}
            );
    }

    static async selectAll(){
        mImpressora.hasMany(mINFORSIS, {foreignKey: 'id_impressora'})
        mINFORSIS.belongsTo(mImpressora, {foreignKey: 'id_impressora'})
        return await mINFORSIS.findAll({include:[mImpressora]});
    }

    // static async select(){
    //     return await this.findOne({where: {id: 1}});
    // }

	// static addAlteracao(){
	// 	mINFOSIS.atualizar( this.id_prod, this.sub_cod, this.cod_prod, this.cod_barr, this.cod_original, this.nome_prod, parseFloat(this.vl_venda1), parseFloat(this.vl_venda2), parseFloat(this.vl_venda3), this.qde_estq, this.qde_min, parseInt(this.contr_estq),parseInt(this.corredor), parseInt(this.prateleira), this.dt_valid, this.dt_cad, this.dt_cad, this.id_grupo, 2, this.id_umed, 1);
	// }

	// static selectAll(){
	// 	return mINFOSIS.selectAll();
	// }

	// static async select(id){
	// 	const item = await mINFOSIS.findOne({where: {id_prod: id}});
	// 	return item;
	// }

}

module.exports = cINFORSIS;