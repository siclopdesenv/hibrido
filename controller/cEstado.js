var path = require('path');

const connection = require(path.join(__dirname + '/../database/connection'));
const mEstado = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_UF'));
const cCadEnderecos = require(path.join(__dirname + '/cCadEnderecos'));
mEstado.init(connection);

class Estado {

    static async insert(){
        return await mEstado.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mEstado.findAll();
    }

    static async delete(){
        return await mEstado.destroy({
            where : {
                id_estado : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mEstado.update(
            { descricao: this.descricao },
            {where: {id_estado: this.id}}
        );
    }

    static async buscaRelacoes(idUF){
        return await cCadEnderecos.selectPorUF(idUF);
    }

}

module.exports = Estado;