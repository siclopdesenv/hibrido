var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mTpProd       = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_TP_PROD'));
mTpProd.init(connection);
const CadProdutos   = require(path.join(__dirname + '/cCadProdutos'));

class cTpProd{

    static async insert(){
        return await mTpProd.create({
            descricao : this.descricao,
            habilitado: this.habilitado
        });
    }

    static async selectAll (){
        return await mTpProd.findAll();
    }

    static async delete(){
        return await mTpProd.destroy({
            where : {
                id_tpprod : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mTpProd.update(
            { descricao: this.descricao,
            habilitado: this.habilitado},
            {where: {id_tpprod: this.id}}
        );
    }

    static async buscaRelacoes(idTipoProd){
        return await CadProdutos.selectPorTipoProd(idTipoProd);
    }

}

module.exports = cTpProd;