var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mLocalPedido  = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_LOCAL_PEDIDO'));
mLocalPedido.init(connection);

class cLocalPedido {
    
    static async insert(){ 
        return await mLocalPedido.create({
            descricao : this.descricao,
            habilitado: this.habilitado
        });
    }

    static async selectAll (){
        return await mLocalPedido.findAll();
    }

    static async delete(){
        return await mLocalPedido.destroy({
            where : {
                id_local : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mLocalPedido.update(
            { descricao: this.descricao,
            habilitado: this.habilitado},
            {where: {id_local: this.id}}
        );
    }

}

module.exports = cLocalPedido;