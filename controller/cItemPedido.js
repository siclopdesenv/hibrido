var path            = require('path');
const {Op, QueryTypes} = require('sequelize');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mItemPedido   = require(path.join(__dirname + '/../database/model/principais/mTB_ITEM_PED.js'));
const mCadPedido    = require(path.join(__dirname + '/../database/model/principais/mTB_PEDIDO.js'));
const mProdutos     = require(path.join(__dirname + '/../database/model/principais/mTB_PRODUTOS.js'));

mCadPedido.init(connection);
mItemPedido.init(connection);
mProdutos.init(connection);

class ItemPedido{
	   
	static async insert(){
        return await mItemPedido.create({
            id_prod:        this.id_prod,
            cod_pedido:     this.cod_pedido,
            desc_produto:   this.desc_produto,
            tam_escolhido:  this.tam_escolhido,
            qtd_prod:       this.qtd_prod,
            vl_unit:        this.vl_unit,
            desconto:       this.desconto,
            acrescimo:      this.acrescimo,
            complemento:    this.complemento
        });
    }

    static async selectAll (){
        return await mItemPedido.findAll();
    }

    static async countAll (){
        return await mItemPedido.count();
    }
    
    static async select(id){
		const item = await mItemPedido.findOne({where: {id_item: id}});
		return item;
    }

    static async selectItensPedido(codPedido){
        mProdutos.hasMany(mItemPedido, {foreignKey: 'id_prod'});
        mItemPedido.belongsTo(mProdutos, {foreignKey: 'id_prod'});
        return await mItemPedido.findAll({where: {cod_pedido: codPedido}});
    }

    static async selectItensDosPedidos(codigosPedidos){
        mProdutos.hasMany(mItemPedido, {foreignKey: 'id_prod'});
        mItemPedido.belongsTo(mProdutos, {foreignKey: 'id_prod'});
        return await mItemPedido.findAll({where: {cod_pedido: { [Op.in] : codigosPedidos }}, include:[mProdutos]});
    }

    static async selectQtdVendidaPorGrupo(codigosPedidos){
        return await connection.query('SELECT sum(`qtd_prod`) AS `sum`, `Produto`.`id_grupo` AS `id_grupo` FROM `tb_item_pedido` AS `tb_item_pedido` JOIN `Produtos` AS `Produto` ON `tb_item_pedido`.`id_prod` = `Produto`.`id_prod` WHERE `tb_item_pedido`.`cod_pedido` IN (' + String(codigosPedidos) + ') GROUP BY `id_grupo`;', {
            type : QueryTypes.SELECT
        });
    }

    static async selectProdutosItens(){
        mProdutos.hasMany(mItemPedido, {foreignKey: 'id_prod'});
        mItemPedido.belongsTo(mProdutos, {foreignKey: 'id_prod'});
        return await mItemPedido.findAll({include:[mProdutos]});
    }

    static update(){        
        return mItemPedido.update(
            { 
                id_prod:        this.id_prod,
                cod_pedido:     this.cod_pedido,
                desc_produto:   this.desc_produto,
                tam_escolhido:  this.tam_escolhido,
                qtd_prod:       this.qtd_prod,
                vl_unit:        this.vl_unit,
                desconto:       this.desconto,
                acrescimo:      this.acrescimo,
                complemento:    this.complemento
            },
            {where: {id_item: this.id_item}}
        );
    }

    static async delete(id){
        return await mItemPedido.destroy({
            where : {
                id_item : parseInt(id)
            }
        });
    }

    static async deleteByCod(id){
        return await mItemPedido.destroy({
            where : {
                cod_pedido : parseInt(id)
            }
        });
    }

    static async like (pesquisa){
        return await mItemPedido.findAll({
            where: {
                id_item: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }
        });
    }

    static async buscaRelacoes(id){
        return 0;
    }
}

module.exports = ItemPedido;