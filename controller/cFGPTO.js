var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mFpgto        = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_FPGTO'));
const Op            = require('sequelize').Op;
const cCadClientes  = require(path.join(__dirname + '/cCadClientes'));

mFpgto.init(connection);

class cFGPTO {

    static async insert(){
        return await mFpgto.create({
            descricao : this.descricao
        });
    }

    static select (){
        return mFpgto.findAll({
            where: {
                id_fpgto: parseInt(this.id)
            }
        });
    }

    static async selectAll (){
        return await mFpgto.findAll();
    }

    static async delete(){
        return await mFpgto.destroy({
            where : {
                id_fpgto : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mFpgto.update(
            { descricao: this.descricao },
            {where: {id_fpgto: this.id}}
        );
    }

    static async like (){
        return await mFpgto.findAll({
            where: {
                descricao: {
                    [Op.like] : '%' + this.descricao + '%'
                },
                id_fpgto: {
                    [Op.like] : '%' + this.descricao + '%'
                }
            }
        });
    }

    static async buscaRelacoes(idFpgto){
        return await cCadClientes.selectPorFpgto(idFpgto);
    }

}

module.exports = cFGPTO;