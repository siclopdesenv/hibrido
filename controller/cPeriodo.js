var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mPeriodo      = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_PERIODO'));
mPeriodo.init(connection);

class cPeriodo {

    static async insert(){
        return await mPeriodo.create({
            descricao:      this.descricao,
            hora_inicio:    this.hora_inicio,
            hora_termino:   this.hora_termino
        });
    }

    static async selectAll (){
        return await mPeriodo.findAll();
    }

    static async selectPeriodoAtual(horaAtual){
		const item = await selectAll.findOne({
            where: {
                [Op.and]: [
                    //isso aqui nao fubnciopna pq o campo e string
                  { hora_inicio: {[Op.lt]: horaAtual} },
                  { hora_termino: {[Op.gt]: horaAtual} }
                ]
            }
        });
		return item;
    }

    static async delete(){
        return await mPeriodo.destroy({
            where : {
                id_periodo : parseInt(this.id)
            }
        });
    }
    
    static async update(){
        return await mPeriodo.update(
            {
                hora_inicio:    this.hora_inicio,
                hora_termino:   this.hora_termino 
            },
            {
                where: {
                    id_periodo: this.id
                }
            }
        );
    }

}

module.exports = cPeriodo;