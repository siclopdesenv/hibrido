var path = require('path');

const connection    = require(path.join(__dirname + '/../database/connection'));
const mMunicipio    = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_MUNICIPIOS'));
const cCadEnderecos = require(path.join(__dirname + '/cCadEnderecos'));
mMunicipio.init(connection);

class cMunicipio {

    static async insert(){
        return await mMunicipio.create({
            descricao : this.descricao
        });
    }

    static async select (id){
        return await mMunicipio.findOne({where:{id_municipio:id}});
    }

    static async selectAll (){
        return await mMunicipio.findAll();
    }

    static async delete(){
        return await mMunicipio.destroy({
            where : {
                id_municipio : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mMunicipio.update(
            { descricao: this.descricao },
            {where: {id_municipio: this.id}}
        );
    }

    static async buscaRelacoes(idMunicipio){
        return await cCadEnderecos.selectPorMunicipio(idMunicipio);
    }
}

module.exports = cMunicipio;