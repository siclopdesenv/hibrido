var path            = require('path');
const Op            = require('sequelize').Op;
const connection    = require(path.join(__dirname + '/../database/connection'));
const mCliente      = require(path.join(__dirname + '/../database/model/principais/mTB_CLIENTE.js'));
mCliente.init(connection);
const mLogradouros  = require(path.join(__dirname + '/../database/model/principais/mTB_LOGRADOUROS.js'));
mLogradouros.init(connection);

class EncontrarCliente{
    static async like (pesquisa){

        mLogradouros.hasMany(mCliente, {foreignKey: 'id_logradouro'})
        mCliente.belongsTo(mLogradouros, {foreignKey: 'id_logradouro'})
        return await mCliente.findAll({
            where: {
                nome_cli: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }, include:[mLogradouros]
        });
    }
    
}

module.exports = EncontrarCliente;