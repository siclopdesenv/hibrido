var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mTpLogr       = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_TP_LOGR'));
const CadEnderecos  = require(path.join(__dirname + '/cCadEnderecos'));
mTpLogr.init(connection);

class cTpLogr{
   
    static async insert(){
        return await mTpLogr.create({
            descricao : this.descricao
        });
    }

    static async select (id){
        return await mTpLogr.findOne({where:{id_tplogr: id}});
    }

    static async selectAll (){
        return await mTpLogr.findAll();
    }

    static async delete(){
        return await mTpLogr.destroy({
            where : {
                id_tplogr : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mTpLogr.update(
            { descricao: this.descricao },
            {where: {id_tplogr: this.id}}
        );
    }

    static async buscaRelacoes(idTpLogr){
        return await cCadEnderecos.selectPorTipoLogr(idTpLogr);
    }

}

module.exports = cTpLogr;