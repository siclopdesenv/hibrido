var path = require('path');

const connection = require(path.join(__dirname + '/../database/connection'));
const mTpPagto = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_FPGTO')); //POSSÍVELMENTE ERRADO
mTpPagto.init(connection);

class TipoPagamento {

    static async insert(){
        return await mTpPagto.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mTpPagto.findAll();
    }

    static async delete(){
        return await mTpPagto.destroy({
            where : {
                id_tppagto : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mTpPagto.update(
            { descricao: this.descricao },
            {where: {id_tppagto: this.id}}
        );
    }

}

module.exports = TipoPagamento;