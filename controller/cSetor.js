var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mSetor        = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_SETOR'));
const CadFunc       = require(path.join(__dirname + '/cCadFunc'));
mSetor.init(connection);

class cSetor {

    static async insert(){
        return await mSetor.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mSetor.findAll();
    }

    static async selectOrderByCod(){
        return await mSetor.findAll({order:['id_setor']});
    }
       
    static async selectOrderByDesc(){
        return await mSetor.findAll({order:['descricao']});
    }

    static async delete(){
        return await mSetor.destroy({
            where : {
                id_setor : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mSetor.update(
            { descricao: this.descricao },
            {where: {id_setor: this.id}}
        );
    }

    static async buscaRelacoes(idSetor){
        return await CadFunc.selectPorSetor(idSetor);
    }

}

module.exports = cSetor;