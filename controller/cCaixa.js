var path                = require('path');
const Op                = require('sequelize').Op;
const sequelize         = require('sequelize');
const connection        = require(path.join(__dirname + '/../database/connection'));
const mCaixa            = require(path.join(__dirname + '/../database/model/principais/mTB_CAIXA.js'));

mCaixa.init(connection);

class Caixa{
	   
	static async insert(){
        return await mCaixa.create({
            dt_caixa: this.dt_caixa,
            fundo_caixa: this.fundo_caixa,
            hr_abre: this.hr_abre,
            hr_fecha: this.hr_fecha,
            id_periodo : this.id_periodo ,
            id_tpterminal : this.id_tpterminal,
            id_oper : this.id_oper
        });
    }

    static async selectAll (){
        return await mCaixa.findAll();
    }

    static async countAll (){
        return await mCaixa.count();
    }
    
    static async select(id){
		const item = await mCaixa.findOne({where: {id_caixa: id}});
		return item;
    }

    static async selectCaixasDia(dataAtual){
		const item = await mCaixa.findAll({
            where: { dt_caixa:dataAtual }
        });
		return item;
    }

    static async selectUltimoCaixa(){
		const item = await mCaixa.findOne({
            order:[['id_caixa', 'DESC']]
        });
		return item;
    }

    static async recuperaValorAtualCaixa(){
		const item = await mCaixa.findOne({
            where: { id_caixa : this.id_caixa }
        });
		return item;
    }


    static async recuperaNrPedido(dataPedido){
        const item = await mCaixa.findAll({
            attributes: ['nr_pedido'], 
            order: [['nr_pedido', 'DESC']],
            where:{dt_pedido : dataPedido}}

        );

        return item;
    }

    static update(){
        return mCaixa.update(
            { 
                dt_caixa: this.dt_caixa,
                fundo_caixa: this.fundo_caixa,
                hr_abre: this.hr_abre,
                hr_fecha: this.hr_fecha,
                id_periodo : this.id_periodo ,
                id_tpterminal : this.id_tpterminal,
                id_oper : this.id_oper
            },
            {where: {id_caixa: this.id_caixa}}
        );
    }
    
    static updateFundoCaixa(idCaixa, fundoCaixa){
        return mCaixa.update(
            {
                fundo_caixa: fundoCaixa,
            },
            {where: {id_caixa: idCaixa}}
        );
    }

    static atualizaStatusPedido(id, statusNovo){        
        return mCaixa.update(
            {
                status_pedido:statusNovo,
            },
            {where: {id_caixa: id}}
        );
    }

    static async delete(id){
        return await mCaixa.destroy({
            where : {
                id_caixa : parseInt(id)
            }
        });
    }

    static async buscaRelacoes(id){
        return 0;
    }
    
    static trocaRelacao(tipo){
        tipo === 'HASONE' ? mCaixa.hasOne(mLogradouros, {foreignKey: 'id_logradouro'}) : tipo === 'BELONGSTO' ? mCaixa.belongsTo(mLogradouros, {foreignKey: 'id_logradouro'}) : '';
    }
}



module.exports = Caixa;