var path = require('path');

const connection = require(path.join(__dirname + '/../database/connection'));
const mDestino = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_LOCAL_PEDIDO'));
mDestino.init(connection);

class Destino {
    
    static async insert(){
        return await mDestino.create({
            descricao : this.descricao,
            habilitado: this.habilitado
        });
    }

    static async selectAll (){
        return await mDestino.findAll();
    }

    static async delete(){
        return await mDestino.destroy({
            where : {
                id_local : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mDestino.update(
            { descricao: this.descricao },
            {where: {id_local: this.id}}
        );
    }

}

module.exports = Destino;