var path            = require('path');
const Op            = require('sequelize').Op;
var swal = require('sweetalert');
const helperAuth    = require(path.join(__dirname + '/../helpers/helperDelete'));
var usuarioLogado      = JSON.parse(window.localStorage.getItem('usuario'));
const connection    = require(path.join(__dirname + '/../database/connection'));
const mCadPess      = require(path.join(__dirname + '/../database/model/principais/mTB_CAD_FUNC.js'));
mCadPess.init(connection);

const mFuncoes      = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_FUNCOES.js'));
mFuncoes.init(connection);
const mNivelUsu     = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_NIVEL_USU.js'));
mNivelUsu.init(connection);

class cCadFunc{
    
    static async insert(){
        return await mCadPess.create({
            operador:       this.operador,
            senha:          this.senha,
            nome:           this.nome,
            rg:             this.rg,
            cpf:            this.cpf,
            ddd:            this.ddd,
            fone1:          this.fone1,
            fone2:          this.fone2,
            dt_adm:         this.dt_adm,
            dt_nasc:        this.dt_nasc,
            num_res:        this.num_res,
            compl_res:      this.compl_res,
            comissao:       this.comissao,
            salario:        this.salario,
            cnh:            this.cnh,
            dt_valid_cnh:   this.dt_valid_cnh,
            banco:          this.banco,
            agencia:        this.agencia,
            conta_banco:    this.conta_banco,
            id_nivel:       this.id_nivel,
            id_setor:       this.id_setor,
            id_funcao:      this.id_funcao,
            id_logradouro:  this.id_logradouro,
            id_tipo_conta:  this.id_tipo_conta,
            id_cat:         this.id_cat,
            frase_seguranca:this.frase_seguranca
        });
    }

    static update(){
        return mCadPess.update(
            { 
            operador:       this.operador,
            senha:          this.senha,
            nome:           this.nome,
            rg:             this.rg,
            cpf:            this.cpf,
            ddd:            this.ddd,
            fone1:          this.fone1,
            fone2:          this.fone2,
            dt_adm:         this.dt_adm,
            dt_nasc:        this.dt_nasc,
            num_res:        this.num_res,
            compl_res:      this.compl_res,
            comissao:       this.comissao,
            salario:        this.salario,
            cnh:            this.cnh,
            dt_valid_cnh:   this.dt_valid_cnh,
            banco:          this.banco,
            agencia:        this.agencia,
            conta_banco:    this.conta_banco,
            id_nivel:       this.id_nivel,
            id_setor:       this.id_setor,
            id_funcao:      this.id_funcao,
            id_logradouro:  this.id_logradouro,
            id_tipo_conta:  this.id_tipo_conta,
            id_cat:         this.id_cat,
            frase_seguranca:this.frase_seguranca},
            {where: {id_oper: this.id_oper}}
        );
    }   

    static atualizaSenha(idUser, novaSenha){
        return mCadPess.update(
            { 
                senha: novaSenha,
            },
            {where: {id_oper: idUser}}
        );
    }   

    static async selectAll (){
        return await mCadPess.findAll();
    }

    static async selectMotoqueiros(){
        mFuncoes.hasMany(mCadPess, { foreignKey: 'id_funcao' });
        mCadPess.belongsTo(mFuncoes, { foreignKey: 'id_funcao' });
        return await mCadPess.findAll({ include: [mFuncoes] });
    }

    static async select(id){
        event.preventDefault();
        const item = await mCadPess.findOne({where: {id_oper: id}});
        return item;
    }
    
    static async selectOrderByNome(){
        return await mCadPess.findAll({order:['nome']});
    }

    static async selectOrderByLogin(){
        return await mCadPess.findAll({order:['operador']});
    }

    static async selectComLogin(login){
        return await mCadPess.findAll({where:{operador:login}});
    }

    static async delete(id){
        if(usuarioLogado.id_nivel !== 3) {
            const escolha = await swal({
                icon: 'warning',
                title: 'Ação não permitida',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Você não tem permissão para concluir excluir registros!',
                buttons: ['Inserir senha administrativa', 'Okay']
            });
            if(escolha) return 'NP';

            const retornoAutenticacao = await helperAuth.solicitaAutenticacaoADM();
            if(!retornoAutenticacao){
                return 'NP';
            }
        }
        return await mCadPess.destroy({
            where : {
                id_oper : parseInt(id)
            }
        });
    }

    static async validaLogin(login, senha){
        return await mCadPess.findAll({
            where: {
                operador: login,
                senha : senha
            }
        })
    }

    static async recuperaFuncao(id){
        mFuncoes.hasMany(mCadPess, {foreignKey: 'id_funcao'})
        mCadPess.belongsTo(mFuncoes, {foreignKey: 'id_funcao'})
        return await mCadPess.findOne({where: {id_oper: id},include:[mFuncoes]});
    }

    static async recuperaNivel(id){
        mNivelUsu.hasMany(mCadPess, {foreignKey: 'id_nivel'})
        mCadPess.belongsTo(mNivelUsu, {foreignKey: 'id_nivel'})
        return await mCadPess.findOne({where: {id_oper: id},include:[mNivelUsu]});
    }

    static async like (pesquisa){
        return await mCadPess.findAll({
            where: {
                nome: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }
        });
    }

    static async selectPorLogradouro(id){
        return await mCadPess.findAll(
            {
                where:
                    {
                        id_logradouro: id
                    }  
            }
        );
    }
    
    static async buscaRelacoes(id){
        return 0;
    }

    static async countAll (){
        return await mCadPess.count();
    }

    static async selectPorSetor(id) {
        return 0;        
    }
}

module.exports = cCadFunc