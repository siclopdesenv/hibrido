var path            = require('path');
const Op            = require('sequelize').Op;
var swal = require('sweetalert');
const helperAuth    = require(path.join(__dirname + '/../helpers/helperDelete'));
var usuarioLogado      = JSON.parse(window.localStorage.getItem('usuario'));
const connection    = require(path.join(__dirname + '/../database/connection'));
const mExtra        = require(path.join(__dirname + '/../database/model/principais/mTB_EXTRA.js'));
mExtra.init(connection);


class ItensExtras{
	//static add(sub_cod, cod_prod, cod_barr, cod_original, nome_prod, vl_venda1, vl_venda2, vl_venda3, qde_estq, qde_min, contr_estq, corredor, prateleira, dt_valid, dt_cad, dt_alt, id_grupo, id_tpprod, id_umed, id_oper){
	static async insert(descricao, acrescimo, dividir){
        return await mExtra.create({
            descricao:  this.descricao,
            acrescimo:  this.acrescimo,
            dividir:    this.dividir
        });
    }

	static update(){        
        return mExtra.update(
            { 
            descricao:  this.descricao,
            acrescimo:  this.acrescimo,
            dividir:    this.dividir },
            {where: {id_extra: this.id_extra}}
        );  
    }  

	static async selectAll(){
		return await mExtra.findAll();
	}

    static async selectOrderByDesc(){
        return await mExtra.findAll({order:['descricao']});
    }

    static async selectOrderByAcres(){
        return await mExtra.findAll({order:['acrescimo']});
    }

    static async countAll (){
        return await mExtra.count();
    }

	static async select(id){
		const item = await mExtra.findOne({where: {id_extra: id}});
		return item;
	}

	static async delete(id){
        if(usuarioLogado.id_nivel !== 3) {
            const escolha = await swal({
                icon: 'warning',
                title: 'Ação não permitida',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Você não tem permissão para concluir excluir registros!',
                buttons: ['Inserir senha administrativa', 'Okay']
            });
            if(escolha) return 'NP';

            const retornoAutenticacao = await helperAuth.solicitaAutenticacaoADM();
            if(!retornoAutenticacao){
                return 'NP';
            }
        }
        return await mExtra.destroy({
            where : {
                id_extra : parseInt(id)
            }
        });
    }

    static async like (pesquisa){
        return await mExtra.findAll({
            where: {
                descricao: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }
        });
    }

    static async buscaRelacoes(id){
        return 0;
    }

    static async countAll (){
        return await mExtra.count();
    }
}

module.exports = ItensExtras;
