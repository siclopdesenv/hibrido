var path = require('path');
const Op = require('sequelize').Op;
const Sequelize = require('sequelize');
var swal = require('sweetalert');
const helperAuth    = require(path.join(__dirname + '/../helpers/helperDelete'));
var usuarioLogado      = JSON.parse(window.localStorage.getItem('usuario'));
const connection = require(path.join(__dirname + '/../database/connection'));
const mGrupos = require(path.join(__dirname + '/../database/model/principais/mTB_GRUPO'));
mGrupos.init(connection);
const mImpressora = require(path.join(__dirname + '/../database/model/principais/mTB_IMPRESSORA.js'));
mImpressora.init(connection);
const cCadProdutos = require(path.join(__dirname + '/cCadProdutos'));

class Grupos {
    static async insert(){
        return await mGrupos.create({
            cod_grupo: this.cod_grupo,
            descricao: this.descricao,
            cupom_separado: this.cupom_separado,
            totaliza: this.totaliza,
            id_cat_padrao: this.id_cat_padrao,
            id_impressora: this.id_impressora
        });
    }

    static update(){
        return mGrupos.update(
            { 
            cod_grupo: this.cod_grupo,
            descricao: this.descricao,
            cupom_separado: this.cupom_separado,
            totaliza: this.totaliza,
            id_cat_padrao: this.id_cat_padrao,
            id_impressora: this.id_impressora},
            {where: {id_grupo: this.id_grupo}}
        );
    }

    static async selectAll (){
        return await mGrupos.findAll();
    }


    static async selectCodMax(){
        const item = await mGrupos.findAll({order:['cod_grupo']});
        return item;
    }

    static async countAll (){
        return await mGrupos.count();
    }

    static async select(id){
        const item = await mGrupos.findOne({where: {id_grupo: id}});
        return item;
    }

    static async selectOrderByCod(){
        return await mGrupos.findAll({order:['cod_grupo']});
    }

    static async selectOrderByDesc(){
        return await mGrupos.findAll({order:['descricao']});
    }

    static async delete(id){
        if(usuarioLogado.id_nivel !== 3) {
            const escolha = await swal({
                icon: 'warning',
                title: 'Ação não permitida',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Você não tem permissão para concluir excluir registros!',
                buttons: ['Inserir senha administrativa', 'Okay']
            });
            if(escolha) return 'NP';

            const retornoAutenticacao = await helperAuth.solicitaAutenticacaoADM();
            if(!retornoAutenticacao){
                return 'NP';
            }
        }
        return await mGrupos.destroy({
            where : {
                id_grupo : parseInt(id)
            }
        });
    }       

    static async recuperaImp(id){
        mImpressora.hasMany(mGrupos, {foreignKey: 'id_impressora'})
        mGrupos.belongsTo(mImpressora, {foreignKey: 'id_impressora'})
        return await mGrupos.findOne({where: {id_grupo: id},include:[mImpressora]});
    }

    static async like (pesquisa){
        return await mGrupos.findAll({
            where: {
                descricao: {
                    [Op.like] : '%' + pesquisa + '%'
                }
            }
        });
    }

    static async selectPorImpressora(id){
        return await mGrupos.findAll(
            {
                where:
                    {
                        id_impressora: id
                    }  
            }
        );
    }  
       
    static async buscaRelacoes(id){
        return await cCadProdutos.selectPorGrupo(id);
    } 
}
 
module.exports = Grupos;