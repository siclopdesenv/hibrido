var path                = require('path');
const Op                = require('sequelize').Op;
const sequelize         = require('sequelize');
const connection        = require(path.join(__dirname + '/../database/connection'));
const mHistVenda        = require(path.join(__dirname + '/../database/model/principais/mTB_HIST_VENDA.js'));
const mTbPedido        = require(path.join(__dirname + '/../database/model/principais/mTB_PEDIDO.js'));


mHistVenda.init(connection);
mTbPedido.init(connection);

class Caixa{
	   
	static async insert(){
        return await mHistVenda.create({
            nr_pedido: this.nr_pedido,
            data: this.data,
            hora: this.hora,
            vl_pedido: this.vl_pedido,
            cliente: this.cliente,
            obs: this.obs,
            id_pedido: this.id_pedido,
            id_oper: this.id_oper,
            id_func_atend: this.id_func_atend,
            id_caixa: this.id_caixa
        });
    }

    static async selectAll (){
        return await mHistVenda.findAll({order: 'dt_caixa'});
    }

    static async countAll (){
        return await mHistVenda.count();
    }
    
    static async select(id){
		const item = await mHistVenda.findOne({where: {id_hist_venda: id}});
		return item;
    }

    static async recuperaNrPedido(dataPedido){
        const item = await mHistVenda.findAll({
            attributes: ['nr_pedido'], 
            order: [['nr_pedido', 'DESC']],
            where:{dt_pedido : dataPedido}}

        );

        return item;
    }

    static update(){        
        return mHistVenda.update(
            { 
                dt_caixa: this.dt_caixa,
                fundo_caixa: this.fundo_caixa,
                hr_abre: this.hr_abre,
                hr_fecha: this.hr_fecha,
                id_periodo : this.id_periodo ,
                id_tpterminal : this.id_tpterminal,
                id_oper : this.id_oper,
                id_func_atend : this.id_func_atend
            },
            {where: {id_hist_venda: this.id_hist_venda}}
        );
    }
    
    static atualizaStatusPedido(id, statusNovo){        
        return mHistVenda.update(
            {
                status_pedido:statusNovo,
            },
            {where: {id_hist_venda: id}}
        );
    }

    static async delete(id){
        return await mHistVenda.destroy({
            where : {
                id_hist_venda : parseInt(id)
            }
        });
    }

    static async buscaVendasJoinPedidoFiltroCaixa(id_caixa){
        mTbPedido.hasMany(mHistVenda, { foreignKey: 'id_pedido' });
        mHistVenda.belongsTo(mTbPedido, { foreignKey: 'id_pedido' });
        return await mHistVenda.findAll(
            {
                include:[mTbPedido],
                where:{
                    'id_caixa' : id_caixa
                }
            }
        )
    }

    static async buscaRelacoes(id){
        return 0;
    }
}



module.exports = Caixa;