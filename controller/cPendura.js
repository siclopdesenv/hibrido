var path                = require('path');
const Op                = require('sequelize').Op;
const sequelize         = require('sequelize');
const connection        = require(path.join(__dirname + '/../database/connection'));
const mPedido           = require(path.join(__dirname + '/../database/model/principais/mTB_PEDIDO.js'));
const mTbPendura        = require(path.join(__dirname + '/../database/model/principais/mTB_PENDURA.js'));

mTbPendura.init(connection);
mPedido.init(connection);

class Pendura{
	   
	static async insert(){
        return await mTbPendura.create({
                dt_receber: this.dt_receber,
                valor_receber: this.valor_receber,
                cliente: this.cliente,
                recebido: this.recebido,
                dt_criacao: this.dt_criacao,
                valor_ja_recebido: this.valor_ja_recebido,
                dt_recebido: this.dt_recebido,
                id_pedido: this.id_pedido,
                id_oper: this.id_oper,
                id_caixa:this.id_caixa 
        });
    }

    static async selectAll(){
        return await mTbPendura.findAll();
    }

    static async selectPendurasCriadasPeriodo(inicial, final){
        return await mTbPendura.findAll({where : { dt_criacao : { [Op.between]:[inicial, final] } } });
    }

    static async countAll (){
        return await mTbPendura.count();
    }
    
    static async select(id){
        mPedido.hasMany(mTbPendura, {foreignKey: 'id_pedido'})
		mTbPendura.belongsTo(mPedido, {foreignKey: 'id_pedido'})
		const item = await mTbPendura.findOne({include:[mPedido], where: {id_pendura: id}});
		return item;
    }

    static update(){
        return mTbPendura.update(
            {
                dt_receber: this.dt_receber,
                valor_receber: this.valor_receber,
                cliente: this.cliente,
                recebido: this.recebido,
                dt_criacao: this.dt_criacao,
                dt_recebido: this.dt_recebido,
                valor_ja_recebido: this.valor_ja_recebido,
                id_pedido: this.id_pedido,
                id_oper: this.id_oper,
                id_caixa:this.id_caixa 
            },
            {where: {id_pendura: this.id_pendura}}
        );
    }

    static async delete(id){
        return await mTbPendura.destroy({
            where : {
                id_pendura : parseInt(id)
            }
        });
    }

    static async buscaRelacoes(id){
        return 0;
    }
}



module.exports = Pendura;