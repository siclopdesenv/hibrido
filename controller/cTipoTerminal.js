var path            = require('path');
const connection    = require(path.join(__dirname + '/../database/connection'));
const mTpTerminal   = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_TP_TRM'));
mTpTerminal.init(connection);

class cTpTrm{
    
    static async insert(){
        return await mTpTerminal.create({
            descricao : this.descricao
        });
    }

    static async selectAll (){
        return await mTpTerminal.findAll();
    }

    static async delete(){
        return await mTpTerminal.destroy({
            where : {
                id_tpterminal : parseInt(this.id)
            }
        });
    }

    static async update(){
        return await mTpTerminal.update(
            { descricao: this.descricao },
            {where: {id_tpterminal: this.id}}
        );
    }

}

module.exports = cTpTrm;