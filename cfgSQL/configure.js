let path = require('path');
let filename = path.join(__dirname, '/mysql5732.zip');
let finalPath = 'C:\\SICLOP\\HIBRIDO';
const sqlDirectoryPath = finalPath + '\\mysql-5.7.35-win32';
const configFilePath = sqlDirectoryPath + '\\my.ini';
const portConfigFilePath = sqlDirectoryPath + '\\portRegister.bat';
const installCPPBatFilePath = sqlDirectoryPath + '\\installCPP.bat';
const firstBatFilePath = sqlDirectoryPath + '\\configureSQL.bat';
const secondBatFilePath = sqlDirectoryPath + '\\configureSQL1.bat';
const contentConfigFile = '[mysqld]\nbasedir=C:/siclop/hibrido/mysql-5.7.35-win32\ndatadir=C:/siclop/hibrido/mysql-5.7.35-win32/data';
const contentCPPFile = 'cd C:/SICLOP/HIBRIDO/mysql-5.7.35-win32\nvcpp2013\n@echo off\nipconfig>C:/SICLOP/HIBRIDO/instalouCPP.txt';
const contentPortBatFile = 'netsh advfirewall firewall add rule name="Porta Entrada Banco" protocol=TCP dir=in localport=3309 action=allow\nnetsh advfirewall firewall add rule name="Porta Saída Banco" protocol=TCP dir=out localport=3309 action=allow';
const contentFBatFile = 'cd C:/SICLOP/HIBRIDO/mysql-5.7.35-win32/bin/\nmysqld --defaults-file=C:/SICLOP/HIBRIDO/mysql-5.7.35-win32/my.ini --initialize-insecure --explicit_defaults_for_timestamp';
const contentSBatFile = 'cd C:/SICLOP/HIBRIDO/mysql-5.7.35-win32/bin/\nmysqld --install MySQL5732 --defaults-file=C:/SICLOP/HIBRIDO/mysql-5.7.35-win32/my.ini\nnet start MySQL5732';
const binPath = sqlDirectoryPath + '\\bin';
let fs = require('fs');
let { exec } = require('child_process');
var DecompressZip = require('decompress-zip');
var unzipper = new DecompressZip(filename);
var swal = require('sweetalert');
var qtdExtraida = 0;
var qtdExtraidaNecessario = 410;

let labelCheckExtrai = document.getElementById('labelCheckExtrai');
let checkExtrai = document.getElementById('checkExtrai');
let checkPortas = document.getElementById('checkPortas');
let checkBanco = document.getElementById('checkBanco');
let checkVisualC = document.getElementById('checkVisualC');

let linhaCheckExtrai = document.getElementById('linhaCheckExtrai');
let linhaCheckPortas = document.getElementById('linhaCheckPortas');
let linhaCheckVisual = document.getElementById('linhaCheckVisual');
let linhaCheckBanco = document.getElementById('linhaCheckBanco');

/*swal({
    icon: 'warning',
    title: 'Aplicando configurações',
    closeOnClickOutside:false,
    closeOnEsc: false,
    text: 'Por favor, aguarde, essa operação vai demorar alguns minutos',
    button: {text: 'Okay', closeModal: false}
});*/

unzipper.on('error', function (err) {
    console.error('Caught an error ' + err);
});

unzipper.on('extract', function (log) {
    console.log('Finished extracting');
    console.log(qtdExtraida);
    if(qtdExtraida !== qtdExtraidaNecessario){
        swal('deu erro na extração');
        return;
    }
    //check-cheeckar primeiro checkbox
    focoLinhaInstalacao(linhaCheckExtrai, false);
    checkExtrai.checked = true;
    runPortRegister();
});

unzipper.on('progress', function (fileIndex, fileCount) {
    focoLinhaInstalacao(linhaCheckExtrai);
    console.log('Extracted file ' + (fileIndex + 1) + ' of ' + fileCount);
    qtdExtraida = (fileIndex + 1);
    labelCheckExtrai.innerHTML = `Extraindo arquivos... (${qtdExtraida} de ${fileCount})`;
});

unzipper.extract({
    path: finalPath,
    filter: function (file) {
        return file.type !== "SymbolicLink";
    }
});

function runPortRegister(){
    focoLinhaInstalacao(linhaCheckPortas);
    fs.writeFileSync(portConfigFilePath, contentPortBatFile);
    exec(path.join(portConfigFilePath), (err, stdout, stdin)=>{
        console.log(err,stdout,stdin);
        checkPortas.checked = true;
        focoLinhaInstalacao(linhaCheckPortas, false);
        configureMySQL();
    });
}

async function configureMySQL(){
    focoLinhaInstalacao(linhaCheckVisual);
    if(!fs.existsSync(sqlDirectoryPath + '\\data')) fs.mkdirSync(sqlDirectoryPath + '\\data');
    fs.writeFileSync(installCPPBatFilePath, contentCPPFile);
    fs.writeFileSync(configFilePath, contentConfigFile);
    fs.writeFileSync(firstBatFilePath, contentFBatFile);
    fs.writeFileSync(secondBatFilePath, contentSBatFile);
    let returnInstallCPP = await exec(path.join(installCPPBatFilePath));        
    
    let passouInstallCPP = false;

    setInterval(()=>{

        if(passouInstallCPP){
            checkVisualC.checked = true;
            focoLinhaInstalacao(linhaCheckVisual, false);
        }

        if(fs.existsSync('C:/SICLOP/HIBRIDO/instalouCPP.txt') && !passouInstallCPP){

            passouInstallCPP = true;

            if(returnInstallCPP.err){
                console.error('Erro ao instalar C++ -> ' + returnInstallCPP.err);
                return;
            }
        
            if(returnInstallCPP){
                focoLinhaInstalacao(linhaCheckBanco);
                exec(path.join(firstBatFilePath), (err, stdout, stdin)=>{
                    if(err){
                        console.error('Erro na ConfigureSQL1 -> ' + err);
                        return;
                    }
                    console.log("Retorno correto CSQL1",stdout);
                    exec(path.join(secondBatFilePath), (err, stdout, stdin)=>{
                        if(err){
                            console.error('Erro na ConfigureSQL2 -> ' + err);
                            return;
                        }
                        console.log("Retorno correto CSQL1",stdout);
                        configuraUsuariosBanco();
            
                        fs.writeFileSync(sqlDirectoryPath + '\\flag.txt', 'false', 'utf-8');
                    });
                });
            }else{
                alert('Não teve retorno correto');
            }

        }
    },3000);
    
}

const cfgHibrido = fs.readFileSync(path.join(caminhoUtilProjeto + 'cfghibridobd.txt'),'utf-8').split('|');
const user = cfgHibrido[0];
//const pass = cfgHibrido[1];
const pass = '';
const database = cfgHibrido[2];
const host = cfgHibrido[3];
const port = cfgHibrido[4];

let mysql = require('mysql2/promise');

function configuraUsuariosBanco(hostRemoto = 'localhost', usuario = 'root'){
    fs.writeFileSync('c:/siclop/hibrido/cfghibridobd.txt', (usuario + '|' + pass + '|' + database + '|' + hostRemoto + '|' + port),'utf-8');

    mysql.createConnection({
        host: hostRemoto,
        user     : usuario,
        password : pass
    }).then(async (connection)=>{

        await connection.query("set password for 'root'@'localhost' = '123456';");
        await connection.query("CREATE USER 'terminal'@'%' IDENTIFIED BY '123456';");
        await connection.query("GRANT ALL PRIVILEGES ON  *.* TO 'terminal'@'%';");
        await connection.query("FLUSH PRIVILEGES;");
        checkBanco.checked = true;
        focoLinhaInstalacao(linhaCheckBanco, false);
        /*swal({
            icon: 'success',
            title: 'MySQL configurado',
            closeOnClickOutside:false,
            closeOnEsc: false,
            text: 'O sistema está pronto para ser utilizado',
            button: {text: 'Prosseguir', closeModal: false}
        });*/
    
    }).catch((err)=>{
    
        console.log(JSON.stringify(err));
    
        if (err.code === 'ER_ACCESS_DENIED_ERROR'){
            swal({
                icon: 'warning',
                title: 'Criando Banco de Dados',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Por favor, aguarde, essa operação vai demorar cerca de 1 minuto',
                button: {text: 'Okay', closeModal: false}
            });
    
            swal({
                icon: 'error',
                text: 'Falha na conexão com o banco de dados: configuração de usuário e/ou senha incorreta!\nContate o suporte SICLOP - (11)2841-6556'
            }).then(c=>{
                const {remote} = require('electron');
                remote.BrowserWindow.getFocusedWindow().close();
            });
            return;
        }
        
        if (err.code === 'ER_BAD_DB_ERROR'){
            require(path.join('../../database/criaBanco'));
            return;
        }
    
        //verificaExistenciaBanco();
        //solicitaInfosBanco();
        return;
        swal({
            icon: 'error',
            text: 'Erro na conexão com o banco de dados: ' + err.message + '\nCódigo do erro: ' + err.code
        });
    
    });
}

function focoLinhaInstalacao(elemento, focar = true){
    if(focar){
        elemento.style.border = '1px solid var(--azulSiclop)';
        elemento.style.borderRadius = '10px';
        elemento.style.boxShadow = '0 0 10px var(--azulSiclop)';
        elemento.style.background = 'var(--azulSiclop)';
        elemento.style.color = '#ffffff';
    }else{
        elemento.style.border = '0px';
        elemento.style.borderRadius = '10px';
        elemento.style.boxShadow = 'none';
        elemento.style.background = '#ffffff';
        elemento.style.color = '#000000';
    }
}