//Helper feito para criar um alerta que pede login e senha do ADM
const cLogin = require(path.join(__dirname + '/../controller/cCadFunc.js'));

async function solicitaAutenticacaoADM(){
    const login = await swal({
        title:'Digite o login',
		content: "input",
        buttons: ["Cancelar", "Inserir senha"]
    });

    const senha = await swal({
        title:'Digite a senha',
		content: {
            element: "input",
            attributes: {
                type: "password",
            },
        },
        buttons: ["Cancelar", "Ok"]
    });
    
    const retornoLogin = await cLogin.validaLogin(login, senha);

    if(retornoLogin == 0) return false
    else if(retornoLogin[0].id_nivel === 3) return true;

    return false;
}

module.exports = {
    solicitaAutenticacaoADM : solicitaAutenticacaoADM
}