var path            = require('path');
const fs = require('fs');
let con = require(path.join(__dirname + '/../database/connection'));
const modelTBA_MUNICIPIOS = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_MUNICIPIOS.js'));
const modelTBA_UF = require(path.join(__dirname + '/../database/model/auxiliares/mTBA_UF.js'));
modelTBA_MUNICIPIOS.init(con);
modelTBA_UF.init(con);

let objUFs = {};
let objCidades = {};
//#region Busca as UFs e cidades para poder associar aos endereços
modelTBA_UF.findAll().then(result=>{
	for(uf of result) objUFs[uf.uf.trim().toUpperCase()] = uf.id_uf;
});

modelTBA_MUNICIPIOS.findAll().then(result=>{
	for(mun of result) objCidades[mun.descricao.trim().toUpperCase()] = mun.id_municipio;
});
//#endregion

let tipoLogr = {
	"R": {
		id_tplogr : 1,
		"descricao": "Rua"
	},
	"AV": {
		id_tplogr : 2,
		"descricao": "Avenida"
	},
	"AC": {
		id_tplogr : 3,
		"descricao": "Acesso"
	},
	"AL": {
		id_tplogr : 4,
		"descricao": "Alameda"
	},
	"AT": {
		id_tplogr : 5,
		"descricao": "Alto"
	},
	"AR": {
		id_tplogr : 6,
		"descricao": "Área"
	},
	"BL": {
		id_tplogr : 7,
		"descricao": "Balão"
	},
	"Bc": {
		id_tplogr : 8,
		"descricao": "Beco"
	},
	"BV": {
		id_tplogr : 9,
		"descricao": "Boulevard"
	},
	"Bos": {
		id_tplogr : 10,
		"descricao": "Bosque"
	},
	"C": {
		id_tplogr : 11,
		"descricao": "Cais"
	},
	"Cc": {
		id_tplogr : 12,
		"descricao": "Calçada"
	},
	"Cam": {
		id_tplogr : 13,
		"descricao": "Caminho"
	},
	"Clu": {
		id_tplogr : 14,
		"descricao": "Clube"
	},
	"Col": {
		id_tplogr : 15,
		"descricao": "Colónia"
	},
	"CJ": {
		id_tplogr : 16,
		"descricao": "Conjunto"
	},
	"Con": {
		id_tplogr : 17,
		"descricao": "Condomínio"
	},
	"Cor": {
		id_tplogr : 18,
		"descricao": "Corredor"
	},
	"Ent": {
		id_tplogr : 19,
		"descricao": "Entrada"
	},
	"Esc": {
		id_tplogr : 20,
		"descricao": "Escada/Escadaria"
	},
	"Estacionamento": {
		id_tplogr : 21,
		"descricao": "Estacionamento"
	},
	"ET": {
		id_tplogr : 22,
		"descricao": "Estrada"
	},
	"FV": {
		id_tplogr : 23,
		"descricao": "Favela"
	},
	"GL": {
		id_tplogr : 24,
		"descricao": "Galeria"
	},
	"GJ": {
		id_tplogr : 25,
		"descricao": "Granja"
	},
	"IL": {
		id_tplogr : 26,
		"descricao": "Ilha"
	},
	"JD": {
		id_tplogr : 27,
		"descricao": "Jardim"
	},
	"LD": {
		id_tplogr : 28,
		"descricao": "Ladeira"
	},
	"LG": {
		id_tplogr : 29,
		"descricao": "Largo"
	},
	"LT": {
		id_tplogr : 30,
		"descricao": "Loteamento"
	},
	"MT": {
		id_tplogr : 31,
		"descricao": "Monte"
	},
	"MR": {
		id_tplogr : 32,
		"descricao": "Morro"
	},
	"PQ": {
		id_tplogr : 33,
		"descricao": "Parque"
	},
	"PA": {
		id_tplogr : 34,
		"descricao": "Pátio"
	},
	"PS": {
		id_tplogr : 35,
		"descricao": "Passagem"
	},
	"Passeio": {
		id_tplogr : 36,
		"descricao": "Passeio"
	},
	"PT": {
		id_tplogr : 37,
		"descricao": "Ponte"
	},
	"PC": {
		id_tplogr : 38,
		"descricao": "Praça"
	},
	"PR": {
		id_tplogr : 39,
		"descricao": "Praia"
	},
	"Q": {
		id_tplogr : 40,
		"descricao": "Quadra"
	},
	"RC": {
		id_tplogr : 41,
		"descricao": "Recanto"
	},
	"Ret": {
		id_tplogr : 42,
		"descricao": "Retorno"
	},
	"RD": {
		id_tplogr : 43,
		"descricao": "Rodovia"
	},
	"RT": {
		id_tplogr : 44,
		"descricao": "Rotatória"
	},
	"TM": {
		id_tplogr : 45,
		"descricao": "Terminal"
	},
	"TR": {
		id_tplogr : 46,
		"descricao": "Travessa"
	},
	"Trecho": {
		id_tplogr : 47,
		"descricao": "Trecho"
	},
	"TV": {
		id_tplogr : 48,
		"descricao": "Trevo"
	},
	"TN": {
		id_tplogr : 49,
		"descricao": "Túnel"
	},
	"VR": {
		id_tplogr : 50,
		"descricao": "Vereda"
	},
	"VL": {
		id_tplogr : 51,
		"descricao": "Vale"
	},
	"VD": {
		id_tplogr : 52,
		"descricao": "Viaduto"
	},
	"VIA": {
		id_tplogr : 53,
		"descricao": "Via"
	},
	"VI": {
		id_tplogr : 54,
		"descricao": "VI"
	}
};

let conteudoMegaTxt = fs.readFileSync(path.join(__dirname + '/../database/ceps.txt'),'utf-8');

async function buscaCepDigitado(cCadEnderecos, campoCEP, campoRua, campoBairro, campoCidade, campoFrete){
    let cepDigitado = campoCEP.value; 
    let result = await cCadEnderecos.selectPorCEP(cepDigitado);
    if(result === null){
        if(conteudoMegaTxt.indexOf(cepDigitado) !== -1) {
            //Pegando o conteudo da linha em que o cep foi encontrado
            const conteudoLinhaEndereco = conteudoMegaTxt.substring(conteudoMegaTxt.indexOf(cepDigitado), conteudoMegaTxt.indexOf(cepDigitado) + 150).split('\n')[0];
            const dadosEndereco = conteudoLinhaEndereco.split(String.fromCharCode(9));
            console.log(dadosEndereco);

            //cadastra o endereco no banco, recupera o id dele e retorna
            //#region Inserindo endereço encontrado no banco de dados
            try{
                //Veriificar a possibilidade de abrir um await swal para o cara digitar o valor do frete para essa rua
				let tipoLogradouroEncontrado = tipoLogr[dadosEndereco[3].substring(0,2).trim().toUpperCase()];
				tipoLogradouroEncontrado = tipoLogradouroEncontrado ? tipoLogradouroEncontrado : {id_tplogr : 1, descricao : 'RUA'}
				const idTipoLogradouro = tipoLogradouroEncontrado.id_tplogr;

				//#region verificando existencia do municipio que está sendo cadastrado
				const nomeMunicipio = (dadosEndereco[1].split('/')[0]).trim().toUpperCase().substring(0,15);
				let municipioEndereco = objCidades[nomeMunicipio];
				if(!municipioEndereco){
					municipioEndereco = await modelTBA_MUNICIPIOS.create({descricao: nomeMunicipio});
					objCidades[municipioEndereco.descricao] = municipioEndereco.id_municipio;
				}
				//#endregion

                cCadEnderecos.titulo = ' ';
                cCadEnderecos.rua = dadosEndereco[3].substring(3, 38).trim().toUpperCase();
                cCadEnderecos.bairro = dadosEndereco[2].substring(0,20);
                cCadEnderecos.CEP = cepDigitado;
                cCadEnderecos.range = '10';
                cCadEnderecos.frete = 0.00;
                cCadEnderecos.id_tplogr = idTipoLogradouro;
                cCadEnderecos.id_uf = objUFs[(dadosEndereco[1].split('/')[1]).trim().toUpperCase()];
                cCadEnderecos.id_municipio = typeof municipioEndereco === 'number' ? municipioEndereco : municipioEndereco.id_municipio;
				result = await cCadEnderecos.insert();

                campoRua ? campoRua.value = tipoLogradouroEncontrado.descricao + ' ' + cCadEnderecos.rua : null;
                campoBairro ? campoBairro.value = cCadEnderecos.bairro : null;
                campoCidade ? campoCidade.value = cCadEnderecos.id_municipio : null;
				
			}catch(e){
				console.error(dadosEndereco);
				console.error(e);
			}
            //#endregion
            
        }
        else result = null;
    }else{
        campoRua ? campoRua.value = result.tba_Tp_Logr.descricao + ' ' + result.rua.toUpperCase() : null;
        campoBairro ? campoBairro.value = result.bairro : null;
        campoCidade ? campoCidade.value = result.id_municipio : null;
        campoFrete ? campoFrete.value = result.frete : null;
    }
    console.log(result);
    return result;
}

module.exports = buscaCepDigitado;