const { data } = require("jquery");

var cCaixa = require(path.join(__dirname + '../../../controller/cCaixa.js'));
var cPeriodo = require(path.join(__dirname + '../../../controller/cPeriodo.js'));
var Helper = require(path.join(__dirname + '../../../controller/Helper.js'));
var inforSistema = JSON.parse(window.localStorage.getItem('dadosEstabelecimento', '{}'))
let periodoAtual ;
let caixaAtual;

verificarCaixa();

function verificarCaixa(){
    if(inforSistema.cfg_separa_periodo === true) buscaPeriodoAtual();
    else buscaCaixaDoDia();
}

setInterval(()=>{verificarCaixa()},300000);

async function buscaCaixaDoDia(){
    let horas = new Date();
	var minuto = String(horas.getMinutes()).padStart(2,'0');
	var hora = String(horas.getHours()).padStart(2,'0');
    let horaAgora = hora + ':' + minuto;
    horaAgora = Helper.horaParaMinuto(horaAgora)

    if(horaAgora < 181){  // Se o horário for depois da meia noite e antes das três da manhã, pegar o ultimo caixa aberto
        pegarUltimoCaixaAberto();
        return;
    }

    pegarCaixaAtual(true);
}

async function buscaPeriodoAtual(){
    let horas = new Date();
	var minuto = String(horas.getMinutes()).padStart(2,'0');
	var hora = String(horas.getHours()).padStart(2,'0');
    let horaAgora = hora + ':' + minuto;
    horaAgora = Helper.horaParaMinuto(horaAgora)
    let periodos = await cPeriodo.selectAll();

    if(horaAgora < 181){  // Se o horário for depois da meia noite e antes das três da manhã, pegar o ultimo caixa aberto
        pegarUltimoCaixaAberto();
        return;
    } 
    
    for(p of periodos){
        const inicio = Helper.horaParaMinuto(p.hora_inicio);
        const fim = Helper.horaParaMinuto(p.hora_termino);
        if(inicio <= horaAgora && fim >= horaAgora){
            periodoAtual = p;
            pegarCaixaAtual();
            break;
        }
        //Continuar pora aqui
    }
}

async function pegarCaixaAtual(diaInteiro = false){
    const date = new Date();
    var minuto = String(date.getMinutes()).padStart(2,'0');
	var hora = String(date.getHours()).padStart(2,'0');
    let horaAgora = hora + ':' + minuto;

    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);

    const horaAgoraMinutos = Helper.horaParaMinuto(horaAgora);

    const caixas = await cCaixa.selectCaixasDia(date);
    for(caixa of caixas){
        if(caixas){
            const horaAbre = Helper.horaParaMinuto(caixa.hr_abre);
            const horaFecha = Helper.horaParaMinuto(caixa.hr_fecha);
            if(horaAbre <= horaAgoraMinutos && horaFecha >= horaAgoraMinutos){
                caixaAtual = caixa;
                window.localStorage.setItem('caixaAtual', JSON.stringify(caixaAtual));
                return;
            } continue
        }
    }

    solicitaInfoCaixa(diaInteiro); //Só chega aqui se não tiver nenhum caixa aberto nesse periodo

    async function solicitaInfoCaixa(diaInteiro = false){
        const txt = diaInteiro ? 'Deseja abrir um novo caixa do dia?' : 'Deseja abrir um novo caixa para o periodo da ' + periodoAtual.descricao.toLowerCase() + '?';
        swal({
            icon: 'warning',
            title: 'Caixa fechado',
            closeOnClickOutside:false,
            text: txt,
            buttons: ['Não', 'Sim']
         }).then(confirm=>{
            if(confirm){
 
                swal({
                   icon: 'warning',
                   title: 'Insira o valor do fundo de caixa',
                   closeOnClickOutside:false,
                   content: 'input',
                   button: {text: 'Confirmar', closeModal: false}
                }).then(confirm=>{
                    confirm = document.getElementsByClassName('swal-content__input')[0].value;
                    if(confirm){
                        const valorConvertido = confirm.replace('.','').replace('.','').replace(',','.');
                        abreNovoCaixa(parseFloat(valorConvertido), diaInteiro);
                    }
                }).catch(e=>{
                   throw e;
                });
 
                inserirMascaraCampoSWAL();
                
            }else{
                voltarHome('swalCaixa');
            }
         }).catch(e=>{
            throw e;
         });
    }

}

async function pegarUltimoCaixaAberto(){
    const caixa = await cCaixa.selectUltimoCaixa();
    caixaAtual = caixa;
    window.localStorage.setItem('caixaAtual', JSON.stringify(caixaAtual));
}

function inserirMascaraCampoSWAL(){
    const domInput = document.getElementsByClassName('swal-content__input')[0];
    domInput.value = '100,00';
    domInput.addEventListener('keyup',(event)=>{
        Helper.maskMoeda(domInput,event);
    });
}

function abreNovoCaixa(valorFundo, diaInteiro = false){
    let dataAtual = new Date();

    var minuto = String(dataAtual.getMinutes()).padStart(2,'0');
	var hora = String(dataAtual.getHours()).padStart(2,'0');

    dataAtual.setHours(0);
    dataAtual.setMinutes(0);
    dataAtual.setSeconds(0);

    cCaixa.dt_caixa      = dataAtual;
    cCaixa.fundo_caixa   = valorFundo;
    if(diaInteiro){
        cCaixa.hr_abre       = hora + ':' + minuto;
        cCaixa.hr_fecha      = '23:59';
        cCaixa.id_periodo    = null;
    }else{
        cCaixa.hr_abre       = hora + ':' + minuto;
        cCaixa.hr_fecha      = periodoAtual.hora_termino;
        cCaixa.id_periodo    = periodoAtual.id_periodo;
    }
    cCaixa.id_tpterminal = 1;
    cCaixa.id_oper       = 1;

    const texto = diaInteiro ? 'Caixa do dia aberto com fundo ' + valorFundo.toFixed(2) : 'Caixa da ' + periodoAtual.descricao +' aberto com fundo ' + valorFundo.toFixed(2);
    cCaixa.insert().then((success=>{
        swal({
            icon: 'success',
            title: 'Caixa aberto!',
            closeOnClickOutside:true,
            text: texto,
            button: {text: 'Okay', closeModal: true}
         });
    })).catch(ex=>{
        throw ex;
    });
    
}

async function somarPedidoNoCaixa(novoValor){
    cCaixa.id_caixa = caixaAtual.id_caixa;
    const valorAtual = await cCaixa.recuperaValorAtualCaixa();
    novoValor += valorAtual.fundo_caixa;
    return await cCaixa.updateFundoCaixa(caixaAtual.id_caixa, novoValor);
}

module.exports = {somarPedidoNoCaixa:somarPedidoNoCaixa}