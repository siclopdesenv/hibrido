async function a(){
	var path = require('path');
	let tpMenu = window.localStorage.tipoMenu;
	const InfoSis = require(path.join(__dirname + '/../../controller/cINFORSIS.js'));

	resul = await InfoSis.selectAll();
	window.localStorage.setItem('dadosEstabelecimento', JSON.stringify(resul[0]));
	if(tpMenu == 1 || tpMenu == undefined){
		setTimeout(()=>{document.location.assign(path.join(__dirname,'../html/menuPrincipal.html'))},500);
	}else{
		setTimeout(()=>{document.location.assign(path.join(__dirname,'../html/menuTradicional.html'))},500);
	}
	
}a();	