var $ = require("jquery");

var path = require('path');
const swal = require("sweetalert");

window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));

const Helper 		= require(path.join(__dirname + '/../../controller/Helper.js'));
const CadEnderecos 	= require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));
const cMunicipios 	= require(path.join(__dirname + '/../../controller/cMunicipio.js'));
const cUF 			= require(path.join(__dirname + '/../../controller/cUF.js'));
const cTpLogr 		= require(path.join(__dirname + '/../../controller/cTipoLogradouro.js'));
var btnSalvar 		= document.getElementById('cadastrar');
var dadosEstab 		= JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
let tbody 			= document.getElementById('listaProdutos');
var MouseTrap = require('mousetrap');

let conteudoEnderecos = '';


MouseTrap.bind('esc', function () {
    addAlt('fechar');
})

MouseTrap.bind('f8', solicitaInfoBairro)

async function solicitaInfoBairro() {
	swal({
		icon:'info',
		title: 'Cobrança de taxa por bairro',
		closeOnClickOutside:true,
		closeOnEsc:false,
		content: 'input',
		buttons: ['Cancelar', 'Ir para taxa de entrega']
	}).then(async confirm=>{
		if(confirm === null) return;
		const nomeDoBairro = confirm.toUpperCase();
		const enderecosEncontrados = await CadEnderecos.selectPorBairro(nomeDoBairro);
		console.log(enderecosEncontrados);

		if(enderecosEncontrados.length === 0){
			solicitaInfoBairro();
			return;
		}

		swal({
		   icon: 'info',
		   title: 'Encontrados ' + enderecosEncontrados.length + ' endereços, insira a taxa de entrega para eles',
		   closeOnClickOutside:false,
		   content: 'input',
		   button: {text: 'Salvar', closeModal: false}
		}).then(async confirm=>{
			confirm = document.getElementsByClassName('swal-content__input')[0].value;
			if(confirm === ''){
				swal.close();
				return;
			}
			const valorInserido = parseFloat((confirm.replace('.','').replace('.','').replace(',','.')));

			for(endereco of enderecosEncontrados){
				console.log(endereco.id_logradouro, valorInserido);
				await CadEnderecos.updateValorTaxa(endereco.id_logradouro, valorInserido);
			}

			swal({
			   icon: 'success',
			   title: 'Valores atualizados!',
			   closeOnClickOutside:true,
			   text: ' endereços com taxas atualizada com sucesso',
			   button: {text: 'Ok', closeModal: true}
			}).then(()=>{
				tbody.innerHTML = '';
				conteudoEnderecos = '';
				selecionarTudo();
			});

		}).catch(e=>{
		   throw e;
		});

		inserirMascaraCampoSWAL();

	}).catch(e=>{
	   throw e;
	});
}

function inserirMascaraCampoSWAL(){
    const domInput = document.getElementsByClassName('swal-content__input')[0];
    domInput.value = '';
    domInput.addEventListener('keyup',(event)=>{
        Helper.maskMoeda(domInput,event);
    });
}

//#region Buscando id da UF do estabelecimento, adicionando listener no botão "Novo" para quando abrir o form de cadastro carregar automaticamente a UF
CadEnderecos.select(dadosEstab.id_logradouro).then(result=>{
	document.getElementById('addAlt').addEventListener('click',()=>{
		document.getElementById('id_uf').value = result.id_uf;
	});
}).catch(err=>{
	console.error(err);
});
//#endregion

async function selecionarTudo(){
	let CadEnderecos = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));
	const qtdEnderecos = await CadEnderecos.countAll();
	swal({
		icon : 'warning',
		title: 'Carregando ' + qtdEnderecos + ' endereços',
		text : 'Por favor, aguarde alguns instantes...',
		closeOnClickOutside : false,
		closeOnEsc: false,
		button: {
			text: "OK",
			closeModal: false,
		}
	});
	let listaProd = CadEnderecos.selectAllJoin();
	await listaProd.then(async (data) => {
		let actuallyData = data;
		for(var i of actuallyData)await insereItemNaTela(i,true);
	});

	setTimeout(() => {
		swal.close();
		tbody.innerHTML = '';
		tbody.innerHTML += conteudoEnderecos;
	}, 5000);

}

async function selecionarItem(item){
	const script = require(path.join(__dirname + '/script.js'));

	//script.alterar();
	let divMain = document.getElementById('conteudoMain');

	txt = `
	<div id="camadaModal" style="display:block;" class="cria-camada-modal h-100 w-100" >
		<div class="modal-info-imp position-relative col-8 offset-2 rounded pt-3 pb-5 px-5">		
			<div class="header-modal col-12 p-0">	
				<button class="btn-sem-decoracao aba-modal abas-info-sis ml-3 w-50">
					<label class="titulo-modal titulo-modal-info-sis
					titulo-modal-info-sis-ativa" id="identificacao-tab"  >
						Dados de Endereço
					</label>
				</button>			
			</div>
			
			<div class="body-modal py-4 rounded-top">
				<div class="col-12">
					<div class="row m-0">

						
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Tipo Logradouro:</span>
							</div>
							<div class="celula-2 col-3">
								<div class="input-group-prepend">
									<select id="id_tplogr_edit" class="custom-select">
									</select>
								</div> 
							</div>
						</div>


						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Título:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="titulo" value="${item.titulo ? item.titulo : ''}" type="text" class="form-control" aria-label="Sizing example input" name="Título" aria-describedby="inputGroup-sizing-default" maxlength="15" placeholder="Ex: Capitão, Major...">
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">*Logradouro:</span>
							</div>
							<div class="celula-2 col-6">
								<input id="rua" value="${item.rua}" type="text" class="form-control" aria-label="Sizing example input" name="Logradouro" aria-describedby="inputGroup-sizing-default" maxlength="30">
							</div>

						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="row m-0">

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Bairro:</span>
							</div>
							<div class="celula-2 col-6">
								<input id="bairro" value="${item.bairro}" type="text" class="form-control" aria-label="Sizing example input" name="Bairro" aria-describedby="inputGroup-sizing-default" maxlength="20">
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
							<span class="label-celula">Cidade:</span>
							</div>
							<div class="celula-2 col-4">
								<select id="id_municipio_edit" class="custom-select">
								</select>
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">UF:</span>
							</div>
							<div class="celula-2 col-3">
								<div class="input-group-prepend">
									<select id="id_uf_edit" class="custom-select">
									</select>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-12">
					<div class="row m-0">					

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">*CEP:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="CEP" value="${item.CEP}" onkeyup="Helper.mascaraNumInt(this)" type="text" class="form-control" aria-label="Sizing example input" name="CEP" aria-describedby="inputGroup-sizing-default" maxlength="9">
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Taxa Entrega:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="frete" value="${item.frete}" type="text" onkeyup="Helper.maskMoeda(this)" class="form-control" aria-label="Sizing example input" name="Taxa Entrega" aria-describedby="inputGroup-sizing-default" maxlength="6">
							</div>
						</div>
					</div>
				</div>
				

				<div class="row footer-modal m-0">
					<div class="offset-8 col-2 my-2">
						<button class="col-12 btn btn-danger position-relative float-left" onclick="addAlt('fechar',true)">Sair</button>
					</div>
					<div class="col-2 my-2">
						<button id="editar" class="col-12 btn btn-success position-relative float-right">Salvar</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	`;

	divMain.insertAdjacentHTML('afterbegin', txt);
	let btnEditar = document.getElementById('editar');
	btnEditar.addEventListener('click', ()=>{salvar(item.id_logradouro)})

	await recuperaDadosAuxiliares(document.getElementById('id_tplogr_edit'), require(path.join(__dirname + '/../../controller/cTipoLogradouro.js')));
	await recuperaDadosAuxiliares(document.getElementById('id_municipio_edit'), require(path.join(__dirname + '/../../controller/cMunicipio.js')));
	await recuperaDadosAuxiliares(document.getElementById('id_uf_edit'), require(path.join(__dirname + '/../../controller/cUF.js')));
	
	document.getElementById('id_tplogr_edit').value = item.id_tplogr;
	document.getElementById('id_municipio_edit').value = item.id_municipio;
	document.getElementById('id_uf_edit').value = item.id_uf;
	document.getElementById('titulo').focus();

	document.getElementById('frete').addEventListener('keyup', Helper.maskMoeda);

}

function salvar(id){

	//lista de campos
	CadEnderecos.titulo = document.getElementById('titulo').value.toUpperCase();
	CadEnderecos.rua = document.getElementById('rua').value.toUpperCase();
	CadEnderecos.bairro = document.getElementById('bairro').value.toUpperCase();
	CadEnderecos.CEP = document.getElementById('CEP').value;
	CadEnderecos.frete = document.getElementById('frete').value != '' ? parseFloat(document.getElementById('frete').value.replace('.','').replace('.','').replace(',','.')) : 0;
	CadEnderecos.range = 2;
	CadEnderecos.id_tplogr = parseInt(document.getElementById('id_tplogr').value);
	CadEnderecos.id_uf = parseInt(document.getElementById('id_uf').value);
	CadEnderecos.id_municipio = parseInt(document.getElementById('id_municipio').value);

	if(CadEnderecos.frete === dadosEstab.cfg_frete_basico) CadEnderecos.frete = 0;

	CadEnderecos.range = 1;

	//verifica se campo esta vazio
	var conteudoNotNull = {
		rua: CadEnderecos.rua,
		CEP: CadEnderecos.CEP,
		frete:CadEnderecos.frete,
		id_tplogr:CadEnderecos.id_tplogr,
		id_uf:CadEnderecos.id_uf,
		id_municipio:CadEnderecos.id_municipio,
	};

	let campoFaltando = false;
	let nomeCampoFaltando = "";

	for(var i in conteudoNotNull){
		if(conteudoNotNull[i] === '' || conteudoNotNull[i] === null){
			campoFaltando = true;
			nomeCampoFaltando = document.getElementById(i).name;
			document.getElementById(i).focus();
			break;
		}
	}

	conteudoNotNull.bairro = CadEnderecos.bairro ? CadEnderecos.bairro : '';
	conteudoNotNull.titulo = CadEnderecos.titulo;

	//insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
	if(campoFaltando == false && id == undefined){
		event.preventDefault();
		CadEnderecos.insert().then((retorno)=>{insereItemNaTela(retorno);addAlt('fechar');});
	}else if(campoFaltando == false && id > 0){
		event.preventDefault();
		CadEnderecos.id_logradouro = id;
		CadEnderecos.id_tplogr = parseInt(document.getElementById('id_tplogr_edit').value);
		CadEnderecos.id_uf = parseInt(document.getElementById('id_uf_edit').value);
		CadEnderecos.id_municipio = parseInt(document.getElementById('id_municipio_edit').value);
		CadEnderecos.update().then(async (rows)=>{
			const objEndereco = {
				rua: CadEnderecos.rua,
				bairro: (CadEnderecos.bairro ? CadEnderecos.bairro : ''),
				CEP: CadEnderecos.CEP,
				frete: CadEnderecos.frete,
				id_tplogr: CadEnderecos.id_tplogr,
				id_uf: CadEnderecos.id_uf,
				id_municipio: CadEnderecos.id_municipio,
			}
			alteraItemNaTela(id, objEndereco);
			addAlt('fechar',true);
		});
	}else if(campoFaltando){
		Helper.alertaCampoVazio(nomeCampoFaltando)
	}
}

async function alteraItemNaTela(id,obj){

	let nomeRua = document.getElementById(`nomeRua${id}`);
	let cep = document.getElementById(`CEP${id}`);
	let bairro = document.getElementById(`bairro${id}`);
	let cidade = document.getElementById(`cidade${id}`);
	let uf = document.getElementById(`uf${id}`);
	let frete = document.getElementById(`frete${id}`);

	let contentCidade = await cMunicipios.select(obj.id_municipio);
	let contentUF = await cUF.select(obj.id_uf);
	let contentTpLogr = await cTpLogr.select(obj.id_tplogr);
	
	nomeRua.innerHTML = (contentTpLogr.descricao ? contentTpLogr.descricao + ' ' : '') + obj.rua;
	cep.innerHTML = obj.CEP;
	bairro.innerHTML = obj.bairro;
	cidade.innerHTML = contentCidade.descricao;
	uf.innerHTML = contentUF.uf;
	frete.innerHTML = "R$"+(parseFloat(obj.frete).toFixed(2)).toString().replace('.',',');
}
let conteudoPesquisa = '';

async function insereItemNaTela(retorno,stringify, contador, pesquisa){
	if(stringify != true){
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	let auxConteudo = '';
	try{
		auxConteudo += `
		<tr id="tr${retorno.id_logradouro}">
			<th  class="border-right" scope="row">
			<center>
				<button class="btn-sem-decoracao editarItem" id="${retorno.id_logradouro}" onclick="editarItem(this.id)">
					<img style="margin-left: -12px;" src="../img/pencil.png"/>
				</button>
	
				<button class="btn-sem-decoracao" id="${retorno.id_logradouro}" onclick="excluirItem(this.id)">
					<img style="margin-left: -12px;" src="../img/trash.png"/>
				</button>
			</center>
			</th>
			<td class="border-right txt-label" id="nomeRua${retorno.id_logradouro}">${(retorno.hasOwnProperty('tba_Tp_Logr') ? retorno.tba_Tp_Logr.descricao + ' ' : '') + retorno.rua}</td>
			<td class="border-right txt-num" id="CEP${retorno.id_logradouro}">${retorno.CEP}</td>
			<td class="border-right txt-label" id="bairro${retorno.id_logradouro}">${retorno.bairro}</td>
			<td class="border-right txt-num" id="cidade${retorno.id_logradouro}">${ retorno.hasOwnProperty('tba_Municipio') ? retorno.tba_Municipio.descricao : retorno.id_municipio}</td>
			<td class="border-right txt-num" id="uf${retorno.id_logradouro}">${retorno.hasOwnProperty('tba_UF') ? retorno.tba_UF.uf : retorno.id_uf}</td>
			<td class="border-right txt-num" id="frete${retorno.id_logradouro}">R$${retorno.frete.toFixed(2).toString().replace('.',',')}</td>
	
		</tr>`;

		conteudoEnderecos += auxConteudo;

	}catch(e){
		console.log(e);
	}

	pesquisa ? conteudoPesquisa += auxConteudo : null;
}

async function innerPesquisa(){
	tbody.innerHTML = '';
	tbody.innerHTML += conteudoPesquisa;
	conteudoPesquisa = '';
	conteudoEnderecos = '';
}

btnSalvar.addEventListener('click',()=>{salvar()});

setTimeout(()=>{
	recuperaDadosAuxiliares(document.getElementById('id_tplogr'), require(path.join(__dirname + '/../../controller/cTipoLogradouro.js')));
	recuperaDadosAuxiliares(document.getElementById('id_municipio'), require(path.join(__dirname + '/../../controller/cMunicipio.js')));
	recuperaDadosAuxiliares(document.getElementById('id_uf'), require(path.join(__dirname + '/../../controller/cUF.js')));
},100);

async function recuperaDadosAuxiliares(elemento, controller){
	elemento.innerHTML = '';
	await controller.selectAll().then((result)=>{
		result.forEach(dado => {
			dado = JSON.parse(JSON.stringify(dado));
			elemento.innerHTML += `<option value="${Object.values(dado)[0]}">${Object.values(dado)[1]}</option>`;
		});
	});
}

function carregaConteudoOriginal(){
	selecionarTudo();
}

module.exports = {
	selecionarTudo: ()=>{selecionarTudo()},
	selecionarItem: (item)=>{selecionarItem(item)},
	excluirItem: (item)=>{excluirItem(item)},
	insereItemNaTela: insereItemNaTela,
	innerPesquisa: innerPesquisa,
	carregaConteudoOriginal: carregaConteudoOriginal
};
