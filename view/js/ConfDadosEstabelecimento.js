let $ = require("jquery");
let path = require('path');
window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));
var Swal = require('sweetalert');
let objCidades = {};
let objUFs = {};
let objImps = {};

//let objUFs = {};

const Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
const cInforsis = require(path.join(__dirname + '/../../controller/cINFORSIS.js'));
const cNicho = require(path.join(__dirname + '/../../controller/cNichos.js'));
const cGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
const cTGrupos = require(path.join(__dirname + '/../../controller/cTGrupos.js'));

let con = require(path.join(__dirname + '/../../database/connection'));
const modelTB_LOGRADOUROS = require(path.join(__dirname + '/../../database/model/principais/mTB_LOGRADOUROS.js'));
const modelTBA_MUNICIPIOS = require(path.join(__dirname + '/../../database/model/auxiliares/mTBA_MUNICIPIOS.js'));
const modelTBA_UF = require(path.join(__dirname + '/../../database/model/auxiliares/mTBA_UF.js'));
modelTB_LOGRADOUROS.init(con);
modelTBA_MUNICIPIOS.init(con);
modelTBA_UF.init(con);
let nichoSalvo = '';

const campoQtdMesas = document.getElementById('qde_mesas');

campoQtdMesas.addEventListener('focusout',()=>{
	const qtdDigitada = parseInt(campoQtdMesas.value);
	
	if(qtdDigitada % 5 > 0) campoQtdMesas.value = (parseInt(qtdDigitada / 5) * 5) + 5;
	else campoQtdMesas.value = qtdDigitada;
});

modelTBA_UF.findAll().then(result=>{
	for(uf of result) objUFs[uf.uf.trim().toUpperCase()] = uf.id_uf;
});

let btnSalvar = document.getElementById('btnEditarEstab');
let idEstab;
let ret;
let comboNicho = document.getElementById('cfg_id_nicho');
let tpNicho;
var cepEstabCarregado = '';
var regiaoCepCarregado = '';
let campoLogradouroCEP = document.getElementById('cfg_cep_logradouro');
let iniciouMigracao = false;

//#region ------- INSERINDO E TRABALHANDO COM OS CAMPOS COM MÁSCARA ---------
//let campoCPF = document.getElementById('cpf');
let campoCNPJ = document.getElementById('cfg_cnpj');

campoCNPJ.addEventListener('keyup', Helper.maskCPFCNPJ);
campoCNPJ.addEventListener('focusout', ()=>{
	let valor = campoCNPJ.value.replace('.','').replace('.','').replace('.','').replace('-','').replace('/','');
	if(valor.length !== 11 && valor.length < 14) campoCNPJ.style.borderColor = '#f90000';
	else if(Helper.validaCPF(valor) || Helper.validaCNPJ(valor))campoCNPJ.style.borderColor = '#00FF00';
	else campoCNPJ.style.borderColor = '#f90000';
});

document.getElementById('cfg_ddd').addEventListener('keyup', ()=>{Helper.controlaTamanhoCampo(document.getElementById('cfg_ddd'), 2);});
// document.getElementById('fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
// document.getElementById('fone2').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('cfg_fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('cfg_fone2').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('cfg_fone3').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('cfg_cep_logradouro').addEventListener('keyup', ()=>{
	Helper.controlaTamanhoCampo(document.getElementById('cfg_cep_logradouro'), 8);
});
//----------------------Fim--------------------
//#endregion

/*const comboFormaPizza = document.getElementById('cfg_formabroto');
let formaPizzaAnterior = '';
comboFormaPizza.addEventListener('change', ()=>{

	if(comboFormaPizza.value === "I") {
		Swal({
			icon: 'warning',
			title: 'Forma individual de cobrar',
			closeOnClickOutside:false,
			closeOnEsc: false,
			text: 'Selecionando esta maneira de cobrar as pizzas você deverá entrar no cadastro e salvar o valor do tamanho de cada uma delas!',
			buttons: ['Cancelar', 'Manter decisão']
		}).then((confirmation)=>{
			if(!confirmation){
				comboFormaPizza.value = formaPizzaAnterior;
			}
		});
	}else{
		formaPizzaAnterior = comboFormaPizza.value;
	}
});
*/
preencheCombo(comboNicho, cNicho);
var comboLogradouro = document.getElementById('id_logradouro');
const cLogradouro	= require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));
const cCidade		= require(path.join(__dirname + '/../../controller/cMunicipio.js'));
const cUf			= require(path.join(__dirname + '/../../controller/cUF.js'));

function preencheCombo(combo, controller){
	controller.selectAll().then(dados=>{
		dados = JSON.parse(JSON.stringify(dados));

		dados.forEach(registro => {
			combo.innerHTML += `<option value="${Object.values(registro)[0]}">${Object.values(registro)[1]}</option>`
			console.log(Object.values(registro)[1]);
		});

	})
}
function preencheComboLogradouro(combo, controller,id){
	controller.selectAll().then(data => {
		let actuallyData = JSON.parse(JSON.stringify(data));
		let v =  document.getElementById("cfg_id_logradouro");
		for(var i of actuallyData){
			if(id != undefined && id == i.id_logradouro){
				v.insertAdjacentHTML('afterbegin',`<option value="${i.id_logradouro}" selected>${i.rua}</option>`);	
			}else{
				v.insertAdjacentHTML('beforeend',`<option value="${i.id_logradouro}">${i.rua}</option>`);
			}
		}
	});
}

async function preencheCampoCEP(id_logradouro){
	let endereco 		= await cLogradouro.select(id_logradouro);
	const objCidade 	= await cCidade.select(endereco.id_municipio);
	const objUf 		= await cUf.select(endereco.id_uf);
	cepEstabCarregado 	= endereco.CEP;
	regiaoCepCarregado 	= cepEstabCarregado.substring(0,3);
	document.getElementById('cfg_cep_logradouro').value = cepEstabCarregado;
	document.getElementById('cfg_nome_rua').value 		= endereco.titulo + ' ' + endereco.rua;
	document.getElementById('cfg_bairro').value 		= endereco.bairro;
	document.getElementById('cfg_cidade').value = objCidade.descricao;
	document.getElementById('cfg_uf').value = objUf.uf;

	campoLogradouroCEP.addEventListener('keyup',()=>{
		let cepDigitado = campoLogradouroCEP.value;
	
		if(cepDigitado.length === 8){
			if(conteudoMegaTxt.indexOf(cepDigitado) !== -1) {
				//Pegando o conteudo da linha em que o cep foi encontrado
				const conteudoLinhaEndereco = conteudoMegaTxt.substring(conteudoMegaTxt.indexOf(cepDigitado), conteudoMegaTxt.indexOf(cepDigitado) + 150).split('\n')[0];
				const dadosEndereco = conteudoLinhaEndereco.split(String.fromCharCode(9));
				document.getElementById('cfg_nome_rua').value 	= dadosEndereco[3];
				document.getElementById('cfg_bairro').value 	= dadosEndereco[2];
				document.getElementById('cfg_cidade').value 	= dadosEndereco[1].split('/')[0];
				document.getElementById('cfg_uf').value 		= dadosEndereco[1].split('/')[1];
				campoLogradouroCEP.style.borderColor = '#00FF00'
			}else zeraCampos();
			
		}else zeraCampos();
	
		function zeraCampos(){
			campoLogradouroCEP.style.borderColor = '#f90000';
			document.getElementById('cfg_nome_rua').value 	= '';
			document.getElementById('cfg_bairro').value 	= '';
			document.getElementById('cfg_cidade').value 	= '';
			document.getElementById('cfg_uf').value 		= '';
		}
		
	});
}
let dadosDoEstab ;
async function carregaDados(){
	resul = await cInforsis.selectAll();
	dadosDoEstab = resul[0];
	idEstab = dadosDoEstab.id;
	preencheCampoCEP(dadosDoEstab.id_logradouro);
	cInforsis.cfg_id_logradouro = dadosDoEstab.id_logradouro;

	document.getElementById("cfg_nome").value = dadosDoEstab.cfg_nome;
	document.getElementById("cfg_nome_fantasia").value = dadosDoEstab.cfg_nome_fantasia;
	document.getElementById('cfg_fone1').value = dadosDoEstab.cfg_fone1;
	document.getElementById('cfg_fone2').value = dadosDoEstab.cfg_fone2;
	document.getElementById('cfg_fone3').value = dadosDoEstab.cfg_fone3;
	document.getElementById('cfg_ddd').value = dadosDoEstab.cfg_ddd;
	document.getElementById('email').value = dadosDoEstab.email;
	document.getElementById('cfg_ie').value = dadosDoEstab.cfg_ie;
	document.getElementById('cfg_im').value = dadosDoEstab.cfg_im;
	document.getElementById('rateio').value = dadosDoEstab.rateio;
	document.getElementById('cfg_numero_local').value = dadosDoEstab.cfg_numero_local;
	document.getElementById('cfg_complemento').value = dadosDoEstab.cfg_complemento;
	document.getElementById('cfg_cnpj').value = dadosDoEstab.cfg_cnpj;
	document.getElementById('cfg_nr_serie').value = dadosDoEstab.cfg_nr_serie;
	document.getElementById('cfg_cod_lib').value = dadosDoEstab.cfg_cod_lib;
	document.getElementById('cfg_pzcombinada').value = dadosDoEstab.cfg_pzcombinada;
	document.getElementById('cfg_formabroto').value = dadosDoEstab.cfg_formabroto;
	document.getElementById('cfg_porc_broto').value = dadosDoEstab.cfg_porc_broto;
	document.getElementById('cfg_porc_media').value = dadosDoEstab.cfg_porc_media;
	document.getElementById('cfg_porc_giga').value = dadosDoEstab.cfg_porc_giga;
	document.getElementById('cfg_frete_basico').value = dadosDoEstab.cfg_frete_basico;
	document.getElementById('cfg_taxa_servico').value = dadosDoEstab.cfg_taxa_servico;
	document.getElementById('cfg_qde_cupom').value = dadosDoEstab.cfg_qde_cupom;
	document.getElementById('qde_mesas').value = dadosDoEstab.qde_mesas;
	// CadFuncionarios.fone1 = document.getElementById('fone1').value.replace('-','');
	// CadFuncionarios.fone2 = document.getElementById('fone2').value.replace('-','');
	// cInforsis.cfg_fone1 = document.getElementById('cfg_fone1').value.replace('-','');
	// cInforsis.cfg_fone2 = document.getElementById('cfg_fone2').value.replace('-','');
	// cInforsis.cfg_fone3 = document.getElementById('cfg_fone3').value.replace('-','');
	
	if(dadosDoEstab.cfg_formabroto === "P"){
		document.getElementById('valuesPizzasCfg').style.display = "block";
		document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (%):";
		document.getElementById("pzMediaSpan").innerText 	= "Pizza Média (%):";
		document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (% a mais):";			
	}else if(dadosDoEstab.cfg_formabroto === "F"){
		document.getElementById('valuesPizzasCfg').style.display = "block";
		document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (Menos):";
		document.getElementById("pzMediaSpan").innerText	= "Pizza Média (Menos):";
		document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (Mais):";
	}else if(dadosDoEstab.cfg_formabroto === "I"){
		document.getElementById("valuesPizzasCfg").style.display = "none";	
	}

	let opcoes = {
		cfg_contr_pagto: dadosDoEstab.cfg_contr_pagto,
		cfg_promo_dia:dadosDoEstab.cfg_promo_dia,
		cfg_separa_cupom: dadosDoEstab.cfg_separa_cupom,
		cfg_codigo_cupom: dadosDoEstab.cfg_codigo_cupom,
		cfg_repete_item:dadosDoEstab.cfg_repete_item,
		cfg_separa_periodo:dadosDoEstab.cfg_separa_periodo,
		cfg_contr_estq: dadosDoEstab.cfg_contr_estq,
		cfg_contr_motoq:dadosDoEstab.cfg_contr_motoq,
		cfg_baixa_aqui:dadosDoEstab.cfg_baixa_aqui,
		cfg_cod_barra:dadosDoEstab.cfg_cod_barra,
		cfg_faz_delivery:dadosDoEstab.cfg_faz_delivery,
		cfg_modulo_fiscal:dadosDoEstab.cfg_modulo_fiscal,
		cfg_modulo_movel:dadosDoEstab.cfg_modulo_movel,
		cfg_modulo_web:dadosDoEstab.cfg_modulo_web,
		rateio:dadosDoEstab.rateio
	}

	for(var i in opcoes){
		if(opcoes[i] == false){
			document.getElementById(`${i}`).selectedIndex = "0"; 
		}else if(opcoes[i] == true){
			document.getElementById(`${i}`).selectedIndex = "1"; 
		}
	}

	//document.getElementById('cfg_id_logradouro').selectedIndex = "0"; 
	//document.getElementById('cfg_id_logradouro').value = dadosDoEstab.cfg_id_logradouro;
	document.getElementById('cfg_id_nicho').value = dadosDoEstab.id_nicho;
	
	//document.getElementById('cfg_id_impressora').value = dadosDoEstab.Impressora.apelido; colocar isso aqui para aparecer o nome da impressora
	document.getElementById('cfg_id_impressora').value = dadosDoEstab.id_impressora;
	nichoSalvo = dadosDoEstab.id_nicho;
	nicho();
	formaPizzaAnterior = dadosDoEstab.cfg_formabroto;
}

//------------Verifica o nicho do estabelecimento
document.getElementById('tpMenu').value = window.localStorage.tipoMenu;
function nicho(){
	tpNicho = dadosDoEstab.id_nicho;
	let mP;
	if(tpNicho == 3){
	  	mP = document.getElementById('moduloPizza').style.display = 'flex';
	}else{
		mP = document.getElementById('moduloPizza').style.display = 'none';
	}
}

let itemSelecionado = document.querySelector('#cfg_id_nicho');
itemSelecionado.addEventListener('change', (event) => {
  tpNicho = event.target.value;
  if(tpNicho == 1){
  	mP = document.getElementById('moduloPizza').style.display = 'flex';
  }else{
  	mP = document.getElementById('moduloPizza').style.display = 'none';
  }
});

//---------------------------------------------------------------------

//#region Funções de migração de endereços
let conteudoMegaTxt = fs.readFileSync(__dirname + '\\..\\..\\database\\ceps.txt', 'utf-8');
let enderecos = conteudoMegaTxt.split('\n');

let tipoLogr = {
	"R": {
		id_tplogr : 1,
		"descricao": "Rua"
	},
	"AV": {
		id_tplogr : 2,
		"descricao": "Avenida"
	},
	"AC": {
		id_tplogr : 3,
		"descricao": "Acesso"
	},
	"AL": {
		id_tplogr : 4,
		"descricao": "Alameda"
	},
	"AT": {
		id_tplogr : 5,
		"descricao": "Alto"
	},
	"AR": {
		id_tplogr : 6,
		"descricao": "Área"
	},
	"BL": {
		id_tplogr : 7,
		"descricao": "Balão"
	},
	"Bc": {
		id_tplogr : 8,
		"descricao": "Beco"
	},
	"BV": {
		id_tplogr : 9,
		"descricao": "Boulevard"
	},
	"Bos": {
		id_tplogr : 10,
		"descricao": "Bosque"
	},
	"C": {
		id_tplogr : 11,
		"descricao": "Cais"
	},
	"Cc": {
		id_tplogr : 12,
		"descricao": "Calçada"
	},
	"Cam": {
		id_tplogr : 13,
		"descricao": "Caminho"
	},
	"Clu": {
		id_tplogr : 14,
		"descricao": "Clube"
	},
	"Col": {
		id_tplogr : 15,
		"descricao": "Colónia"
	},
	"CJ": {
		id_tplogr : 16,
		"descricao": "Conjunto"
	},
	"Con": {
		id_tplogr : 17,
		"descricao": "Condomínio"
	},
	"Cor": {
		id_tplogr : 18,
		"descricao": "Corredor"
	},
	"Ent": {
		id_tplogr : 19,
		"descricao": "Entrada"
	},
	"Esc": {
		id_tplogr : 20,
		"descricao": "Escada/Escadaria"
	},
	"Estacionamento": {
		id_tplogr : 21,
		"descricao": "Estacionamento"
	},
	"ET": {
		id_tplogr : 22,
		"descricao": "Estrada"
	},
	"FV": {
		id_tplogr : 23,
		"descricao": "Favela"
	},
	"GL": {
		id_tplogr : 24,
		"descricao": "Galeria"
	},
	"GJ": {
		id_tplogr : 25,
		"descricao": "Granja"
	},
	"IL": {
		id_tplogr : 26,
		"descricao": "Ilha"
	},
	"JD": {
		id_tplogr : 27,
		"descricao": "Jardim"
	},
	"LD": {
		id_tplogr : 28,
		"descricao": "Ladeira"
	},
	"LG": {
		id_tplogr : 29,
		"descricao": "Largo"
	},
	"LT": {
		id_tplogr : 30,
		"descricao": "Loteamento"
	},
	"MT": {
		id_tplogr : 31,
		"descricao": "Monte"
	},
	"MR": {
		id_tplogr : 32,
		"descricao": "Morro"
	},
	"PQ": {
		id_tplogr : 33,
		"descricao": "Parque"
	},
	"PA": {
		id_tplogr : 34,
		"descricao": "Pátio"
	},
	"PS": {
		id_tplogr : 35,
		"descricao": "Passagem"
	},
	"Passeio": {
		id_tplogr : 36,
		"descricao": "Passeio"
	},
	"PT": {
		id_tplogr : 37,
		"descricao": "Ponte"
	},
	"PC": {
		id_tplogr : 38,
		"descricao": "Praça"
	},
	"PR": {
		id_tplogr : 39,
		"descricao": "Praia"
	},
	"Q": {
		id_tplogr : 40,
		"descricao": "Quadra"
	},
	"RC": {
		id_tplogr : 41,
		"descricao": "Recanto"
	},
	"Ret": {
		id_tplogr : 42,
		"descricao": "Retorno"
	},
	"RD": {
		id_tplogr : 43,
		"descricao": "Rodovia"
	},
	"RT": {
		id_tplogr : 44,
		"descricao": "Rotatória"
	},
	"TM": {
		id_tplogr : 45,
		"descricao": "Terminal"
	},
	"TR": {
		id_tplogr : 46,
		"descricao": "Travessa"
	},
	"Trecho": {
		id_tplogr : 47,
		"descricao": "Trecho"
	},
	"TV": {
		id_tplogr : 48,
		"descricao": "Trevo"
	},
	"TN": {
		id_tplogr : 49,
		"descricao": "Túnel"
	},
	"VR": {
		id_tplogr : 50,
		"descricao": "Vereda"
	},
	"VL": {
		id_tplogr : 51,
		"descricao": "Vale"
	},
	"VD": {
		id_tplogr : 52,
		"descricao": "Viaduto"
	},
	"VIA": {
		id_tplogr : 53,
		"descricao": "Via"
	},
	"VI": {
		id_tplogr : 54,
		"descricao": "VI"
	}
};

async function migrarEnderecos(cepEstabelecimento = ''){
	/*
	Abaixo pegando uma região abaixo e uma região acima, ou seja:
	Se o CEP for 04233220 vai pegar:
	041XXXXX
	042XXXXX
	043XXXXX
	*/

	let regiaoCep = '';
	let regiaoAntes = parseInt(cepEstabelecimento.substring(2,3)) - 1;
	let regiaoDepois = parseInt(cepEstabelecimento.substring(2,3)) + 1;

	regiaoAntes 	= cepEstabelecimento.substring(0,2) + (regiaoAntes < 0 ? 9 : (regiaoAntes > 9 ? 0 : regiaoAntes)).toString();
	regiaoCep 		= cepEstabelecimento.substring(0,3);
	regiaoDepois 	= cepEstabelecimento.substring(0,2) + (regiaoDepois < 0 ? 9 : (regiaoDepois > 9 ? 0 : regiaoDepois)).toString();

	await migraBairrosECidades(regiaoCep, regiaoAntes, regiaoDepois, cepEstabelecimento);
	//Jogar tudo daqui pra baixo para dentro de uma função independente para ser chamada depois de migrar bairros e cidades
}

async function migraBairrosECidades(regiaoEstabelecimento, regiaoAnterior, regiaoPosterior, cepEstabelecimento){
	let listaCidades = [];

	for(let i = 0; i < enderecos.length; i++){
		endereco = enderecos[i].split(String.fromCharCode(9));
		let cepAtual = endereco[0];
		let regiaoCepTxt = cepAtual.substring(0,3);
		if(regiaoCepTxt === regiaoAnterior || regiaoCepTxt === regiaoEstabelecimento || regiaoCepTxt === regiaoPosterior){
			listaCidades.push(endereco[1].split('/')[0]);
		}
		if(endereco[0] === '99980974'){
			let listaCidadesFiltradas = [... new Set(listaCidades)];

			for(let i = 0; i<listaCidadesFiltradas.length; i++){
				let cidade = listaCidadesFiltradas[i].trim().toUpperCase();
				objCidades[cidade] = await modelTBA_MUNICIPIOS.create({
					descricao : cidade.substring(0,15)
				});
				objCidades[cidade] = objCidades[cidade].dataValues;
				if(i === (listaCidadesFiltradas.length - 1)){
					await migraEnderecosPorCep(regiaoEstabelecimento, regiaoAnterior, regiaoPosterior, cepEstabelecimento);		
				}
			}
		}
	}
}

async function migraEnderecosPorCep(regiaoCep, regiaoAntes, regiaoDepois, cepEstabelecimento){
	Swal({
		icon : 'warning',
		title: 'Adicionando endereços próximos a você, por favor aguarde...',
		text : 'Adicionando Logradouros',
		closeOnClickOutside : false,
		closeOnEsc: false,
		button: {
			text: "Okay",
			closeModal: false,
		}
	});

	let limiteInsert = 0;
	let endInseridos = 0;
	let vetorDeEnderecos = [];

	for(let i = 0; i < enderecos.length; i++){
		endereco = enderecos[i].split(String.fromCharCode(9));
		let cepAtual = endereco[0];
		let regiaoCepTxt = cepAtual.substring(0,3);
		if(regiaoCepTxt === regiaoAntes || regiaoCepTxt === regiaoCep || regiaoCepTxt === regiaoDepois){
			try{
				endInseridos ++;
				//Trazer do banco as UFs para pegar o id depois
				let tipoLogradouroEncontrado = tipoLogr[endereco[3].substring(0,2).trim().toUpperCase()];
				tipoLogradouroEncontrado = tipoLogradouroEncontrado ? tipoLogradouroEncontrado : {id_tplogr : 1, descricao : 'RUA'}
				const idTipoLogradouro = tipoLogradouroEncontrado.id_tplogr;
				vetorDeEnderecos.push(
					{
						titulo: ' ',
						rua: endereco[3].substring(3, 38).split(' - ')[0].trim().toUpperCase(),
						bairro: endereco[2].substring(0,20).trim().toUpperCase(),
						CEP: cepAtual,
						range: '10',
						frete: 0.00,
						id_tplogr: idTipoLogradouro,
						id_uf: objUFs[(endereco[1].split('/')[1]).trim().toUpperCase()],
						id_municipio: objCidades[(endereco[1].split('/')[0]).trim().toUpperCase()].id_municipio
					}
				);
				limiteInsert ++;

				if(limiteInsert === 1000){
					console.log('bateuLimite');
					await modelTB_LOGRADOUROS.bulkCreate(vetorDeEnderecos).then(retorno=>{
						limiteInsert = 0 ;
						vetorDeEnderecos = [];
						console.log('inseriu');
					});
				}

				document.getElementsByClassName('swal-text')[0].innerHTML = endereco[3];
				
			}catch(e){
				console.error(endereco);
				console.error(e);
			}
		}
		if(endereco[0] === '99980974') {
			cInforsis.cfg_id_logradouro = (await modelTB_LOGRADOUROS.findOne({ where: { cep: cepEstabelecimento }})).id_logradouro;
			Swal({icon:'success', title: endInseridos + ' Logradouros inseridos com sucesso!'});
			await modelTB_LOGRADOUROS.bulkCreate(vetorDeEnderecos).then(async(retorno)=>{
				limiteInsert = 0 ;
				vetorDeEnderecos = [];
				console.log('inseriu');
				cInforsis.cfg_id_logradouro = (await modelTB_LOGRADOUROS.findOne({ where: { cep: cepEstabelecimento }})).id_logradouro;
				Swal({icon:'success', title: endInseridos + ' Logradouros inseridos com sucesso!'});
			});
		}
	}
}

function existeCEP(cepDigitado){
	if(conteudoMegaTxt.indexOf(cepDigitado) !== -1) return true;
	return false;
}
//#endregion

async function atualizarDados(){

	//lista de campos Dados cadastros (info sis)
	cInforsis.cfg_nome = document.getElementById('cfg_nome').value;
	cInforsis.cfg_fone1 = document.getElementById('cfg_fone1').value;
	cInforsis.cfg_fone2 = document.getElementById('cfg_fone2').value;
	cInforsis.cfg_fone3 = document.getElementById('cfg_fone3').value;
	cInforsis.cfg_numero_local = parseInt(document.getElementById('cfg_numero_local').value);
	cInforsis.cfg_complemento = document.getElementById('cfg_complemento').value.toUpperCase();
	cInforsis.cfg_cnpj = document.getElementById('cfg_cnpj').value;
	cInforsis.cfg_nr_serie = document.getElementById('cfg_nr_serie').value;
	cInforsis.cfg_cod_lib = document.getElementById('cfg_cod_lib').value;
	cInforsis.qde_mesas = document.getElementById('qde_mesas').value;
	cInforsis.cfg_pzcombinada = document.getElementById('cfg_pzcombinada').value;
	cInforsis.cfg_formabroto = document.getElementById('cfg_formabroto').value;
	cInforsis.cfg_porc_broto =  parseFloat(document.getElementById('cfg_porc_broto').value !== '' ? document.getElementById('cfg_porc_broto').value : '65');
	cInforsis.cfg_porc_media =  parseFloat(document.getElementById('cfg_porc_media').value !== '' ? document.getElementById('cfg_porc_media').value : '80');
	cInforsis.cfg_porc_giga =  parseFloat(document.getElementById('cfg_porc_giga').value !== '' ? document.getElementById('cfg_porc_giga').value : '25');
	cInforsis.cfg_frete_basico =  parseFloat(document.getElementById('cfg_frete_basico').value !== '' ? document.getElementById('cfg_frete_basico').value : '0');
	cInforsis.cfg_taxa_servico =  parseFloat(document.getElementById('cfg_taxa_servico').value !== '' ? document.getElementById('cfg_taxa_servico').value : '0');
	cInforsis.cfg_qde_cupom = parseInt(document.getElementById('cfg_qde_cupom').value);

	cInforsis.cfg_nome_fantasia = document.getElementById("cfg_nome_fantasia").value;
	cInforsis.cfg_ddd 			= document.getElementById('cfg_ddd').value;
	cInforsis.email 			= document.getElementById('email').value;
	cInforsis.cfg_ie 			= document.getElementById('cfg_ie').value;
	cInforsis.cfg_im 			= document.getElementById('cfg_im').value;

	cInforsis.cfg_preco_cupom = true;
	cInforsis.cfg_canhoto_cupom = true;

	let opcoes = {
		rateio: document.getElementById('rateio').value,
		cfg_separa_cupom: document.getElementById('cfg_separa_cupom').value,
		cfg_codigo_cupom: document.getElementById('cfg_codigo_cupom').value,
		cfg_contr_pagto: document.getElementById('cfg_contr_pagto').value,
		cfg_contr_estq: document.getElementById('cfg_contr_estq').value,
		cfg_promo_dia: document.getElementById('cfg_promo_dia').value,
		cfg_modulo_movel: parseFloat(document.getElementById('cfg_modulo_movel').value),
		cfg_modulo_web: parseFloat(document.getElementById('cfg_modulo_web').value),
		cfg_modulo_fiscal: parseFloat(document.getElementById('cfg_modulo_fiscal').value),
		cfg_contr_motoq: document.getElementById('cfg_contr_motoq').value,
		cfg_baixa_aqui: document.getElementById('cfg_baixa_aqui').value,
		cfg_repete_item: document.getElementById('cfg_repete_item').value,
		cfg_separa_periodo: document.getElementById('cfg_separa_periodo').value,
		cfg_faz_delivery: document.getElementById('cfg_faz_delivery').value,
		cfg_cod_barra: document.getElementById('cfg_cod_barra').value
	}

	for(var i in opcoes){
		if(opcoes[i] == "1"){
			eval("cInforsis."+i+ "= 1");
		}else if(opcoes[i] == "0"){
			eval("cInforsis."+i+" = 0");
		}
	}

	//cInforsis.cfg_id_logradouro = parseInt(document.getElementById('cfg_cep_logradouro').value);
	cInforsis.cfg_id_nicho = parseInt(document.getElementById('cfg_id_nicho').value);
	cInforsis.id_impressora = parseInt(document.getElementById('cfg_id_impressora').value);

	let tpMenu = document.getElementById('tpMenu').value;
	window.localStorage.setItem('tipoMenu', tpMenu);

	//verifica se campo esta vazio
	var conteudoNotNull = {
		cfg_nome: cInforsis.cfg_nome,
		cfg_nome: cInforsis.cfg_nome,
		cfg_cnpj: cInforsis.cfg_cnpj,
		cfg_fone1: cInforsis.cfg_fone1,
		cfg_cod_lib: cInforsis.cfg_cod_lib,
		cfg_nr_serie: cInforsis.cfg_nr_serie,
		cfg_qde_cupom: cInforsis.cfg_qde_cupom,
		cfg_id_nicho: cInforsis.cfg_id_nicho,
		cfg_id_impressora: cInforsis.id_impressora
	};

	/*
	se for uma das duas opções preencher isso no obj not null
	cfg_porc_broto: cInforsis.cfg_porc_broto,
	cfg_porc_media: cInforsis.cfg_porc_media,
	cfg_porc_giga: cInforsis.cfg_porc_giga,
	*/

	let campoFaltando = false;

	let n = 1;

	for(var i in conteudoNotNull){
		if(conteudoNotNull[i] == '' || conteudoNotNull[i] == undefined){
			campoFaltando = true;
			let footerCont = document.getElementById('alerta');
			footerCont.innerHTML += `<div class="alert alert-danger tag" role="alert">falta campo ${i}</div>`;
		}
		n++;
	}

	//insere no banco se não tiver faltando nada
	if(campoFaltando == false && idEstab){
		//#region Migrando categorias padrão
		let ex = await cGrupos.selectCodMax();
		
		if(nichoSalvo !== cInforsis.cfg_id_nicho){
			let dados = await cTGrupos.select(cInforsis.cfg_id_nicho);
			dados = JSON.parse(JSON.stringify(dados));
			let contGrupo = parseInt(ex[(ex.length - 1)].cod_grupo) + 100; //arrumar isso aqui quando  o campo trocar para inteiro

			for(d of dados){
				cGrupos.cod_grupo = contGrupo;
				cGrupos.descricao = d.desc_grupo;
				cGrupos.cupom_separado = undefined;
				cGrupos.totaliza = undefined;
				cGrupos.id_cat_padrao = 1;
				cGrupos.id_impressora = 1;
				await cGrupos.insert();
				contGrupo++;
			}
		}
		//#endregion
		
		//#region Ativa Migração de endereços
		let valor = campoLogradouroCEP.value;
		if(cepEstabCarregado !== valor){
			if(valor.length < 8 || isNaN(parseInt(valor)) || !existeCEP(valor)) campoLogradouroCEP.style.borderColor = '#f90000';
			else{
				if(regiaoCepCarregado !== valor.substring(0,3)){
					
				}
				iniciouMigracao !== true ? await migrarEnderecos(valor) : null;
				campoLogradouroCEP.style.borderColor = '#00FF00';
				iniciouMigracao = true;
			}
		}
		//#endregion
		
		cInforsis.update(idEstab).then(async (info) =>{
			const dadosInfoSis = (await cInforsis.selectAll())[0];
			window.localStorage.setItem('dadosEstabelecimento', JSON.stringify(dadosInfoSis));
			setTimeout(()=>{document.location.assign(path.join(__dirname,'../html/redirecionar.html'))},1000);
		}).catch(erro =>{
			console.log(erro);
		});
		// ret = await cInforsis.selectAll();
		// window.localStorage.setItem('dadosEstabelecimento', JSON.stringify(ret[0]));
		//event.preventDefault();	 
	}else{
		setTimeout(()=>{
			/*for(var l = 0; l < n; ){
				document.getElementsByClassName("tag")[0].remove();
				l++;
			}*/
		},3000)
		event.preventDefault();
	}
}

btnEditarEstab.addEventListener('click', ()=>{atualizarDados()});

let pizzaCombSelec = document.getElementById("cfg_formabroto");
pizzaCombSelec.addEventListener('change',(event)=>{
	porcPizza = event.target.value;
	setTimeout(() => {
		if(porcPizza === "P"){
			document.getElementById('valuesPizzasCfg').style.display = "block";
			document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (%):";
			document.getElementById("pzMediaSpan").innerText 	= "Pizza Média (%):";
			document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (% a mais):";			
		}else if(porcPizza === "F"){
			document.getElementById('valuesPizzasCfg').style.display = "block";
			document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (Menos):";
			document.getElementById("pzMediaSpan").innerText	= "Pizza Média (Menos):";
			document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (Mais):";
		}else if(porcPizza === "I"){
			document.getElementById("valuesPizzasCfg").style.display = "none";	
		}

		if(porcPizza === "I") {
			Swal({
				icon: 'warning',
				title: 'Forma individual de cobrança',
				closeOnClickOutside:false,
				closeOnEsc: false,
				text: 'Selecionando esta maneira de cobrar as pizzas você deverá entrar no cadastro e salvar o valor do tamanho de cada uma delas!',
				buttons: ['Cancelar', 'Manter decisão']
			}).then((confirmation)=>{
				if(!confirmation){
					pizzaCombSelec.value = formaPizzaAnterior;
					if(formaPizzaAnterior === "P"){
						document.getElementById('valuesPizzasCfg').style.display = "block";
						document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (%):";
						document.getElementById("pzMediaSpan").innerText 	= "Pizza Média (%):";
						document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (% a mais):";			
					}else if(formaPizzaAnterior === "F"){
						document.getElementById('valuesPizzasCfg').style.display = "block";
						document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (Menos):";
						document.getElementById("pzMediaSpan").innerText	= "Pizza Média (Menos):";
						document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (Mais):";
					}
				}
			});
		}else{
			formaPizzaAnterior = pizzaCombSelec.value;
		}
	}, 100);
});

module.exports = {
	selecionarTudo: ()=>{carregaDados()},
	selecionarItem: (item)=>{selecionarItem(item)}
};
