document.getElementById("thDesc").addEventListener('click', function(){
	orderFields('Descrição');
});

document.getElementById("thCodigo").addEventListener('click', function(){
	orderFields('Código');
});

MouseTrap.bind('esc', function () {
    addAlt('fechar');
})

function orderFields(orderField){
	document.getElementById('listaConteudoDoBanco').innerHTML = "";
    document.getElementById('listaAddItem').innerHTML = "";

	if(orderField === "Descrição"){
		let Setor 	 = require(path.join(__dirname + '/../../controller/cSetor.js'));
		let listaGrupos = Setor.selectOrderByDesc();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}else if(orderField === "Código"){
        let Setor 	 = require(path.join(__dirname + '/../../controller/cSetor.js'));
		let listaGrupos = Setor.selectOrderByCod();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
    }
}

async function insereItemNaTela(retorno,stringify){
	if(stringify != true){
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	let tbody = document.getElementById('listaAddItem');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_setor}">							
		<th  class="border-right" scope="row">
		<center>
			<button class="btn-sem-decoracao editarItem" id="${retorno.id_setor}" onclick="editarItem(this.id)">
				<img style="margin-left: -12px;" src="../img/pencil.png"/>
			</button>

			<button class="btn-sem-decoracao" id="${retorno.id_setor}" onclick="excluirItem(this.id)">
				<img style="margin-left: -12px;" src="../img/trash.png"/>
			</button>
		</center>
		</th>
		<td class="border-right txt-label" id="nome${retorno.id_setor}"><center>${retorno.id_setor}</center></td>
		<td class="border-right txt-label" id="login${retorno.id_setor}">${retorno.descricao}</td>
	</tr>`;
}

module.exports = {
    insereItemNaTela: (retorno, stringify) => {insereItemNaTela(retorno, stringify)}
}