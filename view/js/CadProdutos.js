//caminhos e imports
let idPadraoNichoPizza = 3;
var $ = require("jquery");
var path = require('path');
var swal = require('sweetalert');
window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));
const script = require(path.join(__dirname + '/script.js'));
const Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
var MouseTrap = require('mousetrap');

const caminho = path.join(__dirname + '/../../controller/cCadProdutos.js');
const cCadProdutos = require(caminho);

const cGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
const cCatPadrao = require(path.join(__dirname + '/../../controller/cCatFiscalPadrao.js'));
const cMedida = require(path.join(__dirname + '/../../controller/cUnidadeMedida.js'));
//variaveis 
let dadosEstabelecimento = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
let btnSalvar = document.getElementById('cadastrarProduto');
let btnNovo = document.getElementById('salvar');
let campoCodProd = document.getElementById('cod_prod');
const exibeCamposFiscais = dadosEstabelecimento.cfg_modulo_fiscal;
const exibeCodBarras = dadosEstabelecimento.cfg_cod_barra;
let resulSelect;
let tamanho;
let arrayGrupos = [];
let arrayMedidas = [];
let loaded = false;
let ultimoValorCadastrado = '0,00';
let ultimoCodCadastrado = 0;
let ultimoFoiPizza = false;
let ultimoIdGrupo = '0';
let ehEdicao;
let btAddAlt = document.getElementById('addAlt');


MouseTrap.bind('esc', function () {
    addAlt('fechar');
})

setTimeout(() => {
	MouseTrap.bind('f1', function () {
		btAddAlt.click();
	})
}, 1000);

setTimeout(() => {
	insereCamposFiscais();
	insereCampoCodBarras();
	document.getElementById('id_grupo').addEventListener('change', () => { listenerComboGrupo('id_grupo'); });
	
	btAddAlt.disabled = false;
	btAddAlt.addEventListener('click', () => {
		esconderCamposNaoPizza();
		ehEdicao = false;
		recuperaUltimoCodCadastrado();
		atualizaCampoValor();
		esconderControleEstoque();
		esconderValoresIndividuais(false);
		document.getElementById('nome_prod').focus();
		ultimoFoiPizza ? mostrarValoresIndividuais(false) : esconderValoresIndividuais(false);
		document.getElementById('id_grupo').value = ultimoIdGrupo;
	});
}, 500);

function insereCampoCodBarras(edicao = false, codigoAtual){
	if(exibeCodBarras){
		if(!edicao){
			const htmlCampoCodBarras = `
			<div class="cont-celulas">
				<div class="celula-1">
					<span class="label-celula">
						Código Barras:
					</span>
				</div>
				<div class="celula-2 col-2">
					<input id="cod_barr" type="text" class="form-control" aria-label="Sizing example input"
						name="Código Barras" aria-describedby="inputGroup-sizing-default" maxlength="13">
				</div>
			</div>`;
	
			document.getElementById('divCodBarras').innerHTML = htmlCampoCodBarras;
		}else{
			const htmlCampoCodBarras = `
			<div class="cont-celulas">
				<div class="celula-1">
					<span class="label-celula">
						Código Barras:
					</span>
				</div>
				<div class="celula-2 col-2">
					<input id="cod_barr" value="${codigoAtual}" type="text" class="form-control" aria-label="Sizing example input" name="Código Barras" aria-describedby="inputGroup-sizing-default" maxlength="13">
				</div>
			</div>`;
	
			document.getElementById('divCodBarrasEdit').innerHTML = htmlCampoCodBarras;
		}
		
	}
}

function insereCamposFiscais() {
	if (exibeCamposFiscais) {
		const botaoSuperiorFiscal = `
		<button id="btn-aba-secundaria" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 px-0" >
			<label id="lbl-aba-secundaria" class="titulo-modal titulo-modal-info-sis" disabled="" >
				Dados Fiscais
			</label>
		</button>
		`;
		const divBotoesSuperiores = document.getElementById('divBotoesSuperiores');
		divBotoesSuperiores.innerHTML += botaoSuperiorFiscal;

		const htmlCamposFiscais = `
		<div class="col-12" id="dados-secundarios-modal" style="display:none;">
			<hr>
			<div class="row m-0">
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">ncm:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="ncm"  type="text" class="form-control" aria-label="Sizing example input" name="ncm:" aria-describedby="inputGroup-sizing-default" maxlength="6" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cfop:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cfop" type="text" class="form-control" aria-label="Sizing example input" name="cfop" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">icms:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="icms" type="text" class="form-control" aria-label="Sizing example input" name="icms" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">csticms:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="csticms"  type="text" class="form-control" aria-label="Sizing example input" name="csticms" aria-describedby="inputGroup-sizing-default" maxlength="50" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">bcicms:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="bcicms" type="text" class="form-control" aria-label="Sizing example input" name="bcicms" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">pis:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="pis"  type="text" class="form-control" aria-label="Sizing example input" name="pis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cstpis:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cstpis"  type="text" class="form-control" aria-label="Sizing example input" name="cstpis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">bcpis:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="bcpis"  type="text" class="form-control" aria-label="Sizing example input" name="bcpis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cofins:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cofins"  type="text" class="form-control" aria-label="Sizing example input" name="cofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cstcofins:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cstcofins"  type="text" class="form-control" aria-label="Sizing example input" name="cstcofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">bccofins:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="bccofins"  type="text" class="form-control" aria-label="Sizing example input" name="bccofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">porc_mun:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="porc_mun"  type="text" class="form-control" aria-label="Sizing example input" name="porc_mun" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">porc_est:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="porc_est" type="text" class="form-control" aria-label="Sizing example input" name="porc_est" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">porc_fed:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="porc_fed" type="text" class="form-control" aria-label="Sizing example input" name="porc_fed" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cest:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cest" type="text" class="form-control" aria-label="Sizing example input" name="cest" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">id_impress:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="id_impress" type="text" class="form-control" aria-label="Sizing example input" name="id_impress" aria-describedby="inputGroup-sizing-default" maxlength="8" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">totaliza:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="totaliza" type="text" class="form-control" aria-label="Sizing example input" name="totaliza" aria-describedby="inputGroup-sizing-default" maxlength="" required>
					</div>
	
				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">origem:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="origem" type="text" class="form-control" aria-label="Sizing example input" name="origem" aria-describedby="inputGroup-sizing-default" maxlength="" required>
					</div>
				</div>
			</div>
			<hr>
		</div>`;

		const formCadastroProduto = document.getElementById('formCadastroProduto');
		formCadastroProduto.innerHTML += htmlCamposFiscais;
	}
}

campoCodProd.addEventListener('keyup', async (e) => {
	listenerCod('keyup');
});

campoCodProd.addEventListener('focusout', async (e) => {
	listenerCod('focusout');
});

async function listenerCod(evento, nomeCampo = 'cod_prod') {
	let codProdDOM = document.getElementById(nomeCampo);
	codProdDOM.value === '0' ? codProdDOM.value = 1 : null;
	let codDigitado = codProdDOM.value;
	let prod = await cCadProdutos.selectByCod(codDigitado);

	if ('focusout' === evento) carregaDadosDoProdutoExistente(prod);
	if (prod === null || codDigitado === '') codProdDOM.style.borderColor = '#00ff00';
	else codProdDOM.style.borderColor = '#ff0000';
}

function carregaDadosDoProdutoExistente(produto = null) {
	if (produto) {
		swal({
			icon: 'warning',
			title: "Código já existente!",
			text: ("Alterar o produto - " + produto.nome_prod),
			buttons: ['Sim', 'Não']
		}).then((result) => {
			console.log(result);
			if (!result) {
				addAlt('fechar', true);
				selecionarItem(produto);
			} else {
				recuperaUltimoCodCadastrado();
				campoCodProd.style.borderColor = '#00ff00';
				document.getElementById('nome_prod').focus();
			}
		})
	}
}

function esconderControleEstoque() {
	if (dadosEstabelecimento.cfg_contr_estq !== true) {
		document.getElementById('estoque').style.display = 'none';
	}
}

function esconderValoresIndividuais(edit = false) {
	edit ? document.getElementById('divValoresIndividuaisEdit').style.display = 'none' : document.getElementById('divValoresIndividuais').style.display = 'none';
}

function mostrarValoresIndividuais(edit = false) {
	if (dadosEstabelecimento.cfg_formabroto === "I") {
		edit ? document.getElementById('divValoresIndividuaisEdit').style.display = 'block' : document.getElementById('divValoresIndividuais').style.display = 'block';
	} else esconderValoresIndividuais(edit);
}

function esconderCamposNaoPizza(edit = false){
	if(dadosEstabelecimento.id_nicho === idPadraoNichoPizza){
		let camposNaoPizza = ['celula-corredor', 'celula-prateleira', 'celula-sub-cod', 'celula-cod-original', 'celula-valor-2', 'celula-valor-3'];
		if(!edit) for(campo of camposNaoPizza) document.getElementById(campo).style.display = 'none';
		else for(campo of camposNaoPizza) document.getElementById(campo + '-edit').style.display = 'none';
	}
}

async function recuperaUltimoCodCadastrado() {
	let ex = await cCadProdutos.selectCodMax();
	try {
		atualizaCampoCodProduto(ex[(ex.length - 1)].cod_prod);
	} catch (err) {
		atualizaCampoCodProduto(0);
	}
}

async function atualizaCampoValor() {
	document.getElementById('vl_venda1').value = ultimoValorCadastrado.toFixed(2).toString().replace('.',',');
}

function atualizaCampoCodProduto(codigo) {
	ultimoCodCadastrado = codigo + 1;
	document.getElementById('cod_prod').value = ultimoCodCadastrado;
}

Mousetrap.bind(['f1'], function () {
	addAlt();
});


var comboGrupo = document.getElementById('id_grupo');
var comboUmed = document.getElementById('id_umed');

async function preencheCombo(combo, controller, id) {
	controller.selectAll().then(dados => {
		if(combo.id === 'id_grupo') combo.insertAdjacentHTML('beforeend', `<option value="0"></option>`);
		dados = JSON.parse(JSON.stringify(dados));
		dados.forEach(registro => {
			if (id != undefined && id == Object.values(registro)[0]) {
				combo.insertAdjacentHTML('afterbegin', `<option value="${Object.values(registro)[0]}" selected>${Object.values(registro)[1]}</option>`);
			} else {
				combo.insertAdjacentHTML('beforeend', `<option value="${Object.values(registro)[0]}">${Object.values(registro)[1]}</option>`);
			}
		});

	})
}

function selecionarTudo() {
	document.getElementById('listaProdutos').innerHTML = '';
	let cCadProdutos = require(path.join(__dirname + '/../../controller/cCadProdutos.js'));
	let listaProd = cCadProdutos.selectAll();
	var btnEditar;

	listaProd.then(data => {
		let actuallyData = JSON.parse(JSON.stringify(data));
		for (var i of actuallyData) { insereItemNaTela(i, true); }
	});
	listaProd = null;

	preencheCombo(comboGrupo, cGrupos);
	preencheCombo(comboUmed, cMedida);
}

document.getElementById("dt_valid").setAttribute("min", Helper.dataAtualEua(true));

async function selecionarItem(item) {
	ehEdicao = true;
	let dtFormatada;
	if (item.dt_valid != null) {
		dtFormatada = Helper.formataData(item.dt_valid);
	} else {
		dtFormatada = item.dt_valid;
	}

	for (var i in item["_previousDataValues"]) {
		if (item[i] == null) {
			item[i] = "";
		}
	}

	// -------- Recuperando o nome do Grupo -------
	let nomeGrupo;

	await cCadProdutos.recuperaGrupo(item.id_prod).then(dado => {
		//console.log(JSON.parse(JSON.stringify(dado)));
		nomeGrupo = JSON.parse(JSON.stringify(dado)).Grupo.descricao;
	});
	//---------------------------------------------

	// --- Recuperando o nome da Unidade de Medida ----
	let nomeUnidMed;

	await cCadProdutos.recuperaUnidMed(item.id_prod).then(dado => {
		nomeUnidMed = JSON.parse(JSON.stringify(dado)).tba_Unid_Medida.descricao;
	});
	//-------------------------------------------------

	let valorv1 = Helper.valorEmReal(item.vl_venda1);
	let valorv2 = Helper.valorEmReal(item.vl_venda2);
	let valorv3 = Helper.valorEmReal(item.vl_venda3);

	valorv1 = valorv1.replace('R$', '').trim();
	valorv2 = valorv2.replace('R$', '').trim();
	valorv3 = valorv3.replace('R$', '').trim();

	require(path.join(__dirname + '/script.js'));

	//script.alterar();
	let divMain = document.getElementById('conteudoMain');

	//#region Montando barra superior
	let txt = `
	<div id="camadaModal" style="display:block;" class="cria-camada-modal h-100 w-100 pl-5" >
		<div class="modal-info-imp position-relative  offset-1 col-10 rounded pb-5 px-2">		
			<div class="header-modal col-12 p-0">
				<button id="btn-aba-principal" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 ml-3 abas-cad-ativa" >
					<label id="lbl-aba-principal" class="titulo-modal titulo-modal-info-sis titulo-modal-cad titulo-modal-info-sis-ativa" disabled="" >
						Dados do Produto
					</label>
				</button>
	`;

	if (exibeCamposFiscais) {
		txt += `
		<button id="btn-aba-secundaria" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 px-0" >
			<label id="lbl-aba-secundaria" class="titulo-modal titulo-modal-info-sis" disabled="" >
				Dados Fiscais					
			</label>
		</button>
		`;
	}
	//#endregion

	//#region montando os campos
	//Adicionando os campos padrão
	txt += `
			</div>
				<div class="body-modal py-4 rounded-top">	
					<div id="formCadastroProduto" class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
						<div class="col-12" id="dados-principais-modal" style="display:block;">
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">
										*Código:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="cod_prod_edit" value="${item.cod_prod}" type="text" class="form-control" aria-label="Sizing example input" name="Código" aria-describedby="inputGroup-sizing-default" maxlength="6">
								</div>
							</div>
							<div class="cont-celulas" id="celula-sub-cod-edit">
								<div class="celula-1">
									<span class="label-celula">
										SubCódigo:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="sub_cod" value="${item.sub_cod ? item.sub_cod : ''}" type="text" class="form-control" aria-label="Sizing example input" name="SubCódigo" aria-describedby="inputGroup-sizing-default" maxlength="2">
								</div>
							</div>
							<div id="divCodBarrasEdit">
						
							</div>
							<div class="cont-celulas" id="celula-cod-original-edit">
								<div class="celula-1">
									<span class="label-celula">
										Cod. Original:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="cod_original" value="${item.cod_original ? item.cod_original : ''}" type="text" class="form-control" aria-label="Sizing example input" name="Cod Original" aria-describedby="inputGroup-sizing-default" maxlength="20">
								</div>
							</div>

							<hr class="separa-linha">

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">
										*Nome Produto:
									</span>
								</div>
								<div class="celula-2 col-6">
									<input tabindex="0" id="nome_prod" value="${item.nome_prod}" type="text" class="form-control" aria-label="Sizing example input" name="Nome Produto" aria-describedby="inputGroup-sizing-default" maxlength="40">
								</div>
							</div>
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">
										Descrição do Produto:
									</span>
								</div>
								<div class="celula-2 col-6">
									<input id="desc_prod" value="${item.descricao}" type="text" class="form-control" aria-label="Sizing example input" name="Nome Produto" aria-describedby="inputGroup-sizing-default" maxlength="60">
								</div>
							</div>
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">
										Grupo:
									</span>
								</div>
								<div class="celula-2 col-3">
									<select id="id_grupo_edit" class="custom-select" tabindex="1">
									</select>
								</div>
							</div>
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">
										Medida:
									</span>
								</div>
								<div class="celula-2 col-3">
									<select id="id_umed" class="custom-select">
									</select>
								</div>
							</div>

							<hr class="separa-linha">

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">
										*Dt Validade:
									</span>
								</div>
								<div class="celula-2 col-3">
									<input id="dt_valid" value="${dtFormatada}" type="date" class="form-control" aria-label="Sizing example input" name="Dt Validade" aria-describedby="inputGroup-sizing-default" >
								</div>
							</div>
							<div class="cont-celulas" id="celula-corredor-edit">
								<div class="celula-1">
									<span class="label-celula">
										Corredor:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="corredor" value="${item.corredor}" class="form-control" aria-label="Sizing example input" name="Corredor" aria-describedby="inputGroup-sizing-default" maxlength="5">
								</div>
							</div>
							<div class="cont-celulas" id="celula-prateleira-edit">
								<div class="celula-1">
									<span class="label-celula">
										Prateleira:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="prateleira" value="${item.prateleira}" class="form-control" aria-label="Sizing example input" name="Prateleira" aria-describedby="inputGroup-sizing-default" maxlength="5">
								</div>
							</div>

							<div id = "estoque">
								<hr class="separa-linha">
								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">
											*Qtd. Minima:
										</span>
									</div>
									<div class="celula-2 col-2">
										<input id="qde_min" value="${item.qde_min}" type="text" class="form-control" aria-label="Sizing example input" onkeyup="Helper.mascaraNumInt(this)" name="Qtd. Minima" aria-describedby="inputGroup-sizing-default" maxlength="6">
									</div>
								</div>
								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">
											*Qtd. Atual:
										</span>
									</div>
									<div class="celula-2 col-2">
										<input id="qde_estq" value="${item.qde_estq}" type="text" class="form-control" aria-label="Sizing example input" name="Qtd. Atual" onkeyup="Helper.mascaraNumInt(this)" aria-describedby="inputGroup-sizing-default" maxlength="6">
									</div>
								</div>
								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">
											Contr. Estoque:
										</span>
									</div>
									<div class="celula-2 col-3">
										<select id="contr_estq" class="custom-select">
											<option value="S" ${item.contr_estq ? 'selected' : ''}>Ativada</option>
											<option value="N" ${item.contr_estq ? '' : 'selected'}>Desativada</option>
										</select>
									</div>
								</div>
							</div>

							<hr class="separa-linha">

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">
										Preço de Venda*:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input tabindex="2" id="vl_venda1" value="${valorv1.replace('.', ',')}" type="text" class="form-control" aria-label="Sizing example input" name="Valor 1" aria-describedby="inputGroup-sizing-default" maxlength="9" onkeyup="Helper.maskMoeda(this,event)">
								</div>
							</div>
							<div class="cont-celulas" id="celula-valor-2-edit">
								<div class="celula-1">
									<span class="label-celula">
										Valor 2:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="vl_venda2" value="${valorv2.replace('.', ',')}" type="text" class="form-control" aria-label="Sizing example input" name="Valor 2" aria-describedby="inputGroup-sizing-default" maxlength="9" onkeyup="Helper.maskMoeda(this,event)">
								</div>
							</div>
							<div class="cont-celulas" id="celula-valor-3-edit">
								<div class="celula-1">
									<span class="label-celula">
										Valor 3:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="vl_venda3" value="${valorv3.replace('.', ',')}" type="text" class="form-control" aria-label="Sizing example input" name="Valor 3" aria-describedby="inputGroup-sizing-default" maxlength="9" onkeyup="Helper.maskMoeda(this,event)">
								</div>
							</div>
							<div id = "divValoresIndividuaisEdit">
								<hr class="separa-linha">
								<div class="cont-celulas">
									<div class="celula-1">
										<span id="valueChange4" class="label-celula">
											Valor Broto:*
										</span>
									</div>
									<div class="celula-2 col-2">
										<input tabindex="3" id="valorBroto" type="text" class="form-control"
											aria-label="Sizing example input" name="Valor Broto"
											aria-describedby="inputGroup-sizing-default" maxlength="9"
											onkeyup="Helper.maskMoeda(this,event)" value="${item.vl_broto.toString().replace('.', ',')}">
									</div>
								</div>
								<div class="cont-celulas">
									<div class="celula-1">
										<span id="valueChange5" class="label-celula">
											Valor Média:*
										</span>
									</div>
									<div class="celula-2 col-2">
										<input tabindex="4" id="valorMedia" type="text" class="form-control" aria-label="Sizing example input"
											name="Valor Média" aria-describedby="inputGroup-sizing-default" maxlength="9"
											onkeyup="Helper.maskMoeda(this,event)" value="${item.vl_media.toString().replace('.', ',')}">
									</div>
								</div>
								<div class="cont-celulas">
									<div class="celula-1">
										<span id="valueChange6" class="label-celula">
											Valor Gigante:*
										</span>
									</div>
									<div class="celula-2 col-2">
										<input tabindex="5" id="valorGiga" type="text" class="form-control" aria-label="Sizing example input"
											name="Valor Gigante" aria-describedby="inputGroup-sizing-default" maxlength="9"
											onkeyup="Helper.maskMoeda(this,event)" value="${item.vl_giga.toString().replace('.', ',')}">
									</div>
								</div>
							</div>

						</div>
	`;

	//Colocando ou não os campos fiscais
	if (exibeCamposFiscais) {
		txt += `
		<div class="col-12" id="dados-secundarios-modal" style="display:none;">
			<hr>
			<div class="row m-0">
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">ncm:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="ncm" value="${item.ncm}"  type="text" class="form-control" aria-label="Sizing example input" name="ncm:" aria-describedby="inputGroup-sizing-default" maxlength="6" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cfop:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cfop" value="${item.cfop}"  type="text" class="form-control" aria-label="Sizing example input" name="cfop" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">icms:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="icms" value="${item.icms}"  type="text" class="form-control" aria-label="Sizing example input" name="icms" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">csticms:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="csticms" value="${item.csticms}"  type="text" class="form-control" aria-label="Sizing example input" name="csticms" aria-describedby="inputGroup-sizing-default" maxlength="50" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">bcicms:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="bcicms" value="${item.bcicms}"  type="text" class="form-control" aria-label="Sizing example input" name="bcicms" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">pis:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="pis" value="${item.pis}"  type="text" class="form-control" aria-label="Sizing example input" name="pis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cstpis:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cstpis" value="${item.cstpis}"  type="text" class="form-control" aria-label="Sizing example input" name="cstpis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">bcpis:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="bcpis" value="${item.bcpis}"  type="text" class="form-control" aria-label="Sizing example input" name="bcpis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cofins:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cofins" value="${item.cofins}"  type="text" class="form-control" aria-label="Sizing example input" name="cofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cstcofins:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cstcofins" value="${item.cstcofins}"  type="text" class="form-control" aria-label="Sizing example input" name="cstcofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">bccofins:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="bccofins" value="${item.bccofins}"  type="text" class="form-control" aria-label="Sizing example input" name="bccofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">porc_mun:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="porc_mun" value="${item.porc_mun}" type="text" class="form-control" aria-label="Sizing example input" name="porc_mun" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">porc_est:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="porc_est" value="${item.porc_est}" type="text" class="form-control" aria-label="Sizing example input" name="porc_est" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">porc_fed:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="porc_fed" value="${item.porc_fed}" type="text" class="form-control" aria-label="Sizing example input" name="porc_fed" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">cest:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="cest" value="${item.cest}" type="text" class="form-control" aria-label="Sizing example input" name="cest" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">id_impress:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="id_impress" value="${item.id_impress}" type="text" class="form-control" aria-label="Sizing example input" name="id_impress" aria-describedby="inputGroup-sizing-default" maxlength="8" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">totaliza:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="totaliza" value="${item.totaliza}" type="text" class="form-control" aria-label="Sizing example input" name="totaliza" aria-describedby="inputGroup-sizing-default" maxlength="" required>
					</div>

				</div>
				<div class="cont-celulas">
					<div class="celula-1">
						<span class="label-celula">origem:</span>
					</div>
					<div class="celula-2 col-3">
						<input id="origem" value="${item.origem}" type="text" class="form-control" aria-label="Sizing example input" name="origem" aria-describedby="inputGroup-sizing-default" maxlength="" required>
					</div>
				</div>
			</div>
			<hr>
		</div>
		`;
	}
	//#endregion

	//#region Finalizando o HTML com os botões inferiores
	txt += `
					</div>

					<div class="row footer-modal m-0">
						<div class="offset-8 col-2 my-2">
							<button class="col-12 btn btn-danger position-relative float-left" onclick="addAlt('fechar')">Sair</button>
						</div>
						<div class="col-2 my-2">
							<button id="editarProduto" type="submit" class="col-12 btn btn-success position-relative float-right" tabindex="3">Salvar</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	`;
	//#endregion

	divMain.insertAdjacentHTML('afterbegin', txt);
	esconderControleEstoque();
	verificaSeProdutoEhPizzaParaMostrarValores(item.id_grupo);
	ultimoIdGrupo = item.id_grupo;

	insereCampoCodBarras(true, item.cod_barr);

	const btnEditar = document.getElementById('editarProduto');
	btnEditar.addEventListener('click', () => { salvar(item.id_prod, true) })
	document.getElementById('nome_prod').focus();

	preencheCombo(document.getElementById('id_grupo_edit'), cGrupos, item.id_grupo);
	preencheCombo(document.getElementById('id_umed'), cMedida, item.id_umed);

	esconderCamposNaoPizza(true);

	document.getElementById("dt_valid").setAttribute("min", Helper.dataAtualEua(true));

	document.getElementById('cod_prod_edit').addEventListener('keyup', async (e) => {
		listenerCod('keyup', 'cod_prod_edit');
	});

	document.getElementById('cod_prod_edit').addEventListener('focusout', async (e) => {
		listenerCod('focusout', 'cod_prod_edit');
	});

	document.getElementById('id_grupo_edit').addEventListener('focusout', async () => {
		await listenerComboGrupo('id_grupo_edit');
	});

	document.getElementById('nome_prod').focus();

}

function verificaSeProdutoEhPizzaParaMostrarValores(id_grupo) {
	cGrupos.select(id_grupo).then((grupoEncontrado) => {
		if (grupoEncontrado.descricao === 'PIZZA' || grupoEncontrado.descricao === 'PIZZAS' || grupoEncontrado.descricao.indexOf('PIZZA') !== -1) {
			mostrarValoresIndividuais(ehEdicao);
			ultimoFoiPizza = true;
		} else {
			esconderValoresIndividuais(ehEdicao);
			ultimoFoiPizza = false;
		}
	});
}

async function listenerComboGrupo(combo) {
	combo = document.getElementById(combo);
	ultimoIdGrupo = combo.value;

	cCatPadrao.select(combo.value).then(result => {
		let nomeDadosFiscais = [
			'ORIGPROD',
			'NCM',
			'CFOP',
			'CSTICMS',
			'ICMS',
			'BCICMS',
			'CSTPIS',
			'PIS',
			'BCPIS',
			'CSTCOFINS',
			'COFINS',
			'BCCOFINS',
			'TRIBMUN',
			'TRIBEST',
			'TRIBFED',
			'CEST'
		];
		console.log(result);
		let dadosPadrao = result[0].dataValues.dados_fiscais;
		let vetorDados = dadosPadrao.trim().split(' ');
		console.log(vetorDados);
		for (let index = 0; index < vetorDados.length; index++) {
			const dadoFiscal = vetorDados[index];
			console.log(nomeDadosFiscais[index].toLowerCase());
			document.getElementById(nomeDadosFiscais[index].toLowerCase()) ? document.getElementById(nomeDadosFiscais[index].toLowerCase()).value = dadoFiscal : console.log('Não encontrou o campo');
		}
	});

	verificaSeProdutoEhPizzaParaMostrarValores(combo.value);
}

function salvar(id, ehEdicao = false) {

	//#region Arrumar isso depois, inserir campos na tela para captar esses valores
	cCadProdutos.dia_semana = '1234567';
	cCadProdutos.comissao = 0.00;
	cCadProdutos.descricao = document.getElementById('desc_prod').value.toUpperCase();
	//#endregion

	//#region Pegando dados fiscais
	cCadProdutos.bcicms = document.getElementById('bcicms') !== null ? document.getElementById('bcicms').value : '0';
	cCadProdutos.bcpis = document.getElementById('bcpis') !== null ? document.getElementById('bcpis').value : '0';
	cCadProdutos.bccofins = document.getElementById('bccofins') !== null ? document.getElementById('bccofins').value : '0';
	cCadProdutos.cest = document.getElementById('cest') !== null ? document.getElementById('cest').value : '0';
	cCadProdutos.cfop = document.getElementById('cfop') !== null ? document.getElementById('cfop').value : '0';
	cCadProdutos.cofins = document.getElementById('cofins') !== null ? document.getElementById('cofins').value : '0';
	cCadProdutos.csticms = document.getElementById('csticms') !== null ? document.getElementById('csticms').value : '0';
	cCadProdutos.cstpis = document.getElementById('cstpis') !== null ? document.getElementById('cstpis').value : '0';
	cCadProdutos.cstcofins = document.getElementById('cstcofins') !== null ? document.getElementById('cstcofins').value : '0';
	cCadProdutos.icms = document.getElementById('icms') !== null ? document.getElementById('icms').value : '0';
	cCadProdutos.id_impress = document.getElementById('id_impress') !== null ? document.getElementById('id_impress').value : '0';
	cCadProdutos.ncm = document.getElementById('ncm') !== null ? document.getElementById('ncm').value : '0';
	cCadProdutos.origem = parseInt(document.getElementById('origem') !== null ? document.getElementById('origem').value : '0');
	cCadProdutos.pis = document.getElementById('pis') !== null ? document.getElementById('pis').value : '0';
	cCadProdutos.porc_est = document.getElementById('porc_est') !== null ? document.getElementById('porc_est').value : '0';
	cCadProdutos.porc_fed = document.getElementById('porc_fed') !== null ? document.getElementById('porc_fed').value : '0';
	cCadProdutos.porc_mun = document.getElementById('porc_mun') !== null ? document.getElementById('porc_mun').value : '0';
	cCadProdutos.totaliza = document.getElementById('totaliza') !== null ? document.getElementById('totaliza').value : '0';
	//#endregion

	let valor1 = document.getElementById('vl_venda1').value + '';
	valor1 = parseFloat(valor1.replace('.', '').replace(',', '.'));
	let valor2 = document.getElementById('vl_venda2').value + '';
	valor2 = parseFloat(valor2.replace('.', '').replace(',', '.'));
	let valor3 = document.getElementById('vl_venda3').value + '';
	valor3 = parseFloat(valor3.replace('.', '').replace(',', '.'));

	/*cCadProdutos.ncm = null;
	cCadProdutos.origem = null;
	cCadProdutos.cfop = null;
	cCadProdutos.icms = null;
	cCadProdutos.csticms = null;
	cCadProdutos.bcicms = null;
	cCadProdutos.pis = null;
	cCadProdutos.cstpis = null;
	cCadProdutos.bcpis = null;
	cCadProdutos.cofins = null;
	cCadProdutos.cstcofins = null;
	cCadProdutos.bccofins = null;
	cCadProdutos.porc_mun = null;
	cCadProdutos.porc_est = null;
	cCadProdutos.porc_fed = null;
	cCadProdutos.cest = null;*/

	//lista de campos
	cCadProdutos.cod_prod = ehEdicao ? document.getElementById('cod_prod_edit').value : document.getElementById('cod_prod').value;
	document.getElementById('sub_cod').value == "" || document.getElementById('sub_cod').value == "null" ? cCadProdutos.sub_cod = null : cCadProdutos.sub_cod = document.getElementById('sub_cod').value;
	if(exibeCodBarras) document.getElementById('cod_barr').value == "" || document.getElementById('cod_barr').value == "null" ? cCadProdutos.cod_barr = null : cCadProdutos.cod_barr = document.getElementById('cod_barr').value;
	document.getElementById('corredor').value == "" || document.getElementById('corredor').value == "null" ? cCadProdutos.corredor = null : cCadProdutos.corredor = document.getElementById('corredor').value;
	document.getElementById('prateleira').value == "" || document.getElementById('prateleira').value == "null" ? cCadProdutos.prateleira = null : cCadProdutos.prateleira = document.getElementById('prateleira').value;
	document.getElementById('dt_valid').value == "" ? cCadProdutos.dt_valid = null : cCadProdutos.dt_valid = document.getElementById('dt_valid').value;
	document.getElementById('cod_original').value == "" || document.getElementById('cod_original').value == "null" ? cCadProdutos.cod_original = null : cCadProdutos.cod_original = document.getElementById('cod_original').value;
	document.getElementById('qde_estq').value == "" || document.getElementById('qde_estq').value == "null" ? cCadProdutos.qde_estq = null : cCadProdutos.qde_estq = document.getElementById('qde_estq').value;
	document.getElementById('qde_min').value == "" || document.getElementById('qde_min').value == "null" ? cCadProdutos.qde_min = null : cCadProdutos.qde_min = document.getElementById('qde_min').value;
	document.getElementById('desc_prod').value == "" || document.getElementById('desc_prod').value == "null" ? cCadProdutos.descricao = ' ' : cCadProdutos.descricao = document.getElementById('desc_prod').value.toUpperCase();
	cCadProdutos.vl_venda1 = valor1;
	cCadProdutos.vl_venda1 > 0 ? true : cCadProdutos.vl_venda1 = 0;
	cCadProdutos.vl_venda2 = valor2;
	cCadProdutos.vl_venda2 > 0 ? true : cCadProdutos.vl_venda2 = 0;
	cCadProdutos.vl_venda3 = valor3;
	cCadProdutos.vl_venda3 > 0 ? true : cCadProdutos.vl_venda3 = 0;
	cCadProdutos.nome_prod = document.getElementById('nome_prod').value.toUpperCase();
	cCadProdutos.id_grupo = ehEdicao ? document.getElementById('id_grupo_edit').value : document.getElementById('id_grupo').value;
	cCadProdutos.id_umed = document.getElementById('id_umed').value;
	cCadProdutos.dt_cad = Helper.dataAtualEua();
	cCadProdutos.dt_alt = Helper.dataAtualEua();
	cCadProdutos.id_tpprod = 1;
	cCadProdutos.id_oper = JSON.parse(window.localStorage.getItem('usuario')).id_oper;
	cCadProdutos.contr_estq = document.getElementById('contr_estq').value;

	if(cCadProdutos.id_grupo === '0'){
		swal({
		   icon: 'warning',
		   title: 'Grupo vazio',
		   closeOnClickOutside: true,
		   text: 'Escolha um grupo de produtos',
		   button: {text: 'okay', closeModal: true}
		});
		return;
	}

	if (cCadProdutos.contr_estq == 'S') {
		cCadProdutos.contr_estq = 1;
	} else {
		cCadProdutos.contr_estq = 0;
	}

	let moduloEstoq = JSON.parse(window.localStorage.dadosEstabelecimento).cfg_contr_estq;

	//verifica se campo esta vazio
	var conteudoNotNull = {
		dt_cad: cCadProdutos.dt_cad,
		id_umed: cCadProdutos.id_umed,
		id_grupo: cCadProdutos.id_grupo,
		nome_prod: cCadProdutos.nome_prod,
		cod_prod: cCadProdutos.cod_prod
	};
	let dataCorreta = Helper.validaData(cCadProdutos.dt_valid);

	if (moduloEstoq && cCadProdutos.contr_estq) {
		conteudoNotNull.qde_estq = cCadProdutos.qde_estq;
		conteudoNotNull.qde_min = cCadProdutos.qde_min;
	}

	if (dadosEstabelecimento.cfg_formabroto === "I" && ultimoFoiPizza) {
		cCadProdutos.vl_broto = document.getElementById('valorBroto').value !== '' ? document.getElementById('valorBroto').value : null;
		cCadProdutos.vl_broto = cCadProdutos.vl_broto !== null ? parseFloat(cCadProdutos.vl_broto.replace('.', '').replace(',', '.')) : null;
		conteudoNotNull.valorBroto = cCadProdutos.vl_broto;

		cCadProdutos.vl_media = document.getElementById('valorMedia').value !== '' ? document.getElementById('valorMedia').value : null;
		cCadProdutos.vl_media = cCadProdutos.vl_media !== null ? parseFloat(cCadProdutos.vl_media.replace('.', '').replace(',', '.')) : null;
		conteudoNotNull.valorMedia = cCadProdutos.vl_media;

		cCadProdutos.vl_giga = document.getElementById('valorGiga').value !== '' ? document.getElementById('valorGiga').value : null;
		cCadProdutos.vl_giga = cCadProdutos.vl_giga !== null ? parseFloat(cCadProdutos.vl_giga.replace('.', '').replace(',', '.')) : null;
		conteudoNotNull.valorGiga = cCadProdutos.vl_giga;
	}

	let campoFaltando = false;
	let nomeCampoFaltando = "";

	for (var i in conteudoNotNull) {
		if (conteudoNotNull[i] == '' || conteudoNotNull[i] == null) {
			campoFaltando = true;
			nomeCampoFaltando = document.getElementById(i).name;
			document.getElementById(i).focus();
		}
	}
	//insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
	if (dataCorreta != true) {
		swal({
			icon: 'error',
			title: "Data inválida!",
			text: "A data precisa ser no minímo hoje."
		})
	} else if (campoFaltando == false && id == undefined) {
		event.preventDefault();
		cCadProdutos.insert().then((retorno) => {
			insereItemNaTela(retorno);
			addAlt('fechar');
			ultimoValorCadastrado = cCadProdutos.vl_venda1;
		}).catch(trataErroInsert);
	} else if (campoFaltando == false && id > 0) {
		event.preventDefault();
		cCadProdutos.id_prod = id;
		cCadProdutos.update().then((retorno) => {
			addAlt('fechar', true);
			alteraItemNaTela(id, conteudoNotNull, cCadProdutos.vl_venda1);
		});
	} else if (campoFaltando) {
		Helper.alertaCampoVazio(nomeCampoFaltando)
	}
}
/*
function trocaSpan(){
	if()
}
*/
async function alteraItemNaTela(id, obj, vl) {

	let cod = document.getElementById(`cod${id}`);
	let nome = document.getElementById(`nome${id}`);
	let grupo = document.getElementById(`grupo${id}`);
	let venda = document.getElementById(`venda${id}`);

	// -------- Recuperando o nome do Grupo -------
	let nomeGrupo;

	const dado = await cCadProdutos.recuperaGrupo(id);
	nomeGrupo = dado.Grupo.descricao;
	//---------------------------------------------

	cod.innerHTML = obj.cod_prod;
	nome.innerHTML = obj.nome_prod;
	grupo.innerHTML = nomeGrupo;
	venda.innerHTML = Helper.valorEmReal(vl);
}

async function insereItemNaTela(retorno, stringify) {
	if (stringify != true) {
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	// -------- Recuperando o nome do Grupo -------
	let nomeGrupo;

	await cCadProdutos.recuperaGrupo(retorno.id_prod).then(dado => {
		//console.log(JSON.parse(JSON.stringify(dado)));
		nomeGrupo = JSON.parse(JSON.stringify(dado)).Grupo.descricao;
	});
	//---------------------------------------------

	let tbody = document.getElementById('listaProdutos');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_prod}">
		<th  class="border-right" scope="row">
		<center>
			<button class="btn-sem-decoracao editarItem" id="${retorno.id_prod}" onclick="editarItem(this.id, 'produtos')">
				<img style="margin-left: -12px;" src="../img/pencil.png"/>
			</button>
			
			<button class="btn-sem-decoracao" id="${retorno.id_prod}" onclick="excluirItem(this.id)">
				<img style="margin-left: -12px;" src="../img/trash.png"/>
			</button>
		</center>
		</th>
		<td class="border-right txt-num" id="cod${retorno.id_prod}">${retorno.cod_prod}</td>
		<td class="border-right txt-label" id="nome${retorno.id_prod}">${retorno.nome_prod}</td>
		<td class="border-right txt-label" id="grupo${retorno.id_prod}">${nomeGrupo}</td>
		<td class="border-right txt-num" id="venda${retorno.id_prod}">${Helper.valorEmReal(retorno.vl_venda1)}</td>
	</tr>`;
}

buscaGruposEMedidas();

async function buscaGruposEMedidas() {
	await cGrupos.selectAll().then((registros) => {
		if (registros.length === 0) {
			swal({
				icon: 'warning',
				title: 'Ação não permitida!',
				text: 'Para cadastrar novos produtos, você primeiro \n deve criar os Grupos! \n\n você será redirecionado para lá.'
			}).then(() => {
				//animacaoMenu(2);
				carregaFrame(`frameCadGrupos`);
			});
		} else {
			registros.forEach(dado => {
				dado = JSON.parse(JSON.stringify(dado));
				arrayGrupos.push(dado);
			});
		}
	});

	await cMedida.selectAll().then((registros) => {
		if (registros.length === 0) {
			swal({
				icon: 'warning',
				title: 'Ação não permitida!',
				text: 'Para cadastrar novos produtos, você primeiro deve criar as Unidades de Medida!'
			}).then(() => {
				animacaoMenu(2);
			});
		} else {
			registros.forEach(dado => {
				dado = JSON.parse(JSON.stringify(dado));
				arrayMedidas.push(dado);
			});
		}
	});

	//preencheCombosBox();

}

// function preencheCombosBox(){
// 	let comboGrupos = document.getElementById('id_grupo');

// 	for (let i = 0; i < arrayGrupos.length; i++) {
// 		registro = arrayGrupos[i];
// 		comboGrupos.innerHTML += `<option value="${registro.id_grupo}">${registro.descricao}</option>`
// 	}

// 	let comboMedidas = document.getElementById('id_umed');

// 	for (let i = 0; i < arrayMedidas.length; i++) {
// 		registro = arrayMedidas[i];
// 		comboMedidas.innerHTML += `<option value="${registro.id_unid_medida}">${registro.descricao}</option>`
// 	}
// }

function trataErroInsert(error) {
	console.log(error);
	console.log(JSON.stringify(error));
	if (error.name === 'SequelizeUniqueConstraintError') {

		let campo;

		switch (error.errors[0].path.replace('produtos.', '')) {
			case 'cod_prod': campo = 'Código'; document.getElementById('cod_prod').focus; break;
			case 'cod_original': campo = 'Código Original'; break;
			case 'sub_cod': campo = 'SubCódigo'; break;
			case 'cod_barr': campo = 'Código de Barras'; break;
			default: campo = error.errors[0].path.replace('produtos.', '');
		}

		swal({
			icon: 'error',
			text: 'Já existe um produto com esse ' + campo
		});

	}

	swal({
		icon: 'error',
		text: ' Erro : ' + error
	});
}

btnSalvar.addEventListener('click', () => { salvar() });

document.getElementById("thNome").addEventListener('click', function () {
	selecionaGrupoOrdenado('Nome');
})

document.getElementById("thPreco").addEventListener('click', function () {
	selecionaGrupoOrdenado('Preço');
})

document.getElementById("thCodigo").addEventListener('click', function () {
	selecionaGrupoOrdenado('Código');
})

function selecionaGrupoOrdenado(ordemType) {
	document.getElementById('listaProdutos').innerHTML = "";

	if (ordemType === "Nome") {
		let CadProdutos = require(path.join(__dirname + '/../../controller/cCadProdutos.js'));
		let listaGrupos = CadProdutos.selectOrderByNome();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for (var i of actuallyData) {
				insereItemNaTela(i, true)
			}
		});
	} else if (ordemType === "Preço") {
		let CadProdutos = require(path.join(__dirname + '/../../controller/cCadProdutos.js'));
		let listaGrupos = CadProdutos.selectOrderByVlVenda1();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for (var i of actuallyData) {
				insereItemNaTela(i, true)
			}
		});
	} else if (ordemType === "Código") {
		let CadProdutos = require(path.join(__dirname + '/../../controller/cCadProdutos.js'));
		let listaGrupos = CadProdutos.selectOrderByCod();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for (var i of actuallyData) {
				insereItemNaTela(i, true)
			}
		});
	}
}

async function editaItemSelecionado(id){
	id = parseInt(id);
	let resul = await cCadProdutos.select(id);
	selecionarItem(resul);
}
/*document.getElementById("id_grupo").addEventListener('change', function() {
	altearacaoCampos();
})*/

/*function altearacaoCampos(){
	let comboBoxCategoria = document.getElementById("id_grupo").selectedOptions[0].text;

	if (comboBoxCategoria !== "PIZZA") {
		document.getElementById("valueChange1").innerText = "*Valor 1";
		document.getElementById("valueChange2").innerText = "Valor 2";
		document.getElementById("valueChange3").innerText = "Valor 3";
		document.getElementById("valueChange4").innerText = "Valor 4";
	}else{
		document.getElementById("valueChange1").innerText = "*Normal";
		document.getElementById("valueChange2").innerText = "Broto";
		document.getElementById("valueChange3").innerText = "Média";
		document.getElementById("valueChange4").innerText = "Gigante";	
	}	
}*/


//exports
module.exports = {
	selecionarTudo: () => { selecionarTudo() },
	selecionarItem: (item) => { selecionarItem(item) },
	excluirItem: (item) => { excluirItem(item) },
	insereItemNaTela: (item, status) => { insereItemNaTela(item, status) }
};