//caminhos e imports
var $ = require("jquery");
var path = require('path');
let libSelect = require('select2');
const { isObject } = require("util");
window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));
const script = require(path.join(__dirname + '/script.js'));
var MouseTrap = require('mousetrap');
const Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
const caminho = path.join(__dirname + '/../../controller/cCadFunc.js');
const CadEnderecos = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));
const CadMunicipio = require(path.join(__dirname + '/../../controller/cMunicipio.js'));
const helperCampoCEP = require(path.join(__dirname + '/../../helpers/helperCampoCep.js'));
const cCadFunc = require(caminho);
//variaveis 
let btnSalvar = document.getElementById('cadastrar');
let campoCep = document.getElementById('cep_casa');
let campoLogin = document.getElementById('login_func');
const btnAddAlt = document.getElementById('addAlt');
var dadosEstab = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
let resulSelect;
let antiBug = true;
let loginValido = false;
let loginAnterior = '';
let edit = '';

MouseTrap.bind('esc', function () {
    addAlt('fechar');
})

setTimeout(() => {
	MouseTrap.bind('f1', function () {
		btnAddAlt.click();
	})
}, 500);

setTimeout(() => {
	btnAddAlt.addEventListener('click',()=>{
		document.getElementById('ddd').value = dadosEstab.cfg_ddd;
		loginAnterior = '';
	});
}, 400);


function mascaras() {
	//----------- Mascaras -------------------
	// document.getElementById('fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
	// document.getElementById('fone2').addEventListener('keyup', Helper.phoneMaskSemDDD);

	document.getElementById("dt_nasc").setAttribute("max", Helper.dataAtualEua(true));
	document.getElementById("dt_valid_cnh").setAttribute("min", Helper.dataAtualEua(true));

	let campoCPF = document.getElementById('cpf');
	let campoFone1 = document.getElementById('fone1');
	let campoFone2 = document.getElementById('fone2');

	campoCPF.addEventListener('keyup', Helper.maskCPF);
	campoFone1.addEventListener('keyup', Helper.phoneMaskSemDDD);
	campoFone2.addEventListener('keyup', Helper.phoneMaskSemDDD);
	campoCPF.addEventListener('focusout', () => {
		let valor = campoCPF.value.replace('.', '').replace('.', '').replace('.', '').replace('-', '');
		if (valor.length < 11 || !Helper.validaCPF(valor)) {
			campoCPF.style.borderColor = '#f90000'
		}
		else campoCPF.style.borderColor = '#00FF00'
	});
	document.getElementById("cep_casa").addEventListener("keyup", ()=>{
		Helper.controlaTamanhoCampo(document.getElementById("cep_casa"), 8);
	});
}

async function listenFocusOutCep() {
	if (campoCep.value.length === 8) {
		const campoLogradouro = document.getElementById('logradouro_casa');
		const campoBairro = document.getElementById('bairro_casa');
		const campoCidade = document.getElementById('cidade_casa');

		let retornoHelper = await helperCampoCEP(CadEnderecos, campoCep, campoLogradouro, campoBairro);
		let cidade = await CadEnderecos.recuperaCidade(retornoHelper.id_municipio);
		campoCidade.value = cidade.descricao;

		console.log(retornoHelper);
		isObject(retornoHelper) ? cCadFunc.id_logradouro = retornoHelper.id_logradouro : retornoHelper === null ? swal({ icon: 'error', title: 'CEP inexistente!' }) : null;
	}
}

async function listenFocusOutLogin() {
	if(campoLogin.value.length !== 0){
		const retornoBanco = await cCadFunc.selectComLogin(campoLogin.value);
		retornoBanco.length > 0 ? invalido() : valido();
	}else invalido();

	function valido(){campoLogin.style.borderColor = "#28a745"; loginValido = true}
	function invalido(){ if (loginAnterior !== campoLogin.value.toUpperCase()) {campoLogin.style.borderColor = "#dc3545"; loginValido = false;}}
}

campoCep.addEventListener('focusout', listenFocusOutCep);
campoLogin.addEventListener('focusout', listenFocusOutLogin);

// ---------- Preenchendo as combobox com dados do banco --------------
var comboSetor = document.getElementById('id_setor');
var comboNivel = document.getElementById('id_nivel');
var comboFuncao = document.getElementById('id_funcao');
var comboLogradouro = document.getElementById('id_logradouro');

const cSetor = require(path.join(__dirname + '/../../controller/cSetor.js'));
const cNivelUsuario = require(path.join(__dirname + '/../../controller/cNivelUsuario.js'));
const cFuncao = require(path.join(__dirname + '/../../controller/cFuncoes.js'));
const cLogradouro = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));


async function preencheCombo(combo, controller, id) {
	controller.selectAll().then(dados => {
		dados = JSON.parse(JSON.stringify(dados));
		dados.forEach(registro => {
			if (id != undefined && id == Object.values(registro)[0]) {
				combo.insertAdjacentHTML('afterbegin', `<option value="${Object.values(registro)[0]}" selected>${Object.values(registro)[1]}</option>`);
			} else {
				combo.insertAdjacentHTML('beforeend', `<option value="${Object.values(registro)[0]}">${Object.values(registro)[1]}</option>`);
			}
		});
	})
}

//funcoes
function selecionarTudo() {
	document.getElementById('lista').innerHTML = '';
	let cCadFunc = require(path.join(__dirname + '/../../controller/cCadFunc.js'));
	let listaProd = cCadFunc.selectAll();
	var btnEditar;

	listaProd.then(data => {
		let actuallyData = JSON.parse(JSON.stringify(data));
		for (var i of actuallyData) { insereItemNaTela(i, true); }
	});
	listaProd = null;

	mascaras();
	preencheCombo(comboSetor, cSetor);
	preencheCombo(comboNivel, cNivelUsuario);
	preencheCombo(comboFuncao, cFuncao);

}

async function selecionarItem(item) {

	const script = require(path.join(__dirname + '/script.js'));
	let data_nasc_formatado = Helper.converteDataBancoPDataCampo(new Date(item.dt_nasc));
	idSelecionado = item.id_logradouro;
	let Logradouro = await CadEnderecos.select(item.id_logradouro);
	let Municipio = await CadMunicipio.select(Logradouro.id_municipio);
	let divMain = document.getElementById('conteudoMain');
	let txt = `
			<div id="camadaModal" style="display:block;" class="cria-camada-modal h-100 w-100" >
				<div class="modal-info-imp position-relative offset-1 col-10 rounded pb-5 px-5 pt-3">
					<div class="header-modal col-12 p-0">

						<button id="btn-aba-principal" onclick="acoesAbaModais(this.id);" 
							class="btn-sem-decoracao aba-modal abas-cad w-33 ml-3 abas-cad-ativa" >
							<label id="lbl-aba-principal" class="titulo-modal titulo-modal-info-sis titulo-modal-cad titulo-modal-info-sis-ativa" disabled="" >
								Dados Pessoais
							</label>
						</button>

						<button id="btn-aba-secundaria" onclick="acoesAbaModais(this.id);" 
							class="btn-sem-decoracao aba-modal abas-cad w-33 px-0" >
							<label id="lbl-aba-secundaria" class="titulo-modal titulo-modal-info-sis" disabled="" >
								Dados de Residência				
							</label>
						</button>

						<button id="btn-aba-tercearia" onclick="acoesAbaModais(this.id);" 
							class="btn-sem-decoracao aba-modal abas-cad w-25 px-0" >
							<label id="lbl-aba-tercearia" class="titulo-modal titulo-modal-info-sis" disabled="" >
								Dados do Funcionário					
							</label>
						</button>

					</div>
				<div class="body-modal py-4 rounded-top">	
					<div id="formCadastroProduto" class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
						<div class="col-12" id="dados-principais-modal" style="display:block;">

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Nome Funcionário*:
									</span>
								</div>
								<div class="celula-2 col-6">
									<input id="nome" value="${item.nome}" type="text" class="form-control" aria-label="Sizing example input" name="Taxa Entrega" aria-describedby="inputGroup-sizing-sm" maxlength="20">
								</div>
							</div>
									
							<div class="border-top col-12"></div>
							
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*RG:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="rg" value="${item.rg}" type="text" class="form-control" aria-label="Sizing example input" name="RG" aria-describedby="inputGroup-sizing-sm" maxlength="8">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*CPF:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="cpf" value="${item.cpf}" type="text" class="form-control" aria-label="Sizing example input" name="CPF" aria-describedby="inputGroup-sizing-sm" maxlength="14">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">DDD*:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="ddd" value="${item.ddd ? item.ddd : ''}" type="text" class="form-control" aria-label="Sizing example input" name="DDD" aria-describedby="inputGroup-sizing-sm" maxlength="3">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Telefone 1*:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="fone1" value="${item.fone1 ? item.fone1 : ''}" type="text" class="form-control" aria-label="Sizing example input" name="fone1" aria-describedby="inputGroup-sizing-sm" maxlength="10">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Telefone 2:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="fone2" value="${item.fone2 ? item.fone2 : ''}" type="text" class="form-control" aria-label="Sizing example input" name="fone2" aria-describedby="inputGroup-sizing-sm" maxlength="10">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">CNH:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="cnh" value="${item.cnh}"  type="text" class="form-control" aria-label="Sizing example input" name="cnh" aria-describedby="inputGroup-sizing-sm" maxlength="20">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Data de Validade CNH:</span>
								</div>
								<div class="celula-2 col-4">
									<input id="dt_valid_cnh" value="${Helper.converteDataBancoPDataCampo(new Date(item.dt_valid_cnh))}" type="date" class="form-control" aria-label="Sizing example input" name="Dt Validade" aria-describedby="inputGroup-sizing-default" >
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Data de Nascimento:</span>
								</div>
								<div class="celula-2 col-4">
									<input id="dt_nasc" value="${data_nasc_formatado}" type="date" class="form-control" aria-label="Sizing example input" name="Dt Nascimento" aria-describedby="inputGroup-sizing-default" >
								</div>
							</div>

							<hr class="separa-linha">

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Banco:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="banco" value="${item.banco}" type="text" class="form-control" aria-label="Sizing example input" name="banco" aria-describedby="inputGroup-sizing-sm" maxlength="18">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Agência:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="agencia" value="${item.agencia}" type="text" class="form-control" aria-label="Sizing example input" name="agencia" aria-describedby="inputGroup-sizing-sm" maxlength="6">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Conta Bancária:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="conta_banco" value="${item.conta_banco}" type="text" class="form-control" aria-label="Sizing example input" name="conta_banco" aria-describedby="inputGroup-sizing-sm" maxlength="10">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Salário:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="salario" value="${item.salario}" type="text" class="form-control" aria-label="Sizing example input" name="salario" aria-describedby="inputGroup-sizing-sm" maxlength="8">
								</div>
							</div>
							
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Comissao:
									</span>
								</div>
								<div class="celula-2 col-4">
									<input id="comissao" value="${item.comissao}" type="text" class="form-control" aria-label="Sizing example input" name="comissao" aria-describedby="inputGroup-sizing-sm" maxlength="5">
								</div>
							</div>
								
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Setor 
									</span>
								</div>
								<div class="celula-2 col-4">
									<select id="id_setor" class="form-control form-control-sm">
									</select>
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Funções
									</span>
								</div>
								<div class="celula-2 col-4">
									<select id="id_funcao" class="form-control form-control-sm">

									</select>
								</div>
							</div>
							
						</div>
						<div class="col-12" id="dados-secundarios-modal" style="display:none;">
	
							<div class="border-top col-12"></div>
							<br>
							

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">CEP da Casa/empresa*:</span>
								</div>
								<div class="celula-2 col-4">
									<input id="cep_casa_edit" type="number" class="form-control" aria-label="Sizing example input" name="CEP Casa" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="8" value="${Logradouro.CEP}">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Logradouro da Casa/empresa*:</span>
								</div>
								<div class="celula-2 col-4">
									<input id="logradouro_casa" type="text" class="form-control" aria-label="Sizing example input" name="Logaradouro Casa" aria-describedby="inputGroup-sizing-default" placeholder="" disabled value="${(Logradouro.tba_Tp_Logr.descricao ? Logradouro.tba_Tp_Logr.descricao : ' ') + ' ' + Logradouro.rua}">
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Bairro da Casa/empresa:</span>
								</div>
								<div class="celula-2 col-4">
									<input id="bairro_casa" type="text" class="form-control" aria-label="Sizing example input"
										name="Logaradouro Casa" aria-describedby="inputGroup-sizing-default" value="${Logradouro.bairro}"
										disabled>
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Cidade da Casa/empresa:</span>
								</div>
								<div class="celula-2 col-4">
									<input id="cidade_casa" type="text" class="form-control" aria-label="Sizing example input"
										name="Logaradouro Casa" aria-describedby="inputGroup-sizing-default" value="${Municipio.descricao}"
										disabled>
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Numero da Casa:
									</span>
								</div>
								<div class="celula-2 col-2">
									<input id="numero_casa" value="${item.num_res}" type="text" class="form-control" aria-label="Sizing example input" name="numero casa" aria-describedby="inputGroup-sizing-sm" maxlength="6">
								</div>
							</div>		

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">Complemento:
									</span>
								</div>
								<div class="celula-2 col-5">
									<input id="complemento" value="${item.compl_res}" type="text" class="form-control" aria-label="Sizing example input" name="complemento" aria-describedby="inputGroup-sizing-sm" maxlength="50">
								</div>
							</div>		

						</div>

						<div id="dados-tercearios-modal" style="display:none;">
							<div class="col-12">

								<div class="row m-0">
								</div>

								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">Login:*</span>
									</div>
									<div class="celula-2 col-4">
										<input id="login_func_edit" type="text" class="form-control"
											aria-label="Sizing example input" name="LOGIN USER"
											aria-describedby="inputGroup-sizing-default" value="${item.operador}">
									</div>
								</div>

								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">Senha:*</span>
									</div>
									<div class="celula-2 col-4">
										<input id="senha_func" type="text" class="form-control"
											aria-label="Sizing example input" name="SENHA USER"
											aria-describedby="inputGroup-sizing-default" value="${item.senha}">
									</div>
								</div>

								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">Confirmar Senha:*</span>
									</div>
									<div class="celula-2 col-4">
										<input id="conf_senha_func" type="text" class="form-control"
											aria-label="Sizing example input" name="CONFIRMAR SENHA USER"
											aria-describedby="inputGroup-sizing-default" value="${item.senha}">
									</div>
								</div>

								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">Frase de segurança:*</span>
									</div>
									<div class="celula-2 col-5">
										<input id="frase_seguranca" type="text" class="form-control" aria-label="Sizing example input"
											name="frase_seguranca" aria-describedby="inputGroup-sizing-sm" maxlength="30" value="${item.frase_seguranca}">
									</div>
								</div>

								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">Nível Operacional:*</span>
									</div>
									<div class="celula-2 col-3">
										<select id="id_nivel" class="form-control form-control-sm" disabled>

										</select>
									</div>
								</div>

							</div>
						</div>

						<div class="col-12 footer-modal m-0">
							<div class="row">
								<div class="offset-8 col-2 my-2">
									<button class="col-12 btn btn-danger position-relative float-left" onclick="addAlt('fechar')">Sair</button>
								</div>
								<div class="col-2 my-2">
									<button id="editar" class="col-12 btn btn-success position-relative float-right">Salvar</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			</div>
			`;

	divMain.insertAdjacentHTML('afterbegin', txt);

	document.getElementById('cep_casa_edit').addEventListener('focusout', listenFocusOutCep);
	campoCep = document.getElementById('cep_casa_edit');
	document.getElementById('login_func_edit').addEventListener('focusout', listenFocusOutLogin);
	campoLogin = document.getElementById('login_func_edit');
	loginAnterior = item.operador;

	preencheCombo(document.getElementById('id_setor')	, cSetor, item.id_setor);
	preencheCombo(document.getElementById('id_nivel')	, cNivelUsuario, item.id_nivel);
	preencheCombo(document.getElementById('id_funcao')	, cFuncao, item.id_funcao);
	//antiBug == true ? antiBug = false :
	mascaras();

	document.getElementById('nome').focus();

	let btnEditar = document.getElementById('editar');
	btnEditar.addEventListener('click', () => { salvar(item.id_oper, true) });
}

async function salvar(id, edit = false) {
	let confirmSenha = document.getElementById('conf_senha_func').value.toUpperCase();
	//lista de campos
	cCadFunc.nome = document.getElementById('nome').value.toUpperCase();
	cCadFunc.operador = edit ? document.getElementById('login_func_edit').value.toUpperCase() : document.getElementById('login_func').value.toUpperCase();
	cCadFunc.senha = document.getElementById('senha_func').value.toUpperCase();
	cCadFunc.frase_seguranca = document.getElementById('frase_seguranca').value.toUpperCase();
	//if(!loginValido && loginAnterior !== cCadFunc.operador) return;

	campoLogin.value = campoLogin.value.toUpperCase();
	if(campoLogin.value.length !== 0){
		const retornoBanco = await cCadFunc.selectComLogin(campoLogin.value);
		if(retornoBanco.length > 0 && loginAnterior !== campoLogin.value){
			invalido();
			return false;
		}else{
			valido();
		}
	}else{
		invalido();
		return;
	}

	function valido(){campoLogin.style.borderColor = "#28a745"; loginValido = true}
	function invalido(){ 
		if (loginAnterior !== campoLogin.value.toUpperCase()){
			loginValido = false;
			campoLogin.style.borderColor = "#dc3545";
			swal({
				icon: 'error',
				title: 'Este login já existe, tente outro.'
			})
		}
	}

	cCadFunc.rg = document.getElementById('rg').value;
	cCadFunc.cpf = document.getElementById('cpf').value;

	cCadFunc.cnh = document.getElementById('cnh').value;
	cCadFunc.dt_valid_cnh = document.getElementById('dt_valid_cnh').value !== '' ? new Date(document.getElementById('dt_valid_cnh').value) : new Date('01/01/1999');
	cCadFunc.dt_nasc = document.getElementById('dt_nasc').value !== '' ? new Date(document.getElementById('dt_nasc').value) : new Date('01/01/1999');
	cCadFunc.banco = parseInt(document.getElementById('banco').value);
	cCadFunc.agencia = document.getElementById('agencia').value;
	cCadFunc.conta_banco = document.getElementById('conta_banco').value;
	cCadFunc.salario = document.getElementById('salario').value !== '' ? document.getElementById('salario').value : 0;

	cCadFunc.cpf = document.getElementById('cpf').value;
	cCadFunc.fone1 = document.getElementById('fone1').value;
	cCadFunc.fone2 = document.getElementById('fone2').value;
	cCadFunc.ddd = document.getElementById('ddd').value;
	cCadFunc.numero_casa = parseInt(document.getElementById('numero_casa').value);
	cCadFunc.complemento = document.getElementById('complemento').value;
	cCadFunc.comissao = parseFloat(document.getElementById('comissao').value ? document.getElementById('comissao').value : '0.00');

	//cCadFunc.id_logradouro 	= document.getElementById('id_logradouro').value;
	cCadFunc.id_rua_res = cCadFunc.id_rua_com = cCadFunc.id_logradouro;
	document.getElementById('numero_casa').value == "" ? cCadFunc.numero_casa = null : cCadFunc.numero_casa = document.getElementById('numero_casa').value;
	cCadFunc.num_res = cCadFunc.num_com = cCadFunc.numero_casa;
	cCadFunc.complemento = document.getElementById('complemento').value.toUpperCase();
	cCadFunc.complemento.length > 0 ? true : cCadFunc.complemento = ' ';
	cCadFunc.compl_res = cCadFunc.compl_com = cCadFunc.complemento;


	cCadFunc.dt_adm = Helper.dataAtualEua();
	cCadFunc.id_setor = parseInt(document.getElementById('id_setor').value);
	cCadFunc.id_nivel = parseInt(document.getElementById('id_nivel').value);
	cCadFunc.id_funcao = parseInt(document.getElementById('id_funcao').value);

	//verifica se campo esta vazio
	var conteudoNotNull = {
		nome: cCadFunc.nome,
		operador: cCadFunc.operador,
		senha: cCadFunc.senha,
		confirmSenha,
		frase_seguranca: cCadFunc.frase_seguranca,
		//rg: cCadFunc.rg,
		cpf: cCadFunc.cpf,
		fone1: cCadFunc.fone1,
		//fone2: cCadFunc.fone2,
		ddd: cCadFunc.ddd,
		numero_casa: cCadFunc.numero_casa,
		//comissao: cCadFunc.comissao,
		id_nivel: cCadFunc.id_nivel,
		id_funcao: cCadFunc.id_funcao
	};

	let campoFaltando = false;
	let confirmSenhaAlert = false;
	let nomeCampoFaltando = "";

	for (var i in conteudoNotNull) {
		if (conteudoNotNull[i] == '' || conteudoNotNull[i] == null) {
			campoFaltando = true;
			nomeCampoFaltando = document.getElementById(i).name;
			document.getElementById(i).focus();
			break;
		}
	}

	if (confirmSenha !== cCadFunc.senha) {
		swal({
			icon: 'error',
			title: 'Senhas não coincidem!'
		})
		campoFaltando = true;
		confirmSenhaAlert = true;
	}

	//insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
	if (campoFaltando == false && id == undefined) {
		addAlt('fechar');
		cCadFunc.insert().then((retorno) => { insereItemNaTela(retorno) });
	} else if (campoFaltando == false && id > 0) {
		cCadFunc.id_oper = id;
		await cCadFunc.update();
		addAlt('fechar', true);
		await alteraItemNaTela(id, conteudoNotNull);
	} else if (campoFaltando && confirmSenhaAlert) {
		console.log("Senhas não coincidem!");
	} else if (campoFaltando) {
		Helper.alertaCampoVazio(nomeCampoFaltando)
	}
}

async function alteraItemNaTela(id, obj) {

	let funcaoId;
	let nivelId;
	await cCadFunc.recuperaFuncao(id).then(dado => {
		funcaoId = JSON.parse(JSON.stringify(dado)).tba_Funco.descricao;
	});
	await cCadFunc.recuperaNivel(id).then(dado => {
		nivelId = JSON.parse(JSON.stringify(dado)).tba_Nivel_Usu.descricao;
	});

	let nome = document.getElementById(`nome${id}`);
	let login = document.getElementById(`login${id}`);
	let funcao = document.getElementById(`funcao${id}`);
	let nivel = document.getElementById(`nivel${id}`);

	nome.innerHTML = obj.nome;
	login.innerHTML = obj.operador;
	funcao.innerHTML = funcaoId;
	nivel.innerHTML = nivelId;
}

async function insereItemNaTela(retorno, stringify) {
	if (stringify != true) {
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	let funcaoId;
	let nivelId;

	await cCadFunc.recuperaFuncao(retorno.id_oper).then(dado => {
		funcaoId = JSON.parse(JSON.stringify(dado)).tba_Funco.descricao;
	});
	await cCadFunc.recuperaNivel(retorno.id_oper).then(dado => {
		console.log(JSON.parse(JSON.stringify(dado)));
		nivelId = JSON.parse(JSON.stringify(dado)).tba_Nivel_Usu.descricao;
	});


	let tbody = document.getElementById('lista');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_oper}">							
		<th  class="border-right" scope="row">
		<center>
			<button class="btn-sem-decoracao editarItem" id="${retorno.id_oper}" onclick="editarItem(this.id)">
				<img style="margin-left: -12px;" src="../img/pencil.png"/>
			</button>

			<button class="btn-sem-decoracao" id="${retorno.id_oper}" onclick="excluirItem(this.id)">
				<img style="margin-left: -12px;" src="../img/trash.png"/>
			</button>
		</center>
		</th>
		<td class="border-right txt-label" id="nome${retorno.id_oper}">${retorno.nome}</td>
		<td class="border-right txt-label" id="login${retorno.id_oper}">${retorno.operador}</td>
		<td class="border-right txt-label" id="funcao${retorno.id_oper}">${funcaoId}</td>
		<td class="border-right txt-label" id="nivel${retorno.id_oper}">${nivelId}</td>
	</tr>`;
}

document.getElementById("thNome").addEventListener('click', function () {
	selecionaGrupoOrdenado('Nome');
})

document.getElementById("thOperador").addEventListener('click', function () {
	selecionaGrupoOrdenado('Operador');
})

function selecionaGrupoOrdenado(ordemType) {
	document.getElementById('lista').innerHTML = "";

	if (ordemType === "Nome") {
		let CadFunc = require(path.join(__dirname + '/../../controller/cCadFunc.js'));
		let listaGrupos = CadFunc.selectOrderByNome();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for (var i of actuallyData) {
				insereItemNaTela(i, true)
			}
		});
	} else if (ordemType === "Operador") {
		let CadFunc = require(path.join(__dirname + '/../../controller/cCadFunc.js'));
		let listaGrupos = CadFunc.selectOrderByLogin();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for (var i of actuallyData) {
				insereItemNaTela(i, true)
			}
		});
	}
}

btnSalvar.addEventListener('click', () => { salvar() });
//exports
module.exports = {
	selecionarTudo: () => { selecionarTudo() },
	selecionarItem: (item) => { selecionarItem(item) },
	excluirItem: (item) => { excluirItem(item) },
	insereItemNaTela: (item, status) => { insereItemNaTela(item, status) }
};
