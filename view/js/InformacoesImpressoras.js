// 0 - Expansao
// 1 - Dexpansao
// 2 - Condensa
// 3 - Dcondensa
// 4 - Salto
// 5 - Guilhotina
// 6 - Gaveta

function defineCaracteristicasImpressora(tamanho_bobina, marcaImpressora, modeloImpressora) {
    let definicoesImpressora    = [];

    definicoesImpressora[0]     = '';
    definicoesImpressora[1]     = '';
    definicoesImpressora[2]     = '';
    definicoesImpressora[3]     = '';
    definicoesImpressora[4]     = '';
    definicoesImpressora[5]     = '';
    definicoesImpressora[6]     = '';

    //region Impressoras com a bobina de 48
    if(tamanho_bobina === 48){
        //region MECAF, BEMATECH, PERTO E DIEBOLD
        if( ( marcaImpressora === 'MECAF' ) || ( marcaImpressora === 'BEMATECH' ) || ( marcaImpressora === 'PERTO' ) || ( marcaImpressora === 'DIEBOLD' ) ){
            //region Expansao, Condensação
            definicoesImpressora[0]     = String.fromCharCode(14);
            definicoesImpressora[1]     = String.fromCharCode(20);
            definicoesImpressora[2]     = String.fromCharCode(15);
            definicoesImpressora[3]     = String.fromCharCode(18);
            //endregion

            //region Definição: Salto, Guilhotina e Gaveta
            if( marcaImpressora === 'MECAF' ){
                definicoesImpressora[4]     = calculaSalto(8);
            }else if( marcaImpressora === 'DIEBOLD' ){
                definicoesImpressora[4]     = calculaSalto(11);
                definicoesImpressora[5]     = String.fromCharCode(10) + String.fromCharCode(17);
                definicoesImpressora[6]     = String.fromCharCode(27) + '&' + String.fromCharCode(0) + String.fromCharCode(12) + String.fromCharCode(48);
            }else if( marcaImpressora === 'BEMATECH' ){
                definicoesImpressora[4]     = calculaSalto(6);
                definicoesImpressora[6]     = String.fromCharCode(27) + String.fromCharCode(118) + String.fromCharCode(140);
            }else if( marcaImpressora === 'PERTO' ){
                definicoesImpressora[4]     = calculaSalto(8);
            }
            //endregion

        }
        //endregion
        else
        //region ELEBRA, EPSON LX, LAZER, JATO DE TINTA
        if( ( marcaImpressora === 'ELEBRA' ) || ( marcaImpressora === 'EPSON LX' ) || ( marcaImpressora === 'LAZER' ) || ( marcaImpressora === 'JATO DE TINTA' ) ){

            //region Definição: Expansão, Condensação e Salto
            definicoesImpressora[0]     = String.fromCharCode(14) + String.fromCharCode(15);
            definicoesImpressora[1]     = String.fromCharCode(18) + String.fromCharCode(20);
            definicoesImpressora[2]     = String.fromCharCode(15);
            definicoesImpressora[3]     = String.fromCharCode(18);
            definicoesImpressora[4]     = calculaSalto(15);
            //endregion

        }
        //endregion
        else
        //region ELGIN, EPSON (TERMICA/TM-T20), SWEDA, CONTROL ID
        if( ( marcaImpressora === 'ELGIN' ) || ( marcaImpressora === 'EPSON' && ( modeloImpressora === 'Termica' || modeloImpressora === 'TM-T20' )) || ( marcaImpressora === 'SWEDA' ) || ( marcaImpressora === 'CONTROL ID' ) ){
            //region Definição: Expansão e Guilhotina
            definicoesImpressora[0]     = String.fromCharCode(27) + '!' + String.fromCharCode(32);
            definicoesImpressora[1]     = String.fromCharCode(27) + '!' + String.fromCharCode(0);
            definicoesImpressora[5]     = String.fromCharCode(27) + 'm';

            //region Definição: Condensação
            if(modeloImpressora !== 'Termica'){
                definicoesImpressora[2]     = String.fromCharCode(27) + '!' + String.fromCharCode(15);
                definicoesImpressora[3]     = String.fromCharCode(27) + '!' + String.fromCharCode(0);
            }
            //endregion

            //region Definição: Gaveta
            if( marcaImpressora === 'EPSON' ){
                if(modeloImpressora === 'Termica'){
                    definicoesImpressora[6]     = String.fromCharCode(27) + 'p' + String.fromCharCode(0) + String.fromCharCode(25) + String.fromCharCode(250);
                }else{
                    definicoesImpressora[6]     = String.fromCharCode(27) + 'p' + String.fromCharCode(0) + String.fromCharCode(255) + String.fromCharCode(255);
                }
            }else if( marcaImpressora === 'SWEDA' ) {
                definicoesImpressora[6]     = String.fromCharCode(27) + 'p' + String.fromCharCode(0) + String.fromCharCode(255) + String.fromCharCode(255);
            }
            //endregion

            //region Definição: Salto
            if( marcaImpressora === 'SWEDA' || marcaImpressora === 'EPSON'){
                definicoesImpressora[4]     = calculaSalto(3);
            }else if(marcaImpressora === 'CONTROL ID' ){
                definicoesImpressora[4]     = calculaSalto(1);
            }else{
                definicoesImpressora[4]     = calculaSalto(4);
            }
            //endregion

        }
        //endregion
        else if(marcaImpressora === 'TANCA'){
            definicoesImpressora[0]     = String.fromCharCode(27) + '!' + String.fromCharCode(32);
            definicoesImpressora[1]     = String.fromCharCode(27) + '!' + String.fromCharCode(0);
            definicoesImpressora[2]     = String.fromCharCode(27) + '!' + String.fromCharCode(15);
            definicoesImpressora[3]     = String.fromCharCode(27) + '!' + String.fromCharCode(0);
            definicoesImpressora[4]     = calculaSalto(3);
            definicoesImpressora[5]     = String.fromCharCode(27) + 'm';
            definicoesImpressora[6]     = String.fromCharCode(27) + 'p' + String.fromCharCode(0) + String.fromCharCode(255) + String.fromCharCode(255);
        }
    }
    //endregion
    else
    //region Impressoras com bobina de 42
    if(tamanho_bobina === 42){

        //region Definição: Expansão, Condensação, Guilhotina e Gaveta
        definicoesImpressora[0]     = String.fromCharCode(27) + '!' + String.fromCharCode(32);
        definicoesImpressora[1]     = String.fromCharCode(27) + '!' + String.fromCharCode(0);
        definicoesImpressora[2]     = String.fromCharCode(27) + '!' + String.fromCharCode(15);
        definicoesImpressora[3]     = String.fromCharCode(27) + '!' + String.fromCharCode(0);
        definicoesImpressora[5]     = String.fromCharCode(27) + 'm';
        definicoesImpressora[6]     = String.fromCharCode(27) + 'p' + String.fromCharCode(0) + String.fromCharCode(255) + String.fromCharCode(255);
        //endregion

        //region Definição: Salto
        if( marcaImpressora === 'BEMATECH' ){
            definicoesImpressora[4]     = calculaSalto(4);
        }else if( marcaImpressora === 'CONTROL ID' || marcaImpressora === 'EPSON' ){
            definicoesImpressora[4]     = calculaSalto(3);
        }
        //endregion

    }
//endregion
    else
    //region Impressora de 50
    if(tamanho_bobina === 50){

        //region Definição: Expansão, Condensação, Guilhotina, Gaveta e Salto
        definicoesImpressora[0]     = String.fromCharCode(14);
        definicoesImpressora[1]     = String.fromCharCode(20);
        definicoesImpressora[2]     = String.fromCharCode(15);
        definicoesImpressora[3]     = String.fromCharCode(18);
        definicoesImpressora[4]     = calculaSalto(3);
        definicoesImpressora[5]     = String.fromCharCode(27) + 'm';
        definicoesImpressora[6]     = String.fromCharCode(27) + 'v' + String.fromCharCode(140);
        //endregion

    }
    //endregion

    if(definicoesImpressora[0] === ''){
        return 'INI';
    }

    return definicoesImpressora;
}

function calculaSalto(salto){
    let retorno = '';

    for (let j = 0; j < salto; j++){
        retorno = retorno + '\n';
    }

    return retorno;
}

module.exports = {
    defineCaracteristicasImpressora: defineCaracteristicasImpressora
};