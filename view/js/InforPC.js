const packageJson = require('../../package.json');
const ip = require('ip');
var os = require('os');
var fs = require('fs');
var path = require('path');

setTimeout(() => {
    try{
        document.getElementById('botao-fechar').addEventListener('click', function(){
            fecharTela();
        });
    }catch(ex){
        console.warn(ex);
    }
}, 1000);

function selecionarTudo(){
    document.getElementById('nomePc').innerHTML = os.hostname();
    document.getElementById('ipPc').innerHTML = ip.address();
    const conteudoArquivoConfig = fs.readFileSync(path.join('c:/siclop/hibrido/cfghibridobd.txt'), 'utf-8').split('|');
    document.getElementById('tipoPc').innerHTML = (conteudoArquivoConfig[3] !== 'localhost'? 'TERMINAL' : 'SERVIDOR');
    document.getElementById('versaoSistema').innerHTML = packageJson.version;
}

function fecharTela(){
    carregaFrame('frameSubMenuConfiguracoes', 'SubMenu');
}

module.exports = {
    selecionarTudo: selecionarTudo,
    fecharTela: fecharTela
}