let Hel = require('../../controller/Helper.js');
var pjson = require('../../package.json');
let ver = document.getElementById("versao");

var Help = require('../../controller/Helper.js');
var os = require('os');

document.title = "SICLOP HÍBRIDO - VBeta - "+ pjson.version + " - " + os.userInfo().username + " - " + Help.dataAtualBr();

function coresDoSistema(){
	//insere cores ja selecionadas
	let corSis = window.localStorage.getItem('corSistema');
	let corN = window.localStorage.getItem('corNav');

	document.getElementById('conteudoMain').style.background = corSis; 
	document.getElementById('menuTrad').style.background = corN; 
}	coresDoSistema();


var Bind = require('mousetrap');
let { isArray } = require('util');
var Helper = require(path.join(__dirname + '../../../controller/Helper.js'));
var cCadPedido = require(path.join(__dirname + '../../../controller/cCadPedido.js'));
var cItemPedido = require(path.join(__dirname + '../../../controller/cItemPedido.js'));
let gCaixa = require(path.join(__dirname, '/GerenciarCaixa.js'));

const Pedido = require('../../controller/cCadPedido');
let dadosEstab = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
console.log(dadosEstab);

window.sessionStorage.setItem('ultimoAtalho', '');

Bind.bind(['command+1', 'ctrl+1'], function () {
    if(!validaAtalho('ctrl+1')) return 0;
    abrirSearch();
});

Bind.bind(['command+3', 'ctrl+3'], function () {
    if(!validaAtalho('ctrl+3')) return 0;
    carregaFrame("ReservaDeMesa")
});

Bind.bind(['command+5', 'ctrl+5'], function () {
    if(!validaAtalho('ctrl+5')) return 0;
    carregaFrame("frameSubMenuUltimo")
});

Bind.bind(['command+6', 'ctrl+6'], function () {
    if(!validaAtalho('ctrl+6')) return 0;
    carregaFrame("frameSubMenuCaixa")
});

function validaAtalho(atalho){
    if(atalho !== window.sessionStorage.getItem('ultimoAtalho')){
        window.sessionStorage.setItem('ultimoAtalho', atalho);
        return true;
    }
    return false;
}
/* =================== VARIAVEIS DE AMBIENTE  =========================== */

inforsis = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
usuario = JSON.parse(window.localStorage.getItem('usuario'));

const { SelecionaProdutos, 
    clickValor, 
    removeProduto, 
    adicionarQtd, 
    removerQtd, 
    enviaTexto, 
    finalizaPedido, 
    Retornar, 
    abrirIngredientes, 
    fecharModalIngrediente, 
    abrirItensExtras, 
    colocaItemExtra, 
    quantidade,
    fecharItensExtras,
    fecharModalItensExtras } = require(path.join(__dirname, '\\..\\html\\ModuloPedido\\Cardapio.js'));
    
const { guardaCliente, salvaNovoEndereco, listenFocusOutCep, fecharModalCadastro, SelecionaClientes } = require(path.join(__dirname + '\\..\\js\\SubEncontrarCliente.js'));
/* FUNÇÕES DE TELA */

function abrirSearch(){
    document.getElementById('container-search').style.display = 'block';

    let btnOkSearch = document.getElementById('ok-search');

    btnOkSearch.addEventListener('click' , function (){
        
        window.localStorage.setItem('searchDelivery', document.getElementById('input-search').value.toUpperCase());
        carregaFrame('frameSubEncontrarCliente');
    })
}

async function abrirModais(tela) {
    if (tela === "delivery") {
        let produtos = await (require('../../controller/cCadProdutos.js').selectAll());
        if(produtos.length === 0) {
            swal({
               icon: 'warning',
               title: 'Você ainda não cadastrou produtos!',
               closeOnClickOutside:false,
               text: 'Deseja cadastrar agora?',
               buttons:['Não', 'Cadastrar produtos']
            }).then(confirm=>{
               if(confirm === true) carregaFrame(`frameCadProdutos`);
            }).catch(e=>{
               throw e;
            });
            return;
        }
        document.getElementById('container-search').style.display     = 'block';
    } else if (tela === "cliente") {
        document.getElementById('container-cliente').style.display    = 'block';
    } else if (tela === "endereco") {
        document.getElementById('container-endereco').style.display   = 'block';
    }else if (tela === "itens-extras"){
        document.getElementById('itens-extras').style.display         = 'block';
    }else if (tela === "seleciona-item-extra"){
        document.getElementById('seleciona-item-extra').style.display = 'block';
    }else if (tela === "troca"){
        document.getElementById('container-troca-end').style.display = 'block';
    }
}

function fecharModais(tela) {
    if (tela === "delivery") {
        document.getElementById('container-search').style.display     = 'none';
    } else if (tela === "cliente") {
        document.getElementById('container-cliente').style.display    = 'none';
    } else if (tela === "endereco") {
        document.getElementById('container-endereco').style.display   = 'none';
    }else if (tela === "itens-extras"){
        document.getElementById('itens-extras').style.display         = 'none';
    }else if (tela === "seleciona-item-extra"){
        document.getElementById('seleciona-item-extra').style.display = 'none';
    }else if (tela === "troca"){
        document.getElementById('container-troca-end').style.display  = 'none';
    }
}


module.exports = {
	coresDoSistema : ()=>{coresDoSistema()}
};
