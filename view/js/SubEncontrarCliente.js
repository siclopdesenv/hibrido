const helperCampoCEP = require(path.join(__dirname + '/../../helpers/helperCampoCep.js'));
const cCadClientes = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
const CadEnderecos = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));

let campoCep;
let btnSalvar;
let campoLogradouro;
let campoBairro;
let campoCidade;
let campoFrete;

const { isObject } = require("util");

let search = window.localStorage.getItem('searchDelivery');

clientes = [];

clienteSelecionado = [];

let enderecoAlterado = {
    id_logradouro: 0,
    rua: "",
    num_res: "",
    compl_res: "",
    frete: ""
};

window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));

async function selecionarTudoOld() {

    let search = window.localStorage.getItem('searchDelivery');

    let listaCliente = 0;
    let listaClienteLog = 0;

    let resultSelects = [];
    let cCadCliente = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
    listaCliente = await cCadCliente.like(search);
    listaClienteLog = await cCadCliente.likeLog(search);

    for (c of listaCliente) {
        resultSelects.push(c);
    }
    for (c of listaClienteLog) {
        resultSelects.push(c);
    }

    const data = resultSelects;
    if (data.length === 0) {
        swal({
            icon: "warning",
            text: "Cliente não encontrado, criando tela para adicionar esse cliente...",
            timer: 2000,
            button: {
                text: 'Okay', closeModal: false
            }
        });

        setTimeout(() => {
            document.getElementById('container-endereco').style.display = 'block';
            recuperaUltimoCodCadastrado();
        }, 2000);
    } else {
        for (let i = 0; i < data.length; i++) {
            //clientes.push(actuallyData[i]);
            if (data.length > 1) {
                clienteSelecionado = [];
                insereItemNaTela(data[i], true, i);
            } else {
                clientes.push(data[i]);
                guardaCliente(0);
            }
        }
    }

    window.localStorage.setItem('searchDelivery', "");
}

async function selecionarTudo() {
    let search = window.localStorage.getItem('searchDelivery');

    let listaClienteNome = 0;
    let listaClienteLogr = 0;
    let listaClienteFone = 0;
    let data;

    let cCadCliente = require(path.join(__dirname + '/../../controller/cCadClientes.js'));

    listaClienteNome = await cCadCliente.like(search);
    listaClienteLogr = await cCadCliente.likeLog(search);
    listaClienteFone = await cCadCliente.likeByPhone(search);

    let resultSelectNome = [];
    let resultSelectLogr = [];
    let resultSelectFone = [];

    for (c of listaClienteNome) {
        resultSelectNome.push(c);
    }

    for (c of listaClienteLogr) {
        resultSelectLogr.push(c);
    }

    for (c of listaClienteFone) {
        resultSelectFone.push(c);
    }

    if (resultSelectNome.length > 0) {
        data = resultSelectNome;
    } else if (resultSelectLogr.length > 0) {
        data = resultSelectLogr;
    } else if (resultSelectFone.length > 0) {
        data = resultSelectFone;
    }

    if (data === undefined) {
        swal({
            icon: "warning",
            text: "Cliente não encontrado, criando tela para adicionar esse cliente...",
            timer: 2000,
            button: {
                text: 'Okay', closeModal: false
            }
        });

        setTimeout(() => {
            document.getElementById('container-endereco').style.display = 'block';
            if(!isNaN(parseInt(search))){
                const dddCli = document.getElementById('ddd_cli');
                const campoFone1 = document.getElementById('fone1');

                if(search.length === 10 || search.length === 11){
                    //Deduz que é um telefone que foi digitado
                    const dddSearch = search.substring(0,2);
                    const numSearch = search.substring(2, search.length);
                    dddCli.value = dddSearch;
                    campoFone1.value = numSearch.substring(0, numSearch.length-4) + '-' + numSearch.substring(numSearch.length-4, numSearch.length);
                }
                /* implementar isso quando quiser interpretar 8 caract
                else if(search.length === 8 || search.length === 9){
                    dddCli.value = dadosEstab.cfg_ddd;
                    campoFone1.value = search.substring(0, search.length-4) + '-' + search.substring(search.length-4, search.length);
                }*/
            }
            
            recuperaUltimoCodCadastrado();
        }, 2000);
    } else {
        for (let i = 0; i < data.length; i++) {
            //clientes.push(actuallyData[i]);
            if (data.length > 1) {
                clienteSelecionado = [];
                insereItemNaTela(data[i], true, i);
            } else {
                clientes.push(data[i]);
                guardaCliente(0);
            }
        }
    }

    window.localStorage.setItem('searchDelivery', "");
}

async function recuperaUltimoCodCadastrado() {
    btnSalvar = document.getElementById('cadastrar');
    btnSalvar.addEventListener('click', () => { salvar() });

    campoCep = document.getElementById('cep_casa');
    campoLogradouro = document.getElementById('logradouro_casa');
    campoBairro = document.getElementById('bairro_casa');
    campoCidade = document.getElementById('cidade_casa');

    campoCep.addEventListener('focusout', listenFocusOutCep);
    document.getElementById('fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
    let ex = await cCadClientes.selectCodMax();
    try {
        atualizaCampoCodCliente(ex[(ex.length - 1)].cod_cli);
    } catch (err) {
        atualizaCampoCodCliente(0);
    }
}

async function listenFocusOutCep() {
    if (campoCep.value.length === 8) {
        let retornoHelper = await helperCampoCEP(CadEnderecos, campoCep, campoLogradouro, campoBairro, campoCidade, campoFrete);
        let cidade = await CadEnderecos.recuperaCidade(retornoHelper.id_municipio);
        campoCidade.value = cidade.descricao;
        console.log(retornoHelper);
        isObject(retornoHelper) ? cCadClientes.id_logradouro = retornoHelper.id_logradouro : retornoHelper === null ? swal({ icon: 'error', title: 'CEP inexistente!' }) : null;
    }
}

function atualizaCampoCodCliente(codigo = 0) {
    ultimoCodCadastrado = codigo + 1;
    document.getElementById('cod_cli').value = ultimoCodCadastrado;
}

async function insereItemNaTela(retorno, stringify, posicaoVetor) {
    if (stringify != true) {
        retorno = JSON.parse(JSON.stringify(retorno));
    }

    clientes.push(retorno);
    let tbody = document.getElementById('listaClienteDelivery');
    tbody.innerHTML += `
	<tr style = "cursor:pointer;" onclick = "guardaCliente(${posicaoVetor})" class = "clientRow" id="tr${retorno.id_cli}">							
		<td  class="border-right" scope="row">
		    <center>
                (${retorno.ddd_cli}) ${retorno.fone1}
		    </center>
		</td>
		<td class="border-right txt-label">${retorno.nome_cli}</td>
		<td class="border-right txt-label" id="nome${retorno.id_cli}">${retorno.Logradouro.titulo} ${retorno.Logradouro.rua} Nº ${retorno.num_res} - ${retorno.Logradouro.bairro}</td>
	</tr>`;
}

function guardaCliente(posicaoCliente) {
    clienteSelecionado = clientes[posicaoCliente];
    document.getElementById('container-alterado').style.display = "block";
    trocarEnderecoPedido();
}

function trocarEnderecoPedido() {
    document.getElementById('modal-cliente').innerHTML = clienteSelecionado.nome_cli;
    document.getElementById('modal-tel').innerHTML = clienteSelecionado.fone1;
    document.getElementById('modal-cep').innerHTML = clienteSelecionado.Logradouro.CEP;
    document.getElementById('modal-rua').innerHTML = clienteSelecionado.Logradouro.rua;
    document.getElementById('modal-bairro').innerHTML = clienteSelecionado.Logradouro.bairro;
    document.getElementById('modal-frete').innerHTML = Helper.valorEmReal(clienteSelecionado.Logradouro.frete);
}

function modalTrocaEndereco() {

    fecharModais('endereco')
    abrirModais('troca');

    let containerEndereco = document.getElementById("container-content-troca");

    body = `
            <h2 class="mt-3">Atualização de endereço</h2>
            <hr size = "30" style = "background-color: #000; width: 50rem; height: 8px;"/>
            <div class = "input-container">
                    <div class = "container-column">
                        <div class = "container-row">
                            <label>CEP: </label>    
                            <input name = "CEP" type = "text" id = "input_cep" style="width: 100px;" maxlength="8"/>
                        </div>

                        <div class = "container-row">
                            <label>Rua: </label>    
                            <input name = "RUA" type = "text" id = "input-rua" style="width: 350px;" disabled/>
                        </div>

                        <div class = "container-row" id = "container-numero">
                            <label style="margin-left: 15px;">Nº: </label>    
                            <input name = "NÚMERO" type = "text" id = "input-numero"/>
                        </div>
                    </div>

                    <div class = "container-column">
                        <div class = "container-row">
                            <label>Complemento: </label>    
                            <input type = "text" id = "input-compl"/>
                        </div>
                        
                        <div class = "container-row" style="margin-right:30px; ">
                            <label>Frete: </label>    
                            <input name = "FRETE" type = "text" length = "6" style="width: 100px;" id = "input-frete" onkeyup="Helper.maskMoeda(this, event)"/>
                        </div>
                    </div>
                </div>
                <div class = "align-buttons">
                    <button class="button-ok" onclick = "salvaNovoEndereco()">OK</button>
                    <button class="button-cancel" onclick = "abrirModalTroca();">Retornar</button>
                </div>
                `

    containerEndereco.innerHTML = body;

    document.getElementById('input_cep').addEventListener('focusout', listenFocusOutCep);
    campoCep = document.getElementById('input_cep');
    campoLogradouro = document.getElementById('input-rua');
    campoBairro = document.getElementById('bairro_casa');
    campoCidade = document.getElementById('cidade_casa');
    campoFrete = document.getElementById('input-frete');
}

function abrirPedidoDelivery(origem) {
    if(document.getElementById("listaClientes")) document.getElementById("listaClientes").style.display = "none";
    document.getElementById("container-endereco").style.display = "none";
    abrirModais('cliente')
    abrirModalPedidoDelivery(origem);

    if (tipoMenu === "0") {
        document.getElementById('menuTrad').style.display = "none";
    }
}

async function salvaNovoEndereco() {
    let logradouro = 0;
    let cep = document.getElementById('input_cep');
    let rua = document.getElementById("input-rua");
    let numero = document.getElementById("input-numero");
    let compl = document.getElementById("input-compl");
    let frete = document.getElementById("input-frete");

    let campoVazio = false;
    let nomeCampoFaltando = "";

    let conteudoNotNull = {
        cep,
        rua,
        numero,
        frete
    }

    let cepDigitado = campoCep.value;
    let result = CadEnderecos.selectPorCEP(cepDigitado);
    await result.then(data => {
        let actuallyData = JSON.parse(JSON.stringify(data));
        logradouro = actuallyData.id_logradouro;
    });

    enderecoAlterado.id_logradouro = logradouro;
    enderecoAlterado.rua = rua.value.toUpperCase();
    enderecoAlterado.num_res = numero.value;
    enderecoAlterado.compl_res = compl.value.toUpperCase();
    enderecoAlterado.frete = parseFloat(frete.value.toUpperCase());

    for (i in conteudoNotNull) {
        if (conteudoNotNull[i].value == "" || conteudoNotNull[i].value == null) {
            campoVazio = true
            nomeCampoFaltando += document.getElementById('input-' + i).name + ", ";
            document.getElementById('input-' + i).focus();
        }
    }

    if (campoVazio) {
        Helper.alertaCampoVazio(nomeCampoFaltando)
    } else {
        abrirPedidoDelivery('enderecoAlterado')
    }
}

function abrirModalPedidoDelivery(origem) {
    let conteudoModPedido = fs.readFileSync(__dirname + '\\..\\html\\ModuloPedido\\Cardapio.html', 'utf-8')
    document.getElementById('container-troca-end').style.display = 'none';
    let modalPedidoDelivery = document.getElementById("container-cliente");

    modalPedidoDelivery.innerHTML = conteudoModPedido;
    SelecionaProdutos();

    if (origem === 'enderecoAlterado') {
        document.getElementById('nomeCli').innerHTML = clienteSelecionado.nome_cli;
        document.getElementById('ruaCli').innerHTML = enderecoAlterado.rua;
        document.getElementById('numCli').innerHTML = enderecoAlterado.num_res;
        document.getElementById('foneCli').innerHTML = clienteSelecionado.fone1;
        //document.getElementById('freteTot').innerHTML = Helper.valorEmReal(enderecoAlterado.frete);
        document.getElementById('compCli').innerHTML = enderecoAlterado.compl_res === "" ? "Sem Complemento" : enderecoAlterado.compl_res;

        window.localStorage.setItem('enderecoAlterado', JSON.stringify(enderecoAlterado));
        window.localStorage.setItem('tipoEndPed', "2");

    } else if (origem === 'enderecoBanco') {
        document.getElementById('nomeCli').innerHTML = clienteSelecionado.nome_cli;
        document.getElementById('ruaCli').innerHTML = clienteSelecionado.Logradouro.rua;
        document.getElementById('numCli').innerHTML = clienteSelecionado.num_res;
        document.getElementById('foneCli').innerHTML = clienteSelecionado.fone1;
        //document.getElementById('freteTot').innerHTML = Helper.valorEmReal(clienteSelecionado.Logradouro.frete);
        document.getElementById('compCli').innerHTML = clienteSelecionado.compl_res === "" ? "Sem Complemento" : clienteSelecionado.compl_res;

        window.localStorage.setItem('clienteSelecionado', JSON.stringify(clienteSelecionado));
        window.localStorage.setItem('tipoEndPed', "1");
    }
}

async function salvar(id) {
    cCadClientes.id_cli = null;

    //let desconto = document.getElementById('desconto').value + '';
    //desconto = desconto === '' ? 0 : parseFloat(desconto.replace('.','').replace('.','.'));

    //let credito = document.getElementById('credito').value + '';
    //credito = credito=== '' ? 0 : parseFloat(credito.replace('.','').replace('.','.'));

    //lista de campos
    cCadClientes.cod_cli = document.getElementById('cod_cli').value;
    cCadClientes.nome_cli = document.getElementById('nome_cli').value.toUpperCase();
    cCadClientes.apelido = '';
    cCadClientes.ddd_cli = document.getElementById('ddd_cli').value;
    cCadClientes.fone1 = document.getElementById('fone1').value;
    cCadClientes.fone2 = '';
    cCadClientes.fone3 = '';
    //cCadClientes.id_logradouro 	= document.getElementById('id_logradouro').value;
    cCadClientes.num_res = document.getElementById('numero_casa').value === "" ? null : document.getElementById('numero_casa').value;
    cCadClientes.num_com = cCadClientes.num_res;
    cCadClientes.compl_res = document.getElementById('complemento').value.toUpperCase();
    cCadClientes.compl_res.length > 0 ? true : cCadClientes.compl_res = ' ';
    cCadClientes.compl_com = cCadClientes.compl_res;
    cCadClientes.referencia = document.getElementById('referencia').value.toUpperCase();
    cCadClientes.referencia.length > 0 ? true : cCadClientes.referencia = ' ';
    cCadClientes.credito = 0.0
    cCadClientes.desconto = 0.0
    cCadClientes.dt_nasc = "" !== '' ? new Date(document.getElementById('dt_nasc').value) : null;
    cCadClientes.dt_cad = Helper.dataAtualEua();
    cCadClientes.dt_alt = Helper.dataAtualEua();
    cCadClientes.id_situacao = 1;
    cCadClientes.id_fpgto = 1;
    cCadClientes.id_oper = JSON.parse(window.localStorage.getItem('usuario')).id_oper;

    //console.log(cCadClientes.credito)

    //verifica se campo esta vazio
    var conteudoNotNull = {
        cod_cli: cCadClientes.cod_cli,
        nome_cli: cCadClientes.nome_cli,

        ddd_cli: cCadClientes.ddd_cli,
        fone1: cCadClientes.fone1,

        //id_logradouro:cCadClientes.id_logradouro,
        numero_casa: cCadClientes.num_res,

        dt_cad: cCadClientes.dt_cad,
        dt_alt: cCadClientes.dt_alt,

        id_situacao: cCadClientes.id_situacao,
        id_fpgto: cCadClientes.id_fpgto
    };

    let nomeCampoFaltando = "";
    let campoFaltando = false;

    for (var i in conteudoNotNull) {
        if (conteudoNotNull[i] == '' || conteudoNotNull[i] == null) {
            campoFaltando = true;
            nomeCampoFaltando = document.getElementById(i).name;
            document.getElementById(i).focus();
        }
    }

    //insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
    if (campoFaltando == false && id == undefined) {
        event.preventDefault();
        cCadClientes.insert()
            .then((retorno) => {

                let cCadCliente = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
                let listaCliente = cCadCliente.select(retorno.id_cli);

                listaCliente.then(data => {
                    let actuallyData = JSON.parse(JSON.stringify(data));
                    clienteSelecionado = actuallyData;
                    setTimeout(() => {
                        abrirPedidoDelivery("enderecoBanco");
                    }, 100);
                });
            })
            .catch((error) => {
                trataErroInsert(error)
            });
    } else if (campoFaltando == false && id > 0) {
        event.preventDefault();
        cCadClientes.id_cli = id;
        await cCadClientes.update();
        addAlt('fechar', true);
        alteraItemNaTela(id, conteudoNotNull);
    } else if (campoFaltando) {
        Helper.alertaCampoVazio(nomeCampoFaltando)
    }
}

function fecharModalCadastro() {
    carregaFrame('frameSubMenuMovimento', 'SubMenu');
}

function trataErroInsert(error) {
    if (error.name === 'SequelizeUniqueConstraintError') {

        let campo;

        switch (error.errors[0].path.replace('clientes.', '')) {
            case 'cod_cli': campo = 'Código'; break;
            default: campo = error.errors[0].path.replace('clientes.', '');
        }

        swal({
            icon: 'error',
            text: 'Já existe um grupo com esse ' + campo
        });

    }
}

module.exports = {
    selecionarTudo: () => { selecionarTudo() },
    insereItemNaTela: (item, status, posicaoVetor) => { insereItemNaTela(item, status, posicaoVetor) },
    guardaCliente: (posicaoCliente) => { guardaCliente(posicaoCliente) },
    salvaNovoEndereco: salvaNovoEndereco,
    listenFocusOutCep: listenFocusOutCep,
    fecharModalCadastro: fecharModalCadastro,
    modalTrocaEndereco: modalTrocaEndereco,
    abrirPedidoDelivery: abrirPedidoDelivery
};