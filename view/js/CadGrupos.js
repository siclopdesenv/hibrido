const Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
const CadGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
const cCatPadrao = require(path.join(__dirname + '/../../controller/cCatFiscalPadrao.js'));
const ConfImpressoras = require(path.join(__dirname + '/../../controller/cConfImpressoras.js'));

var MouseTrap = require('mousetrap');
let btnSalvar = document.getElementById('cadastrarCategoria');
let descricaoCat = document.getElementById('descricao');
let moduloFiscal = JSON.parse(window.localStorage.dadosEstabelecimento).cfg_modulo_fiscal;
let displayFiscal = document.getElementById("btn-aba-secundaria");
let btAddAlt = document.getElementById('addAlt');
let ultimoCodCadastrado = 0;

MouseTrap.bind('esc', function () {
    addAlt('fechar');
})

setTimeout(() => {
	btAddAlt.disabled = false;
	MouseTrap.bind('f1', function () {
		btAddAlt.click();
	})
}, 1000);

btAddAlt.addEventListener('click',()=>{
	recuperaUltimoCodCadastrado();
});

async function recuperaUltimoCodCadastrado(){
	let ex = await CadGrupos.selectCodMax();
	atualizaCampoCodGrupo(ex[(ex.length - 1)].cod_grupo);
}

function atualizaCampoCodGrupo(codigo){
	ultimoCodCadastrado = codigo + 1;
	document.getElementById('cod_grupo').value = ultimoCodCadastrado;
}

descricaoCat.addEventListener('focusout',async ()=>{
	cCatPadrao.descricao = descricaoCat.value;
	let retorno = await cCatPadrao.selectPorDesc();

	if(retorno === null) retorno = await cCatPadrao.selectDescLike();
	if(retorno !== null) preencheCamposFiscaisPadrao(retorno);
});

function preencheCamposFiscaisPadrao(dadosPadrao){
	console.log(dadosPadrao);
	CadGrupos.id_cat_padrao = dadosPadrao.id_cat_padrao;
	/*console.log(dadosPadrao.dados_fiscais);
	let nomeDadosFiscais = [
		'ORIGPROD',
		'NCM',
		'CFOP',
		'CSTICMS',
		'ICMS',
		'BCICMS',
		'CSTPIS',
		'PIS',
		'BCPIS',
		'CSTCOFINS',
		'COFINS',
		'BCCOFINS',
		'TRIBMUN',
		'TRIBEST',
		'TRIBFED',
		'CEST'
	];
	let vetorDados = dadosPadrao.dados_fiscais.trim().split(' ');
	console.log(vetorDados);
	for (let index = 0; index < vetorDados.length; index++) {
		const dadoFiscal = vetorDados[index];
		document.getElementById(nomeDadosFiscais[index].toLowerCase()).value = dadoFiscal;
	}*/
}

let idItemSelecionado;
function adicionaOpcoesDaModal(){
	let listaEnd = ConfImpressoras.selectAll();
	listaEnd.then(data => {
		let actuallyData = JSON.parse(JSON.stringify(data));
		let v =  document.getElementById("id_impress");
		for(var i of actuallyData){
			if(idItemSelecionado != undefined && idItemSelecionado == i.id_impressora){
				v.insertAdjacentHTML('afterbegin',`<option value="${i.id_impressora}" selected>${i.apelido}</option>`);	
			}else{
				v.insertAdjacentHTML('beforeend',`<option value="${i.id_impressora}">${i.apelido}</option>`);
			}
		}
	});
}

function selecionarTudo(){
	document.getElementById('listaCategorias').innerHTML = '';
	let CadGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
	let listaGrupos = CadGrupos.selectAll();

	listaGrupos.then(data => {
		let actuallyData = JSON.parse(JSON.stringify(data));
		for(var i of actuallyData){insereItemNaTela(i,true)}
	});
	adicionaOpcoesDaModal();

	if(moduloFiscal == false){
		//displayFiscal.style.display = 'none';
		document.getElementById('btn-aba-principal').classList.add("offset-1");
	}

}

function selecionarItem(item){	
	const script = require(path.join(__dirname + '/script.js'));
	let divMain = document.getElementById('conteudoMain');
	idItemSelecionado = item.id_impressora;

	for(var i in item["_previousDataValues"]){
		if(item[i] == null){
			item[i] = "";
		}
	}

	let txt = `
		<div id="camadaModal" style="display:block;" class="cria-camada-modal h-100 w-100 pl-5" >
		<div class="modal-info-imp position-relative col-10 offset-1 rounded pb-5 px-5">		
			<div class="header-modal col-12 p-0">
				<button id="btn-aba-principal" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 ml-3 abas-cad-ativa" >
					<label id="lbl-aba-principal" class="titulo-modal titulo-modal-info-sis titulo-modal-cad titulo-modal-info-sis-ativa" disabled="" >
						Dados de Grupo
					</label>
				</button>

				<button id="btn-aba-secundaria" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 px-0" >
					<label id="lbl-aba-secundaria" class="titulo-modal titulo-modal-info-sis" disabled="" >
					 	Dados Fiscais					
					</label>
				</button>
			</div>
			
			<div class="body-modal py-4 rounded shadow">	
				<div id="formCadastroProduto" class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

					<div class="col-12" id="dados-principais-modal" style="display:block;">
					<hr>

					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">
								Código*:
							</span>
						</div>
						<div class="celula-2 col-3">
						<input id="cod_grupo" value="${item.cod_grupo}" type="text" class="form-control" aria-label="Sizing example input" name="Código" aria-describedby="inputGroup-sizing-default" maxlength="3" required disabled>
						</div>
					</div>

					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">
								*Descrição:
							</span>
						</div>
						<div class="celula-2 col-3">
							<input id="descricao" value="${item.descricao}" type="text" class="form-control" aria-label="Sizing example input" name="Descrição" aria-describedby="inputGroup-sizing-default" maxlength="20" required>
						</div>
					</div>

					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">
								*Impressora Padrão:
							</span>
						</div>
						<div class="celula-2 col-3">
							<div class="input-group-prepend">
								<select id="id_impress" class="custom-select">
									<!-- inserido pelo js -->
								</select>
							</div>
						</div>
					</div>

					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">
								cupom separado:
							</span>
						</div>
						<div class="celula-2 col-3">
							<select id="cupom_separado" class="custom-select">
								<option value="0">Não</option>
								<option value="1">Sim</option>
							</select>
						</div>
					</div>

				</div>					

				<div class="col-12" id="dados-secundarios-modal" style="display:none;">
					<hr>
					<div class="row m-0">
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">ncm:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="ncm" value="${item.ncm}"  type="text" class="form-control" aria-label="Sizing example input" name="ncm:" aria-describedby="inputGroup-sizing-default" maxlength="6" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">cfop:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="cfop" value="${item.cfop}"  type="text" class="form-control" aria-label="Sizing example input" name="cfop" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">icms:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="icms" value="${item.icms}"  type="text" class="form-control" aria-label="Sizing example input" name="icms" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">csticms:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="csticms" value="${item.csticms}"  type="text" class="form-control" aria-label="Sizing example input" name="csticms" aria-describedby="inputGroup-sizing-default" maxlength="50" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">bcicms:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="bcicms" value="${item.bcicms}"  type="text" class="form-control" aria-label="Sizing example input" name="bcicms" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">pis:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="pis" value="${item.pis}"  type="text" class="form-control" aria-label="Sizing example input" name="pis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">cstpis:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="cstpis" value="${item.cstpis}"  type="text" class="form-control" aria-label="Sizing example input" name="cstpis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">bcpis:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="bcpis" value="${item.bcpis}"  type="text" class="form-control" aria-label="Sizing example input" name="bcpis" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">cofins:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="cofins" value="${item.cofins}"  type="text" class="form-control" aria-label="Sizing example input" name="cofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">cstcofins:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="cstcofins" value="${item.cstcofins}"  type="text" class="form-control" aria-label="Sizing example input" name="cstcofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">bccofins:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="bccofins" value="${item.bccofins}"  type="text" class="form-control" aria-label="Sizing example input" name="bccofins" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">porc_mun:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="porc_mun" value="${item.porc_mun}" type="text" class="form-control" aria-label="Sizing example input" name="porc_mun" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">porc_est:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="porc_est" value="${item.porc_est}" type="text" class="form-control" aria-label="Sizing example input" name="porc_est" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">porc_fed:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="porc_fed" value="${item.porc_fed}" type="text" class="form-control" aria-label="Sizing example input" name="porc_fed" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">cest:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="cest" value="${item.cest}" type="text" class="form-control" aria-label="Sizing example input" name="cest" aria-describedby="inputGroup-sizing-default" maxlength="30" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">id_impress:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="id_impress" value="${item.id_impressora}" type="text" class="form-control" aria-label="Sizing example input" name="id_impress" aria-describedby="inputGroup-sizing-default" maxlength="8" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">totaliza:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="totaliza" value="${item.totaliza}" type="text" class="form-control" aria-label="Sizing example input" name="totaliza" aria-describedby="inputGroup-sizing-default" maxlength="" required>
							</div>

						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">origem:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="origem" value="${item.origem}" type="text" class="form-control" aria-label="Sizing example input" name="origem" aria-describedby="inputGroup-sizing-default" maxlength="" required>
							</div>
						</div>
					</div>
					<hr>
				</div>

				</div>

				<div class="row footer-modal m-0">
					<div class="offset-8 col-2 my-2">
						<button class="col-12 btn btn-danger position-relative float-left" onclick="addAlt('fechar',true)">Sair</button>
					</div>
					<div class="col-2 my-2">
						<button id="editar" type="submit" class="col-12 btn btn-success position-relative float-right">Salvar</button>
					</div>
				</div>

			</div>
		</div>
	</div>
	`;

	divMain.insertAdjacentHTML('afterbegin', txt);	

	let btnEditar = document.getElementById('editar');
	btnEditar.addEventListener('click', ()=>{salvar(item.id_grupo)})
	adicionaOpcoesDaModal();

	if(moduloFiscal == false){
		document.getElementById('btn-aba-secundaria').style.display = 'none';
		document.getElementById('btn-aba-principal').classList.add("offset-1");
	}
	
	document.getElementById('cod_grupo').focus();
}

async function salvar(id){
	
	CadGrupos.cod_grupo = document.getElementById('cod_grupo').value;
	CadGrupos.descricao = document.getElementById('descricao').value.toUpperCase();
	CadGrupos.descricao = Helper.removeCaracEspeciais(CadGrupos.descricao);
	// CadGrupos.id_impress = parseInt(document.getElementById('id_impress').value);
	// CadGrupos.cupom_separado = document.getElementById('cupom_separado').value;
	// CadGrupos.ncm = document.getElementById('ncm').value;
	// CadGrupos.cfop = document.getElementById('cfop').value;
	// CadGrupos.icms = document.getElementById('icms').value;
	// CadGrupos.origem = parseInt(document.getElementById('origem').value);
	// CadGrupos.csticms = parseInt(document.getElementById('csticms').value);
	// CadGrupos.bcicms = document.getElementById('bcicms').value;
	// CadGrupos.pis = document.getElementById('pis').value;
	// CadGrupos.cstpis = document.getElementById('cstpis').value;
	// CadGrupos.bcpis = document.getElementById('bcpis').value;
	// CadGrupos.cofins = document.getElementById('cofins').value;
	// CadGrupos.cstcofins = document.getElementById('cstcofins').value;
	// CadGrupos.bccofins = document.getElementById('bccofins').value;
	// CadGrupos.porc_mun = document.getElementById('porc_est').value;
	// CadGrupos.porc_mun = document.getElementById('porc_fed').value;
	// CadGrupos.porc_mun = document.getElementById('porc_mun').value;
	// CadGrupos.cest = document.getElementById('cest').value;
	CadGrupos.id_impressora = document.getElementById('id_impress').value;
	// CadGrupos.totaliza = document.getElementById('totaliza').value;


	//verifica se campo esta vazio
	var conteudoNotNull = {
		cod_grupo: CadGrupos.cod_grupo,
		descricao: CadGrupos.descricao,
		id_impressora: CadGrupos.id_impressora
	};

	let campoFaltando = false;

	for(var i in conteudoNotNull){
		if(conteudoNotNull[i] == '' || conteudoNotNull[i] == null){
			campoFaltando = true;
			nomeCampoFaltando = document.getElementById(i).name;
			document.getElementById(i).focus();
			break;
		}
	}

	//insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
	if(campoFaltando == false && id == undefined){
		event.preventDefault();
		CadGrupos.insert().then((retorno)=>{
			addAlt('fechar');
			insereItemNaTela(retorno)
		}).catch(trataErroInsert);		
	}else if(campoFaltando == false && id > 0){
		event.preventDefault();
		CadGrupos.id_grupo = id;
		await CadGrupos.update();
		addAlt('fechar',true);
		await alteraItemNaTela(id,conteudoNotNull);
	}else if(campoFaltando){
		Helper.alertaCampoVazio(nomeCampoFaltando)
	}
}

async function alteraItemNaTela(id,obj){

	let nomeImp;
	await CadGrupos.recuperaImp(id).then(dado =>{
		nomeImp = JSON.parse(JSON.stringify(dado)).Impressora.apelido;
	});

	let cod = document.getElementById(`cod${id}`);
	let descricao = document.getElementById(`descricao${id}`);
	let impressora = document.getElementById(`impressora${id}`);

	cod.innerHTML = obj.cod_grupo;
	descricao.innerHTML = obj.descricao;
	impressora.innerHTML = nomeImp;
}

async function insereItemNaTela(retorno,stringify){
	//debugger;
	if(stringify != true){
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	let nomeImp;
	await CadGrupos.recuperaImp(retorno.id_grupo).then(dado =>{
		nomeImp = JSON.parse(JSON.stringify(dado)).Impressora.apelido;
		//nomeImp = 'IMPRESSORA REMENDO';
	});
	
	let tbody = document.getElementById('listaCategorias');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_grupo}">							
		<th  class="border-right" scope="row">
		<center>
			<button class="btn-sem-decoracao editarItem" id="${retorno.id_grupo}" onclick="editarItem(this.id)">
				<img style="margin-left: -12px;" src="../img/pencil.png"/>
			</button>

			<button class="btn-sem-decoracao" id="${retorno.id_grupo}" onclick="excluirItem(this.id)">
				<img style="margin-left: -12px;" src="../img/trash.png"/>
			</button>
		</center>
		</th>
		<td class="border-right txt-num" id="cod${retorno.id_grupo}">${retorno.cod_grupo}</td>
		<td class="border-right txt-label" id="descricao${retorno.id_grupo}">${retorno.descricao}</td>
		<td class="border-right txt-label" id="impressora${retorno.id_grupo}">${nomeImp}</td>
	</tr>`;
}

setTimeout(() => {
	btnSalvar.addEventListener('click',()=>{salvar()});
},2000);

function trataErroInsert(error){
	if(error.name === 'SequelizeUniqueConstraintError'){

		let campo;

		switch(error.errors[0].path.replace('grupos.','')){
			case 'cod_grupo': campo = 'Código'; break;
			default: campo = error.errors[0].path.replace('produtos.','');
		}

		swal({
			icon:'error',
			text: 'Já existe um grupo com esse ' + campo
		});

	}
}

document.getElementById("thDesc").addEventListener('click', function(){
	selecionaGrupoOrdenado('Descrição');
})

document.getElementById("thCat").addEventListener('click', function(){
	selecionaGrupoOrdenado('Categorias');
})

function selecionaGrupoOrdenado(ordemType){
	document.getElementById('listaCategorias').innerHTML = "";

	if(ordemType === "Descrição"){
		let CadGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
		let listaGrupos = CadGrupos.selectOrderByDesc();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}else if(ordemType === "Categorias"){
		let CadGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
		let listaGrupos = CadGrupos.selectOrderByCod();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}
}

module.exports = {
	selecionarTudo: ()=>{selecionarTudo()},
	selecionarItem: (item)=>{selecionarItem(item)},
	excluirItem: (item)=>{excluirItem(item)},
	insereItemNaTela: (item,status)=>{insereItemNaTela(item,status)},
	selecionaGrupoOrdenado: (ordemType) => {selecionaGrupoOrdenado(ordemType)}
};