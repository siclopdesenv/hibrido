var idBotao;
var st = 0;
let user = JSON.parse(window.localStorage.getItem('usuario', '{}'));

var os = require('os');
let Hel = require('../../controller/Helper.js');
var pjson = require('../../package.json');
let ver = document.getElementById("versao");
var path = require('path');
document.title = "SICLOP HÍBRIDO - VBeta - "+ pjson.version + " - " + os.userInfo().username + " - " + Hel.dataAtualBr();

let btnApareceMenu = document.getElementById('botao-aparecer-menu'),
	contSubMenu    = document.getElementsByClassName("cont-sub-menu"),
 	menu 	   = document.getElementById('menu'),
	menuVisivel    = false;

btnApareceMenu.addEventListener("click", ()=>{
	modelaMenu()
});

window.addEventListener('click', function(e){
	if (document.getElementById('botao-aparecer-menu').contains(e.target)){
		console.log('clicou no menu');
	}else{
		if(menuVisivel){
			menuVisivel = true;
			modelaMenu();
		}
	}
});

async function modelaMenu(){
	if(!menuVisivel){
		menu.style.maxWidth = '300px';
		menu.style.left     = "0px";
		setTimeout(e => menu.style.overflowY= "auto" , 320)
		
		for(var i of contSubMenu){
			i.style.overflow = 'visible';
			i.style.maxHeight= "400px";
			i.style.maxWidth = "400px";
		}
		menuVisivel = true;
	}else{
		menu.style.maxWidth = '100px';
		menu.style.overflowY= "hidden";
		menu.style.left     = "10px";
		for(var i of contSubMenu){
			i.style.overflow = 'hidden';
			i.style.maxHeight= "0px";
			i.style.maxWidth = "70px";

		}

		menuVisivel = false;
	}
}

async function modelaMenuForMovimento(){
	menuVisivel = false;
}

async function animacaoMenu(botao){
	var	botao1 	   = document.getElementById('botao1'),
	botao2 		   = document.getElementById('botao2'),
	botao3 		   = document.getElementById('botao3'),
	botao4 		   = document.getElementById('botao4'),
	botao4 		   = document.getElementById('botao4'),
	tituloSiclop   = document.getElementById('titulo-siclop'),
	cabecalho      = document.getElementById('cabecalho'),
	conteudoMain   = document.getElementById('conteudoMain'),
	contContMenu   = document.getElementById('container-cont-menu');

	cabecalho.classList.add("d-none");
	tituloSiclop.classList.add("d-none");
	menu.classList.remove("col-md-6");
	menu.classList.remove("col-4");
	menu.classList.remove("offset-md-3");
	menu.classList.remove("offset-4");
	menu.classList.add("col-md-2");
	menu.classList.add("mt-md-0");
	menu.classList.add("menu-fixado");	
	menu.style.margin = '0px';
	menu.style.maxWidth = '100px';
	menu.style.transition = 'all .6s';
	conteudoMain.classList.remove('col-9');
	conteudoMain.classList.add('offset-1','mr-5','d-block');
	btnApareceMenu.classList.add('d-flex');

	//console.log(contSubMenu);
	for(var i of contSubMenu){
		i.classList.add("sub-menu-escondido","position-relative");

	}

	botao1.classList.remove("my-3");
	botao2.classList.remove("my-3");
	botao3.classList.remove("my-3");
	botao4.classList.remove("my-3");
	botao5.classList.remove("my-3");

	botao1.classList.add("col-md-12","ml-2","my-1");
	botao2.classList.add("col-md-12","ml-2","my-1");
	botao3.classList.add("col-md-12","ml-2","my-1");
	botao4.classList.add("col-md-12","ml-2","my-1");
	botao5.classList.add("col-md-12","ml-2","my-1");
	contContMenu.classList.add("container-cont-menu");

	
	if(botao == 1){
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuMovimento`,'SubMenu');
	}else if(botao == 2){
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuCadastros`,'SubMenu');
	}else if(botao == 3){
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuCaixa`,'SubMenu');
	}else if (botao == 4) {
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuEstatisticas`,'SubMenu');
	}else if (botao == 5){
		document.getElementById('conteudoMain').innerHTML = await carregaFrame(`frameSubMenuConfiguracoes`,'SubMenu');
		setTimeout(()=>{
			user.id_nivel === 0 ? document.getElementById('divTpLogCidadeEstado').style.visibility = 'visible' : null;
		},50);
	}

	if(st == 0){
		// conteudoMain.insertAdjacentHTML("afterend", `
		// 	<div class="col-md-1">
		// 		<button id="sair" class="btn-sem-decoracao float-right" onclick="logout()">
		// 			<img class="mt-3 img-saida" src="../img/saida.png" alt="">
		// 			<center class="text-light"><h5>Sair</h5> </center>
		// 		</button>
		// 	</div>
		// `);
	}else{
		modelaMenu();
	}
	
	if(botao){
		conteudoMain.classList.remove("bg-transparent");
		conteudoMain.classList.add("semi-transparente");
	}
	st++;
}

async function voltarSubMenu(botao){
	var	botao1 	   = document.getElementById('botao1'),
	botao2 		   = document.getElementById('botao2'),
	botao3 		   = document.getElementById('botao3'),
	botao4 		   = document.getElementById('botao4'),
	botao4 		   = document.getElementById('botao4'),
	tituloSiclop   = document.getElementById('titulo-siclop'),
	cabecalho      = document.getElementById('cabecalho'),
	conteudoMain   = document.getElementById('conteudoMain'),
	contContMenu   = document.getElementById('container-cont-menu');
	cabecalho.classList.add("d-none");
	tituloSiclop.classList.add("d-none");
	//menu.classList.remove("col-md-6");
	//menu.classList.remove("col-4");
	//menu.classList.remove("offset-md-3");
	//menu.classList.remove("offset-4");
	///menu.classList.add("col-md-2");
	//menu.classList.add("mt-md-0");
	//menu.classList.add("menu-fixado");	
	//menu.style.margin = '0px';
	//menu.style.maxWidth = '100px';
	//menu.style.transition = 'all .6s';
	conteudoMain.classList.remove('col-9');
	conteudoMain.classList.add('offset-1','mr-5','d-block');
	btnApareceMenu.classList.add('d-flex');

	//console.log(contSubMenu);
	for(var i of contSubMenu){
		i.classList.add("sub-menu-escondido","position-relative");

	}

	botao1.classList.remove("my-3");
	botao2.classList.remove("my-3");
	botao3.classList.remove("my-3");
	botao4.classList.remove("my-3");
	botao5.classList.remove("my-3");

	botao1.classList.add("col-md-12","ml-2","my-1");
	botao2.classList.add("col-md-12","ml-2","my-1");
	botao3.classList.add("col-md-12","ml-2","my-1");
	botao4.classList.add("col-md-12","ml-2","my-1");
	botao5.classList.add("col-md-12","ml-2","my-1");
	contContMenu.classList.add("container-cont-menu");

	
	if(botao == 1){
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuMovimento`,'SubMenu');
	}else if(botao == 2){
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuCadastros`,'SubMenu');
	}else if(botao == 3){
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuCaixa`,'SubMenu');
	}else if (botao == 4) {
		document.getElementById('conteudoMain').innerHTML = carregaFrame(`frameSubMenuEstatisticas`,'SubMenu');
	}else if (botao == 5){
		document.getElementById('conteudoMain').innerHTML = await carregaFrame(`frameSubMenuConfiguracoes`,'SubMenu');
		setTimeout(()=>{
			user.id_nivel === 0 ? document.getElementById('divTpLogCidadeEstado').style.visibility = 'visible' : null;
		},50);
	}

	if(st == 0){
		// conteudoMain.insertAdjacentHTML("afterend", `
		// 	<div class="col-md-1">
		// 		<button id="sair" class="btn-sem-decoracao float-right" onclick="logout()">
		// 			<img class="mt-3 img-saida" src="../img/saida.png" alt="">
		// 			<center class="text-light"><h5>Sair</h5> </center>
		// 		</button>
		// 	</div>
		// `);
	}else{
		modelaMenuForMovimento();
	}
	
	if(botao){
		conteudoMain.classList.remove("bg-transparent");
		conteudoMain.classList.add("semi-transparente");
	}
	st++;
}

let cadpess = JSON.parse(window.localStorage.getItem('usuario', '{}'));