var cItemPedido = require(path.join(__dirname + '../../../controller/cItemPedido.js'));

let tipoMenu = window.localStorage.getItem('tipoMenu');

async function irParaCardapioBalcao(tipoOperacao) {
    let produtos = await (require('../../controller/cCadProdutos.js').selectAll());
    if(produtos.length === 0) {
        swal({
            icon: 'warning',
            title: 'Você ainda não cadastrou produtos!',
            closeOnClickOutside:false,
            text: 'Deseja cadastrar agora?',
            buttons:['Não', 'Cadastrar produtos']
        }).then(confirm=>{
            if(confirm === true) carregaFrame(`frameCadProdutos`);
        }).catch(e=>{
            throw e;
        });
        return;
    }
    
    document.getElementById("container-comanda").style.display = "block";

    //nomeCliComanda = document.getElementById('domCli').value;
    //comandaCli = document.getElementById('domComanda').value;

    //console.log(`NOME: ${nomeCliComanda} COMANDA: ${comandaCli}`);

    if (tipoMenu === "0") {
        document.getElementById('menuTrad').style.display = "none";
    }

    colocaHTML(tipoOperacao);
}

function colocaHTML(tipoOperacao) {
    document.getElementById('container-comanda').style.display = "none";
    document.getElementById('container-mesa').style.display = "block";
    let conteudoModPedido = fs.readFileSync(__dirname + '\\..\\html\\PedidoMesa.html', 'utf-8')
    let modalPedidoDelivery = document.getElementById("container-mesa");
    modalPedidoDelivery.innerHTML = conteudoModPedido;
    let adicionaHTML = document.getElementById('InfoPed');

        adicionaHTML.innerHTML = `
        <div style = "display: flex; flex-direction: row; align-items: center; justify-content: center; margin-top: 20px;">
            <p class = "textBalcony">Cliente: </p>
            <input class = "inputBalcao" id="domBalcao"/>
            
            <p class = "textBalcony" style = "margin-left: 20px; padding-right: 20px; font-size: 1.2rem;font-weight: 800;">Telefone: </p>
            <input class = "inputBalcao" id="domTelefone" style="width: 200px; margin-bottom: 15px; height: 50px; font-weight: 800;"/>
        </div>
        `
 
        document.getElementById('domTelefone').addEventListener('keyup', Helper.phoneMaskSemDDD);

    SelecionaProdutos(tipoOperacao, undefined, 'Balcao');
}

module.exports = {
    irParaCardapioBalcao : irParaCardapioBalcao,
}