var $ = require("jquery");

var path = require('path');

window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));

var Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
const CadItensExtras = require(path.join(__dirname + '/../../controller/cCadItensExtras.js'));

var btnSalvar = document.getElementById('cadastrar');
let tpNicho = JSON.parse(window.localStorage.dadosEstabelecimento);

//let dE = document.getElementById("dividirExtra").style.display = 'none';
//let thDE = document.getElementById("thDividir").style.display = 'none';
var MouseTrap = require('mousetrap');
MouseTrap.bind('esc', function () {
    addAlt('fechar');
})

function selecionarTudo(){
	document.getElementById('listaItens').innerHTML = '';
	let CadItensExtras = require(path.join(__dirname + '/../../controller/cCadItensExtras.js'));
	let listaProd = CadItensExtras.selectAll();

	listaProd.then(data => {
		let actuallyData = JSON.parse(JSON.stringify(data));
		for(var i of actuallyData){insereItemNaTela(i,true);}
	});
	
	if(tpNicho.id_nicho != 3){
		//document.getElementById("dividirExtra").style.display = 'none';
		document.getElementById("thDividir").style.display = 'none';
	}

}

function selecionarItem(item){

	console.log(item);
	
	const script = require(path.join(__dirname + '/script.js'));
		//acrescimo = parseFloat(acrescimo.replace('.','').replace(',','.'));
		//script.alterar();
		let divMain = document.getElementById('conteudoMain');		

		let txt = `
		<div id="camadaModal" style="display:block;" class="cria-camada-modal h-100 w-100" >
			<div class="modal-info-imp position-relative col-8 offset-2 rounded pt-3 pb-5 px-5">		
				<div class="header-modal col-12 p-0">
					<button class="btn-sem-decoracao aba-modal abas-info-sis ml-3 w-50">
						<label class="titulo-modal titulo-modal-info-sis
						titulo-modal-info-sis-ativa" id="identificacao-tab"  >
							Dados de Itens Extras
						</label>
					</button>
				</div>
				
				<div class="body-modal py-4 rounded-top">
					<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

						<div class="col-12">
							<div class="row m-0">
								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">*Descrição:</span>
									</div>
									<div class="celula-2 col-6">
										<input id="descricao" value="${item.descricao}" type="text" class="form-control" aria-label="Sizing example input" name="Descrição" aria-describedby="inputGroup-sizing-default" maxlength="15">
									</div>
								</div>

								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">*Acréscimo:</span>
									</div>
									<div class="celula-2 col-6">
										<input id="acrescimo" value="${item.acrescimo}" type="text" class="form-control" aria-label="Sizing example input" name="Acréscimo" aria-describedby="inputGroup-sizing-default" onkeyup="helper.maskMoeda(this,event)">
									</div>
								</div>

								<div class="cont-celulas">
									<div class="celula-1">
										<span class="label-celula">Dividir Pizzas combinadas:</span>
									</div>
									<div class="celula-2 col-6">
										<select id="dividir" class="custom-select">
											<option value="NÃO">NÃO</option>
											<option value="SIM">SIM</option>
										</select>
									</div> 
								</div>

							</div>
						</div>	

						
					</div>

					<div class="row footer-modal m-0">
						<div class="offset-8 col-2 my-2">
							<button class="col-12 btn btn-danger position-relative float-left" onclick="addAlt('fechar',true)">Sair</button>
						</div>
						<div class="col-2 my-2">
							<button id="editar" class="col-12 btn btn-success position-relative float-right">Salvar</button>
						</div>
					</div>
					
				</div>
			</div>
		</div>		
		`;		

		divMain.insertAdjacentHTML('afterbegin', txt);	

		let btnEditar = document.getElementById('editar');
		btnEditar.addEventListener('click', ()=>{salvar(item.id_extra)})

		document.getElementById('descricao').focus();
}

function salvar(id){
	
	let acrescimo = document.getElementById('acrescimo').value + '' ? document.getElementById('acrescimo').value + '' : '0.00';
	acrescimo = parseFloat(acrescimo.replace('.','').replace(',','.'));

	//lista de campos
	CadItensExtras.descricao = document.getElementById('descricao').value.toUpperCase();
	CadItensExtras.acrescimo = acrescimo;
	CadItensExtras.dividir = document.getElementById('dividir').value;

	if(dividir == 'SIM'){
		dividir = 1;
	}else{
		dividir = 0;
	}

	//verifica se campo esta vazio
	var conteudoNotNull = {
		descricao: CadItensExtras.descricao,
		dividir: CadItensExtras.dividir
	};

	let campoFaltando = false;
	let nomeCampoFaltando = "";

	for(var i in conteudoNotNull){
		if(conteudoNotNull[i] === "" || conteudoNotNull[i] == null){
			campoFaltando = true;
			nomeCampoFaltando = document.getElementById(i).name;
			document.getElementById(i).focus();
			break
		}
	}

	conteudoNotNull.acrescimo = CadItensExtras.acrescimo;

	//insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
	if(campoFaltando == false && id == undefined){
		event.preventDefault();
		addAlt('fechar');
		CadItensExtras.insert().then((retorno)=>{insereItemNaTela(retorno)});		
	}else if(campoFaltando == false && id > 0){
		event.preventDefault();
		CadItensExtras.id_extra = id;
		CadItensExtras.update();
		addAlt('fechar',true);
		alteraItemNaTela(id,conteudoNotNull);
	}else if(campoFaltando){
		Helper.alertaCampoVazio(nomeCampoFaltando)
	}
}

function alteraItemNaTela(id,obj){

	let descricao = document.getElementById(`descricao${id}`);
	let acrescimo = document.getElementById(`acrescimo${id}`);
	let divisao = document.getElementById(`divisao${id}`);

	descricao.innerHTML = obj.descricao;
	acrescimo.innerHTML = Helper.valorEmReal(obj.acrescimo);
	divisao.innerHTML = obj.dividir;
}

function insereItemNaTela(retorno,stringify){
	if(stringify != true){
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	let tbody = document.getElementById('listaItens');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_extra}">							
		<th  class="border-right" scope="row">
		<center>
			<button class="btn-sem-decoracao editarItem" id="${retorno.id_extra}" onclick="editarItem(this.id)">
			<img style="margin-left: -12px;" src="../img/pencil.png"/>
			</button>

			<button class="btn-sem-decoracao" id="${retorno.id_extra}" onclick="excluirItem(this.id)">
			<img style="margin-left: -12px;" src="../img/trash.png"/>
			</button>
		</center>
		</th>
		<td class="border-right txt-label" id="descricao${retorno.id_extra}">${retorno.descricao}</td>
		<td class="border-right txt-num" style = "display: flex;" id="acrescimo${retorno.id_extra}">${Helper.valorEmReal(retorno.acrescimo)}</td>
		<td class="border-right txt-label" id="divisao${retorno.id_extra}">${retorno.dividir}</td>
	</tr>`;
	if(tpNicho.id_nicho != 3){
		let dE = document.getElementById("divisao"+retorno.id_extra).style.display = 'none';
	}
}

document.getElementById("thDesc").addEventListener('click', function(){
	selecionaGrupoOrdenado('Descrição');
})

document.getElementById("thAcréscimo").addEventListener('click', function(){
	selecionaGrupoOrdenado('Acréscimo');
})

function selecionaGrupoOrdenado(ordemType){
	document.getElementById('listaItens').innerHTML = "";

	if(ordemType === "Descrição"){
		let CadItensExtras = require(path.join(__dirname + '/../../controller/cCadItensExtras.js'));
		let listaGrupos = CadItensExtras.selectOrderByDesc();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}else if(ordemType === "Acréscimo"){
		let CadItensExtras = require(path.join(__dirname + '/../../controller/cCadItensExtras.js'));
		let listaGrupos = CadItensExtras.selectOrderByAcres();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}
}

btnSalvar.addEventListener('click',()=>{salvar()});

module.exports = {
	selecionarTudo: ()=>{selecionarTudo()},
	selecionarItem: (item)=>{selecionarItem(item)},
	excluirItem: (item)=>{excluirItem(item)},
	insereItemNaTela: (item,status)=>{insereItemNaTela(item,status)}
};
