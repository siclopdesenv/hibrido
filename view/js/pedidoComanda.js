let nomeCliComanda = "";
let comandaCli = "";

var objTamanhos = {
    'nn': '(NORMAL)',
    'br': '(BROTO)',
    'md': '(MÉDIA)',
    'gi': '(GIGANTE)',
}

function buscaPedidoComanda() {
    window.sessionStorage.setItem('idPedidoSelecionadoFechamento', null);
    let adicionaTabela = document.getElementById('adicionarItensPedido');
    let tbody = "";
    let campoComanda = document.getElementById('domComanda');

    let nomeItemForSearch = [];
    let qtdsItensForSearch = [];
    let nomeItemForComparar = [];
    let qtdsItens = [];
    let complementoXml = "";
    let arrayTiposItensExtras = [];
    //let complementoArray = [];

    let primeiroSabor = 0;
    let segundoSabor = 0;
    let terceiroSabor = 0;
    let quartoSabor = 0;
    let itemNormal = 0;
    let controleLabel = 0;
    let entrandoObjSabores = 0;

    if (pedidosTotais !== null && itensDosPedidos !== null) {
        for (let i = 0; i < pedidosTotais.length; i++) {
            if (pedidosTotais[i].numero_mesa === campoComanda.value && pedidosTotais[i].tipo_entrega !== "MESA" && campoComanda.value !== "") {
                document.getElementById('domCli').value = pedidosTotais[i].cliente;
                window.sessionStorage.setItem('idPedidoSelecionadoFechamento', pedidosTotais[i].id_pedido);
                for (let k = 0; k < itensDosPedidos.length; k++) {
                    if (pedidosTotais[i].id_pedido === itensDosPedidos[k].cod_pedido) {

                        if (itensDosPedidos[k].complemento.search('/') > 0) {
                            let itensExtrasArray = itensDosPedidos[k].complemento.split('/');
                            for (j of itensExtrasArray) {
                                let separandoItensEQtds = j.split('x');
                                if (separandoItensEQtds[0] !== "") {
                                    nomeItemForSearch.push(separandoItensEQtds[0]);
                                    qtdsItensForSearch.push(parseInt(separandoItensEQtds[1]));
                                }
                            }
                            nomeItemForComparar.push(nomeItemForSearch);
                            qtdsItens.push(qtdsItensForSearch);
                            nomeItemForSearch = [];
                            qtdsItensForSearch = [];
                        }

                        let complements = nomeItemForComparar[0];

                        for (complementoText in complements) {
                            if (complements[complementoText].search('1') > -1) {
                                primeiroSabor++;
                            } else if (complements[complementoText].search('2') > -1) {
                                segundoSabor++;
                            } else if (complements[complementoText].search('3') > -1) {
                                terceiroSabor++;
                            } else if (complements[complementoText].search('4') > -1) {
                                quartoSabor++;
                            } else if (complements[complementoText].search("-") === -1) {
                                itemNormal++;
                            }
                        }

                        arrayTiposItensExtras.push(primeiroSabor);
                        arrayTiposItensExtras.push(segundoSabor);
                        arrayTiposItensExtras.push(terceiroSabor);
                        arrayTiposItensExtras.push(quartoSabor);
                        arrayTiposItensExtras.push(itemNormal);

                        if (complements !== undefined) {
                            for (montandoItemExtras of arrayTiposItensExtras) {
                                for (let contador = 0; contador < montandoItemExtras; contador++) {
                                    controleLabel = montandoItemExtras;
                                    if (complements[0] !== undefined) {
                                        if (montandoItemExtras >= 1 && complements[0].search('-') > -1) {

                                            complementoXml += `
                                        ${montandoItemExtras === controleLabel ? '<b style = "font-size: 0.7rem">' + ObjSaboresMesas[entrandoObjSabores] + '</b>' : ""}
                                        <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0].substring(2)} x ${qtdsItens[0][0]}</p>
                                    `

                                            complements.splice(0, 1);
                                            qtdsItens[0].splice(0, 1);

                                            controleLabel--;
                                        } else {

                                            if (montandoItemExtras >= 1) {
                                                complementoXml += `
                                            <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0]} x ${qtdsItens[0][0]}</p>
                                        `

                                                complements.splice(0, 1);
                                                qtdsItens[0].splice(0, 1);
                                                controleLabel--;
                                            }

                                        }
                                    }

                                    entrandoObjSabores++;
                                }
                            }
                        }

                        tbody += `
                            <tr id = "tr-mesa-${i}" style = "height: 40px;">
                                <td style = "text-align: center;" id = "table-cell">${itensDosPedidos[k].qtd_prod}</td>
                                <td style = "text-align: left; font-size: 0.8rem;" id = "table-cell">${itensDosPedidos[k].desc_produto} <p style = "font-weight: 800; font-size: 0.7rem; margin-bottom: 0px">${objTamanhos[itensDosPedidos[k].tam_escolhido] !== "(NORMAL)" ? "Tamanho: " + objTamanhos[itensDosPedidos[k].tam_escolhido] : ''}</p> ${complementoXml}</td>
                                <td id = "table-cell">${Helper.valorEmReal((itensDosPedidos[k].vl_unit + itensDosPedidos[k].acrescimo) * itensDosPedidos[k].qtd_prod)}</td>
                            </tr>
                         `

                        //valorTotal = valorTotal + ((itensDosPedidos[k].vl_unit + itensDosPedidos[k].acrescimo) * itensDosPedidos[k].qtd_prod);

                        complementoXml = "";
                        nomeItemForComparar = [];
                        qtdsItens = [];
                        arrayTiposItensExtras = [];

                    }

                }
            }
        }
    }

    adicionaTabela.innerHTML = tbody;
}

async function irParaCardapioComanda(tipoOperacao) {
    let produtos = await (require('../../controller/cCadProdutos.js').selectAll());
    if (produtos.length === 0) {
        swal({
            icon: 'warning',
            title: 'Você ainda não cadastrou produtos!',
            closeOnClickOutside: false,
            text: 'Deseja cadastrar agora?',
            buttons: ['Não', 'Cadastrar produtos']
        }).then(confirm => {
            if (confirm === true) carregaFrame(`frameCadProdutos`);
        }).catch(e => {
            throw e;
        });
        return;
    }
    document.getElementById("container-comanda").style.display = "block";

    nomeCliComanda = document.getElementById('domCli').value;
    comandaCli = document.getElementById('domComanda').value;
    if (pedidosTotais !== null && itensDosPedidos !== null) {
        for (i of pedidosTotais) {
            if (i.numero_mesa === comandaCli && i.tipo_entrega === "COMANDA") {
                window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                for (k of itensDosPedidos) {
                    if (i.id_pedido === k.cod_pedido) {
                        itensDosPedidos = [];
                        arrayDosItensPedidos.push(k);
                    }
                }
            }
        }
    }

    if (tipoMenu === "0") {
        document.getElementById('menuTrad').style.display = "none";
    }

    colocaHTML(tipoOperacao);
}

function colocaHTML(tipoOperacao) {
    document.getElementById('container-comanda').style.display = "none";
    document.getElementById('container-mesa').style.display = "block";
    let conteudoModPedido = fs.readFileSync(__dirname + '\\..\\html\\PedidoMesa.html', 'utf-8')
    let modalPedidoDelivery = document.getElementById("container-mesa");
    modalPedidoDelivery.innerHTML = conteudoModPedido;
    let adicionaHTML = document.getElementById('InfoPed');

    setTimeout(() => {
        adicionaHTML.innerHTML = `
        <div>
            <div>COMANDA: <b id="comandaCli">${comandaCli}</b></div>
            <div>NOME: <b id="clienteComandaCli">${nomeCliComanda.toUpperCase()}</b></div>
        </div>        
        `
    }, 100);


    SelecionaProdutos(tipoOperacao, undefined, 'Comanda');
}

function cancelarPedidoComanda(tipoEntrega, tipoId, telaAtual) {
    let numeroMesa = "";

    if(tipoEntrega === 'COMANDA'){
        tipoId === undefined ? numeroMesa = document.getElementById('domComanda').value : numeroMesa = tipoId;
        for (pedido of pedidosTotais) {
            if (numeroMesa === pedido.numero_mesa) {
                numeroMesa = pedido.id_pedido;
            }
        }
    }else{
        tipoId === undefined ? numeroMesa = document.getElementById('domMesa').value : numeroMesa = tipoId;
    }
    
    if (pedidosTotais !== null) {
        for (pedidosCancel of pedidosTotais) {
            if(tipoEntrega === 'MESA') {
                if (String(numeroMesa) === pedidosCancel.numero_mesa) { confirmaCancelamento(pedidosCancel.id_pedido, telaAtual); break; }
            } else {
                if (numeroMesa === pedidosCancel.id_pedido) { confirmaCancelamento(pedidosCancel.id_pedido, telaAtual); break; }
            }
        }
    }
}

function confirmaCancelamento(idPedido = null, telaAtual = ''){
    swal('Tem certeza que deseja excluir ?', {
        icon: "warning",
        buttons: ['SIM','NÃO']
    }).then((acao) => {
        if (acao !== true) {
            cCadPedido.numero_mesa = "CANCELADO";
            cCadPedido.status_pedido = "CANCELADO";
            cCadPedido.id_pedido = idPedido;

            

            cCadPedido.update().then(retorno => {
                if (telaAtual === "Reserva") {
                    window.sessionStorage.removeItem('pedidosTotais');
                    window.sessionStorage.removeItem('itensDosPedidos');
                    arrayMesas = [];
                    pedidosTotais = [];
                    itensDosPedidos = [];

                    document.getElementById('buttons-container').innerHTML = "";
                    document.getElementById('adiciona-pedido').innerHTML = "";
                    document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                    document.getElementById('valorTotalMesaBtns').innerHTML = "";
                    document.getElementById('domCliente').value = "";
                    document.getElementById('domMesa').value = "";

                    selecionarTudo();

                } else if (telaAtual === "Ultimo") {
                    window.sessionStorage.removeItem('pedidosTotais');
                    window.sessionStorage.removeItem('itensDosPedidos');
                    arrayMesas = [];
                    pedidosTotais = [];
                    itensDosPedidos = [];

                    document.getElementById('listaPedidos').innerHTML = "";

                    refazerTabela();
                } else if (telaAtual === 'Comanda'){
                    document.getElementById('domComanda').value = '';
                    document.getElementById('domCli').value = '';
                    document.getElementById('adicionarItensPedido').innerHTML = '';
                }

                swal({
                    icon: 'success',
                    text: 'Pedido cancelado!',
                    button: 'OK'
                })

            }).catch(e => {
                swal({
                    icon: 'error',
                    text: e,
                    button: 'OK'
                })
            })
        }
    });
    
}

module.exports = {
    irParaCardapioComanda: irParaCardapioComanda,
    buscaPedidoComanda: buscaPedidoComanda,
    cancelarPedidoComanda: cancelarPedidoComanda
}