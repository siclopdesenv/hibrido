//#region Importações de bibliotecas e arquivos JS
var path = require('path');
var Swel = require('sweetalert2');
var Mousetrap = require('mousetrap');
const { parse } = require('path');
const { default: Swal } = require('sweetalert2');
var cINFORSIS = require(path.join(__dirname + '../../../controller/cINFORSIS.js'));
var cItemPedido = require(path.join(__dirname + '../../../controller/cItemPedido.js'));
var cCadEndereco = require(path.join(__dirname + '../../../controller/cCadEnderecos.js'))
const { refazerTabela } = require(path.join(__dirname + '/../js/SubMenuUltimo.js'));

//#endregion


//#region Variaveis de ambiente

let screenWidth = window.innerWidth;
let screenHeight = window.innerHeight;
let itemDosPedidosMesa = [];
let pedidosTotais = [];
let itensDosPedidos = [];
let arrayMesas = [];
let arrayDosItensPedidos = [];
let statusMesas = "";
let nomeCli = "";
let mesaCli = "";
let tipoMenu = window.localStorage.getItem('tipoMenu');



let ObjSaboresMesas = {
    0: 'PRIMEIRO SABOR',
    1: 'SEGUNDO SABOR',
    2: 'TERCEIRO SABOR',
    3: 'QUARTO SABOR',
}

var objTamanhos = {
    'nn': '(NORMAL)',
    'br': 'BROTO',
    'md': 'MÉDIA',
    'gi': 'GIGANTE',
}

//#endregion

function zerarVariaveis() {
    pedidos = {};

    /*pedido = {
        produtos: [],
        itex: [],
        valores: [],
        valoresCalculados: [],
        qtds: [],
    };*/

    codPedido = 0;
    pedidoMesa = {};
    pedidosTotais = [];
    arrayDosItensPedidos = [];
    itemDosPedidosMesa = [];
    itensDosPedidos = [];
    arrayMesas = [];
    window.sessionStorage.removeItem('pedidoMesa');
    window.sessionStorage.removeItem('itensDosPedidos');
    window.sessionStorage.removeItem('pedidosTotais');
    window.sessionStorage.removeItem('itemDoPedidoMesa');
}

window.onload = function () {
    selecionaPedidos();
}

window.onresize = function () {
    if (screenWidth <= 1024) {
        estiloResponsivoParaElementosDeMesa(screenWidth);
    } else if (screenWidth >= 1024) {
        estiloResponsivoParaElementosDeMesa(screenWidth);
    }
}

async function selecionarTudo() {
    window.sessionStorage.removeItem('itensDosPedidos');
    window.sessionStorage.removeItem('itemDoPedidoMesa');
    window.sessionStorage.removeItem('pedidoMesa');
    window.sessionStorage.removeItem('pedidosTotais');

    document.getElementById('buttons-container').innerHTML;

    pedidosTotais = [];
    itensDosPedidos = [];

    let selectINFORSIS = cINFORSIS.selectAll();
    selectINFORSIS.then(data => {
        let actuallyData = JSON.parse(JSON.stringify(data));
        selecionaPedidos();
        setTimeout(() => {
            criarMesasNaTela(actuallyData);
        }, 500);
    });

    estiloResponsivoParaElementosDeMesa(screenHeight);
}

async function selecionaPedidos() {
    let pedidos = cCadPedido.selectAll();
    await pedidos.then(data => {
        let pedidoData = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < pedidoData.length; i++) {
            if(pedidoData[i].status_pedido !== "RECEBIDO" && pedidoData[i].status_pedido !== "CANCELADO"){
                pedidosTotais.push(pedidoData[i]);
                window.sessionStorage.setItem('pedidosTotais', JSON.stringify(pedidosTotais))
            }
        }
    });
    selecionaItensDoPedido();
}

async function selecionaItensDoPedido() {
    let itensPedido = cItemPedido.selectProdutosItens();
    await itensPedido.then(data => {
        let itensPedidoData = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < itensPedidoData.length; i++) {
            itensDosPedidos.push(itensPedidoData[i]);
            window.sessionStorage.setItem('itensDosPedidos', JSON.stringify(itensDosPedidos));
        }
    });
    // console.log(pedidosTotais);
}

var objStatusMesasCarregadas = [];

async function criarMesasNaTela(retornoQuery) {
    let buttonsContainer = document.getElementById('buttons-container');
    let adicionaROW = "";
    let valorDivido = 0;

    pedidosTotais = JSON.parse(window.sessionStorage.getItem('pedidosTotais'));
    itensDosPedidos = JSON.parse(window.sessionStorage.getItem('itensDosPedidos'));

    if (pedidosTotais !== null) {
        for (let i = 0; i < pedidosTotais.length; i++) {
            if (pedidosTotais[i].status_pedido === "OCUPADO") {
                arrayMesas[pedidosTotais[i].numero_mesa] = "OCUPADO";
            } else if (pedidosTotais[i].status_pedido === "FECHAMENTO") {
                arrayMesas[pedidosTotais[i].numero_mesa] = "FECHAMENTO";
            }
        }
    }

    // console.log(arrayMesas);
    let qtdMesas = retornoQuery["0"].qde_mesas;
    window.sessionStorage.setItem('numTotalMesasBuscadas',qtdMesas);
    valorDivido = retornoQuery["0"].qde_mesas / 5;
    valorDivido = Math.ceil(valorDivido)


    let contLinha = 0;
    
    let classeBtnMesaPorStatus =  {
        ocupado: 'btnMesaOcupada',
        fechamento: 'btnMesaFechamento',
        livre: 'btnMesaLivre'
    }

    for (let i = 0; i <= qtdMesas; i++) {
        contLinha++;
        if (contLinha === 1) {
            adicionaROW = `<div class = 'container-row-buttons' id = 'container-row-buttons-${i}'>`;
        }

        //const found = arrayMesas.find(element => element == i);
        const found = arrayMesas[i + 1];

        let statusMesaAtual = found !== undefined ? found.toLowerCase() : 'livre';
        objStatusMesasCarregadas[i] = statusMesaAtual; //Sendo preenchido aqui para ser consultado posteriormente

        const htmlBotaoMesa = `<button id = "${statusMesaAtual}" class="btnMesa ${classeBtnMesaPorStatus[statusMesaAtual]}" onclick = "selecionaMesa(${i + 1}, '${statusMesaAtual}');">${i + 1}</button>`;
        adicionaROW += htmlBotaoMesa;

        if (contLinha === 5 || i === (qtdMesas - 1)) {
            adicionaROW += '</div>';
            buttonsContainer.innerHTML += adicionaROW;
            contLinha = 0;
        }
    }
}

let idsBotoesOperação = [
    'botoes-reserva-add',
    'botoes-reserva-fec',
    'botoes-reserva-rec',
    'botoes-reserva-edi',
    'botoes-reserva-can',
]

function selecionaMesa(numMesa, statusMesa = 'livre') {

    window.sessionStorage.setItem('idPedidoSelecionadoFechamento', null);
    document.getElementById('domMesa').value = numMesa;
    if(statusMesa === 'livre' || statusMesa === undefined) {
        document.getElementById('domCliente').value = '';
        for(id of idsBotoesOperação) id !== 'botoes-reserva-add' ? document.getElementById(id).disabled = true : null; 

    }else for(id of idsBotoesOperação) document.getElementById(id).disabled = false;

    let tablePedido = document.getElementById('adiciona-pedido');
    statusMesas = statusMesa;
    let valorTotal = 0;

    let nomeItemForComparar = [];
    let qtdsItens = [];
    let nomeItemForSearch = [];
    let qtdsItensForSearch = [];
    let arrayTiposItensExtras = [];
    let complementoXml = "";

    let primeiroSabor = 0;
    let segundoSabor = 0;
    let terceiroSabor = 0;
    let quartoSabor = 0;
    let itemNormal = 0;
    let controleLabel = 0;
    let entrandoObjSabores = 0;


    pedido = {
        produtos: [],
        itex: [],
        valores: [],
        valoresCalculados: [],
        qtds: [],
    };


    pedidosTotais = JSON.parse(window.sessionStorage.getItem('pedidosTotais'));
    itensDosPedidos = JSON.parse(window.sessionStorage.getItem('itensDosPedidos'));

    let body = "";

    if(numMesa === ''){
        valorTotal = 0;

        complementoXml = "";
        nomeItemForComparar = [];
        qtdsItens = [];
        arrayTiposItensExtras = [];
        tablePedido.innerHTML = body;
        valorTotalSomadoMesaBtns(valorTotal, valorTotal);
        //Colocar opacidade nos botões de opção
        //e desativar botões
        return;
    }

    let valorPagar = 0;
    
    if (pedidosTotais !== null && itensDosPedidos !== null) {
        for (let i = 0; i < pedidosTotais.length; i++) {
            if (parseInt(pedidosTotais[i].numero_mesa) === numMesa && pedidosTotais[i].tipo_entrega === "MESA") {
                document.getElementById('domCliente').value = pedidosTotais[i].cliente;
                window.sessionStorage.setItem('idPedidoSelecionadoFechamento', pedidosTotais[i].id_pedido);

                valorPagar = pedidosTotais[i].vl_total;

                for (let k = 0; k < itensDosPedidos.length; k++) {

                    if (pedidosTotais[i].id_pedido === itensDosPedidos[k].cod_pedido) {

                        if (itensDosPedidos[k].complemento.search('/') > 0) {
                            let itensExtrasArray = itensDosPedidos[k].complemento.split('/');
                            for (j of itensExtrasArray) {
                                let separandoItensEQtds = j.split('x');
                                if (separandoItensEQtds[0] !== "") {
                                    nomeItemForSearch.push(separandoItensEQtds[0]);
                                    qtdsItensForSearch.push(parseInt(separandoItensEQtds[1]));
                                }
                            }
                            nomeItemForComparar.push(nomeItemForSearch);
                            qtdsItens.push(qtdsItensForSearch);
                            nomeItemForSearch = [];
                            qtdsItensForSearch = [];
                        }

                        let complements = nomeItemForComparar[0];

                        for (complementoText in complements) {
                            if (complements[complementoText].search('1') > -1) {
                                primeiroSabor++;
                            } else if (complements[complementoText].search('2') > -1) {
                                segundoSabor++;
                            } else if (complements[complementoText].search('3') > -1) {
                                terceiroSabor++;
                            } else if (complements[complementoText].search('4') > -1) {
                                quartoSabor++;
                            } else if (complements[complementoText].search("-") === -1) {
                                itemNormal++;
                            }
                        }

                        arrayTiposItensExtras.push(primeiroSabor);
                        arrayTiposItensExtras.push(segundoSabor);
                        arrayTiposItensExtras.push(terceiroSabor);
                        arrayTiposItensExtras.push(quartoSabor);
                        arrayTiposItensExtras.push(itemNormal);

                        if (complements !== undefined) {
                            for (montandoItemExtras of arrayTiposItensExtras) {
                                for (let contador = 0; contador < montandoItemExtras; contador++) {
                                    controleLabel = montandoItemExtras;
                                    if (complements[0] !== undefined) {
                                        if (montandoItemExtras >= 1 && complements[0].search('-') > -1) {

                                            complementoXml += `
                                            ${montandoItemExtras === controleLabel ? '<b style = "font-size: 0.7rem">' + ObjSaboresMesas[entrandoObjSabores] + '</b>' : ""}
                                            <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0].substring(2)} x ${qtdsItens[0][0]}</p>
                                        `

                                            complements.splice(0, 1);
                                            qtdsItens[0].splice(0, 1);

                                            controleLabel--;
                                        } else {

                                            if (montandoItemExtras >= 1) {
                                                complementoXml += `
                                                <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0]} x ${qtdsItens[0][0]}</p>
                                            `

                                                complements.splice(0, 1);
                                                qtdsItens[0].splice(0, 1);
                                                controleLabel--;
                                            }

                                        }
                                    }
                                }

                                entrandoObjSabores++;
                            }

                        }

                        body += `
                            <tr id = "tr-mesa-${i}">
                                <td style = "text-align: center;" id = "table-cell">${itensDosPedidos[k].qtd_prod}</td>
                                <td style = "font-size: 0.7rem;" id = "table-cell">${itensDosPedidos[k].desc_produto} <p style = "font-weight: 800; font-size: 0.7rem; margin-bottom: 0px">${objTamanhos[itensDosPedidos[k].tam_escolhido] !== "(NORMAL)" ? "Tamanho: " + objTamanhos[itensDosPedidos[k].tam_escolhido] : ''}</p> ${complementoXml}</td>
                                <td id = "table-cell">${Helper.valorEmReal((itensDosPedidos[k].vl_unit + itensDosPedidos[k].acrescimo) * itensDosPedidos[k].qtd_prod)}</td>
                            </tr>
                         `

                        valorTotal = valorTotal + ((itensDosPedidos[k].vl_unit + itensDosPedidos[k].acrescimo) * itensDosPedidos[k].qtd_prod);

                        complementoXml = "";
                        nomeItemForComparar = [];
                        qtdsItens = [];
                        arrayTiposItensExtras = [];

                    }
                }
                break;
            }
        }
    }

    tablePedido.innerHTML = body;
    valorTotalSomadoMesaBtns(valorTotal, valorPagar);
}


function listenerCampoNumMesa(elementDomMesa, event){
    event.preventDefault();
    let numMesaDigitado = elementDomMesa.value;
    console.log('MUDOU', parseInt(numMesaDigitado));
    console.log(numMesaDigitado);
    if(isNaN(parseInt(numMesaDigitado))){
        selecionaMesa('');
        return;
    }
    if(numMesaDigitado.length > 3){
        elementDomMesa.value = numMesaDigitado.substring(0,3);
        return;
    }
    numMesaDigitado = parseInt(numMesaDigitado);
    if(numMesaDigitado < 1) {
        elementDomMesa.value = '';
        return
    }
    let qtdTotalMesas = parseInt(window.sessionStorage.getItem('numTotalMesasBuscadas'));
    if(numMesaDigitado > qtdTotalMesas) numMesaDigitado = qtdTotalMesas;
    
    selecionaMesa(numMesaDigitado);
}

function validaCamposAntesDeProsseguir(nBotao){
    if(document.getElementById('domMesa').value !== '') {
        switch(nBotao){
            case 1: irParaCardapio('Adicionar', 'Reserva', undefined, 'MESA'); break;
            case 2: irParaFechamento('Fechamento', 'Reserva'); break;
            case 3: irParaRecebimento('MESA'); break;
            case 4: irParaCardapio('Editar', 'Reserva'); break;
            case 5: cancelarPedido('MESA', undefined, 'Reserva'); break;
        }
    }
    else document.getElementById('domMesa').focus();
}


function validaAntesDeProsseguirComanda(nBotao = 0){
    const domComandaValidacao = document.getElementById('domComanda')
    if(domComandaValidacao.value === '') {
        domComandaValidacao.focus();
        return;
    }

    if(nBotao === 1){
        irParaCardapio('Adicionar', 'Comanda', undefined, 'Comanda');
        return;
    }

    if(document.getElementById('adicionarItensPedido').outerHTML !== '<tbody id="adicionarItensPedido"></tbody>') {
        switch(nBotao){
            case 2: irParaFechamento('Fechamento', 'Comanda'); break;
            case 3: irParaRecebimento('COMANDA'); break;
            case 4: irParaCardapio('Editar', 'Comanda', undefined, 'Comanda'); break;
            case 5: cancelarPedidoComanda('COMANDA', undefined, 'Comanda'); break;
        }
    }
    else document.getElementById('domComanda').focus();
}


async function irParaCardapio(tipoOperacao, telaAtual, idPedido, tipoPedido) {

    let produtos = await (require('../../controller/cCadProdutos.js').selectAll());
    if(produtos.length === 0) {
        swal({
            icon: 'warning',
            title: 'Você ainda não cadastrou produtos!',
            closeOnClickOutside:false,
            text: 'Deseja cadastrar agora?',
            buttons:['Não', 'Cadastrar produtos']
        }).then(confirm=>{
            if(confirm === true) carregaFrame(`frameCadProdutos`);
        }).catch(e=>{
            throw e;
        });
        return;
    }
    
    if(tipoPedido === 'ENTREGA') window.localStorage.setItem('cobrarTaxaEntregaPedido', 'true');
    else window.localStorage.setItem('cobrarTaxaEntregaPedido', 'false');
    pedidoMesa = window.sessionStorage.getItem('pedidoMesa');
    if (tipoOperacao === "VoltandoFechamento") {
        if (tipoPedido === "Mesa") {
            document.getElementById("container-mesa").style.display = "block";
            console.log(nomeCli);
            console.log(mesaCli);

            arrayDosItensPedidos = [];

            if (pedidosTotais !== null && itensDosPedidos !== null) {
                for (i of pedidosTotais) {
                    if (i.numero_mesa === mesaCli) {
                        window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                        for (k of itensDosPedidos) {
                            if (i.id_pedido === k.cod_pedido) {
                                arrayDosItensPedidos.push(k);
                                itensDosPedidos = [];
                            }
                        }
                    }
                }
            }

            window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));
        } else if (tipoPedido === "Comanda") {
            document.getElementById("container-mesa").style.display = "block";

            console.log(nomeCli);
            console.log(mesaCli);

            arrayDosItensPedidos = [];
            if (pedidosTotais !== null && itensDosPedidos !== null) {
                for (i of pedidosTotais) {
                    if (i.numero_mesa === mesaCli) {
                        window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                        for (k of itensDosPedidos) {
                            if (i.id_pedido === k.cod_pedido) {
                                arrayDosItensPedidos.push(k);
                                itensDosPedidos = [];
                            }
                        }
                    }
                }
            }

            window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));
        }
        // console.log(`NOME: ${nomeCli} MESA: ${mesaCli}`);
    } else if (telaAtual === 'Ultimo') {
        document.getElementById("container-mesa").style.display = "block";
        /*nomeCli = document.getElementById('domCliente').value;
        mesaCli = document.getElementById('domMesa').value;*/

        arrayDosItensPedidos = [];

        //pedidosTotais.push(JSON.parse(window.sessionStorage.getItem('pedidosTotais')))
        //itensDosPedidos.push(JSON.parse(window.sessionStorage.getItem('itensDosPedidos')));

        if ((pedidosTotais !== null && itensDosPedidos !== null) && (pedidosTotais.length !== 0 && itensDosPedidos.length !== 0)) {
            for (i of pedidosTotais) {
                if (idPedido === i.id_pedido) {
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            itensDosPedidos = [];
                        }
                    }
                }
            }
        } else {
            for (pedtot of JSON.parse(window.sessionStorage.getItem('pedidosTotais'))) {
                pedidosTotais.push(pedtot);
            }

            for (itPedTot of JSON.parse(window.sessionStorage.getItem('itensDosPedidos'))) {
                itensDosPedidos.push(itPedTot);
            }

            for (i of pedidosTotais) {
                if (idPedido === i.id_pedido) {
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            itensDosPedidos = [];
                        }
                    }
                }
            }
        }

        window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

        // console.log(`NOME: ${nomeCli} MESA: ${mesaCli}`);

        if (tipoMenu === "0") {
            document.getElementById('menuTrad').style.display = "none";
        }
    } else if (telaAtual === "Reserva") {

        document.getElementById("container-mesa").style.display = "block";
        nomeCli = document.getElementById('domCliente').value.toUpperCase();
        mesaCli = document.getElementById('domMesa').value.toUpperCase();
        arrayDosItensPedidos = [];

        if (pedidosTotais !== null && itensDosPedidos !== null) {
            for (i of pedidosTotais) {
                if (i.numero_mesa === mesaCli && i.tipo_entrega === "MESA") {
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            itensDosPedidos = [];
                        }
                    }
                }
            }
        }

        window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

        // console.log(`NOME: ${nomeCli} MESA: ${mesaCli}`);

        if (tipoMenu === "0") {
            document.getElementById('menuTrad').style.display = "none";
        }

    } else if (telaAtual === "Comanda") {

        document.getElementById("container-mesa").style.display = "block";
        nomeCli = document.getElementById('domCli').value.toUpperCase();
        mesaCli = document.getElementById('domComanda').value.toUpperCase();

        arrayDosItensPedidos = [];


        if (pedidosTotais !== null && itensDosPedidos !== null) {
            for (i of pedidosTotais) {
                if (i.numero_mesa === mesaCli && i.tipo_entrega === "COMANDA") {
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            itensDosPedidos = [];
                        }
                    }
                }
            }
        }

        window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

        // console.log(`NOME: ${nomeCli} MESA: ${mesaCli}`);

        if (tipoMenu === "0") {
            document.getElementById('menuTrad').style.display = "none";
        }

    } else if (telaAtual === "Fechamento") {
        if (tipoOperacao === "Editar") {
            if (tipoPedido != 'Comanda') {
                document.getElementById("container-mesa").style.display = "block";
                nomeCli = document.getElementById('domCliente').value.toUpperCase();
                mesaCli = document.getElementById('domMesa').value.toUpperCase();

                arrayDosItensPedidos = [];
                itensDosPedidos = JSON.parse(window.sessionStorage.getItem('itensDosPedidos'));

                if (pedidosTotais !== null && itensDosPedidos !== null) {
                    for (i of pedidosTotais) {
                        if (i.numero_mesa === mesaCli) {
                            window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                            for (k of itensDosPedidos) {
                                if (i.id_pedido === k.cod_pedido) {
                                    arrayDosItensPedidos.push(k);
                                    itensDosPedidos = [];
                                }
                            }
                        }
                    }
                }

                window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));
            } else {

                document.getElementById("container-mesa").style.display = "block";
                nomeCli = document.getElementById('domClienteFechamento').value.toUpperCase();
                mesaCli = document.getElementById('domComandaFechamento').value.toUpperCase();

                arrayDosItensPedidos = [];
                itensDosPedidos = JSON.parse(window.sessionStorage.getItem('itensDosPedidos'));

                if (pedidosTotais !== null && itensDosPedidos !== null) {
                    for (i of pedidosTotais) {
                        if (i.numero_mesa === mesaCli) {
                            window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                            for (k of itensDosPedidos) {
                                if (i.id_pedido === k.cod_pedido) {
                                    arrayDosItensPedidos.push(k);
                                    itensDosPedidos = [];
                                }
                            }
                        }
                    }
                }

                window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));


            }


        } else if (tipoOperacao === "Adicionar") {
            if (telaAtual === "Reserva") {
                document.getElementById("container-mesa").style.display = "block";
                nomeCli = document.getElementById('domCliente').value.toUpperCase();
                mesaCli = document.getElementById('domMesa').value.toUpperCase();

                itemDosPedidosMesa = JSON.parse(window.sessionStorage.getItem("itemDoPedidoMesa"));
                arrayDosItensPedidos = [];

                if (pedidosTotais !== null && itensDosPedidos !== null) {
                    for (i of pedidosTotais) {
                        if (i.numero_mesa === mesaCli) {
                            window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                            arrayDosItensPedidos.push(itemDosPedidosMesa);
                        }
                    }
                }

                window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

                // console.log(`NOME: ${nomeCli} MESA: ${mesaCli}`);
            } else if (tipoPedido === "Comanda") {
                document.getElementById("container-mesa").style.display = "block";
                //document.getElementById('container-mesa').innerHTML = "";
                nomeCli = document.getElementById('domClienteFechamento').value.toUpperCase();
                mesaCli = document.getElementById('domComandaFechamento').value.toUpperCase();

                itemDosPedidosMesa = JSON.parse(window.sessionStorage.getItem("itemDoPedidoMesa"));
                arrayDosItensPedidos = [];

                if (pedidosTotais !== null && itensDosPedidos !== null) {
                    for (i of pedidosTotais) {
                        if (i.numero_mesa === mesaCli) {
                            window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                            arrayDosItensPedidos.push(itemDosPedidosMesa);
                        }
                    }
                }

                window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

            }
        }

        if (tipoMenu === "0") {
            document.getElementById('menuTrad').style.display = "none";
        }
    } else if (telaAtual === "Balcão") {
        if (tipoOperacao === "adicionar") {
            if (tipoMenu === "0") {
                document.getElementById('menuTrad').style.display = "none";
            }
        }
    }

    colocaHTML(tipoOperacao, telaAtual, tipoPedido);
}

async function irParaFechamento(tipoOperacao, telaAtual, numDom) {
    debugger
    if (telaAtual === "Reserva" || telaAtual === 'Comanda') {
        document.getElementById("container-mesa").style.display = "block";
        
        if(telaAtual === 'Reserva'){
            nomeCli = document.getElementById('domCliente').value;
            mesaCli = document.getElementById('domMesa').value;
        }else if(telaAtual === 'Comanda'){
            nomeCli = document.getElementById('domCli').value;
            mesaCli = document.getElementById('domComanda').value;
        }
    
        arrayDosItensPedidos = [];
    
        if (pedidosTotais !== null) {
            let pedidoSelecionado = await cCadPedido.buscaPedidoSelecionado(mesaCli, telaAtual === 'Reserva' ? 'MESA' : 'COMANDA', "CANCELADO");
            window.sessionStorage.setItem('pedidoMesa', JSON.stringify(pedidoSelecionado));
            if(pedidoSelecionado){
                const idMesaSelecionado = pedidoSelecionado.id_pedido;
                for (k of itensDosPedidos) {
                    if (idMesaSelecionado === k.cod_pedido) {
                        arrayDosItensPedidos.push(k);
                        itensDosPedidos = [];
                    }
                }
            }else return
        }
    
        window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));
    
        // console.log(`NOME: ${nomeCli} MESA: ${mesaCli}`);
    
        if (tipoMenu === "0") {
            document.getElementById('menuTrad').style.display = "none";
        }
    
    }else if(telaAtual === 'Ultimo'){
        //Ver como preencher esse elemento do container mesa
        let nomeItemForComparar = [];
        let qtdsItens = [];
        let nomeItemForSearch = [];
        let qtdsItensForSearch = [];
        let complementoArray = [];
        let arrayTiposItensExtras = [];
        let complementoXml = "";

        let primeiroSabor = 0;
        let segundoSabor = 0;
        let terceiroSabor = 0;
        let quartoSabor = 0;
        let itemNormal = 0;
        let controleLabel = 0;
        let entrandoObjSabores = 0;

        let conteudoModPedido = fs.readFileSync(__dirname + '\\visualizaPedido.html', 'utf-8')
        document.getElementById("container-mesa").style.display = "block";
        document.getElementById("container-mesa").innerHTML += conteudoModPedido;

        // nomeCli = document.getElementById('nome' + numDom).value;
        // mesaCli = document.getElementById('cod' + numDom).value;
        document.getElementById('box-fechamento').children[0].innerHTML = 'Visualização do Pedido';
        console.log(document.getElementsByClassName('container-row-input'));

        nomeCli = document.getElementById('nome' + numDom).innerHTML;
        mesaCli = numDom;

        arrayDosItensPedidos = [];

        if (pedidosTotais !== null) {
            for (i of pedidosTotais) {
                if (i.id_pedido === mesaCli && i.status_pedido !== "CANCELADO") {
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    document.getElementById('nPedido').value = i.nr_pedido;
                    document.getElementById('nomeCli').value = i.cliente;
                    document.getElementById('divFreteFechamento').style.display = 'none';
                    document.getElementById('divInfoLogradouro').style.display = 'none';
                
                    if(i.tipo_entrega === 'ENTREGA'){
                        const endereco = await cCadEndereco.select(i.id_logradouro);
                        document.getElementById('divInfoLogradouro').style.display = 'flex';
                        document.getElementById('nomeRuaEntrega').value = endereco.tba_Tp_Logr.descricao + ' ' + endereco.rua;
                        document.getElementById('nRuaEntrega').value = i.num_endereco;
                        document.getElementById('divFreteFechamento').style.display = 'block';
                        document.getElementById('input-frete').value = i.frete.toFixed(2).toString().replace(',','').replace('.',',');
                    }

                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            //itensDosPedidos = [];
                        }
                    }

                    pedidoMesa = JSON.parse(JSON.stringify(i));
                    break;
                }
            }
        }

        window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

        let tbody = "";
        let conteudoHTML = document.getElementById('itensFechamento')
        let valorTotal = 0;

        for (itensMesa in arrayDosItensPedidos) {

            if (arrayDosItensPedidos[itensMesa].complemento.search('/') > 0) {
                let itensExtrasArray = arrayDosItensPedidos[itensMesa].complemento.split('/');
                for (j of itensExtrasArray) {
                    let separandoItensEQtds = j.split('x');
                    if (separandoItensEQtds[0] !== "") {
                        nomeItemForSearch.push(separandoItensEQtds[0]);
                        qtdsItensForSearch.push(parseInt(separandoItensEQtds[1]));
                    }
                }
                nomeItemForComparar.push(nomeItemForSearch);
                qtdsItens.push(qtdsItensForSearch);
                nomeItemForSearch = [];
                qtdsItensForSearch = [];
            }

            let complements = nomeItemForComparar[0];

            for (complementoText in complements) {
                if (complements[complementoText].search('1') > -1) {
                    primeiroSabor++;
                } else if (complements[complementoText].search('2') > -1) {
                    segundoSabor++;
                } else if (complements[complementoText].search('3') > -1) {
                    terceiroSabor++;
                } else if (complements[complementoText].search('4') > -1) {
                    quartoSabor++;
                } else if (complements[complementoText].search("-") === -1) {
                    itemNormal++;
                }
            }

            arrayTiposItensExtras.push(primeiroSabor);
            arrayTiposItensExtras.push(segundoSabor);
            arrayTiposItensExtras.push(terceiroSabor);
            arrayTiposItensExtras.push(quartoSabor);
            arrayTiposItensExtras.push(itemNormal);


            if (complements !== undefined) {
                for (montandoItemExtras of arrayTiposItensExtras) {
                    for (let conta = 0; conta < montandoItemExtras; conta++) {
                        controleLabel = montandoItemExtras;
                        if (montandoItemExtras >= 1 && complements[0].search('-') > -1) {

                            complementoXml += `
                            ${montandoItemExtras === controleLabel ? '<b style = "font-size: 0.7rem">' + ObjSaboresMesas[entrandoObjSabores] + '</b>' : ""}
                            <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0].substring(2)} x ${qtdsItens[0][0]}</p>
                        `

                            complements.splice(0, 1);
                            qtdsItens[0].splice(0, 1);

                            controleLabel--;
                        } else {
                            complementoXml += `
                                <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0]} x ${qtdsItens[0][0]}</p>
                            `

                            complements.splice(0, 1);
                            qtdsItens[0].splice(0, 1);
                            controleLabel--;
                        }
                    }

                    entrandoObjSabores++;
                }
            }

            complementoArray.push(complementoXml);

            tbody += `
            <tr style = "height: 60px; font-weight: 600;">
                <td style = "font-weight: 800;">${arrayDosItensPedidos[itensMesa].qtd_prod}</td>
                <td style = "text-align: left; font-size: 0.8rem;">${arrayDosItensPedidos[itensMesa].desc_produto} <p style = "font-weight: 800; font-size: 0.7rem; margin-bottom: 0px"> ${objTamanhos[arrayDosItensPedidos[itensMesa].tam_escolhido] !== "(NORMAL)" ? "Tamanho: " + objTamanhos[arrayDosItensPedidos[itensMesa].tam_escolhido] : ''} </p> ${complementoArray[itensMesa] === undefined ? "" : complementoArray[itensMesa]}</td>
                <td>${Helper.valorEmReal((arrayDosItensPedidos[itensMesa].acrescimo * arrayDosItensPedidos[itensMesa].qtd_prod) + (arrayDosItensPedidos[itensMesa].qtd_prod * arrayDosItensPedidos[itensMesa].vl_unit))}</td>
            </tr>
        `

            nomeItemForComparar = [];
            qtdsItens = [];
            arrayTiposItensExtras = [];
            primeiroSabor = 0;
            segundoSabor = 0;
            terceiroSabor = 0;
            quartoSabor = 0;
            itemNormal = 0;
            controleLabel = 0;
            entrandoObjSabores = 0;
            complementoXml = "";

            valorTotal += (arrayDosItensPedidos[itensMesa].acrescimo * arrayDosItensPedidos[itensMesa].qtd_prod) + (arrayDosItensPedidos[itensMesa].qtd_prod * arrayDosItensPedidos[itensMesa].vl_unit);
        }

        complementoArray = [];

        conteudoHTML.innerHTML = tbody;

        valorTotal += pedidoMesa.frete;

        let valorLimpoSemDescontoAcrescimo = valorTotal;

        sessionStorage.setItem('valorTotalLimpoPedido', valorLimpoSemDescontoAcrescimo);
        sessionStorage.setItem('valorTotalCalculadoFechamento', valorLimpoSemDescontoAcrescimo);

        document.getElementById('acrescimoFechamento').value = pedidoMesa.acrescimo.toFixed(2).toString().replace('.', ',');
        document.getElementById('descontoFechamento').value = pedidoMesa.desconto.toFixed(2).toString().replace('.', ',');

        document.getElementById('somaItensFechado').value = Helper.valorEmReal(valorTotal);
        document.getElementById('valorTotalFechado').value = Helper.valorEmReal(valorTotal);
        sessionStorage.setItem('valorTotalCalculadoFechamento', valorTotal);

        if (tipoMenu === "0") {
            document.getElementById('menuTrad').style.display = "none";
        }
        return;
    }

    colocaHTML(tipoOperacao, telaAtual);
}

async function colocaHTML(tipoOperacao, telaAtual, tipoPedido) {
    pedidoMesa = JSON.parse(window.sessionStorage.getItem('pedidoMesa'));

    let nomeItemForComparar = [];
    let qtdsItens = [];
    let nomeItemForSearch = [];
    let qtdsItensForSearch = [];
    let complementoArray = [];
    let arrayTiposItensExtras = [];
    let complementoXml = "";

    let primeiroSabor = 0;
    let segundoSabor = 0;
    let terceiroSabor = 0;
    let quartoSabor = 0;
    let itemNormal = 0;
    let controleLabel = 0;
    let entrandoObjSabores = 0;

    if(tipoOperacao === 'Fechamento' || tipoOperacao === 'VoltandoFechamento'){
        //if(telaAtual === 'Ultimo') return;
        let conteudoModPedido = '';
    
        if(telaAtual === 'Reserva' || telaAtual === 'Mesa') conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoMesa\\FechamentoMesa.html', 'utf-8');
        else if (telaAtual === 'Comanda') conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoComanda\\Comanda.html', 'utf-8');
        else if (telaAtual === 'Ultimo') conteudoModPedido = fs.readFileSync(__dirname + '\\visualizaPedido.html', 'utf-8');

        let modalPedidoDelivery = document.getElementById("container-mesa");
        modalPedidoDelivery.innerHTML = conteudoModPedido;

        if(telaAtual === 'Reserva' || telaAtual === 'Mesa'){
            document.getElementById('nomeCli').value = pedidoMesa.cliente.toUpperCase();
            document.getElementById('mesaCli').value = pedidoMesa.numero_mesa;
        }else if (telaAtual === 'Comanda'){
            document.getElementById('domClienteFechamento').value = pedidoMesa.cliente.toUpperCase();
            document.getElementById('domComandaFechamento').value = pedidoMesa.numero_mesa;
        }
    
        let tbody = "";
        let conteudoHTML = document.getElementById('itensFechamento')
        let valorTotal = 0;
    
        for (itensMesa in arrayDosItensPedidos) {
    
            if (arrayDosItensPedidos[itensMesa].complemento.search('/') > 0) {
                let itensExtrasArray = arrayDosItensPedidos[itensMesa].complemento.split('/');
                for (j of itensExtrasArray) {
                    let separandoItensEQtds = j.split('x');
                    if (separandoItensEQtds[0] !== "") {
                        nomeItemForSearch.push(separandoItensEQtds[0]);
                        qtdsItensForSearch.push(parseInt(separandoItensEQtds[1]));
                    }
                }
                nomeItemForComparar.push(nomeItemForSearch);
                qtdsItens.push(qtdsItensForSearch);
                nomeItemForSearch = [];
                qtdsItensForSearch = [];
            }
    
            let complements = nomeItemForComparar[0];
    
            for (complementoText in complements) {
                if (complements[complementoText].search('1') > -1) {
                    primeiroSabor++;
                } else if (complements[complementoText].search('2') > -1) {
                    segundoSabor++;
                } else if (complements[complementoText].search('3') > -1) {
                    terceiroSabor++;
                } else if (complements[complementoText].search('4') > -1) {
                    quartoSabor++;
                } else if (complements[complementoText].search("-") === -1) {
                    itemNormal++;
                }
            }
    
            arrayTiposItensExtras.push(primeiroSabor);
            arrayTiposItensExtras.push(segundoSabor);
            arrayTiposItensExtras.push(terceiroSabor);
            arrayTiposItensExtras.push(quartoSabor);
            arrayTiposItensExtras.push(itemNormal);
    
    
            if (complements !== undefined) {
                for (montandoItemExtras of arrayTiposItensExtras) {
                    for (let conta = 0; conta < montandoItemExtras; conta++) {
                        controleLabel = montandoItemExtras;
                        if (montandoItemExtras >= 1 && complements[0].search('-') > -1) {
    
                            complementoXml += `
                            ${montandoItemExtras === controleLabel ? '<b style = "font-size: 0.7rem">' + ObjSaboresMesas[entrandoObjSabores] + '</b>' : ""}
                            <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0].substring(2)} x ${qtdsItens[0][0]}</p>
                        `
    
                            complements.splice(0, 1);
                            qtdsItens[0].splice(0, 1);
    
                            controleLabel--;
                        } else {
                            complementoXml += `
                                <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0]} x ${qtdsItens[0][0]}</p>
                            `
    
                            complements.splice(0, 1);
                            qtdsItens[0].splice(0, 1);
                            controleLabel--;
                        }
                    }
    
                    entrandoObjSabores++;
                }
            }
    
            complementoArray.push(complementoXml);
    
            tbody += `
            <tr style = "height: 60px; font-weight: 600;">
                <td style = "font-weight: 800;">${arrayDosItensPedidos[itensMesa].qtd_prod}</td>
                <td style = "text-align: left; font-size: 0.8rem;">${arrayDosItensPedidos[itensMesa].desc_produto} <p style = "font-weight: 800; font-size: 0.7rem; margin-bottom: 0px"> ${objTamanhos[arrayDosItensPedidos[itensMesa].tam_escolhido] !== "(NORMAL)" ? "Tamanho: " + objTamanhos[arrayDosItensPedidos[itensMesa].tam_escolhido] : ''} </p> ${complementoArray[itensMesa] === undefined ? "" : complementoArray[itensMesa]}</td>
                <td>${Helper.valorEmReal((arrayDosItensPedidos[itensMesa].acrescimo * arrayDosItensPedidos[itensMesa].qtd_prod) + (arrayDosItensPedidos[itensMesa].qtd_prod * arrayDosItensPedidos[itensMesa].vl_unit))}</td>
            </tr>
        `
    
            nomeItemForComparar = [];
            qtdsItens = [];
            arrayTiposItensExtras = [];
            primeiroSabor = 0;
            segundoSabor = 0;
            terceiroSabor = 0;
            quartoSabor = 0;
            itemNormal = 0;
            controleLabel = 0;
            entrandoObjSabores = 0;
            complementoXml = "";
    
            valorTotal += (arrayDosItensPedidos[itensMesa].acrescimo * arrayDosItensPedidos[itensMesa].qtd_prod) + (arrayDosItensPedidos[itensMesa].qtd_prod * arrayDosItensPedidos[itensMesa].vl_unit);
        }
    
        complementoArray = [];

        conteudoHTML.innerHTML = tbody;

        let valorLimpoSemDescontoAcrescimo = valorTotal;
        //let valorLimpoSemDescontoAcrescimo = valorTotal - pedidoMesa.acrescimo;
        //valorLimpoSemDescontoAcrescimo = valorLimpoSemDescontoAcrescimo + pedidoMesa.desconto;

        sessionStorage.setItem('valorTotalLimpoPedido', valorLimpoSemDescontoAcrescimo);
        sessionStorage.setItem('valorTotalCalculadoFechamento', valorLimpoSemDescontoAcrescimo);


        

        //valorTotal += pedidoMesa.acrescimo;
        //valorTotal -= pedidoMesa.desconto;

        document.getElementById('acrescimoFechamento').value = pedidoMesa.acrescimo.toFixed(2).toString().replace('.', ',');
        document.getElementById('descontoFechamento').value = pedidoMesa.desconto.toFixed(2).toString().replace('.', ',');

        if(telaAtual === 'Reserva' || telaAtual === 'Mesa'){
            document.getElementById('somaItensFechado').value = Helper.valorEmReal(pedidoMesa.vl_total);
            document.getElementById('valorTotalFechado').value = Helper.valorEmReal(pedidoMesa.vl_total);
        }else if(telaAtual === 'Comanda'){
            document.getElementById('somaItensFechadoComanda').value = Helper.valorEmReal(pedidoMesa.vl_total);
            document.getElementById('valorTotalFechadoComanda').value = Helper.valorEmReal(pedidoMesa.vl_total);
        }
    } else if (telaAtual === "Reserva") {

        let conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoMesa.html', 'utf-8')
        let modalPedidoDelivery = document.getElementById("container-mesa");
        modalPedidoDelivery.innerHTML = conteudoModPedido;

        let adicionaHTML = document.getElementById('InfoPed');

        adicionaHTML.innerHTML = `
                <div>
                    <div>NOME: <b id="nomeCli">${nomeCli}</b></div>
                    <div>MESA: <b id="mesaCli">${mesaCli}</b></div>
                </div>
            `
        SelecionaProdutos(tipoOperacao, telaAtual);
    } else if (telaAtual === "Fechamento") {
        let conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoMesa.html', 'utf-8')
        let modalPedidoDelivery = document.getElementById("container-mesa");
        modalPedidoDelivery.innerHTML = conteudoModPedido;

        let adicionaHTML = document.getElementById('InfoPed');
        if (tipoPedido === "Mesa")  adicionaHTML.innerHTML = `<div><div>NOME: <b id="nomeCli">${nomeCli}</b></div><div>MESA: <b id="mesaCli">${mesaCli}</b></div></div>`;
        else if(tipoPedido === 'Comanda') adicionaHTML.innerHTML = `<div><div>COMANDA: <b id="nomeCli">${pedidoMesa.numero_mesa}</b></div><div>NOME: <b id="ruaCli">${pedidoMesa.cliente}</b></div></div>`;

        SelecionaProdutos(tipoOperacao, telaAtual, tipoPedido);
    } else if (telaAtual === "Comanda") {

        let conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoMesa.html', 'utf-8')
        let modalPedidoDelivery = document.getElementById("container-mesa");
        modalPedidoDelivery.innerHTML = conteudoModPedido;

        let nomeCliComanda = document.getElementById('domCli').value.toUpperCase();
        let comandaCli = document.getElementById('domComanda').value;

        let adicionaHTML = document.getElementById('InfoPed');

        adicionaHTML.innerHTML = `
                <div style = "margin-left: 30px;">
                    <div>NOME: <b id="nomeCli">${nomeCliComanda}</b></div>
                    <div>COMANDA: <b id="mesaCli">${comandaCli}</b></div>
                </div>
            `

        SelecionaProdutos(tipoOperacao, telaAtual, tipoPedido);
    } else if (telaAtual === "Balcão") {
        if (tipoOperacao === "adicionar") {
            document.getElementById('container-comanda') ? document.getElementById('container-comanda').style.display = "none" : null;
            document.getElementById('container-mesa').style.display = "block";
            let conteudoModPedido = fs.readFileSync(__dirname + '\\..\\html\\PedidoMesa.html', 'utf-8')
            let modalPedidoDelivery = document.getElementById("container-mesa");
            modalPedidoDelivery.innerHTML = conteudoModPedido;
            let adicionaHTML = document.getElementById('InfoPed');

            adicionaHTML.innerHTML = `
                <div id = "container-inputs-balcony" style = "display: flex; flex-direction: row; align-items: center; justify-content: center; margin-top: 20px;">
                    <p class = "textBalcony" id = "domTextCli">Cliente: </p>
                    <input class = "inputBalcao" id="domBalcao"/>
                    
                    <p class = "textBalcony" id = "domTextTel">Telefone: </p>
                    <input class = "inputBalcao" id="domTelefone"/>
                </div>
                `



            document.getElementById('domTelefone').addEventListener('keyup', Helper.phoneMaskSemDDD);

            SelecionaProdutos(tipoOperacao, undefined, 'Balcao');
        }
    } else {
        let conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoMesa.html', 'utf-8')
        let modalPedidoDelivery = document.getElementById("container-mesa");
        modalPedidoDelivery.innerHTML = conteudoModPedido;
        let adicionaHTML = document.getElementById('InfoPed');

        if(tipoPedido === 'MESA'){
            adicionaHTML.innerHTML = `
                <div style = "margin-left: 30px">
                    <div>NOME: <b id="nomeCli">${pedidoMesa.cliente}</b></div>
                    <div>MESA: <b id="mesaCli">${pedidoMesa.numero_mesa}</b></div>
                </div>`;
        }else if(tipoPedido === 'ENTREGA'){
            let enderecoClienteSelect = await cCadEndereco.select(pedidoMesa.id_logradouro);

            adicionaHTML.innerHTML = `
                <div style = "margin-left: 30px;">
                    <div class = "textoInfo">NOME: <b class = "textoInfo" id="nomeCli">${pedidoMesa.cliente}</b></div>
                    <div class = "textoInfo">RUA: <b class = "textoInfo" id="ruaCli">${enderecoClienteSelect.rua}</b></div>
                    <div class = "textoInfo">Nº <b class = "textoInfo" id="numCli">${pedidoMesa.num_endereco}</b></div>
                </div>
                <div>
                    <div class = "textoInfo">COMPL: <b class = "textoInfo" id="compCli">${pedidoMesa.comp_endereco}</b></div>
                    <div class = "textoInfo">FONE: <b class = "textoInfo" id="foneCli">${pedidoMesa.telefone}</b></div>
                    <div class = "textoInfo">FRETE: <b class = "textoInfo" id="freteTot">${Helper.valorEmReal(pedidoMesa.frete)}</b></div>
                </div>`;
        }else if(tipoPedido === 'BALCÃO'){
            adicionaHTML.innerHTML = `
                <div style = "margin-left: 30px">
                    <div>NOME: <b id="nomeCli">${pedidoMesa.cliente}</b></div>
                    <div>TELEFONE: <b id="ruaCli">${pedidoMesa.telefone}</b></div>
                </div>`;
        }else if(tipoPedido === 'COMANDA'){
            adicionaHTML.innerHTML = `
                <div style = "margin-left: 30px;">
                    <div>COMANDA: <b id="nomeCli">${pedidoMesa.numero_mesa}</b></div>
                    <div>NOME: <b id="ruaCli">${pedidoMesa.cliente}</b></div>
                </div>`;
        }
        
        SelecionaProdutos(tipoOperacao, telaAtual, tipoPedido, tipoPedido === 'ENTREGA' ? pedidoMesa : undefined);
    }
}

async function listenerCampoAcrescimoDesconto(tela = ''){
    const campoAcrescimo = document.getElementById('acrescimoFechamento');
    const campoDesconto = document.getElementById('descontoFechamento');

    const valorAcrescimo = campoAcrescimo.value !== '' ? parseFloat(campoAcrescimo.value.replace('.','').replace('.','').replace(',','.')) : 0;
    const valorDesconto = campoDesconto.value !== '' ? parseFloat(campoDesconto.value.replace('.','').replace('.','').replace(',','.')) : 0;

    let valorTotal = parseFloat(sessionStorage.getItem('valorTotalCalculadoFechamento'));

    valorTotal -= valorDesconto;
    valorTotal += valorAcrescimo;

    await cCadPedido.atualizaValorAcrescimo(pedidoMesa.id_pedido, valorAcrescimo);
    await cCadPedido.atualizaValorDesconto(pedidoMesa.id_pedido, valorDesconto);
    await cCadPedido.atualizaValorPedidoAposAcrDesc(pedidoMesa.id_pedido, valorTotal);
    const pedidosTotaisDesatualizado = JSON.parse(sessionStorage.getItem('pedidosTotais'));
    for(p of pedidosTotaisDesatualizado){
        if(p.id_pedido === pedidoMesa.id_pedido){
            p.acrescimo = valorAcrescimo;
            p.desconto = valorDesconto;
            p.vl_total = valorTotal;
            sessionStorage.setItem('pedidosTotais', JSON.stringify(pedidosTotaisDesatualizado));
            break;
        }
    }

    document.getElementById('valorTotalFechado' + tela).value = Helper.valorEmReal(valorTotal);
    sessionStorage.setItem('valorTotalCalculadoFechamentoAposMudanca', valorTotal);
}

function retornaXMLRececimento(tipoPedido){
    let cabecalhoInformacoesPedido = "";
    if(tipoPedido === "DELIVERY"){

        cabecalhoInformacoesPedido = `
            <div class = "row-receb">
                <div class = "row-inputs-receb">
                    <p>Nº do Pedido</p><input id = "nrPedido" type = "text" style = "width: 70px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>Dt.do Pedido</p><input id = "dtPedido" type = "text" style = "width: 119px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>as</p><input id = "horas" type = "text" style = "width: 70px;"/>
                </div>
            </div>
            <div class = "row-receb">
                <div class="row-inputs-receb">
                    <p>Cliente</p><input id = "cliente" type = "text" style = "width: 250px;"/>
                </div>
                <div class="row-inputs-receb">
                    <p>Fone</p><input id = "fone" type = "text" style = "width: 139px;"/>
                </div>
            </div>
            <div class = "row-receb">
                <div class="row-inputs-receb">
                    <p>Endereço</p><input id = "endereco" type = "text" style = "width: 446px;"/>
                </div>
            </div>
        `
        return cabecalhoInformacoesPedido;

    } else if (tipoPedido === "MESA"){

        cabecalhoInformacoesPedido = `
            <div class = "row-receb">
                <div class = "row-inputs-receb">
                    <p>Nº do Pedido: </p><input id = "nrPedido" type = "text" style = "width: 70px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>Dt.do Pedido: </p><input id = "dtPedido" type = "text" style = "width: 119px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>As: </p><input id = "horas" type = "text" style = "width: 70px;"/>
                </div>
            </div>
            <div class = "row-receb">
                <div class="row-inputs-receb">
                    <p>Cliente: </p><input id = "cliente" type = "text" style = "width: 250px;"/>
                </div>
                <div class="row-inputs-receb">
                    <p>Mesa: </p><input id = "mesa" type = "text" style = "width: 139px;"/>
                </div>
            </div>
        `
        return cabecalhoInformacoesPedido;
    
    } else if (tipoPedido === "COMANDA"){

        cabecalhoInformacoesPedido = `
            <div class = "row-receb">
                <div class = "row-inputs-receb">
                    <p>Nº do Pedido: </p><input id = "nrPedido" type = "text" style = "width: 70px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>Dt.do Pedido: </p><input id = "dtPedido" type = "text" style = "width: 119px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>As: </p><input id = "horas" type = "text" style = "width: 70px;"/>
                </div>
            </div>
            <div class = "row-receb">
                <div class="row-inputs-receb">
                    <p>Cliente: </p><input id = "cliente" type = "text" style = "width: 250px;"/>
                </div>
                <div class="row-inputs-receb">
                    <p>Comanda: </p><input id = "comanda" type = "text" style = "width: 139px;"/>
                </div>
            </div>
        `
        return cabecalhoInformacoesPedido;

    } else if (tipoPedido === "BALCÃO"){
        
        cabecalhoInformacoesPedido = `
            <div class = "row-receb">
                <div class = "row-inputs-receb">
                    <p>Nº do Pedido: </p><input id = "nrPedido" type = "text" style = "width: 70px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>Dt.do Pedido: </p><input id = "dtPedido" type = "text" style = "width: 119px;"/>
                </div>
                <div class = "row-inputs-receb">
                    <p>As: </p><input id = "horas" type = "text" style = "width: 70px;"/>
                </div>
            </div>
            <div class = "row-receb">
                <div class="row-inputs-receb">
                    <p>Cliente: </p><input id = "cliente" type = "text" style = "width: 250px;"/>
                </div>
                <div class="row-inputs-receb">
                    <p>Fone: </p><input id = "fone" type = "text" style = "width: 139px;"/>
                </div>
            </div>
        `
        return cabecalhoInformacoesPedido;

    }   
}   

function irParaRecebimento(tipoPedido, telaAtual, idPedido){
    if((tipoPedido === "MESA" || tipoPedido === "COMANDA") && telaAtual !== "ULTIMO"){
        let conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoMesa\\telaPagamento.html', 'utf-8')
        let modalPedidoDelivery = document.getElementById("container-receb");
        modalPedidoDelivery.innerHTML = conteudoModPedido;
        modalPedidoDelivery.style.display = "flex";

        let conteudoCabecalhoInfo = retornaXMLRececimento(tipoPedido);

        setTimeout(() => {
            document.getElementById('informacoesPedidoCliente').innerHTML = conteudoCabecalhoInfo;
        }, 100);

        let idPedido = parseInt(window.sessionStorage.getItem('idPedidoSelecionadoFechamento'));
        arrayDosItensPedidos = [];

        if (pedidosTotais !== null && itensDosPedidos !== null) {
            for (i of pedidosTotais) {
                if (i.id_pedido === idPedido) {
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            itensDosPedidos = [];
                        }
                    }
                }
            }
        }

        window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

        setTimeout(() => {
            selecionarFormasPgto();
          //  adicionaPedidoNaTela();
        }, 100);

    } else if (telaAtual === "ULTIMO"){

        let conteudoModPedido = fs.readFileSync(__dirname + '\\PedidoMesa\\telaPagamento.html', 'utf-8')
        let modalPedidoDelivery = document.getElementById("container-receb");
        modalPedidoDelivery.innerHTML = conteudoModPedido;
        modalPedidoDelivery.style.display = "flex";

        let conteudoCabecalhoInfo = retornaXMLRececimento(tipoPedido);

        setTimeout(() => {
            document.getElementById('informacoesPedidoCliente').innerHTML = conteudoCabecalhoInfo;
        }, 100);

        arrayDosItensPedidos = [];

        if ((pedidosTotais !== null && itensDosPedidos !== null) && (pedidosTotais.length !== 0 && itensDosPedidos.length !== 0)) {
            for (i of pedidosTotais) {
                if (idPedido === i.id_pedido) {
                    if (i.status_pedido === 'RECEBIDO') {
                        swal({
                            icon:'info',
                            title:'Pedido já recebido'
                        });
                        fecharTelaPagamento();
                        return;
                    }
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            itensDosPedidos = [];
                        }
                    }
                }
            }
        } else {
            for (pedtot of JSON.parse(window.sessionStorage.getItem('pedidosTotais'))) {
                pedidosTotais.push(pedtot);
            }

            for (itPedTot of JSON.parse(window.sessionStorage.getItem('itensDosPedidos'))) {
                itensDosPedidos.push(itPedTot);
            }

            for (i of pedidosTotais) {
                if (idPedido === i.id_pedido) {
                    window.sessionStorage.setItem('pedidoMesa', JSON.stringify(i));
                    for (k of itensDosPedidos) {
                        if (i.id_pedido === k.cod_pedido) {
                            arrayDosItensPedidos.push(k);
                            itensDosPedidos = [];
                        }
                    }
                }
            }
        }

        window.sessionStorage.setItem('itemDoPedidoMesa', JSON.stringify(arrayDosItensPedidos));

        setTimeout(() => {
            selecionarFormasPgto();
           // adicionaPedidoNaTela();
        }, 100);

    }
}

function mostrarItensPedidos() {
    let tbody = "";
    let tabelaFechamento = document.getElementById('itensFechamento');

    let valorTotal = 0;

    for (pedidoFechado in arrayDosItensPedidos) {
        tbody += `
            <tr style = "height: 60px;" id = "trPedFechado${pedidoFechado}">
                <td style = "height: 60px; font-weight: 600;" id = "table-cell">${arrayDosItensPedidos[pedidoFechado].qtd_prod}</td>
                <td style = "height: 60px; font-weight: 600;" class = "td-align" id = "table-cell">${arrayDosItensPedidos[pedidoFechado].desc_produto} ${objTamanhos[arrayDosItensPedidos[pedidoFechado].tam_escolhido] !== "(NORMAL)" ? objTamanhos[arrayDosItensPedidos[pedidoFechado].tam_escolhido] : ''} ${arrayDosItensPedidos[pedidoFechado].complemento === "Sem Complemento" ? "" : "( " + arrayDosItensPedidos[pedidoFechado].complemento + ")"}</td>
                <td style = "height: 60px; font-weight: 600;" id = "table-cell">${Helper.valorEmReal((arrayDosItensPedidos[pedidoFechado].qtd_prod * arrayDosItensPedidos[pedidoFechado].vl_unit))}</td>
            </tr>
        `

        valorTotal = valorTotal + (arrayDosItensPedidos[pedidoFechado].vl_unit * arrayDosItensPedidos[pedidoFechado].qtd_prod);
    }

    document.getElementById('somaItensFechado').value = Helper.valorEmReal(valorTotal);
    document.getElementById('valorTotalFechado').value = Helper.valorEmReal(valorTotal);
    sessionStorage.setItem('valorTotalCalculadoFechamento', valorTotal);

    tabelaFechamento.innerHTML = tbody;
}

function retornarFechamento(telaAtual) {
    if (telaAtual !== "Fechamento") {
        document.getElementById('itensFechamento').innerHTML = "";
        document.getElementById('container-mesa').style.display = "none";
        arrayDosItensPedidos = [];
        itensDosPedidos = [];
        window.sessionStorage.removeItem('itemDoPedidoMesa');
    } else {
        document.getElementById('container-mesa').innerHTML = "";
        document.getElementById('container-mesa').style.display = "none";
        document.getElementById('container-comanda').style.display = "block";
        arrayDosItensPedidos = [];
        itensDosPedidos = [];
        window.sessionStorage.removeItem('itemDoPedidoMesa');
    }
}

function cancelarPedido(tipoEntrega, tipoId, telaAtual, idPedido) {
    let numeroMesa = "";
    let pedidosTotais = JSON.parse(window.sessionStorage.getItem('pedidosTotais'));
    
    if (tipoEntrega === "MESA") {
        tipoId === undefined ? numeroMesa = document.getElementById('domMesa').value : numeroMesa = tipoId;
        
        if (pedidosTotais !== null) {
            for (pedidosCancel of pedidosTotais) {
                if (String(numeroMesa) === pedidosCancel.numero_mesa) {
                    cCadPedido.numero_mesa = "CANCELADO";
                    cCadPedido.status_pedido = "CANCELADO";
                    cCadPedido.id_pedido = pedidosCancel.id_pedido;
                    swal('Tem certeza que deseja excluir ?', {
                        icon: "warning",
                        buttons: ['SIM','NÃO']
                    }).then((acao) => {
                        if (acao !== true) {
                            cCadPedido.update().then(retorno => {
                                if (telaAtual === "Reserva") {
                                    window.sessionStorage.removeItem('pedidosTotais');
                                    window.sessionStorage.removeItem('itensDosPedidos');


                                    zerarVariaveis();

                                    document.getElementById('buttons-container').innerHTML = "";
                                    document.getElementById('adiciona-pedido').innerHTML = "";
                                    document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                    document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                    document.getElementById('domCliente').value = "";
                                    document.getElementById('domMesa').value = "";

                                    selecionarTudo();

                                } else if (telaAtual === "Ultimo") {
                                    window.sessionStorage.removeItem('pedidosTotais');
                                    window.sessionStorage.removeItem('itensDosPedidos');

                                    zerarVariaveis();

                                    document.getElementById('listaPedidos').innerHTML = "";

                                    refazerTabela();
                                } else if (telaAtual === "Fechamento") {

                                    //SÓ ESSE IF QUE TEM A MAIS, DEPOIS VAI PRO SWAL

                                    window.sessionStorage.removeItem('pedidosTotais');
                                    window.sessionStorage.removeItem('itensDosPedidos');

                                    zerarVariaveis();

                                    document.getElementById('container-mesa').innerHTML = "";
                                    document.getElementById('container-mesa').style.display = "none";
                                    document.getElementById('buttons-container').innerHTML = "";
                                    document.getElementById('adiciona-pedido').innerHTML = "";
                                    document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                    document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                    document.getElementById('domCliente').value = "";
                                    document.getElementById('domMesa').value = "";

                                    selecionarTudo();
                                }

                                swal({
                                    icon: 'success',
                                    text: 'Pedido cancelado!',
                                    button: 'OK'
                                })

                            })
                        }
                    })
                }
            }
        }
    } else if (tipoEntrega === "DELIVERY" || tipoEntrega === "ENTREGA") {
        tipoId === undefined ? numeroMesa = document.getElementById('domMesa').value : numeroMesa = tipoId;

        if (pedidosTotais !== null) {
            for (pedidosCancel of pedidosTotais) {
                if (idPedido === pedidosCancel.id_pedido) {
                    swal('Tem certeza que deseja excluir ?', {
                        icon: "warning",
                        buttons: ['SIM','NÃO']
                    }).then((acao) => {
                        if (acao !== true) {
                            cCadPedido.numero_mesa = "CANCELADO";
                            cCadPedido.status_pedido = "CANCELADO";
                            cCadPedido.id_pedido = pedidosCancel.id_pedido;
                            cCadPedido.update().then(retorno => {
                                if (telaAtual === "Reserva") {
                                    window.sessionStorage.removeItem('pedidosTotais');
                                    window.sessionStorage.removeItem('itensDosPedidos');

                                    zerarVariaveis();

                                    document.getElementById('buttons-container').innerHTML = "";
                                    document.getElementById('adiciona-pedido').innerHTML = "";
                                    document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                    document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                    document.getElementById('domCliente').value = "";
                                    document.getElementById('domMesa').value = "";

                                    selecionarTudo();

                                } else if (telaAtual === "Ultimo") {
                                    window.sessionStorage.removeItem('pedidosTotais');
                                    window.sessionStorage.removeItem('itensDosPedidos');

                                    zerarVariaveis();

                                    document.getElementById('listaPedidos').innerHTML = "";

                                    refazerTabela();
                                }

                                swal({
                                    icon: 'success',
                                    text: 'Pedido cancelado!',
                                    button: 'OK'
                                })

                            })
                        }
                    })
                }
            }
        }
    } else if (tipoEntrega === "BALCÃO") {
        tipoId === undefined ? numeroMesa = document.getElementById('domMesa').value : numeroMesa = tipoId;

        if (pedidosTotais !== null) {
            for (pedidosCancel of pedidosTotais) {
                if (idPedido === pedidosCancel.id_pedido) {
                    cCadPedido.numero_mesa = "CANCELADO";
                    cCadPedido.status_pedido = "CANCELADO";
                    cCadPedido.id_pedido = pedidosCancel.id_pedido;
                    swal('Tem certeza que deseja excluir ?', {
                        icon: "warning",
                        buttons: ['SIM','NÃO']
                    }).then((acao) => {
                        if (acao !== true) {
                            cCadPedido.update().then(retorno => {
                                if (telaAtual === "Reserva") {
                                    window.sessionStorage.removeItem('pedidosTotais');
                                    window.sessionStorage.removeItem('itensDosPedidos');

                                    zerarVariaveis();

                                    document.getElementById('buttons-container').innerHTML = "";
                                    document.getElementById('adiciona-pedido').innerHTML = "";
                                    document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                    document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                    document.getElementById('domCliente').value = "";
                                    document.getElementById('domMesa').value = "";

                                    selecionarTudo();

                                } else if (telaAtual === "Ultimo") {
                                    window.sessionStorage.removeItem('pedidosTotais');
                                    window.sessionStorage.removeItem('itensDosPedidos');

                                    zerarVariaveis();

                                    document.getElementById('listaPedidos').innerHTML = "";

                                    refazerTabela();
                                }

                                swal({
                                    icon: 'success',
                                    text: 'Pedido cancelado!',
                                    button: 'OK'
                                })

                            })
                        }
                    })
                }
            }
        }
    } else if (tipoEntrega === "COMANDA") {
        if (telaAtual !== 'Fechamento') {
            tipoId === undefined ? numeroMesa = document.getElementById('domComanda').value : numeroMesa = tipoId;

            if (pedidosTotais !== null) {
                for (pedidosCancel of pedidosTotais) {
                    if (idPedido === pedidosCancel.id_pedido) {
                        cCadPedido.numero_mesa = "CANCELADO";
                        cCadPedido.status_pedido = "CANCELADO";
                        cCadPedido.id_pedido = pedidosCancel.id_pedido;
                        swal('Tem certeza que deseja excluir ?', {
                            icon: "warning",
                            buttons: ['SIM','NÃO']
                        }).then((acao) => {
                            if (acao !== true) {

                                cCadPedido.update().then(retorno => {
                                    if (telaAtual === "Reserva") {
                                        window.sessionStorage.removeItem('pedidosTotais');
                                        window.sessionStorage.removeItem('itensDosPedidos');

                                        zerarVariaveis();

                                        document.getElementById('buttons-container').innerHTML = "";
                                        document.getElementById('adiciona-pedido').innerHTML = "";
                                        document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                        document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                        document.getElementById('domCliente').value = "";
                                        document.getElementById('domMesa').value = "";

                                        selecionarTudo();

                                    } else if (telaAtual === "Ultimo") {
                                        window.sessionStorage.removeItem('pedidosTotais');
                                        window.sessionStorage.removeItem('itensDosPedidos');

                                        zerarVariaveis();

                                        document.getElementById('listaPedidos').innerHTML = "";

                                        setTimeout(() => {
                                            refazerTabela();
                                        }, 1000);
                                    }

                                    swal({
                                        icon: 'success',
                                        text: 'Pedido cancelado!',
                                        button: 'OK'
                                    })

                                })
                            }
                        })
                    }
                }
            }
        } else {
            tipoId === undefined ? numeroMesa = document.getElementById('domComanda').value : numeroMesa = tipoId;

            if (pedidosTotais !== null) {
                for (pedidosCancel of pedidosTotais) {
                    if (numeroMesa === pedidosCancel.numero_mesa) {
                        cCadPedido.numero_mesa = "CANCELADO";
                        cCadPedido.status_pedido = "CANCELADO";
                        cCadPedido.id_pedido = pedidosCancel.id_pedido;
                        swal('Tem certeza que deseja excluir ?', {
                            icon: "warning",
                            buttons: ['SIM','NÃO']
                        }).then((acao) => {
                            if (acao !== true) {
                                cCadPedido.update().then(retorno => {
                                    if (telaAtual === "Reserva") {
                                        window.sessionStorage.removeItem('pedidosTotais');
                                        window.sessionStorage.removeItem('itensDosPedidos');

                                        zerarVariaveis();

                                        document.getElementById('buttons-container').innerHTML = "";
                                        document.getElementById('adiciona-pedido').innerHTML = "";
                                        document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                        document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                        document.getElementById('domCliente').value = "";
                                        document.getElementById('domMesa').value = "";

                                        selecionarTudo();

                                    } else if (telaAtual === "Fechamento") {
                                        window.sessionStorage.removeItem('pedidosTotais');
                                        window.sessionStorage.removeItem('itensDosPedidos');

                                        zerarVariaveis();

                                        document.getElementById('container-mesa').style.display = "none";
                                        document.getElementById('container-comanda').style.display = "block";
                                        document.getElementById('adicionarItensPedido').innerHTML = "";
                                        document.getElementById('domCli').value = "";
                                        document.getElementById('domComanda').value = "";

                                        selecionarTudo();

                                    } else if (telaAtual === "Ultimo") {
                                        window.sessionStorage.removeItem('pedidosTotais');
                                        window.sessionStorage.removeItem('itensDosPedidos');

                                        zerarVariaveis();

                                        document.getElementById('listaPedidos').innerHTML = "";

                                        setTimeout(() => {
                                            refazerTabela();
                                        }, 1000);
                                    }

                                    swal({
                                        icon: 'success',
                                        text: 'Pedido cancelado!',
                                        button: 'OK'
                                    })

                                })
                            }
                        })
                    }
                }
            }
        }
    }
}

function goBack(telaAtual) {
    if (telaAtual === "Reserva") {
        if (tipoMenu === "0") {
            window.location.href = "../html/menuTradicional.html";
        } else if (tipoMenu === "1") {
            voltarSubMenu(1)
        }

    } else if (telaAtual === "Pedido") {
        if (tipoMenu === "0") {
            carregaFrame("ReservaDeMesa");
        } else if (tipoMenu === "1") {
            carregaFrame('ReservaDeMesa');
        }
    }
}

function valorTotalSomadoMesa() {
    let valorTotalLabel = document.getElementById("valorTotal");
    let somaDosItens = document.getElementById("somaDosItens");
    let itex = pedido.itex;
    let acrescimoTotal = 0;

    let valorTotal = 0;

    for (let i = 0; i < pedido.produtos.length; i++) {
        valorTotal += pedido.produtos[i].vl_vendatot;
    }

    for (valorAcres of itex) {
        if (valorAcres.nomeItem.length > 1) {
            let arrayItens = valorAcres.nomeItem;
            for (posicaoItemVetor in arrayItens) {
                acrescimoTotal += (arrayItens[posicaoItemVetor].acrescimo * valorAcres.qtdsItens[posicaoItemVetor]);
            }
        } else if (valorAcres.nomeItem.length === 1) {
            acrescimoTotal += (valorAcres.nomeItem[0].acrescimo * valorAcres.qtdsItens[0]);
        }

    }

    valorTotal += acrescimoTotal;

    valorTotalLabel.innerHTML = Helper.valorEmReal(valorTotal);
    somaDosItens.innerHTML = Helper.valorEmReal(valorTotal);
}

function valorTotalSomadoMesaBtns(valorTotal, valorPagar) {

    let valorTotalLabel = document.getElementById("somaDosItensMesaBtns");
    let somaDosItens = document.getElementById("valorTotalMesaBtns");
    let txEntrega = document.getElementById('taxaEntregaMesa')

    valorTotalLabel.innerHTML = Helper.valorEmReal(valorTotal);
    somaDosItens.innerHTML = Helper.valorEmReal(valorPagar);
    window.sessionStorage.setItem('valorTotalPedidoPAdicao', valorTotal);// Valor vai ser consultado posteriormente para adicionar e somar os valores dos itens ao pedido corretamente
    txEntrega.innerHTML = 0;
}

function voltarFechamento(telaAtual, tipoPed) {
    if (telaAtual === "Fechamento") {
        if (tipoPed === "Comanda") {
            document.getElementById('container-mesa').innerHTML = "";
            document.getElementById('container-mesa').style.display = "none";
            document.getElementById('container-comanda').style.display = "block";

            document.getElementById('domComanda').value = "";
            document.getElementById('domCli').value = "";
            document.getElementById('adicionarItensPedido').innerHTML = "";

            zerarVariaveis();
            selecionaPedidos();

        } else if (tipoPed === "Mesa") {
            /* ITENS XML TELA DO FECHAMENTO DE MESA */
            document.getElementById('itensFechamento').innerHTML = "";
            document.getElementById('nomeCli').value = "";
            document.getElementById('mesaCli').value = "";
            document.getElementById('somaItensFechado').value = "";
            document.getElementById('valorTotalFechado').value = "";
            /* ITENS XML TELA DE RESERVA DE MESA */
            document.getElementById('adiciona-pedido').innerHTML = "";
            document.getElementById('somaDosItensMesaBtns').innerHTML = "";
            document.getElementById('taxaEntregaMesa').innerHTML = "";
            document.getElementById('valorTotalMesaBtns').innerHTML = "";
            document.getElementById('domMesa').value = "";
            document.getElementById('domCliente').value = "";

            arrayDosItensPedidos = [];
            itensDosPedidos = [];
            window.sessionStorage.removeItem('itemDoPedidoMesa');

            document.getElementById('container-mesa').style.display = 'none';

            //document.getElementById('buttons-container').innerHTML = '';
            //selecionarTudo();

        }
    } else if (telaAtual === "Comanda") {
        document.getElementById('adicionarItensPedido').innerHTML = "";
        document.getElementById('domCli').value = "";
        document.getElementById('domComanda').value = "";
        zerarVariaveis();
        fecharModais('comanda');
        selecionaPedidos();
    }
}

function estiloResponsivoParaElementosDeMesa(tamanhoMonitor) {
    if (tamanhoMonitor <= 1024) {

        try{
            document.getElementById('domCliente').style.width                           = "15rem"   ;
            document.getElementById('domCliente').style.fontSize                        = "0.9rem"  ;
            //document.getElementById('buttons-container-identity').style.marginRight     = "100px"   ;
            document.getElementById('botoes-reserva-fec').style.width                   = "5rem"    ;
            document.getElementById('botoes-reserva-rec').style.width                   = "5rem"    ;
            document.getElementById('botoes-reserva-edi').style.width                   = "5rem"    ;
            document.getElementById('botoes-reserva-add').style.width                   = "5rem"    ;
            document.getElementById('botoes-reserva-can').style.width                   = "5rem"    ;
            document.getElementById('area-amarela').style.marginRight                   = ""    ;
            document.getElementById('area-amarela').style.padding                       = "0 0 50px 0";
            document.getElementById('area-amarela').style.width                         = "100%"     ;
            document.getElementById('info-table').style.width                           = "66%"     ;
        }catch(ex){
            console.warn(ex);
        }

    } else if (tamanhoMonitor >= 1024) {

        try{
            document.getElementById('botoes-reserva-add').style.width                   = "6rem"    ;
            document.getElementById('botoes-reserva-fec').style.width                   = "6rem"    ;
            document.getElementById('botoes-reserva-rec').style.width                   = "6rem"    ;
            document.getElementById('botoes-reserva-edi').style.width                   = "6rem"    ;
            document.getElementById('botoes-reserva-can').style.width                   = "6rem"    ;
            document.getElementById('area-amarela').style.width                         = "93%"     ;
            document.getElementById('area-amarela').style.marginRight                   = "12px"    ;
            document.getElementById('area-amarela').style.padding                       = "0 0 50px 0";
            document.getElementById('info-table').style.width                           = "66%"     ;
            document.getElementById('domCliente').style.width                           = "20rem"   ;
            document.getElementById('domCliente').style.fontSize                        = "1rem"    ;
        }catch(ex){
            console.warn(ex);
        }

    }
}

function fechaVisuPedido(){
    document.getElementById('container-mesa').style.display = 'none';
}

module.exports = {
    selecionarTudo: selecionarTudo,
    selecionaMesa : selecionaMesa,
    irParaCardapio: irParaCardapio,
    cancelarPedido: cancelarPedido,
    irParaRecebimento: irParaRecebimento
}
