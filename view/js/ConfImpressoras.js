var $ = require("jquery");

var path = require('path');

window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));

var Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
const ConfImpressoras = require(path.join(__dirname + '/../../controller/cConfImpressoras.js'));
var btnSalvar = document.getElementById('cadastrar');
var btAddAlt = document.getElementById('addAlt');
var campoLPT;
var lptAtual = '0';

btAddAlt.addEventListener('click', async ()=>{
	const impsOrdenadas = await ConfImpressoras.selectMaiorLPT();
	console.log(impsOrdenadas[impsOrdenadas.length - 1]);
	campoLPT = document.getElementById('portaLPT');
	campoLPT.addEventListener('keyup', listenerCampoPortaLPT);
	campoLPT.value = parseInt(impsOrdenadas[impsOrdenadas.length - 1].portaLPT) + 1;
});

async function listenerCampoPortaLPT(){
	const retornoBanco = await ConfImpressoras.selectPorLPT(campoLPT.value);
	retornoBanco !== null ? campoLPT.style.borderColor = '#dc3545' : campoLPT.style.borderColor = '#28a745';
	campoLPT.value  === '' ? campoLPT.style.borderColor = '#dc3545' : campoLPT.value === lptAtual ? campoLPT.style.borderColor = '#28a745' : null;
}


function selecionarTudo(){
	document.getElementById('lista').innerHTML = '';
	let ConfImpressoras = require(path.join(__dirname + '/../../controller/cConfImpressoras.js'));
	let listaProd = ConfImpressoras.selectAll();

	listaProd.then(data => {
		let actuallyData = JSON.parse(JSON.stringify(data));
		for(var i of actuallyData){insereItemNaTela(i,true);}
	});
}

function selecionarItem(item){
	const script = require(path.join(__dirname + '/script.js'));
	let divMain = document.getElementById('conteudoMain');
	let txt =`
		<div id="camadaModal" style="display:block;" class="cria-camada-modal h-100 w-100" >
			<div class="modal-info-imp position-relative col-8 offset-2 rounded ">
				<div class="header-modal col-12 bg-transparent">
					<div class="titulo-modal mt-4 col-4 offset-4">
						<span class="my-2">
							<center>
								Editar Impressora
							</center>
						</span>
					</div>
				</div>
				
				<div class="body-modal">
					<div class="col-12 px-1">
						<div class="row m-0 pt-2">
							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Apelido:</span>
								</div>
								<div class="celula-2 col-2">
									<input type="text" id="apelido" value="${item.apelido}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="10" >
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Nome do PC onde está instalada:</span>
								</div>
								<div class="celula-2 col-2">
									<input type="text" id="nomePC" value="${item.nomePC}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="10" >
								</div>
							</div> 

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Obs Impressora:</span>
								</div>
								<div class="celula-2 col-4">
									<input type="text" id="obs_impressora" value="${item.obs_impressora}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="20">
								</div>
							</div>


							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Ip da Impressora:</span>
								</div>
								<div class="celula-2 col-3">
									<input type="text" id="ip_impressora" value="${item.ip_impressora}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="12" >
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="col-12 px-1">
						<div class="row m-0 pt-2">

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Marca:</span>
								</div>
								<div class="celula-2 col-2">
									<input type="text" id="marca" value="${item.marca}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="10" >
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Modelo:</span>
								</div>
								<div class="celula-2 col-2">
									<input type="text" id="modelo" value="${item.modelo}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="10" >
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Largura:</span>
								</div>
								<div class="celula-2 col-2">
									<input type="text" id="largura" value="${item.largura}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="5" >
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Salto:</span>
								</div>
								<div class="celula-2 col-2">
									<input type="text" id="salto" value="${item.salto}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="5" >
								</div>
							</div>

							<div class="cont-celulas">
								<div class="celula-1">
									<span class="label-celula">*Porta LPT:</span>
								</div>
								<div class="celula-2 col-2">
									<input type="text" id="portaLPTEdit" value="${item.portaLPT}" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" maxlength="6" >
								</div>
							</div>

						</div>
					</div>		
				<div class="row footer-modal m-0">
					<div class="offset-8 col-2 my-2">
						<button class="col-12 btn btn-danger position-relative float-left" onclick="addAlt('fechar',true)">Sair</button>
					</div>
					<div class="col-2 my-2">
						<button id="editar" class="col-12 btn btn-success position-relative float-right">Salvar</button>
					</div>
				</div>
				</div>
			</div>
		</div>
	`;

	divMain.insertAdjacentHTML('afterbegin', txt);

	campoLPT = document.getElementById('portaLPTEdit');
	lptAtual = campoLPT.value;
	campoLPT.addEventListener('keyup', listenerCampoPortaLPT);

	document.getElementById('apelido').focus();

	let btnEditar = document.getElementById('editar');
	btnEditar.addEventListener('click', ()=>{salvar(item.id_impressora, true)})

}

async function salvar(id, edit = false){

	//lista de campos
	ConfImpressoras.apelido = document.getElementById('apelido').value.trim().toUpperCase();
	ConfImpressoras.nomePC = document.getElementById('nomePC').value.trim().toUpperCase();
	ConfImpressoras.portaLPT = edit ? document.getElementById('portaLPTEdit').value : document.getElementById('portaLPT').value;
	ConfImpressoras.obs_impressora = document.getElementById('obs_impressora').value.trim().toUpperCase();
	ConfImpressoras.ip_impressora = document.getElementById('ip_impressora').value.trim().toUpperCase();
	ConfImpressoras.marca = document.getElementById('marca').value.trim().toUpperCase();
	ConfImpressoras.modelo = document.getElementById('modelo').value.trim().toUpperCase();
	ConfImpressoras.largura = document.getElementById('largura').value.trim().toUpperCase();
	ConfImpressoras.largura > 0 ? true : ConfImpressoras.largura = 0 ;
	ConfImpressoras.salto = document.getElementById('salto').value.trim().toUpperCase();
	ConfImpressoras.salto > 0 ? true : ConfImpressoras.salto = 0 ;

	//exec('net use lpt4 \\' + )


	//verifica se campo esta vazio
	var conteudoNotNull = {
		apelido: ConfImpressoras.apelido,
		nomePC : ConfImpressoras.nomePC,
		portaLPT: ConfImpressoras.portaLPT,
		obs_impressora: ConfImpressoras.obs_impressora,
		
		ip_impressora:ConfImpressoras.ip_impressora,
		marca: ConfImpressoras.marca,		
		modelo:ConfImpressoras.modelo,

		largura:ConfImpressoras.largura,
		salto:ConfImpressoras.salto
	};

	let campoFaltando = false;

	for(var i in conteudoNotNull){
		if(conteudoNotNull[i] == '' || conteudoNotNull[i] == null){
			campoFaltando = true;
			document.getElementById(i).focus();
		}
	}

	//insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
	if(campoFaltando == false && id == undefined){
		event.preventDefault();
		addAlt('fechar');
		ConfImpressoras.insert().then((retorno)=>{insereItemNaTela(retorno)});		
	}else if(campoFaltando == false && id > 0){
		//event.preventDefault();
		ConfImpressoras.id_impressora = id;
		ConfImpressoras.update(ConfImpressoras.portaLPT !== lptAtual, lptAtual);
		addAlt('fechar',true);
		alteraItemNaTela(id,conteudoNotNull);
	}
}

function alteraItemNaTela(id,obj){

	let apelido = document.getElementById(`apelido${id}`);
	let obs = document.getElementById(`obs${id}`);
	let ipImp = document.getElementById(`ipImp${id}`);
	let marca = document.getElementById(`marca${id}`);

	apelido.innerHTML = obj.apelido;
	obs.innerHTML = obj.obs_impressora;
	ipImp.innerHTML = obj.ip_impressora;
	marca.innerHTML = obj.marca;
}

function insereItemNaTela(retorno,stringify){
	if(stringify != true){
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	let tbody = document.getElementById('lista');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_impressora}">							
		<th  class="border-right" scope="row">
		<center>
			<button class="btn-sem-decoracao editarItem" id="${retorno.id_impressora}" onclick="editarItem(this.id)">
				<img style="margin-left: -12px;" src="../img/pencil.png"/>
			</button>
			<button class="btn-sem-decoracao" id="${retorno.id_impressora}" onclick="excluirItem(this.id)">
				<img style="margin-left: -12px;" src="../img/trash.png"/>
			</button>
		</center>
		</th>
		<td class="border-right txt-label" id="apelido${retorno.id_impressora}">${retorno.apelido}</td>
		<td class="border-right txt-label" id="obs${retorno.id_impressora}">${retorno.obs_impressora}</td>
		<td class="border-right txt-num" id="ipImp${retorno.id_impressora}">${retorno.ip_impressora}</td>
		<td class="border-right txt-label" id="marca${retorno.id_impressora}">${retorno.marca}</td>
	</tr>`;
}

btnSalvar.addEventListener('click',()=>{salvar()});


module.exports = {
	selecionarTudo: ()=>{selecionarTudo()},
	selecionarItem: (item)=>{selecionarItem(item)},
	excluirItem: (item)=>{excluirItem(item)},
	insereItemNaTela: (item,status)=>{insereItemNaTela(item,status)}
};