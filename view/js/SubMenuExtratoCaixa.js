var cCadPedidos = require(path.join(__dirname + '/../../controller/cCadPedido.js'));
var cCadGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
var cItemPedido = require(path.join(__dirname + '/../../controller/cItemPedido.js'));
var cCaixas = require(path.join(__dirname + '/../../controller/cCaixa.js'));
var cHistoricoPgtos = require(path.join(__dirname + '/../../controller/cHistPgto.js'));
var cHistoricoVendas = require(path.join(__dirname + '/../../controller/cHistVenda.js'));
var Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
var cFPGTO = require(path.join(__dirname + '../../../controller/cFGPTO.js'));
var cPendura = require(path.join(__dirname + '../../../controller/cPendura.js'));
var Impressao = require(path.join(__dirname + '/Impressao.js'));

let dataInicialCaixas, dataFinalCaixas;
let objDataInicial, objDataFinal;
let gruposCadastrados = {};
let caixasDentroDoPeriodo = [];
let vendasDentroDoPeriodo = [];
let pedidosDentroDoPeriodo = [];
let pgtosPedidosDentroPeriodo = [];
let valorTotalPagamentos = 0;
let valorFundoCaixaSomado = 0;
let dadosPImpressao = {};
var os = require('os');
const nomePc = document.getElementById('nomePc');
const perCaixa = document.getElementById('perCaixa');
const pedRealizados = document.getElementById('pedRealizados');
const pedCancelados = document.getElementById('pedCancelados');
const entrouCaixa = document.getElementById('entrouCaixa');
const btnImp = document.getElementById('btnImprimeExtrato');

btnImp.addEventListener('click', produzImpressaoExtrato);
recuperaGruposCadastrados();

async function recuperaGruposCadastrados(){
    const grupos = await cCadGrupos.selectAll();
    for(g of grupos) gruposCadastrados[g.id_grupo] = g;
}

function preencheInformacoesCaixa(){
    nomePc.innerHTML = os.hostname();

    //#region Exibindo o periodo selecionado
    let dataIFormatada = dataInicialCaixas.split('-');
    let dataFFormatada = dataFinalCaixas.split('-');
    dataIFormatada = dataIFormatada[2] + '/' + dataIFormatada[1] + '/' + dataIFormatada[0];
    dataFFormatada = dataFFormatada[2] + '/' + dataFFormatada[1] + '/' + dataFFormatada[0];
    
    perCaixa.innerHTML = 'De ' + dataIFormatada + ' até ' + dataFFormatada;
    dadosPImpressao.periodo = {
        de: dataIFormatada,
        ate: dataFFormatada
    }
    //#endregion

    selecionaCaixasPeriodo();

}

async function selecionaCaixasPeriodo(){
    let caixasSalvos = await cCaixas.selectAll();

    const ini = new Date(objDataInicial);
    const fi = new Date(objDataFinal);

    ini.setHours(0);
    fi.setHours(0);
    for (caixa of caixasSalvos){
        if(caixa.dt_caixa >= ini && caixa.dt_caixa <= fi){
            caixasDentroDoPeriodo.push(caixa);
        }
    }

    //selecionaPedidosDentroPeriodo();
    selecionaPedidosLinkadosAosCaixas(caixasDentroDoPeriodo);
    //Aqui deve ser seleciona pedidos dentro dos caixas dentro do periodo

}

async function selecionaPedidosDentroPeriodo(){
    pedidosDentroDoPeriodo = await cCadPedidos.selectDentroData(objDataInicial, objDataFinal);
    objDataInicial.setDate(objDataInicial.getDate()-1)
    //objDataFinal.setDate(objDataFinal.getDate()-1)
    preencheInformacoesTabelas();
    return
    pedRealizados.innerHTML = pedidosDentroDoPeriodo.length;    
    selecionaPedidosCanceladosPeriodo();
}

async function selecionaPedidosLinkadosAosCaixas(caixas){
    let pedidosCaixa = [];
    for(caixa of caixas) pedidosCaixa.push( ...(await cHistoricoVendas.buscaVendasJoinPedidoFiltroCaixa(caixa.id_caixa)));

    pedidosDentroDoPeriodo.push(...(pedidosCaixa.map((pedido)=>{return pedido.tb_pedido})));
    objDataInicial.setDate(objDataInicial.getDate()-1)
    preencheInformacoesTabelas();
}

async function selecionaVendasPeriodo(){
    let pgtosPedidosDentroPeriodo = await cHistoricoPgtos.selectValorSomadoDentroPeriodo(objDataInicial, objDataFinal);

    let valorSomadoPagamentos = 0;
    for(pgto of pgtosPedidosDentroPeriodo){valorSomadoPagamentos += pgto.vl}
    
    entrouCaixa.innerHTML = 'R$ ' + Helper.valorEmReal(valorSomadoPagamentos.toFixed(2));
}

async function selecionaPedidosCanceladosPeriodo(){
    // usar esse filter no futuro
    let pedidosCanceladosPeriodo = pedidosDentroDoPeriodo.filter(ped => {return ped.status_pedido === 'CANCELADO';});
    pedCancelados.innerHTML = pedidosCanceladosPeriodo.length;
    selecionaVendasPeriodo();
}

//preencheInformacoesTabelas();

async function preencheInformacoesTabelas(){
    await preencheInfoPrimeiraTabela();
    await preencheInfoSegundaTabela();
    await preencheInfoTerceiraTabela();
    await preencheInfoQuartaTabela();
    await preencheInfoSextaTabela();
    await preencheInfoSetimaTabela();
}

async function preencheInfoPrimeiraTabela(){
    let camposPrimeiraTabela = ['fatBruto','fiadosFeitos','descontos1','valeCompra','despesas','fatLiquido','fiadosReceb','valorCaixa'];
    let dadosPTabela = {'fatBruto':0,'fiadosFeitos':0,'descontos1':0,'valeCompra':0,'despesas':0,'fatLiquido':0,'fiadosReceb':0,'valorCaixa':0};

    //#region Valor Bruto
    let valorSomadoBruto = 0;
    for(ped of pedidosDentroDoPeriodo){valorSomadoBruto += ped.vl_total}
    const valorTotalBruto = valorSomadoBruto;
    dadosPTabela.fatBruto = 'R$ ' + Helper.valorEmReal(valorTotalBruto.toFixed(2));
    //#endregion

    //#region Fiados feitos
    //Buscar no banco, quais os fiados feitos no periodo
    let fiadosFeitos = 0;
    let pendurasFeitasHoje = await cPendura.selectPendurasCriadasPeriodo(objDataInicial, objDataFinal);
    fiadosFeitos = pendurasFeitasHoje.length;
    dadosPTabela.fiadosFeitos = fiadosFeitos;
    //#endregion

    //#region pegando os valores de desconto
    let valorTotalDesconto = 0;
    const pedidosComDesconto = pedidosDentroDoPeriodo.filter(p => p.desconto > 0 ? p : null);
    for(pedido of pedidosComDesconto) valorTotalDesconto += pedido.desconto;

    dadosPTabela.descontos1 = 'R$ ' + Helper.valorEmReal(valorTotalDesconto.toFixed(2));
    //#endregion

    //#region vale compra
    const valorValeCompra = 0;
    dadosPTabela.valeCompra = 'R$ ' + Helper.valorEmReal(valorValeCompra.toFixed(2));
    //#endregion

    //#region Despesas
    const valorDespesas = 0;
    
    //Verificar se o troco entra como despesa

    dadosPTabela.despesas = 'R$ ' + Helper.valorEmReal(valorDespesas.toFixed(2));
    //#endregion

    //#region Fat. líquido
    pgtosPedidosDentroPeriodo = await cHistoricoPgtos.selectValorSomadoDentroPeriodo(objDataInicial, objDataFinal);
    let valorSomadoPagamentos = 0;
    for(pgto of pgtosPedidosDentroPeriodo){valorSomadoPagamentos += (pgto.vl - pgto.troco)};
    valorTotalPagamentos = valorSomadoPagamentos;
    dadosPTabela.fatLiquido = 'R$ ' + Helper.valorEmReal(valorTotalPagamentos.toFixed(2));
    //#endregion

    //#region Fiados recebidos
    const fiadosRecebidos = 0;
    //buscar na tabela de penduras, quais foram pagas
    dadosPTabela.fiadosReceb = 'R$ ' + Helper.valorEmReal(fiadosRecebidos.toFixed(2));
    //#endregion

    //#region Valor no caixa
    // Valor liquido + penduras pagas
    for(caixa of caixasDentroDoPeriodo){
        valorFundoCaixaSomado += caixa.fundo_caixa;
    }
    const valorCaixa = valorTotalPagamentos + fiadosRecebidos + valorFundoCaixaSomado;
    dadosPTabela.valorCaixa = 'R$ ' + Helper.valorEmReal(valorCaixa.toFixed(2)); //somar o valor do fundo de caixa
    //#endregion

    for(campo of camposPrimeiraTabela) document.getElementById(campo).innerHTML = dadosPTabela[campo];

    dadosPImpressao.primeira = dadosPTabela;
}

async function preencheInfoSegundaTabela(){
    let domSegundaTabela    = document.getElementById('segundaTabela');

    let dadosPSegundaTabela = [];

    const formasPagamentoCadastradas = await cFPGTO.selectAll();

    for(forma of formasPagamentoCadastradas){
        let somatoriaValorFormaPgto = 0;
        const formasPeriodoFiltrado = pgtosPedidosDentroPeriodo.filter(p=> p.id_fpgto === forma.id_fpgto ? p : null);
        for(f of formasPeriodoFiltrado) somatoriaValorFormaPgto += (f.vl - f.troco);
        dadosPSegundaTabela.push({descricao: forma.descricao, valorPago: ('R$ ' + Helper.valorEmReal(somatoriaValorFormaPgto.toFixed(2))) })
    }

    for(dado of dadosPSegundaTabela){
        domSegundaTabela.innerHTML += `
        <tr>
            <td>${dado.descricao}</td>
            <td>${dado.valorPago}</td>
        </tr>
        `;
    }

    //#region Pegando os pagamentos em pendura (que tem o id_fpgto como null)
    const pgtoPendurasPeriodoFiltrado = pgtosPedidosDentroPeriodo.filter(p=> p.id_fpgto === null ? p : null);
    let somatoriaValorFormaPgto = 0;
    for(f of pgtoPendurasPeriodoFiltrado) somatoriaValorFormaPgto += (f.vl - f.troco);
    const valorPagoPenduras = 'R$ ' + Helper.valorEmReal(somatoriaValorFormaPgto.toFixed(2));
    //#endregion

    const totalNoCaixa = valorTotalPagamentos + valorFundoCaixaSomado;

    domSegundaTabela.innerHTML += `
    <tr>
        <td>Penduras</td>
        <td>${valorPagoPenduras}</td>
    </tr>
    <tr>
        <td>Total Registrado</td>
        <td>${'R$ ' + Helper.valorEmReal(valorTotalPagamentos.toFixed(2))}</td>
    </tr>
    <tr>
        <td>Fundo de Caixa</td>
        <td>${'R$ ' + Helper.valorEmReal(valorFundoCaixaSomado.toFixed(2))}</td>
    </tr>
    <tr>
        <td>Total no Caixa</td>
        <td>${'R$ ' + Helper.valorEmReal(totalNoCaixa.toFixed(2))}</td>
    </tr>
    `;

    dadosPImpressao.segunda = [dadosPSegundaTabela, 
        {
            valorTotalPagamentos:  'R$ ' + valorTotalPagamentos.toFixed(2),
            valorFundoCaixaSomado: 'R$ ' + valorFundoCaixaSomado.toFixed(2),
            totalNoCaixa:          'R$ ' + totalNoCaixa.toFixed(2)
        }];
}

async function preencheInfoTerceiraTabela(){
    let domTerceiraTabela = document.getElementById('terceiraTabela');

    let somaAcrescimo = 0;
    let somaTxEntrega = 0;
    let somaTxServico = 0;
    let somaGorjeta = 0;

    for(p of pedidosDentroDoPeriodo){
        somaAcrescimo += p.acrescimo;
        somaTxEntrega += p.frete;
        somaTxServico += p.tx_serv;
        somaGorjeta = 0;
    }

    domTerceiraTabela.innerHTML += `
    <tr>
        <td>Acréscimos</td>
        <td>${'R$ ' + Helper.valorEmReal(somaAcrescimo.toFixed(2))}</td>
    </tr>
    <tr>
        <td>Taxas de entrega</td>
        <td>${'R$ ' + Helper.valorEmReal(somaTxEntrega.toFixed(2))}</td>
    </tr>
    <tr>
        <td>Taxas de serviço</td>
        <td>${'R$ ' + Helper.valorEmReal(somaTxServico.toFixed(2))}</td>
    </tr>
    <tr>
        <td>Gorjetas</td>
        <td>${'R$ ' + Helper.valorEmReal(somaGorjeta.toFixed(2))}</td>
    </tr>
    `;

    dadosPImpressao.terceira = {
        somaAcrescimo : 'R$ ' + somaAcrescimo.toFixed(2),
        somaTxEntrega : 'R$ ' + somaTxEntrega.toFixed(2),
        somaTxServico : 'R$ ' + somaTxServico.toFixed(2),
        somaGorjeta  :  'R$ ' + somaGorjeta.toFixed(2), 
    }

}

async function preencheInfoQuartaTabela(){

    let qtdComandasBaixadas = [];
    let qtdPedidosEntrega = [];
    let qtdPedidosBalcao = [];
    let qtdPedidosSalao = [];
    let qtdOutrosPedidos = [];

    for(p of pedidosDentroDoPeriodo){
        if(p.status_pedido === 'RECEBIDO'){
            qtdComandasBaixadas.push(p);
            if(p.tipo_entrega === 'ENTREGA') qtdPedidosEntrega.push(p)
            else if(p.tipo_entrega === 'BALCÃO') qtdPedidosBalcao.push(p)
            else if(p.tipo_entrega === 'MESA') qtdPedidosSalao.push(p)
            else qtdOutrosPedidos.push(p)
        }
    }
    /*qtdPedidosEntrega   = pedidosDentroDoPeriodo.filter(p => p.tipo_entrega === 'ENTREGA');
    qtdPedidosBalcao    = pedidosDentroDoPeriodo.filter(p => p.tipo_entrega === 'BALCÃO');
    qtdPedidosSalao     = pedidosDentroDoPeriodo.filter(p => p.tipo_entrega === 'MESA');*/
    //qtdOutrosPedidos    = pedidosDentroDoPeriodo.filter(p => p.tipo_entrega === '');

    document.getElementById('comBaixadas').innerHTML = qtdComandasBaixadas.length;
    document.getElementById('pedEntrega').innerHTML  = qtdPedidosEntrega.length;
    document.getElementById('pedBalcao').innerHTML   = qtdPedidosBalcao.length;
    document.getElementById('pedSalao').innerHTML    = qtdPedidosSalao.length;
    document.getElementById('pedOutros').innerHTML   = qtdOutrosPedidos.length;

    dadosPImpressao.quarta = {
        qtdComandasBaixadas,
        qtdPedidosEntrega,
        qtdPedidosBalcao,
        qtdPedidosSalao
    }

}

async function preencheInfoSextaTabela(){
    const descTamanho = {
        'br': "Broto",
        'me': "Media",
        'nn': "Normal",
        'gi': "Gigante"
    }

    let qtdsPizzas = {
        'combinadas' : {
            tamanho: 'combinadas',
            qtd: 0
        }
    };

    for(ped of pedidosDentroDoPeriodo){
        const prods = await cItemPedido.selectItensPedido(ped.id_pedido);
        const pizzas = prods.filter(prod=>{return prod.id_prod.indexOf('/') !== -1 });
        for(pzz of pizzas){
            const tmEscolhido = pzz.tam_escolhido;
            if(!qtdsPizzas[tmEscolhido]){
                qtdsPizzas[tmEscolhido] = {tamanho: descTamanho[tmEscolhido], qtd: 0};
            }
            qtdsPizzas[tmEscolhido].qtd += pzz.qtd_prod;

            if(pzz.id_prod.split('/').length > 2) qtdsPizzas['combinadas'].qtd += pzz.qtd_prod;
        }
    }

    for([id, item] of Object.entries(qtdsPizzas)) document.querySelector(`#pz${id}`).innerHTML = `${item.qtd}`;
}

async function preencheInfoSetimaTabela(){
    let domSetimaTabela = document.getElementById('setimaTabela');

    let codigosDosPedidos = [];

    for(p of pedidosDentroDoPeriodo){
        codigosDosPedidos.push(p.id_pedido);
    }

    const resultadoQuery = await cItemPedido.selectQtdVendidaPorGrupo(codigosDosPedidos);
    let conteudoHTML = `
    <tr>
        <td colspan="2" class="tituloTabela">Outros grupos vendidos</td>
    </tr>
    `;

    let dadosSetima = [];

    for(obj of resultadoQuery){
        conteudoHTML += `
        <tr>
            <td>${gruposCadastrados[obj.id_grupo].descricao}</td>
            <td>${obj.sum}</td>
        </tr>
        `;
        
        dadosSetima.push({
            nomeGrupo : gruposCadastrados[obj.id_grupo].descricao,
            qtd : obj.sum
        })
    }

    domSetimaTabela.innerHTML = conteudoHTML;

    dadosPImpressao.setima = dadosSetima;
    
}

function selecionarTudo(){
    dataInicialCaixas = window.sessionStorage.getItem('dataInicialCaixa');
    dataFinalCaixas = window.sessionStorage.getItem('dataFinalCaixa');

    objDataInicial = new Date(dataInicialCaixas);
    objDataFinal = new Date(dataFinalCaixas);
    objDataInicial.setDate(objDataInicial.getDate()+1)
    objDataFinal.setDate(objDataFinal.getDate()+1)

    /*objDataInicial.setHours(-3);
    objDataFinal.setHours(21);
    objDataInicial.setMinutes(0);
    objDataFinal.setMinutes(0);
    objDataInicial.setSeconds(0);
    objDataFinal.setSeconds(0);*/

    preencheInformacoesCaixa();
}

function produzImpressaoExtrato(){
    swal({
       icon: 'success',
       title: 'Função em deesenvolvimento',
       closeOnClickOutside:true,
       text: 'Função indisponível pra teste',
       button: {text: 'Ok', closeModal: true}
    });

    Impressao.imprimeExtratoCaixa(dadosPImpressao);
}

module.exports ={
    selecionarTudo: selecionarTudo
}