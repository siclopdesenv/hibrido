//#region Variáveis
const path              = require('path');
let btHome              = document.getElementById("btHome");
let load_circle         = document.getElementById("load-circle");
let arquivo             = document.getElementById("arquivoCardapio");

let Firebase            ;
let refNoFotos          ;
let referencia          ;
let ManipulaJanela      ;

let porcentagemProgresso = 0;
//#endregion

//#region IMPORTAÇÃO DOS MODULOS NODE
const fs = require('fs');
Firebase            = require(path.join(__dirname, '..\\src\\Firebase'));
//#endregion

let noFotos             = [];
let infoProdutos        = [];
let infoCategorias      = [];
let infoItemExtras      = [];

let qtdProdutos = 0;
let qtdCategorias = 0;
let qtditens = 0;

refNoFotos = Firebase.ref('fotos_produtos');

async function recuperaNoFotos(caminhoArquivo){
     await refNoFotos.on('value', (snapshot)=>{
        noFotos = JSON.parse(JSON.stringify(snapshot))[codigoADM];
        leArquivo(caminhoArquivo, noFotos);
    });
}

async function migrarCardapio(caminhoArquivo){
    await recuperaNoFotos(caminhoArquivo);
}

insereTodasInformacoes = ()=>{
    insereProdutos();
    insereCategorias();
    insereItensExtras();
}

leArquivo = (caminho, noFotos) => {

    let conteudoArquivo = fs.readFileSync(caminho, 'utf-8');

    conteudoArquivo = (conteudoArquivo.replace(new RegExp(String.fromCharCode(13),'g'), ''));

    conteudoArquivo = conteudoArquivo.split('\n');

    let retornoFuncao;

    for (let i = 0; i < conteudoArquivo.length; i++) {
        //Percorrendo o arquivo inteiro para separas as informações de


        if(conteudoArquivo[i].match('PRODUTOS ---')){

            retornoFuncao       = retornainformacoesEspecificas(conteudoArquivo, i);

            infoProdutos        = (retornoFuncao[0]);
            i                   = retornoFuncao[1];


        }else if(conteudoArquivo[i].match('CATEGORIA---')){

            retornoFuncao       = retornainformacoesEspecificas(conteudoArquivo, i);

            infoCategorias      = (retornoFuncao[0]);
            i                   = retornoFuncao[1];

        }else if(conteudoArquivo[i].match('ITEM EXTRA')){

            retornoFuncao       = retornainformacoesEspecificas(conteudoArquivo, i);

            infoItemExtras      = (retornoFuncao[0]);
            i                   = retornoFuncao[1];

        }

    }

    insereTodasInformacoes(noFotos);
};

retornainformacoesEspecificas = (conteudoArquivo, i) => {
    let informacoes = [];

    for (let j = i; j < conteudoArquivo.length; j++) {

        if(conteudoArquivo[j] !== ""){

            informacoes.push(conteudoArquivo[j]);

        }else{

            i = j;
            break;

        }

    }

    return [informacoes, i];

};
//#endregion

//#region Inserindo produtos no Firebase

insereProdutos = (noFotos) => {
    console.log(noFotos);
    //const url_foto_padrao = "https://firebasestorage.googleapis.com/v0/b/olhonorango-4bb82.appspot.com/o/imagens%2Frest_6996-6996?alt=media&token=500c44dc-60e2-4410-90d7-6a27c5afe7bc";
    const url_foto_padrao = "";
    referencia = Firebase.ref('cardapios');//pego o nó onde estão os produtos do estabelecimento

    //Apagar os produtos do banco || ou desativalos

    let protduto = {
        broto:"0",
        char_categoria:"",
        categoria:"",
        codigo_produto:"",
        descricao:"",
        gigante:"0",
        key_estabelecimento:"",
        media:"0",
        nome:"",
        observacoes:"",
        promocao:false,
        valor:"",
        url_foto_prod: url_foto_padrao
    };

    let categoria;

    for (let i = 1; i < infoProdutos.length; i++) {

        categoria = infoProdutos[i].substring(7,26).trim();

        categoria === 'PIZZA' || categoria === 'PIZZAS' ? categoria = 'Pizza' : null ;

        const codProd = infoProdutos[i].substring(0,4).trim();
        protduto.codigo_produto     = codProd;
        protduto.char_categoria     = infoProdutos[i].substring(5,6).trim();
        protduto.categoria          = categoria;
        protduto.nome               = infoProdutos[i].substring(28,67).trim();
        protduto.descricao          = infoProdutos[i].substring(69,169).trim();
        protduto.valor              = infoProdutos[i].substring(170,176).trim();
        protduto.broto              = infoProdutos[i].substring(177,183).trim();
        protduto.media              = infoProdutos[i].substring(184,190).trim();
        protduto.gigante            = infoProdutos[i].substring(191,198).trim();
        protduto.observacoes        = "";
        protduto.promocao           = false;
        protduto.url_foto_prod      = noFotos ? (noFotos[codProd] ? noFotos[codProd].url : url_foto_padrao) : url_foto_padrao;

        //Jogar produtos dentro de um vetor e inserir com bulk insert
    }

};

//#endregion

//#region Inserindo categorias no Firebase
insereCategorias = () => {
    referencia = Firebase.ref('categorias'); //pego o nó onde estão as categorias do estabelecimento

    //Apagar categorias || ou desativalas

    let categoria = {
        nome_categoria : ""
    };

    let nomeCategoriaAux ;

    for (let i = 1; i < infoCategorias.length; i++) {
        nomeCategoriaAux = infoCategorias[i].substring(5,30).trim();

        if(nomeCategoriaAux !== 'PIZZAS' && nomeCategoriaAux !== 'ACOMPANHAMENTO' && nomeCategoriaAux !== 'BEIRUTES' && nomeCategoriaAux !== 'PIZZA' && nomeCategoriaAux !== 'FOGAÇAS' && nomeCategoriaAux !== 'SUCOS' && nomeCategoriaAux !== 'ALMOÇOS' && nomeCategoriaAux !== 'ESFIHAS' && nomeCategoriaAux !== 'LANCHES' && nomeCategoriaAux !== 'REFRIGERANTES'){
            categoria.nome_categoria = nomeCategoriaAux;
            //Jogar categorias dentro de um vetor e inserir com bulk insert
        }

    }

};
//#endregion

//#region Inserindo itens Extra no Firebase
insereItensExtras = () => {
    referencia = Firebase.ref('itemExtras'); //pego o nó onde estão os itens extras do estabelecimento

    //Apagar itens extras ou desativalos

    let itemExtra = {
        categoria_item:{
            
        },
        codigo_item_extra : "",
        nome_item : "",
        valor_item : ""
    };

    for (let i = 1; i < infoItemExtras.length; i++) {
        itemExtra.categoria_item = {};
        
        let categ = infoItemExtras[i].substring(0,20).trim();

        if(categ === 'PIZZAS') categ = 'Pizza';
        
        itemExtra.categoria_item = JSON.parse('{ "' + categ + '": "Sim"}');

        itemExtra.nome_item     = infoItemExtras[i].substring(20,44).trim();
        itemExtra.valor_item    = infoItemExtras[i].substring(45,50).trim();
        itemExtra.codigo_item_extra = infoItemExtras[i].substring(54,59).trim();

        itemExtra.pausa = false;

        //Jogar itens extras dentro de um vetor e inserir com bulk insert
    }
};
//#endregion

module.exports = migrarCardapio;