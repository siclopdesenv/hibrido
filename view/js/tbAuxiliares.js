let swal = require('sweetalert');
let idDosCampos = 0;

async function recuperaDadosBanco(fragment, isterminais, isPeriodo){
	controller = require(path.join(__dirname + '/../../controller/c' + fragment));

	let retorno = controller.selectAll();

	/*controller.descricao = 'R';
	controller.like().then(data => {
		let retornol = JSON.parse(JSON.stringify(data));
		console.log('Retorno like => ' + JSON.stringify(data));
	});*/

	await retorno.then(data => {
		retorno = JSON.parse(JSON.stringify(data));
	});

	percorreArrayRetorno(retorno, isterminais, isPeriodo);
}

function percorreArrayRetorno(dados, isterminais, isPeriodo){
	for (let i = 0; i < dados.length; i++) {
		//Object.values(dados[i])[1]; <-- maneira de pegar o valor sem saber o nome do atributo, nesse caso eu estou pegando o dado atual e o segundo valor dele, ou o segundo atributo representado pelo [1] ao invés do nome do atributo
		isterminais === true ? insereInformacoesNaTelaTerminais(Object.values(dados[i])[0], Object.values(dados[i])[1]) : (isPeriodo === true) ? insereInformacoesNaTelaPeriodos(Object.values(dados[i])[0], Object.values(dados[i])[1], Object.values(dados[i])[2], Object.values(dados[i])[3]) : insereInformacoesNaTela(Object.values(dados[i])[0], Object.values(dados[i])[1]);
		
		idDosCampos ++;
	}
}

function insereInformacoesNaTela(id, descricao){
	let idTrDados	= 'idTrDados'+ idDosCampos;
	let idBtEdit 	= 'edit' 	+ idDosCampos;
	let idBtDelete 	= 'delete' 	+ idDosCampos;
	let idLblId 	= 'id' 		+ idDosCampos;
	let idLblDesc 	= 'desc' 	+ idDosCampos;

	let tbody = document.getElementById('listaConteudoDoBanco');

	let tr = document.createElement('tr');
	tr.setAttribute('id',idTrDados);
	tr.innerHTML = `<tr>
	<th  class="border-right" scope="row">
	<center>
	<button class="btn-sem-decoracao" id="${idBtEdit}">
	<img style="margin-left: -12px;" src="../img/pencil.png"/>
	</button>
	<button class="btn-sem-decoracao" id="${idBtDelete}">
	<img style="margin-left: -12px;" src="../img/trash.png"/>
	</button>
	</center>
	</th>
	<td class="border-right" id="${idLblId}" ><center>${id}</center></td>
	<td class="border-right" id="${idLblDesc}" >${descricao}</td>
	</tr>`;

	try{

		tbody.appendChild(tr);

		document.getElementById(idBtEdit).addEventListener('click', ()=>{
			modalAlterar(id, descricao, idLblDesc).then(result => {
				document.getElementById(idLblDesc).innerHTML = `${result}`;
			});
		});
		
		document.getElementById(idBtDelete).addEventListener('click', ()=>{
			apagarRegistro(id, descricao, idTrDados);
		});

	}catch(ex){
		ex.toString().match('appendChild') ? insereInformacoesNaTelaSemAddItem(id, descricao) : alert(ex);
	}

}

function insereInformacoesNaTelaPeriodos(id, descricao, horaComeco, horaTermino){
	let tbody = document.getElementById('listaFuncoes');
	let idBtSave = "btnSavePeriodo"+ id;
	let idInputComeco = "horaComeco" + idDosCampos;
	let idInputTermino = "horaTermino" + idDosCampos;

	tbody.innerHTML += `
	<tr>
	<th>
	<center>
	<button class="btn-sem-decoracao" id="${idBtSave}">
	<img src="../img/disket.png"/>
	</button>
	</center>
	</th>
	<td>
	${descricao}
	</td>
	<td>
	<div class="input-group input-group-sm" >
	<input type="time" id="${idInputComeco}" value="${horaComeco}">
	</div>
	</td>
	<td>
	<div class="input-group input-group-sm" >
	<input type="time" id="${idInputTermino}" value="${horaTermino}">
	</div>
	</td>
	</tr>
	`;

	setTimeout(()=>{
		document.getElementById(idBtSave).addEventListener('click',(e)=>{
			controller.id = id;
			controller.hora_inicio = document.getElementById(idInputComeco).value;
			controller.hora_termino = document.getElementById(idInputTermino).value
		
			//console.log(Helper.horaParaMinuto(controller.hora_inicio) + ' --- ' + Helper.horaParaMinuto(controller.hora_termino));
		
			if(!validaHorario(controller.hora_inicio, controller.hora_termino, descricao.toUpperCase() === 'NOITE')){
				swal({
					text: 'O horário de término deve ser maior que o de início!',
					icon: 'warning'
				});
			}else{
				controller.update()
				.then((retorno)=>{
					swal({
						text: 'Horário atualizado!',
						icon: 'success'
					});
				})
				.catch(err =>{
					swal({
						text: err,
						icon: 'error'
					})
				});
			}
		
		});
	},500);
	

	function validaHorario(inicio, termino, ehnoite = false) {
		inicio = Helper.horaParaMinuto(inicio);
		termino = Helper.horaParaMinuto(termino);

		if(inicio >= termino && !ehnoite){
			swal({
				text: 'O horário de término deve ser maior do que o de inicio!',
				icon: 'error'
			})
			return false;
		}

		return true;
		
	}

}

function insereInformacoesNaTelaTerminais(idTpTerminal, descricao){
	let idTrDados	= 'idTrDados'+ idDosCampos;
	let idBtEdit 	= 'edit' 	+ idDosCampos;
	let idBtDelete 	= 'delete' 	+ idDosCampos;
	let idLblId 	= 'id' 		+ idDosCampos;
	let idLblIdTpTerm = 'idtpterminal' 		+ idDosCampos;
	let idLblDesc 	= 'desc' 	+ idDosCampos;

	let tbody = document.getElementById('listaAddItem');

	let tr = document.createElement('tr');
	tr.setAttribute('id', idTrDados);

	tr.innerHTML = `<tr>
	<th  class="border-right" scope="row">
	<center>
	<button class="btn-sem-decoracao" id="${idBtEdit}">
	<img style="margin-left: -12px;" src="../img/pencil.png"/>
	</button>
	<button class="btn-sem-decoracao" id="${idBtDelete}">
	<img style="margin-left: -12px;" src="../img/trash.png"/>
	</button>
	</center>
	</th>
	<td class="border-right" id="${idLblIdTpTerm}" ><center>${idTpTerminal}</center></td>
	<td class="border-right" id="${idLblDesc}" >${descricao}</td>
	</tr>`;

	tbody.appendChild(tr);

	document.getElementById(idBtEdit).addEventListener('click', ()=>{
		modalAlterarTerminal(idTpTerminal, descricao).then(result => {
			console.log(result);
			document.getElementById(idLblDesc).innerHTML = `${result}`;
		});
	});
	
	document.getElementById(idBtDelete).addEventListener('click', ()=>{
		apagarRegistro(idTpTerminal, descricao, idTrDados);
	});

}

function insereInformacoesNaTelaSemAddItem(id, descricao){

	let idBtEdit 	= 'edit' 	+ idDosCampos;
	let idLblId 	= 'id' 		+ idDosCampos;
	let idLblDesc 	= 'desc' 	+ idDosCampos;

	let tbody = document.getElementById('listaFuncoes');

	let tr = document.createElement('tr');

	tr.innerHTML = `
	<tr>
	<th  class="border-right" scope="row">
	<center>
	<button class="btn-sem-decoracao" id="${idBtEdit}">
	<img style="margin-left: -12px;" src="../img/pencil.png"/>
	</button>
	</center>
	</th>
	<td class="border-right" id="${idLblId}" ><center>${id}</center></td>
	<td class="border-right" id="${idLblDesc}" >${descricao}</td>
	</tr>
	`;

	tbody.appendChild(tr);

	document.getElementById(idBtEdit).addEventListener('click', ()=>{
		modalAlterar(id, descricao).then(result => {
			document.getElementById(idLblDesc).innerHTML = `${result}`;
		});
	});

}

function apagarRegistro(id, descricao, idTr){

	try{
		controller.buscaRelacoes(id).then(retorno => { 
			console.log('\n\n' + JSON.stringify(retorno) +'\n\n');
			if(retorno.length !== 0 && retorno !== 0){
				swal({
					icon:'warning',
					title:'Ação não permitida!',
					text: 'Existe(m) ' + retorno.length + ' registro(s) relacionado(s) com essa informação, por favor altere-o(s) antes de excluir este dado!'
				});
			}else{
				alertApagarRegistro(id, descricao, idTr);
			}
		});
	}
	catch(ex){
		console.log('Deu pinto');
		alertApagarRegistro(id, descricao, idTr);
	}

}

function alertApagarRegistro(id, descricao, idTr){
	swal('Tem certeza que deseja excluir - "' + descricao + '"?',
	{
		icon: "warning",
		buttons: ["Sim", "Não"],
	}).then((confirmation)=>{
		if(!confirmation){
			controller.id = id;
			controller.delete().then((rows)=>{
				rows == 1 ? swal('\"' + descricao + '\" removido!', {icon:"success"}) : swal('Não removido - > ' + rows);
				document.getElementById(idTr).innerHTML = '';
			});
		}
	});
}

async function modalAlterar(id, descricao, idCampo = false){
	let novo = "";
	let descCampo;

	if(!idCampo)
		idCampo = id;

	try {
		descCampo = document.getElementById(idCampo).innerHTML;
	} catch(e) {
		descCampo = descricao;
	}

	setTimeout(()=>{
		document.getElementsByClassName('swal-content__input')[0].value += descCampo;
		document.getElementsByClassName('swal-content__input')[0].focus();
	},100);

	await swal({
		text: 'Alterar "' + descCampo + '" para:',
		content: "input",
		buttons: ["Cancelar", "Salvar"],
	}).then(novaDescricao => {

		if (!novaDescricao) return descCampo;
		
		//console.log(novaDescricao);
		controller.id = id;
		controller.descricao = novaDescricao.toUpperCase();
		controller.update();

		return novaDescricao.toUpperCase();

	}).then(result=>{
		novo = result;
	})

	return novo;

}

async function modalAlterarTerminal(idTpTerminal,descricao){
	let novo = "";

	await swal({
		text: 'Alterar "' + descricao + '" para:',
		content: "input",
		buttons: ["Cancelar", "Salvar"],
	}).then(novaDescricao => {
		if (!novaDescricao) return descricao;
		
		controller.id_tpterminal = idTpTerminal;
		controller.descricao = novaDescricao;
		controller.update();

		return novaDescricao;

	}).then(result=>{
		novo = result;
	})

	return novo;

}

function addCampos(campoHabilitado = false) {

	console.log(campoHabilitado);
	var tbody = document.getElementById('listaAddItem');

	let idTR 	= 'tr_insert' + qtdItens;
	let id 		= 'id' + qtdItens;
	let save 	= 'save' + qtdItens;
	let nome 	= 'nome' + qtdItens;

	let tr = document.createElement('tr');
	tr.setAttribute("id", 'tr_insert' + qtdItens);
	tr.innerHTML = `<tr class="aparecendo"><th><center><button class="btn-sem-decoracao" id="${save}"><img src="../img/disket.png"/></button></center></th><td></td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="Descricao" id="${nome}" maxlength="15"></div></td></tr>`;
	tbody.appendChild(tr);
	document.getElementById(save).addEventListener('click', (e) => {
		e.preventDefault();

		//controller.id 			= document.getElementById(id).value;
		controller.descricao 	= document.getElementById(nome).value.toUpperCase();
		campoHabilitado ? controller.habilitado	= true : null; // Verificando se essa controller necessita do campo "habilitado"
		if(campoHabilitado === 'unidadeMedida') controller.sigla = controller.descricao.substring(0,2);

		if(controller.descricao != ""){
			controller.insert().then(result=>{
				result = JSON.parse(JSON.stringify(result));
				insereInformacoesNaTela(Object.values(result)[0], controller.descricao);
				document.getElementById(idTR).innerHTML = '';
			});

			idDosCampos++;
		}else{
			swal({
				text: "O campo não pode ser vazio!",
				icon: 'error'
			})
		}
	});
}

function addCamposTerminais(fragment, idDom) {

	let cont = require(path.join(__dirname + '/../../controller/cTerminal'));

	var tbody = document.getElementById('listaAddItem');
	let save = 'save' + qtdItens;
	let idTerminal = 'terminal' + qtdItens;
	let nome = 'nome' + qtdItens;
	let tr = document.createElement('tr');
	//tr.innerHTML = `<tr class="aparecendo"><th><center><button class="btn-sem-decoracao" id="${save}"><img src="../img/add-auxiliar.png"/></button></center></th><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="ID"></div> </td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="ID Tipo Terminal" id="${idTerminal}"></div></td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="Descrição" id="${nome}"></div> </td></tr>`;
	//tbody.appendChild(tr);
	// document.getElementById(save).addEventListener('click', (e) => {
	// 	e.preventDefault();
	// });
	//cont["id"] 			  = document.getElementById("terminal"+idDom).value;
	cont["tipo_terminal"] = idTpTerminal = document.getElementById("terminal"+idDom).value.toUpperCase();
	cont["descricao"]     = descTerminal = document.getElementById("nome"+idDom).value.toUpperCase();
	cont.insert().then(e=>{
		document.getElementById(`trTemp${idDom}`).remove();
		cont.selectAll().then(e =>{
			insereInformacoesNaTelaTerminais(idTpTerminal, descTerminal);
		});		
	});
}

function addCamposUnidMed(fragment, idDom){
	controller.sigla = 'TS';

	//refazer toda essa parte de baixo

	let cont = require(path.join(__dirname + '/../../controller/cTerminal'));

	var tbody = document.getElementById('listaAddItem');
	let save = 'save' + qtdItens;
	let idTerminal = 'terminal' + qtdItens;
	let nome = 'nome' + qtdItens;
	let tr = document.createElement('tr');
	//tr.innerHTML = `<tr class="aparecendo"><th><center><button class="btn-sem-decoracao" id="${save}"><img src="../img/add-auxiliar.png"/></button></center></th><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="ID"></div> </td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="ID Tipo Terminal" id="${idTerminal}"></div></td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="Descrição" id="${nome}"></div> </td></tr>`;
	//tbody.appendChild(tr);
	// document.getElementById(save).addEventListener('click', (e) => {
	// 	e.preventDefault();
	// });
	//cont["id"] 			  = document.getElementById("terminal"+idDom).value;
	cont["tipo_terminal"] = idTpTerminal = document.getElementById("terminal"+idDom).value;
	cont["descricao"]     = descTerminal = document.getElementById("nome"+idDom).value;
	cont.insert().then(e=>{
		document.getElementById(`trTemp${idDom}`).remove();
		cont.selectAll().then(e =>{
			//console.log(e);
			insereInformacoesNaTelaTerminais(e[e.length - 1].id_terminal,idTpTerminal, descTerminal);
		});		
	});

}

module.exports = {
	recuperaDadosBanco: recuperaDadosBanco,
	addCampos: addCampos,
	addCamposTerminais: addCamposTerminais
}