const btnExtratoCaixa = document.getElementById('btnExtrato');
btnExtratoCaixa.addEventListener('click', listenerExtrato)
const btnPendura = document.getElementById('btnPenduras');
btnPendura.addEventListener('click', listenerPendura)
const btnSangria = document.getElementById('btnSangria');
btnSangria.addEventListener('click', exibeSwal)
const btnAReceber = document.getElementById('btnAReceber');
btnAReceber.addEventListener('click', exibeSwal)
const btnEntrega = document.getElementById('btnEntrega');
btnEntrega.addEventListener('click', exibeSwal)
const btnRecibo = document.getElementById('btnRecibo');
btnRecibo.addEventListener('click', exibeSwal)

var dataHoje = new Date();
const dataLimite = dataHoje.getFullYear() + '-' + String(dataHoje.getMonth() + 1).padStart(2,'0') + '-' + String(dataHoje.getDate()).padStart(2,'0');
console.log(dataLimite);
const campoDataInicial = document.getElementById('dtInicial');
const campoDataFinal = document.getElementById('dtFinal');
campoDataInicial.addEventListener('change',()=>{
    campoDataFinal.min = campoDataInicial.value;
});
campoDataInicial.max = dataLimite;
campoDataFinal.max = dataLimite;

function verificaDatas(){
    const dataInicial = campoDataInicial.value;
    const dataFinal = campoDataFinal.value;
    if(dataInicial === '' || dataFinal === ''){
        swal({
           icon: 'warning',
           title: 'Preencha as duas datas',
           closeOnClickOutside:true,
           text: '',
           button: {text: 'okay', closeModal: true}
        });
        return false;
    }

    window.sessionStorage.setItem('dataInicialCaixa', dataInicial);
    window.sessionStorage.setItem('dataFinalCaixa', dataFinal);
    return true;
}

function listenerPendura(){
    console.log('PENDURA');
    const retornoValidacao = verificaDatas();
    if(retornoValidacao) carregaFrame('frameSubMenuPenduras', 'SubMenu');
}

function listenerExtrato(){
    const retornoValidacao = verificaDatas();
    if(retornoValidacao) carregaFrame('frameSubMenuExtratoCaixa');
}

function exibeSwal(context) {
    console.log(context);
    swal({
       icon: 'warning',
       title: context.toElement.innerText,
       closeOnClickOutside:true,
       text: 'Em desenvolvimento',
       button: {text: 'okay', closeModal: true}
    });
}

function selecionarTudo(){

}

module.exports ={
    selecionarTudo: selecionarTudo
}