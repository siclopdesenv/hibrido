var $ = require("jquery");

var path = require('path');
const swal = require("sweetalert");
const { isObject } = require("util");
var MouseTrap = require('mousetrap');

window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));

var dadosEstab = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
var Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
const cCadClientes = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
const CadEnderecos = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));
const CadMunicipio = require(path.join(__dirname + '/../../controller/cMunicipio.js'));
const helperCampoCEP = require(path.join(__dirname + '/../../helpers/helperCampoCep.js'));
var btnSalvar = document.getElementById('cadastrar');
var btnAdicionar = document.getElementById('btnAdicionar');
let campoCep = document.getElementById('cep_casa');
let btAddAlt = document.getElementById('addAlt');

//---------- declaração de mascaras
	document.getElementById('fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
	document.getElementById('fone2').addEventListener('keyup', Helper.phoneMaskSemDDD);
	document.getElementById('fone3').addEventListener('keyup', Helper.phoneMaskSemDDD);
	//document.getElementById('desconto').addEventListener('keyup', helper.maskMoeda(this,event));
	document.getElementById("dt_nasc").setAttribute("max", Helper.dataAtualEua(true));
	document.getElementById("cep_casa").addEventListener("keyup", ()=>{
		Helper.controlaTamanhoCampo(document.getElementById("cep_casa"), 8);
	});
// ----------fim

let ultimoCodCadastrado = 0;

setTimeout(() => {
	MouseTrap.bind('f1', function () {
		btAddAlt.click();
	})
}, 1000);

MouseTrap.bind('esc', function () {
    addAlt('fechar');
})

setTimeout(()=>{
	btAddAlt.disabled = false;
	btAddAlt.addEventListener('click',()=>{
		recuperaUltimoCodCadastrado();
		limparNumerosSection();
		document.getElementById('ddd_cli').value = dadosEstab.cfg_ddd;
	});
},500);

//funcoes bonitas

async function recuperaUltimoCodCadastrado(){
	let ex = await cCadClientes.selectCodMax();
	try{
		atualizaCampoCodCliente(ex[(ex.length - 1)].cod_cli);
	}catch(err){
		atualizaCampoCodCliente(0);
	}
}

function atualizaCampoCodCliente(codigo = 0){
	ultimoCodCadastrado = codigo + 1;
	document.getElementById('cod_cli').value = ultimoCodCadastrado;
}

async function listenFocusOutCep(){
	if(campoCep.value.length === 8){
		campoCep.style.borderColor = '#00FF00';
		const campoLogradouro = document.getElementById('logradouro_casa');
		const campoBairro = document.getElementById('bairro_casa');
		const campoCidade = document.getElementById('cidade_casa');
		let retornoHelper = await helperCampoCEP(CadEnderecos, campoCep, campoLogradouro, campoBairro, campoCidade);
		if(!retornoHelper){
			campoCep.style.borderColor = '#FF0000';
			return;
		}
		let cidade = await CadEnderecos.recuperaCidade(retornoHelper.id_municipio);
		campoCidade.value = cidade.descricao;
		console.log(retornoHelper);
		isObject(retornoHelper) ? cCadClientes.id_logradouro = retornoHelper.id_logradouro : retornoHelper === null ? swal({icon:'error', title:'CEP inexistente!'}) : null;
	}else{
		campoCep.style.borderColor = '#FF0000';
	}
}

campoCep.addEventListener('focusout', listenFocusOutCep); 

async function selecionarTudo(){
	document.getElementById('listaProdutos').innerHTML = '';
	let cCadClientes = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
	let listaClientes = cCadClientes.selectAll();

	listaClientes.then(async (data) => {
		let actuallyData = await JSON.parse(JSON.stringify(data));
		for(var i of actuallyData){insereItemNaTela(i,true);}
	});
}

function limparNumerosSection(){
	document.getElementById('section-contato').innerHTML = "";
}

btnAdicionar.addEventListener('click', function(){adicionarNovoTelefone()})

function adicionarNovoTelefone(){
	let txt = `
		<div class="cont-celulas">
			<div class="celula-1">
				<span class="label-celula">Telefone:*</span>
			</div>
			<div class="celula-2 col-3">
				<input id="fone1" type="text" class="form-control" aria-label="Sizing example input" name="Telefone" aria-describedby="inputGroup-sizing-default" placeholder="">
			</div>
		</div>
	`;

	$('#section-contato').append(txt);

}

async function selecionarItem(item){
	//inserindo tela de alteração
	const script = require(path.join(__dirname + '/script.js'));
	let divMain = document.getElementById('conteudoMain');
	let data_nasc_formatado = Helper.converteDataBancoPDataCampo(new Date(item.dt_nasc));
	idSelecionado = item.id_logradouro;
	let Logradouro = await CadEnderecos.select(item.id_logradouro);
	let Municipio = await CadMunicipio.select(Logradouro.id_municipio)
	let txt = `
	<div id="camadaModal" style="display:block;" class="cria-camada-modal h-100 w-100 pl-5" >
		<div class="modal-info-imp position-relative col-10 offset-1 rounded pt-3 pb-5 px-5">		
			<div class="header-modal col-12 p-0">
				<button id="btn-aba-principal" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 ml-3 abas-cad-ativa" >
					<label id="lbl-aba-principal" class="titulo-modal titulo-modal-info-sis titulo-modal-cad titulo-modal-info-sis-ativa" disabled="" >
						Dados do Cliente
					</label>
				</button>

				<button id="btn-aba-tercearia" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 px-0" >
					<label id="lbl-aba-tercearia" class="titulo-modal titulo-modal-info-sis" disabled="" >
						Endereço
					</label>
				</button>

				<button id="btn-aba-secundaria" onclick="acoesAbaModais(this.id);" class="btn-sem-decoracao aba-modal abas-cad w-25 px-0" >
					<label id="lbl-aba-secundaria" class="titulo-modal titulo-modal-info-sis" disabled="" >
					 	Financeiro
					</label>
				</button>
			</div>
			
			<div class="body-modal py-4">
				<div id="dados-principais-modal" style="display:block;">
				<hr>
				<div class="col-12">
					<div class="row m-0">
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Código*:</span>
							</div>
							<div class="celula-2 col-2">
							<input id="cod_cli" value="${item.cod_cli}" type="text" class="form-control" aria-label="Sizing example input" name="Código" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="7" disabled>
							</div>
						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Nome*:</span>
							</div>
							<div class="celula-2 col-5">
								<input id="nome_cli" value="${item.nome_cli}" type="text" class="form-control" aria-label="Sizing example input" name="Nome" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="40">
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="col-12">
					<div class="row m-0 ">

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Data de Nascimento:</span>
							</div>
							<div class="celula-2 col-4">
								<input id="dt_nasc" value="${data_nasc_formatado}" type="date" class="form-control" aria-label="Sizing example input" name="Dt Validade" aria-describedby="inputGroup-sizing-default" >
							</div>
						</div>

					</div>
				</div>
				<div class="col-12">
					<div class="row m-0 ">
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">DDD*:</span>
							</div>
							<div class="celula-2 col-1">
								<input id="ddd_cli" type="text" class="form-control" aria-label="Sizing example input" name="DDD" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="3" value="${item.ddd_cli}">
							</div>
						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Telefone*:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="fone1" type="text" class="form-control" aria-label="Sizing example input" name="Telefone" aria-describedby="inputGroup-sizing-default" placeholder="" value="${item.fone1}" onkeyup=Helper.phoneMaskSemDDD>
							</div>
						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Telefone 2:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="fone2" type="text" class="form-control" aria-label="Sizing example input" name="Telefone 2" aria-describedby="inputGroup-sizing-default" placeholder="" value="${item.fone2}">
							</div>
						</div>
						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Telefone 3:</span>
							</div>
							<div class="celula-2 col-3">
								<input id="fone3" type="text" class="form-control" aria-label="Sizing example input" name="Telefone 3" aria-describedby="inputGroup-sizing-default" placeholder="" value="${item.fone3}">
							</div>
						</div>
					</div>
				</div>			
			</div>

			<div id="dados-secundarios-modal" style="display:none;">
				<hr>
				<div class="col-12">
					<div class="row m-0">
					</div>
					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">Credito:</span>
						</div>
						<div class="celula-2 col-2">
							<input id="credito" value="${item.credito}" type="text" class="form-control" aria-label="Sizing example input" name="" aria-describedby="inputGroup-sizing-default" onkeyup="Helper.maskMoeda(this,event)">
						</div>
					</div>
					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">Desconto:</span>
						</div>
						<div class="celula-2 col-2">
							<input id="desconto" value="${item.desconto}" type="text" class="form-control" aria-label="Sizing example input" name="" aria-describedby="inputGroup-sizing-default" onkeyup="Helper.maskMoeda(this,event)">
						</div>
					</div>
					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">Situação:</span>
						</div>
						<div class="celula-2 col-3">
							<div class="input-group-prepend">
								<select id="id_situacao" class="custom-select">
									<option value="1">Ativada</option>
									<option value="0">Desativada</option>
								</select>
							</div> 
						</div>

					</div>
					<div class="cont-celulas">
						<div class="celula-1">
							<span class="label-celula">Tipo:</span>
						</div>
						<div class="celula-2 col-3">
							<div class="input-group-prepend">
								<select id="id_fpgto" class="custom-select">
									<option value="1">Dinheiro</option>
									<option value="2">Cartão</option>
								</select>
							</div> 
						</div>
					</div>
				</div>
			</div>

			<div id="dados-tercearios-modal" style="display:none;">
				<div class="col-12">

					<div class="row m-0">
					</div>

					<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">CEP da Casa/empresa:*</span>
							</div>
							<div class="celula-2 col-4">
								<input id="cep_casa_edit" type="number" value = ${Logradouro.CEP} class="form-control" aria-label="Sizing example input" name="CEP Casa" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="8">
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Logradouro da Casa/empresa:*</span>
							</div>
							<div class="celula-2 col-3 input-sizeLograd">
								<input id="logradouro_casa" value = "${(Logradouro.tba_Tp_Logr.descricao ? Logradouro.tba_Tp_Logr.descricao : ' ') + ' ' + Logradouro.rua}" type="text" class="form-control" aria-label="Sizing example input" name="Logaradouro Casa" aria-describedby="inputGroup-sizing-default" placeholder="" disabled>
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Bairro da Casa/empresa:*</span>
							</div>
							<div class="celula-2 col-4">
								<input id="bairro_casa" value = "${Logradouro.bairro}" type="text" class="form-control" aria-label="Sizing example input" name="Logaradouro Casa" aria-describedby="inputGroup-sizing-default" placeholder="" disabled>
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Cidade da Casa/empresa:*</span>
							</div>
							<div class="celula-2 col-4">
								<input id="cidade_casa" value = "${Municipio.descricao}" type="text" class="form-control" aria-label="Sizing example input" name="Logaradouro Casa" aria-describedby="inputGroup-sizing-default" placeholder="" disabled>
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Numero da Casa/empresa:*</span>
							</div>
							<div class="celula-2 col-2">
								<input id="numero_casa" value = "${item.num_res}" type="text" class="form-control" aria-label="Sizing example input" name="Num. Casa" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="8">
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Complemento:</span>
							</div>
							<div class="celula-2 col-5">
								<input id="complemento" value = "${item.compl_res}" type="text" class="form-control" aria-label="Sizing example input" name="Complemento" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="50">
							</div>
						</div>

						<div class="cont-celulas">
							<div class="celula-1">
								<span class="label-celula">Referencia:</span>
							</div>
							<div class="celula-2 col-5">
								<input id="referencia" value="${item.referencia}" type="text" class="form-control" aria-label="Sizing example input" name="Referencia" aria-describedby="inputGroup-sizing-default" placeholder="" maxlength="50">
							</div>
						</div>


				</div>
			</div>

			<div class="row footer-modal m-0">
						<div class="offset-8 col-2 my-2">
							<button class="col-12 btn btn-danger position-relative float-left" onclick="addAlt('fechar',true)">Sair</button>
						</div>
						<div class="col-2 my-2">
							<button id="editar" class="col-12 btn btn-success position-relative float-right" >Salvar</button>
						</div>
				</div>
			</div>
		</div>
	</div>	
	`;

	divMain.insertAdjacentHTML('afterbegin', txt);

	document.getElementById('cep_casa_edit').addEventListener('focusout', listenFocusOutCep);
	campoCep = document.getElementById('cep_casa_edit');
	document.getElementById('fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
	document.getElementById('fone2').addEventListener('keyup', Helper.phoneMaskSemDDD);
	document.getElementById('fone3').addEventListener('keyup', Helper.phoneMaskSemDDD);
	document.getElementById('cod_cli').focus();
	//document.getElementById('desconto').addEventListener('keyup', helper.maskMoeda(this,event));
	//document.getElementById("dt_nasc").setAttribute("max", Helper.dataAtualEua(true));

	let btnEditar = document.getElementById('editar');
	btnEditar.addEventListener('click', ()=>{salvar(item.id_cli)})
}

async function salvar(id){
	cCadClientes.id_cli = null;

	let desconto = document.getElementById('desconto').value + '';
	desconto = desconto === '' ? 0 : parseFloat(desconto.replace('.','').replace('.','.'));
	
	let credito = document.getElementById('credito').value + '';
	credito = credito=== '' ? 0 : parseFloat(credito.replace('.','').replace('.','.'));

	//lista de campos
	cCadClientes.cod_cli 		= document.getElementById('cod_cli').value;
	cCadClientes.nome_cli 		= document.getElementById('nome_cli').value.toUpperCase();
	cCadClientes.apelido 		= '';
	cCadClientes.ddd_cli 		= document.getElementById('ddd_cli').value;
	cCadClientes.fone1 			= document.getElementById('fone1').value;
	cCadClientes.fone2 			= document.getElementById('fone2').value;
	cCadClientes.fone3 			= document.getElementById('fone3').value;
	//cCadClientes.id_logradouro 	= document.getElementById('id_logradouro').value;
	cCadClientes.num_res		= document.getElementById('numero_casa').value === "" ? null : document.getElementById('numero_casa').value;
	cCadClientes.num_com		= cCadClientes.num_res;
	cCadClientes.compl_res		= document.getElementById('complemento').value.toUpperCase();
	cCadClientes.compl_res.length > 0 ? true : cCadClientes.compl_res = ' ';
	cCadClientes.compl_com 		= cCadClientes.compl_res;
	cCadClientes.referencia 	= document.getElementById('referencia').value.toUpperCase();
	cCadClientes.referencia.length > 0 ? true : cCadClientes.referencia = ' ';
	cCadClientes.credito 		= document.getElementById('credito').value;
	cCadClientes.credito > 0 ? true : cCadClientes.credito = credito ;
	cCadClientes.desconto 		= document.getElementById('desconto').value;
	cCadClientes.desconto > 0 ? true : cCadClientes.desconto = desconto ;
	cCadClientes.dt_nasc 		= document.getElementById('dt_nasc').value !== '' ? new Date(document.getElementById('dt_nasc').value) : null;
	cCadClientes.dt_cad 		= Helper.dataAtualEua();
	cCadClientes.dt_alt 		= Helper.dataAtualEua();
	cCadClientes.id_situacao 	= document.getElementById('id_situacao').value;
	cCadClientes.id_fpgto 		= document.getElementById('id_fpgto').value;
	cCadClientes.id_oper	 	= JSON.parse(window.localStorage.getItem('usuario')).id_oper;

	//console.log(cCadClientes.credito)

	//verifica se campo esta vazio
	var conteudoNotNull = {
		cod_cli: cCadClientes.cod_cli,
		nome_cli: cCadClientes.nome_cli,
		
		ddd_cli:cCadClientes.ddd_cli,
		fone1: cCadClientes.fone1,
		
		//id_logradouro:cCadClientes.id_logradouro,
		numero_casa:cCadClientes.num_res,
		
		dt_cad:cCadClientes.dt_cad,
		dt_alt:cCadClientes.dt_alt,

		id_situacao:cCadClientes.id_situacao,
		id_fpgto:cCadClientes.id_fpgto
	};

	let nomeCampoFaltando = "";
	let campoFaltando = false;

	for(var i in conteudoNotNull){
		if(conteudoNotNull[i] == '' || conteudoNotNull[i] == null){
			campoFaltando = true;
			nomeCampoFaltando = document.getElementById(i).name;
			document.getElementById(i).focus();
			break;
		}
	}

	//insere no banco se não tiver faltando nada e verifica se é edicao ou novo item
	if(campoFaltando == false && id == undefined){
		event.preventDefault();
		addAlt('fechar');
		cCadClientes.insert()
		.then((retorno)=>{
			insereItemNaTela(retorno)
		})
		.catch((error)=>{
			trataErroInsert(error)
		});
	}else if(campoFaltando == false && id > 0){
		event.preventDefault();
		cCadClientes.id_cli = id;
		await cCadClientes.update();
		addAlt('fechar',true);
		alteraItemNaTela(id,conteudoNotNull);
	}else if(campoFaltando){
		Helper.alertaCampoVazio(nomeCampoFaltando)
	}
}

async function alteraItemNaTela(id,obj){
	let nomeId;
	await cCadClientes.recuperaLogradouro(id).then(dado =>{
		nomeId = JSON.parse(JSON.stringify(dado)).Logradouro.rua;
	});

	let cod = document.getElementById(`cod${id}`);
	let nome = document.getElementById(`nome${id}`);
	let telefone = document.getElementById(`telefone${id}`);
	let endereco = document.getElementById(`endereco${id}`);

	cod.innerHTML = obj.cod_cli;
	nome.innerHTML = obj.nome_cli;
	telefone.innerHTML = obj.fone1;
	endereco.innerHTML = nomeId;
}

async function insereItemNaTela(retorno,stringify){
	if(stringify != true){
		retorno = JSON.parse(JSON.stringify(retorno));
	}
	////console.log(retorno);
	let Logradouro = await CadEnderecos.select(retorno.id_logradouro);
	let nomeId;
	nomeId = (Logradouro.tba_Tp_Logr.descricao ? Logradouro.tba_Tp_Logr.descricao : ' ') + ' ' + Logradouro.rua;

	let tbody = document.getElementById('listaProdutos');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_cli}">							
		<th  class="border-right" scope="row">
		<center>
			<button class="btn-sem-decoracao editarItem" id="${retorno.id_cli}" onclick="editarItem(this.id, 'clientes')">
				<img style="margin-left: -12px;" src="../img/pencil.png"/>
			</button>
			<button class="btn-sem-decoracao" id="${retorno.id_cli}" onclick="excluirItem(this.id)" >
				<img style="margin-left: -12px;" src="../img/trash.png"/>
			</button>
		</center>
		</th>
		<td class="border-right txt-num" id="cod${retorno.id_cli}">${retorno.cod_cli}</td>
		<td class="border-right txt-label" id="nome${retorno.id_cli}">${retorno.nome_cli}</td>
		<td class="border-right txt-num" id="telefone${retorno.id_cli}">${retorno.fone1}</td>
		<td class="border-right txt-label" id="endereco${retorno.id_cli}">${nomeId}</td>	</tr>`;
}

btnSalvar.addEventListener('click',()=>{salvar()});

function trataErroInsert(error){
	if(error.name === 'SequelizeUniqueConstraintError'){

		let campo;

		switch(error.errors[0].path.replace('clientes.','')){
			case 'cod_cli': campo = 'Código'; break;
			default: campo = error.errors[0].path.replace('clientes.','');
		}

		swal({
			icon:'error',
			text: 'Já existe um grupo com esse ' + campo
		});

	}
}

document.getElementById("thNome").addEventListener('click', function(){
	selecionaGrupoOrdenado('Nome');
})

document.getElementById("thCod").addEventListener('click', function(){
	selecionaGrupoOrdenado('Código');
})

document.getElementById("thTelefone").addEventListener('click', function(){
	selecionaGrupoOrdenado('Telefone');
})

function selecionaGrupoOrdenado(ordemType){
	document.getElementById('listaProdutos').innerHTML = "";

	if(ordemType === "Nome"){
		let CadCliente = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
		let listaGrupos = CadCliente.selectOrderByNome();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}else if(ordemType === "Código"){
		let CadCliente = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
		let listaGrupos = CadCliente.selectOrderByCod();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}else if(ordemType === "Telefone"){
		let CadCliente = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
		let listaGrupos = CadCliente.selectOrderByTelefone();

		listaGrupos.then(data => {
			let actuallyData = JSON.parse(JSON.stringify(data));
			for(var i of actuallyData){
				insereItemNaTela(i,true)
			}
		});
	}
}

module.exports = {
	selecionarTudo: ()=>{selecionarTudo()},
	selecionarItem: (item)=>{selecionarItem(item)},
	excluirItem: (item)=>{excluirItem(item)},
	insereItemNaTela: (item,status)=>{insereItemNaTela(item,status)},
	selecionaGrupoOrdenado: (ordemType) => {selecionaGrupoOrdenado(ordemType)},
	adicionarNovoTelefone : adicionarNovoTelefone
};