async function imprimeFechameto(segundaVia = false){
    //atualizar o valor de descontos e acresimos e o valor total
    const dadosPedidoSelecionado = JSON.parse(window.sessionStorage.getItem('pedidoMesa'));
    const itensDoPedidoSelecionado = JSON.parse(window.sessionStorage.getItem('itemDoPedidoMesa'));

    for(prod of itensDoPedidoSelecionado){
        const selecaoProd = await cCadProdutos.recuperaGrupo(prod.id_prod);
        prod.Grupo = selecaoProd.Grupo;
    }

    try{
        criaConteudo(dadosPedidoSelecionado, 'Mesa', itensDoPedidoSelecionado, segundaVia, false, true);
    }catch(e){
        console.log(e);
    }

    swal({
        icon: 'success',
        title: 'Fechamento enviado para impressão!',
        closeOnClickOutside:true,
        button: {text: 'Okay', closeModal: true}
     });
}