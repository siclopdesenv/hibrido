var path = require('path');
var fs   = require('fs');
var exec = require('child_process').exec;
const dadosEstabelecimento = JSON.parse(localStorage.getItem('dadosEstabelecimento'));
let infoImpressora = null;
let dadosEspeciaisImp = null;
let Expansao      = '';
let Dexpansao     = '';
let Condensa      = '';
let Dcondensa     = '';
let Salto         = '';
let Guilhotina    = '';
let Gaveta        = '';
var cCadEnderecos = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));
var cConfImpressoras = require(path.join(__dirname + '/../../controller/cConfImpressoras.js'));
let { caminhoUtilProjeto } = require('../../constantes.js');
let enderecoEstab = null;

recuperaInfoImpressora();

recuperaLogradouroEstab();

async function recuperaLogradouroEstab(){
    enderecoEstab = await cCadEnderecos.select(dadosEstabelecimento.id_logradouro);
}

async function recuperaInfoImpressora(){
    //var {charExpansao, charDexpansao} = require(path.join(__dirname + '/../js/InformacoesImpressoras.js'))(infoImpressora.marca + '' + infoImpressora.largura);
    //dadosEspeciaisImp = require(path.join(__dirname + '/../js/InformacoesImpressoras.js'))(infoImpressora.marca + '' + infoImpressora.largura);
    infoImpressora = (await cConfImpressoras.selectAll())[0];
    let InformacoesImpressoras = require(path.join(__dirname + '/../js/InformacoesImpressoras.js'));
    const vetorInformacoes = InformacoesImpressoras.defineCaracteristicasImpressora(parseInt(infoImpressora.largura), infoImpressora.marca, 'Generic Text');
    if(vetorInformacoes !== 'INI'){
        Expansao      = vetorInformacoes[0];
        Dexpansao     = vetorInformacoes[1];
        Condensa      = vetorInformacoes[2];
        Dcondensa     = vetorInformacoes[3];
        Salto         = vetorInformacoes[4];
        Guilhotina    = vetorInformacoes[5];
        Gaveta        = vetorInformacoes[6];
    }
}

//#region Variáveis para controllers
var cCadItemPedido  = require(path.join(__dirname + "../../../controller/cItemPedido.js"));
var cCadProdutos    = require(path.join(__dirname + "../../../controller/cCadProdutos.js"));
//#endregion

let produtos = [];

let conteudoFormatado = {
    Pedido: {},
    Endereco: {},
    //ItemPedido: {},
    Formatado: {}
}

async function criaConteudo(pedidoData, tipoImpressao, produtos, segundaVia = false, ehEdicao = false, ehFechamento = false){
    let vetorProdutos = [];
    let itemPedido = {};
    let enderecoImp = {};

    await cCadEnderecos.select(pedidoData.id_logradouro).then(data => {
        let enderecoData = JSON.parse(JSON.stringify(data));
        enderecoImp = enderecoData;
    });

    for(item of produtos) vetorProdutos.push(item);
    conteudoFormatado.Formatado = vetorProdutos;

    await cCadProdutos.selectAll().then(data => {
        let prodData = JSON.parse(JSON.stringify(data));
        for(prod of prodData){
            produtos.push(prod);
        }
    });

    conteudoFormatado.Pedido = pedidoData;
    conteudoFormatado.Endereco = enderecoImp;
    //converteItensDoPedidoParaArrayComProdutosEGrupos(itemPedido);
    //conteudoFormatado.Formatado = converteItensDoPedidoParaArrayComProdutosEGrupos(itemPedido);
    //console.log(conteudoFormatado);
    criarConteudoTxt(conteudoFormatado, tipoImpressao, segundaVia, ehEdicao, ehFechamento);
}

function converteItensDoPedidoParaArrayComProdutosEGrupos(itensDosPedidos){
    let arrayCodigos = []
    let tamanhoEscolhidos = [];
    let idItemPed = [];
    let idPedido = [];
    let pizzaCombinada = [];
    let itensPedido = {};
    let codigoPizzas;
    let produtosAlterado = {};
    
    pedido = {
        produtos: [],
        itex: [],
        valores: [],
        valoresCalculados: [],
        qtds: [],
    };
    
    itensPedido = itensDosPedidos;

    for (itPed of itensPedido) {
        if (itPed.id_prod.search('/') > 0) {
            codigoPizzas = itPed.id_prod.substr(-20, (itPed.id_prod.length - 1))
            let concatCodigos = codigoPizzas.split('/');
            arrayCodigos.push(concatCodigos);
            concatCodigos = [];
            codigoPizzas = "";
        } else {
            arrayCodigos.push(itPed.id_prod);
        }

        idItemPed.push(itPed.id_item);
        idPedido.push(itPed.cod_pedido);
        tamanhoEscolhidos.push(itPed.tam_escolhido);
        pedido.valoresCalculados.push(itPed.vl_unit * itPed.qtd_prod);
        pedido.valores.push(itPed.vl_unit);
        pedido.qtds.push(itPed.qtd_prod);
        //pedido.valoresCalculados.push(itPed.vl_unit * itPed.qtd_prod);
    }

    let cont = 0;

    for (cod of arrayCodigos) {

        if (isArray(cod)) {
            for (pizzaComb of cod) {
                for (prod of produtos) {
                    if (parseInt(pizzaComb) === prod.cod_prod) {
                        prod.tamanhoEscolhido = tamanhoEscolhidos[cont];
                        prod.id_item = idItemPed[cont];
                        prod.cod_pedido = idPedido[cont];
                        pizzaCombinada.push(prod);
                    }
                }
            }
            pedido.produtos.push(pizzaCombinada);
            pizzaCombinada = [];
        } else {
            for (prod in produtos) {
                if (parseInt(cod) === produtos[prod].cod_prod) {

                    produtos[prod].tamanhoEscolhido = tamanhoEscolhidos[cont];
                    produtos[prod].id_item = idItemPed[cont];
                    produtos[prod].cod_pedido = idPedido[cont];
                    pedido.produtos.push(produtos[prod]);

                }
            }


        }

        cont++;
    }

    
    conteudoFormatado.Formatado = pedido;
}

function criarConteudoTxt(conteudoFormatado, tipoImpressao, segundaVia, ehEdicao, ehFechamento){
    try {
        let data = new Date();
        let conteudoArquivo = criaPrimeiroBlocoImpressao(conteudoFormatado, infoImpressora, tipoImpressao, segundaVia, ehEdicao);
        conteudoArquivo += criaBlocoProdutosImpressao(conteudoFormatado, infoImpressora);
        conteudoArquivo += ''.padStart(infoImpressora.largura, '-') + '\n\n';
        conteudoArquivo +=  tipoImpressao.toUpperCase() === 'MESA' && !ehFechamento ? '\n\n' : criaBlocoFinal(conteudoFormatado, infoImpressora);
        conteudoArquivo += ehFechamento ? `\nDividir por: ${document.getElementById('campoDividirFechamento').value}`: '';
        conteudoArquivo += Salto;

        const nomeArquivo = 'c:\\siclop\\hibrido\\impressoes\\' + data.getMonth() + '' + data.getDate() + ''+ data.getFullYear() + '' + data.getHours() + data.getSeconds() + '' + conteudoFormatado.Pedido.nr_pedido + '.txt';
        fs.writeFileSync(nomeArquivo, conteudoArquivo,'utf-8');
        enviaImpressaoParaImpressora(conteudoFormatado, infoImpressora, nomeArquivo);
        
        //#region Declrando variáveis globais na impressão
        let tamanhoBobina           ;
        let nomeImpressora          ;
        let nomeComputador          ;
        let marcaImpressora         ;
        let drive                   ;
        let blocos                  ;
        let infoColunas             ;
        let linhas                  ;
        let categoria               ;
        let distancia               ;
        let conteudoLinha           = "";
        let blocoConteudoParcial    = "";
        let blocoConteudoTotal      = "";
        let primeiraColuna          ;
        let segundaColuna           ;
        let terceiraColuna          ;
        let quartaColuna            ;
        let colunaVariavel          ;
        let conteudoVariavel        ;
        let ultimaImpressora        = "";
        let ultimoComputador        = "";
        let accImpressao            = 0;
        let portaLPT                = 4;
        let mesmoComputador         = false;
        let mesmaImpressora         = false;
        let computadorLocal         ;
        let apagarLPT               ;
        //#endregion                

        console.log(conteudoFormatado, tipoImpressao);

    } catch (e) {
        console.error('ERRO AO FORMATAR IMPRESSÃO' + e);
    }
}

function enviaImpressaoParaImpressora(conteudoFormatado, infoImpressora, nomeArquivo){
    exec('type ' + nomeArquivo + ' > lpt' + infoImpressora.portaLPT);
}

//#region Manipulação de blocos
function criaPrimeiroBlocoImpressao(conteudoPedido, infoImp, tipoPedido, segundaVia, ehEdicao){
    const {Pedido, Endereco} = conteudoPedido;
    const {largura} = infoImp;

    let conteudoTotal = '';

    conteudoTotal += adaptaConteudoColuna('SICLOP HIBRIDO', largura) + '\n\n';
    conteudoTotal += adaptaConteudoColunaSemQuebra(enderecoEstab.tba_Tp_Logr.descricao + ' ' + enderecoEstab.rua + ', ' + dadosEstabelecimento.cfg_numero_local + ' - ' + enderecoEstab.bairro + ' - ' + enderecoEstab.tba_Municipio.descricao, largura);
    conteudoTotal += adaptaConteudoColuna('(' + dadosEstabelecimento.cfg_ddd + ')' + dadosEstabelecimento.cfg_fone1, largura) + '\n';

    conteudoTotal +='\n';
    segundaVia ? conteudoTotal += '2a VIA\n\n' : null;

    conteudoTotal += adaptaConteudoColuna('Cliente: ' + (Pedido.cliente !== '' ? Pedido.cliente : tipoPedido), largura) + '\n';
    conteudoTotal += (Pedido.telefone !== '' && Pedido.telefone !== undefined ? adaptaConteudoColuna('Fone: ' + Pedido.telefone, largura) + '\n' : '');
    conteudoTotal += adaptaConteudoColuna('Pedido: ' + Pedido.nr_pedido + (ehEdicao ? ' ALTERADO' : ''), largura) + '\n';
    let dataPedido = Pedido.dt_pedido.split('-');
    conteudoTotal += adaptaConteudoColuna(('Data: ' + dataPedido[2].toString().padStart(2, '0') + '/' + (dataPedido[1] + 1).toString().padStart(2, '0') + '/' + dataPedido[0].toString().padStart(4, '0')), largura) + '\n';

    conteudoTotal +='\n';
    
    if(tipoPedido.toUpperCase() === 'DELIVERY' || tipoPedido.toUpperCase() === 'ENTREGA'){
        const conteudoEndereco = Endereco.tba_Tp_Logr.descricao + ' ' + Endereco.rua + ', ' + Pedido.num_endereco + ' - ' + Endereco.bairro + ' - ' + Endereco.tba_Municipio.descricao;
        conteudoTotal += adaptaConteudoColunaSemQuebra(conteudoEndereco, largura, true) + '\n';

        if(Pedido.comp_endereco.trim()) conteudoTotal += adaptaConteudoColunaSemQuebra('Complemento: ' + Pedido.comp_endereco, largura) + '\n\n';
    }
    
    return conteudoTotal;
}

function criaBlocoProdutosImpressao(conteudoPedido, infoImp){
    const {Formatado} = conteudoPedido;
    let {largura} = infoImp;
    const qtdEspacoEntreConteudo = 3;
    largura = largura - qtdEspacoEntreConteudo;

    let conteudoTotal = '';

    let exibirCodigo = true;

    const tamColunaCodigo = exibirCodigo ? 6 : 0;
    const tamColunaQTD = 4;
    const tamColunaValor = 6;
    const tamColunaConteudo = largura - (tamColunaCodigo + tamColunaQTD + tamColunaValor);

    conteudoTotal += 'Qtd'.padStart(tamColunaQTD) + ' ' + (exibirCodigo ? 'Cod'.padStart(tamColunaCodigo) + ' ' : '') + 'Descricao'.padEnd(tamColunaConteudo) + ' ' + 'Valor'.padStart(tamColunaValor) + '\n';

    let conteudoLinhas = '';
    let baseTamanhos = {
        'br':'*BROTO* ',
        'md':'*MEDIA* ',
        'nn':'',
        'gi':'*GIGANTE* '
    }

    let descricoesGrupos = Formatado.map((element)=>{return element.Grupo.descricao});
    descricoesGrupos = descricoesGrupos.filter((este, i) => descricoesGrupos.indexOf(este) === i);
    descricoesGrupos = descricoesGrupos.sort();

    for(grupo of descricoesGrupos){
        conteudoLinhas += ('---' + grupo).padEnd(infoImp.largura, '-') + '\n';
        for(item of Formatado){
            if(item.Grupo.descricao !== grupo) continue;

            let codigo = item.id_prod.toString().substring(0,6);
            if(codigo.substr(-1) === '/') codigo = codigo.substring(0, (codigo.length - 1)); // Retirando a barra que sobra no final

            const qtd = item.qtd_prod.toString();
            const valor = item.vl_unit.toFixed(2).toString();
            const conteudo = (baseTamanhos[item.tam_escolhido]) + (item.desc_produto + ' ' + item.complemento.toLowerCase().replace('sem complemento','')).replace(/<br>/g, ' ').trim();
    
            let colCod      = exibirCodigo ? codigo.padStart(tamColunaCodigo) : '';
            let colQtd      = qtd.padStart(tamColunaQTD); 
            let colDesc     = conteudo.padEnd(tamColunaConteudo);
            let colValor    = valor.padStart(tamColunaValor);
            
            if(colDesc.length > tamColunaConteudo) conteudoLinhas += adaptaConteudoProdutos({colQtd, colCod, colDesc, colValor}, {tamColunaQTD, tamColunaCodigo, tamColunaConteudo, tamColunaValor});
            else conteudoLinhas += colQtd + ' ' + colCod + ' ' + colDesc + ' ' + colValor + '\n';
        }
    }

    conteudoTotal += conteudoLinhas;
    
    return conteudoTotal;
}

function criaBlocoFinal(conteudoPedido, infoImp){
    //Se quiser alinhar o conteudo a direita é só no momento da atribuição do conteúdo string na variável dar um 'conteudo'.padStart(com o tamanho da coluna)
    const {Pedido} = conteudoPedido;
    let conteudoTotal = '';
    let {largura} = infoImp;
    const qtdEspacoEntreConteudo = 3;
    largura = largura - qtdEspacoEntreConteudo;

    let colUm     = '';
    let colDois   = '';
    let colTres   = '';
    let colQuatro = '';

    let tamColunaUm       = 16;
    let tamColunaDois     = 0;
    let tamColunaTres     = 0;
    let tamColunaQuatro   = largura - (tamColunaUm + tamColunaDois + tamColunaTres);

    if(Pedido.desconto !== 0){
        colUm     = 'Desconto:';
        colDois   = '';
        colTres   = '';
        colQuatro = ('R$ ' + Pedido.desconto.toFixed(2)).padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
    }
    
    
    if(Pedido.acrescimo !== 0){
        colUm     = 'Acrescimo:';
        colDois   = '';
        colTres   = '';
        colQuatro = ('R$ ' + Pedido.acrescimo.toFixed(2)).padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';    
    }
 
    if(Pedido.frete !== 0){
        colUm     = 'Taxa de Entrega:';
        colDois   = '';
        colTres   = '';
        colQuatro = ('R$ ' + Pedido.frete.toFixed(2)).padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
    }
    
    const charExpansao = Expansao;
    const charDexpansao = Dexpansao;

    colUm     = 'Total a Pagar:';
    colDois   = '';
    colTres   = '';
    colQuatro = (charExpansao + ('R$ ' + Pedido.vl_total.toFixed(2)) + charDexpansao).padStart(Math.ceil(tamColunaQuatro / 2));
    //colQuatro = (charExpansao + 'R$ 200,00' + charDexpansao).padStart(Math.ceil(tamColunaQuatro / 2));
    conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';

    colUm     = 'Troco:';
    colDois   = '';
    colTres   = '';
    colQuatro = ('R$ ' + (Pedido.troco ? Pedido.troco.toFixed(2) : '0,00')).padStart(tamColunaQuatro);
    conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';

    return conteudoTotal;

}
//#endregion

//#region Manipulação de conteúdo de colunas

function alinhaConteudoAoCentro(conteudo, tamColuna, caractereAosLados = ' '){
    let qtdEsquerda = tamColuna/2;
    qtdEsquerda = qtdEsquerda - (conteudo.length / 2);

    let conteudoFinal = '';

    for(let i = 0; i < qtdEsquerda; i++) conteudoFinal += caractereAosLados;

    conteudoFinal += conteudo;
    return conteudoFinal.padEnd(tamColuna, caractereAosLados);
}

function adaptaConteudoColuna(conteudo, tamColuna){
    //Esta função formata do conteudo de acordo com a coluna, ele permite a quebra de palavras
    let contFormatado = '';
    if(conteudo.length > tamColuna){
        const tamanhoConteudoOriginal = conteudo.length;
        const qtdLinhas = Math.ceil((tamanhoConteudoOriginal / tamColuna));
    
        for(let i = 0; i < qtdLinhas; i++){
            const comecoQuebra = tamColuna * i;
            const finalQuebra = tamColuna * (i + 1);
            contFormatado += conteudo.substring(comecoQuebra, finalQuebra).trim().padEnd(tamColuna);
            (i + 1) !== qtdLinhas ? contFormatado += '\n' : null;
        }
    }else{
        contFormatado = conteudo.padEnd(tamColuna);
    }

    return contFormatado;
}

function adaptaConteudoColunaSemQuebra(conteudo, tamColuna, expandido = false){
    //Esta função formata do conteudo de acordo com a coluna, ele não permite a quebra de palavras
    var charExpansao = '';
    var charDexpansao = '';

    if(expandido){
        charExpansao = Expansao;
        charDexpansao = Dexpansao;
        tamColuna = tamColuna/2;
    }

    let contFormatado = '';
    if(conteudo.length > tamColuna){
        const palavras = conteudo.split(' ');
        let linha = '';
        let linhaAux = charExpansao;

        for(let i = 0; i < palavras.length; i++){
            const simulaLinhaComPalavraNova = linhaAux + palavras[i];
            
            if(simulaLinhaComPalavraNova.length <= tamColuna) {
                linhaAux = simulaLinhaComPalavraNova + ' ';
            }
            else {
                linha += linhaAux.trim().padEnd(tamColuna) + charDexpansao + '\n';
                linhaAux = charExpansao + palavras[i] + ' ';
            }
            if((i + 1) === palavras.length) linha += linhaAux.trim().padEnd(tamColuna) + charDexpansao + '\n';
        }
        contFormatado = linha;
    }else{
        contFormatado = conteudo.padEnd(tamColuna);
    }

    return contFormatado;
}

function adaptaConteudoProdutos(conteudoProduto, colunas){
    let {colCod, colQtd, colDesc, colValor} = conteudoProduto;
    let {tamColunaCodigo, tamColunaQTD, tamColunaConteudo, tamColunaValor} = colunas;
    
    const palavras = colDesc.split(' ');
    let linha = '';
    let linhaAux = '';
    let conteudoVariavel = '';
    let conteudoDuasPrimeirasCol = '';
    let contLinha = 0;
    let contTrocaLinha = 0;

    for(let i = 0; i < palavras.length; i++){
        if(i === 0) conteudoDuasPrimeirasCol = colQtd.padStart(tamColunaQTD) + ' ' + colCod.padStart(tamColunaCodigo);

        if(contLinha !== contTrocaLinha) contTrocaLinha++;

        const simulaLinhaComPalavraNova = conteudoVariavel + palavras[i];
        
        if(simulaLinhaComPalavraNova.length <= tamColunaConteudo) {
            conteudoVariavel = simulaLinhaComPalavraNova + ' ';
        }
        else {
            linhaAux = conteudoDuasPrimeirasCol + ' ' + conteudoVariavel.trim().padEnd(tamColunaConteudo);
            if(contLinha === 0) linhaAux = linhaAux + ' ' + colValor.padStart(tamColunaValor) + '\n';
            else{
                linhaAux = linhaAux + ' ' + ''.padStart(tamColunaValor) + '\n';
            }

            linha                       += linhaAux;
            conteudoDuasPrimeirasCol    = ''.padStart(tamColunaCodigo) + ' ' + ''.padStart(tamColunaQTD);
            conteudoVariavel            = palavras[i] + ' ';
            contLinha++;
        }

        if((i + 1) === palavras.length) linha += (conteudoDuasPrimeirasCol + ' ' + conteudoVariavel.trim().padEnd(tamColunaConteudo) + ' ' + ''.padStart(tamColunaValor) + '\n');
    }

    return linha;
}

function adaptaConteudoMultiplasColunas(conteudos, colunas){
    let {colUm, colDois, colTres, colQuatro} = conteudos;
    let {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro} = colunas;

    return (colUm.padEnd(tamColunaUm) + ' ' + colDois.padEnd(tamColunaDois) + ' ' + colTres.padEnd(tamColunaTres) + ' ' + colQuatro.padEnd(tamColunaQuatro) + ' ');
}
//#endregion

function calculaSalto(qtdSalto){
    let conteudo = '';
    for(let i = 0; i < qtdSalto; i++){
        conteudo += '\n';
    }
    return conteudo;
}

function imprimeExtratoCaixa(dados){
    console.log(dados);
    let conteudoImpressao = criaPrimeiroBlocoImpExtrato(dados, infoImpressora);
    conteudoImpressao += criaSegundoBlocoImpExtrato(dados, infoImpressora);
    conteudoImpressao += criaTerceiroBlocoImpExtrato(dados, infoImpressora);
    conteudoImpressao += criaQuartoBlocoImpExtrato(dados, infoImpressora);
    conteudoImpressao += criaSetimoBlocoImpExtrato(dados, infoImpressora);
    conteudoImpressao += Salto;
    let data = new Date();
    const nomeArquivo = 'c:\\siclop\\hibrido\\impressoes\\' + data.getMonth() + '' + data.getDate() + ''+ data.getFullYear() + '' + data.getHours() + data.getSeconds() + 'extcx.txt';
    fs.writeFileSync(nomeArquivo, conteudoImpressao,'utf-8');
    enviaImpressaoParaImpressora(conteudoImpressao, infoImpressora, nomeArquivo);
}

function criaPrimeiroBlocoImpExtrato(conteudoImpExtrato, infoImp){
    const {primeira} = conteudoImpExtrato;
    const {largura} = infoImp;

    let conteudoTotal = '';

    conteudoTotal += alinhaConteudoAoCentro('SICLOP HIBRIDO', largura) + '\n\n';
    conteudoTotal += alinhaConteudoAoCentro(' PERIODO SELECIONADO ', largura , '-') + '\n';
    conteudoTotal += alinhaConteudoAoCentro('DE ' + conteudoImpExtrato.periodo.de, largura, ' ') + '\n';
    conteudoTotal += alinhaConteudoAoCentro('ATE ' + conteudoImpExtrato.periodo.ate, largura, ' ') + '\n\n';
    conteudoTotal += alinhaConteudoAoCentro(' EXTRATO DO CAIXA ', largura, '-') + '\n\n';

    let vetorInfos = [];

    vetorInfos.push({esq:`Faturamento Bruto:`,      dir: `${separaCifraValor(primeira.fatBruto)}`});
    vetorInfos.push({esq:`Fiados Feitos:`,          dir: `${primeira.fiadosFeitos}`});
    let retornoSepara = separaCifraValor(primeira.descontos1);
    if(retornoSepara !== -1) vetorInfos.push({esq:`Descontos:`,              dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(primeira.valeCompra);
    if(retornoSepara !== -1) vetorInfos.push({esq:`Vale Compra:`,            dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(primeira.despesas);
    if(retornoSepara !== -1) vetorInfos.push({esq:`Despesas:`,               dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(primeira.fatLiquido);
    if(retornoSepara !== -1) vetorInfos.push({esq:`Faturamento Liquido:`,    dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(primeira.fiadosReceb);
    if(retornoSepara !== -1) vetorInfos.push({esq:`Fiados Recebidos:`,       dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(primeira.valorCaixa);
    if(retornoSepara !== -1) vetorInfos.push({esq:`Valor em Caixa:`,         dir: `${retornoSepara}`});

    let colUm     = '';
    let colDois   = '';
    let colTres   = ''; 
    let colQuatro = '';

    let tamColunaUm       = 20;
    let tamColunaDois     = 0;
    let tamColunaTres     = 0;
    let tamColunaQuatro   = (largura - (tamColunaUm + tamColunaDois + tamColunaTres)) - 4;

    for(info of vetorInfos){
        colUm     = `${info.esq.substring(0,20)}`;
        colQuatro = `${info.dir}`.padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
    }

    conteudoTotal +='\n';

    return conteudoTotal;
}

function criaSegundoBlocoImpExtrato(conteudoImpExtrato, infoImp){
    const {segunda} = conteudoImpExtrato;
    const {largura} = infoImp;

    let conteudoTotal = '';

    let vetFormasPgto = segunda[0];
    let vetValoresTotais = segunda[1];

    conteudoTotal += alinhaConteudoAoCentro(' FORMAS DE PAGAMENTO ', largura, '-') + '\n\n';

    let colUm     = '';
    let colDois   = '';
    let colTres   = '';
    let colQuatro = '';

    let tamColunaUm       = 32;
    let tamColunaDois     = 0;
    let tamColunaTres     = 0;
    let tamColunaQuatro   = (largura - (tamColunaUm + tamColunaDois + tamColunaTres)) - 4;

    for(f of vetFormasPgto){
        colUm     = `${Helper.retiraAcento(f.descricao.substring(0,(tamColunaUm-1)))}:`;
        const cont4 = separaCifraValor(f.valorPago);
        //colQuatro = `${f.valorPago}`.padStart(tamColunaQuatro);
        if(cont4 !== -1){
            colQuatro = cont4.padStart(tamColunaQuatro);
            conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
        }
    }

    //VALORES TOTAIS
    conteudoTotal += '\n';

    let vetorInfos = [];

    let retornoSepara = separaCifraValor(vetValoresTotais.valorTotalPagamentos);
    if(retornoSepara !== -1) vetorInfos.push({esq:`TOTAL PAGAMENTOS:`,       dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(vetValoresTotais.valorFundoCaixaSomado);
    if(retornoSepara !== -1) vetorInfos.push({esq:`FUNDO DE CAIXA SOMADO:`,  dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(vetValoresTotais.totalNoCaixa);
    if(retornoSepara !== -1) vetorInfos.push({esq:`TOTAL EM CAIXA:`,         dir: `${retornoSepara}`});

    for(info of vetorInfos){
        colUm     = `${info.esq.substring(0,(tamColunaUm-1))}`;
        colQuatro = `${info.dir}`.padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
    }

    return (conteudoTotal + '\n');
}

function criaTerceiroBlocoImpExtrato(conteudoImpExtrato, infoImp){
    const {terceira} = conteudoImpExtrato;
    const {largura} = infoImp;

    let conteudoTotal = '';
    conteudoTotal += alinhaConteudoAoCentro(' OUTROS VALORES ', largura, '-') + '\n\n';

    let vetorInfos = [];

    let retornoSepara = separaCifraValor(terceira.somaAcrescimo);
    if(retornoSepara !== -1) vetorInfos.push({esq:`ACRÉSCIMOS:`,        dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(terceira.somaTxEntrega);
    if(retornoSepara !== -1) vetorInfos.push({esq:`TAXA DE ENTREGA:`,   dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(terceira.somaTxServico);
    if(retornoSepara !== -1) vetorInfos.push({esq:`TAXAS DE SERVICO:`,  dir: `${retornoSepara}`});
    retornoSepara = separaCifraValor(terceira.somaGorjeta);
    if(retornoSepara !== -1) vetorInfos.push({esq:`GORJETAS:`,          dir: `${retornoSepara}`});

    let colUm     = '';
    let colDois   = '';
    let colTres   = '';
    let colQuatro = '';

    let tamColunaUm       = 20;
    let tamColunaDois     = 0;
    let tamColunaTres     = 0;
    let tamColunaQuatro   = (largura - (tamColunaUm + tamColunaDois + tamColunaTres)) - 4;

    for(info of vetorInfos){
        colUm     = `${info.esq.substring(0,20)}`;
        colQuatro = `${info.dir}`.padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
    }

    conteudoTotal +='\n';

    return conteudoTotal;
}

function criaQuartoBlocoImpExtrato(conteudoImpExtrato, infoImp){
    const {quarta} = conteudoImpExtrato;
    const {largura} = infoImp;

    let conteudoTotal = '';
    conteudoTotal += alinhaConteudoAoCentro(' COMANDAS BAIXADAS ', largura, '-') + '\n\n';

    let vetorInfos = [];

    vetorInfos.push({esq:`TOTAL:`,  dir: `${quarta.qtdComandasBaixadas.length}`});
    vetorInfos.push({esq:`ENTREGA:`,dir: `${quarta.qtdPedidosEntrega.length}`});
    vetorInfos.push({esq:`BALCAO:`, dir: `${quarta.qtdPedidosBalcao.length}`});
    vetorInfos.push({esq:`MESA:`,   dir: `${quarta.qtdPedidosSalao.length}`});

    let colUm     = '';
    let colDois   = '';
    let colTres   = '';
    let colQuatro = '';

    let tamColunaUm       = 20;
    let tamColunaDois     = 0;
    let tamColunaTres     = 0;
    let tamColunaQuatro   = (largura - (tamColunaUm + tamColunaDois + tamColunaTres)) - 4;

    for(info of vetorInfos){
        colUm     = `${info.esq.substring(0,20)}`;
        colQuatro = `${info.dir}`.padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
    }

    conteudoTotal +='\n';

    return conteudoTotal;
}

function criaSetimoBlocoImpExtrato(conteudoImpExtrato, infoImp){
    const {setima} = conteudoImpExtrato;
    const {largura} = infoImp;

    let conteudoTotal = '';

    conteudoTotal += alinhaConteudoAoCentro(' OUTROS GRUPOS VENDIDOS ', largura, '-') + '\n\n';

    let colUm     = '';
    let colDois   = '';
    let colTres   = '';
    let colQuatro = '';

    let tamColunaUm       = 20;
    let tamColunaDois     = 0;
    let tamColunaTres     = 0;
    let tamColunaQuatro   = (largura - (tamColunaUm + tamColunaDois + tamColunaTres)) - 4;

    for(grupo of setima){
        colUm     = `${Helper.retiraAcento(grupo.nomeGrupo.substring(0,20))}:`;
        colQuatro = `${grupo.qtd}`.padStart(tamColunaQuatro);
        conteudoTotal += adaptaConteudoMultiplasColunas({colUm, colDois, colTres, colQuatro}, {tamColunaUm, tamColunaDois, tamColunaTres, tamColunaQuatro}) + '\n';
    }

    conteudoTotal += alinhaConteudoAoCentro(' ', largura, '-') + '\n\n';
        
    return conteudoTotal;
}

function separaCifraValor(string, espacoEsquerdaValor = 9){
    const cifra = string.split(' ')[0];
    const valor = string.split(' ')[1];
    if(parseFloat(valor) === 0) return -1;
    return cifra + valor.padStart(espacoEsquerdaValor);
}

module.exports = {
    criaConteudo: criaConteudo,
    imprimeExtratoCaixa: imprimeExtratoCaixa
}