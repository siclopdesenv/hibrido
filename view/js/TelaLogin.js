let email = document.getElementById('inputEmail');
let senha = document.getElementById('inputSenha');
let btnEntrar = document.getElementById('btnEntrar');
let swal = require('sweetalert');
let fs = require('fs');
let path = require('path');
var os = require('os');
var { exec } = require('child_process');
var Mousetrap = require('mousetrap');

let Hel = require('../../controller/Helper.js');
var pjson = require('../../package.json');
var { caminhoUtilProjeto } = require('../../constantes.js');
let ver = document.getElementById("versao");
document.title = "SICLOP HÍBRIDO - VBeta - "+ pjson.version + " - " + os.userInfo().username + " - " + Hel.dataAtualBr();

let data = new Date();
let day = data.getDay();
let dia = data.getDate() + 10;
let hora = data.getHours();
let minuto = data.getMinutes();
let segundos = data.getSeconds() + 10;

const cfgHibrido = fs.readFileSync(path.join(caminhoUtilProjeto + 'cfghibridobd.txt'),'utf-8').split('|');
const user = cfgHibrido[0];
const pass = cfgHibrido[1];
const database = cfgHibrido[2];
const host = cfgHibrido[3];
const port = cfgHibrido[4];

btnEntrar.addEventListener('submit', validaCamposLogin);

btnEntrar.addEventListener('click', validaCamposLogin);

document.getElementById('togglePassword').addEventListener('click', ()=>{
    senha.setAttribute('type', senha.getAttribute('type') === 'password' ? 'text' : 'password');
    senha.focus();
});

/*Mousetrap.bind('enter', function () {
    btnEntrar.click();
})*/

function validaCamposLogin(){
    if(email.value !== '' && senha.value !== ''){
        let manterLogado = document.getElementById('manterLogado').checked + '';
        (email.value).toUpperCase() === 'SICLOP' ? validaSenhaCoringa(senha.value, manterLogado) : validaLoginBanco(email.value, senha.value, manterLogado);
    }else{
        swal({
            text: 'Preencha todos os campos!',
            icon: 'warning'
        });
    }
}

function validaSenhaCoringa(senha, manterLogado){
    if (senha === (dia + '' + segundos)){
        guardaDadosLocalStorage({
            id_oper: null,
            operador: 'SICLOP',
            senha: (dia + '' + minuto),
            nome: 'Tecnico',
            id_nivel: 3
        }, 'false');
    }
}

function validaLoginBanco(email, senha, manterLogado){
    let cCadFunc = require(path.join(__dirname + '../../../controller/cCadFunc'));
    cCadFunc.validaLogin(email, senha).then((retornoBanco) =>{
        
        if(!retornoBanco[0]){ 
            swal({
                text: 'Operador ou Senha inválidos!!',
                icon: 'error'
            })
        }else if(retornoBanco.length > 0) {
            guardaDadosLocalStorage(retornoBanco[0].dataValues, manterLogado);
        }else if(retornoBanco[1]){
            swal({
                text: 'Senha incorreta!',
                icon: 'error'
            })
        }
    })
}

async function guardaDadosLocalStorage(objUsuario, manterLogado){
    
    //--- Nesta Função guardamos momentaneamente os Dados do Usuario Logado e do estabelecimento
    //--- para posteriores funções que necessitam destes dados funcionarem
    let InfoSis = require(path.join(__dirname + '/../../controller/cINFORSIS.js'));
    resul = await InfoSis.selectAll();

    window.localStorage.setItem('manterLogado', manterLogado);
    window.localStorage.setItem('usuario', (JSON.stringify(objUsuario)));
    window.localStorage.setItem('dadosEstabelecimento', JSON.stringify(resul[0]));
    window.localStorage.getItem('tipoMenu') === null ? window.localStorage.setItem('tipoMenu', '1') : null;
    
    redirecionaUsuarioMenu();
}

function redirecionaUsuarioMenu(){
    document.location.assign(path.join(__dirname+'/loading.html'))
}

let intervaloCheckArquivo = {};
const caminhoArquivoFalgSQL = 'C:\\SICLOP\\hibrido\\mysql-5.7.35-win32\\flag.txt';

function verificaConfiguracaoDoMYSQL(){
    if(!fs.existsSync(caminhoArquivoFalgSQL)){
        require(path.join(__dirname, '../../cfgSQL/configure'));
        //Aqui dá display flex na div de informações da instalação
        document.getElementById('divItensInstalacaoServ').style.display = 'block';
        document.getElementById('conjuntoLoading').style.display = 'none';
        intervaloCheckArquivo = setInterval(performaInterval, 15000);
    }else{
        checaStatusServicoSQL();
        //Se entrar aqui dar display none na div geral, pois  já está instalado
    }
}

function verificaExistenciaBanco(){

    var Helper = require(path.join(__dirname, '../../controller/Helper'));

    Helper.verificaExistencia('cfghibrido.txt', path.join(caminhoUtilProjeto), 'arquivo');

    setTimeout(()=>{
        fs.readFile(path.join(caminhoUtilProjeto + 'cfghibrido.txt'), 'utf-8', (err, data)=>{
        
            if (err) throw err;
        
            if(data === 'dbnaocriado'){
                solicitaInfosBanco();
            }else{
                //Se o banco já tiver sido criado, chama o arquivo e a função que verifica a existência e cria novas tabelas
                testaComunicacao(host, user)//solicitaInfosBanco();
            }
        
        });
    },2000);
}

function performaInterval(){
    console.log('Entrou no interval');
    if(fs.existsSync(caminhoArquivoFalgSQL)){
        console.log('cabou');
        //Isso significa que terminou a configuração por parte do bat e pode seguir pras proximas ações
        testaComunicacao('localhost', 'root');
        clearInterval(intervaloCheckArquivo);
    }
}

function checaStatusServicoSQL(fecharSWAL = false){
    const nomeServicoSQL = 'MySQL5732';

    exec('sc query ' + nomeServicoSQL, (err, stdout, stdin)=>{
        if(err) {
            swal('Erro ao consultar serviço MYSQL');
            return;
        }
        const estadoDoServico = stdout.split('\n')[3];

        if(estadoDoServico.indexOf('RUNNING') === -1){
            //Se entrar aqui significa que o serviço do MYSQL não está rodando, logo deve ser iniciado
            exec('net start ' + nomeServicoSQL, (err, stdout, stdin)=>{
                if(err){
                    swal('Erro ao inicializar serviço MYSQL ' + err);
                    return;
                }

                if(stdout.indexOf('1058') !== -1){
                    //Se entrar aqui caiu no erro de sistema 1058, que significa que o serviço não está instalado
                    //Fazer algo, rodar de novo o bat de configurar ou sla
                    swal('erro na config do MYSQL, serviço não existe erro 1058');
                }else{
                    //Iniciar toda comunicacao no banco
                    //if(fecharSWAL) swal.close();

                    testaComunicacao('localhost', 'root');
                }
            });
        }else{
            //iniciar toda config do banco
            testaComunicacao('localhost', 'root');
        }
    });
}

let vetorIPs = [];
let contadorIPs = 0;

function solicitaInfosBanco(){
    document.getElementById('divConteudoInstalacao').style.display = 'flex';
    swal({
        icon: 'warning',
        title: 'O que vamos configurar?',
        closeOnClickOutside:false,
        closeOnEsc: false,
        text: 'Terminal, ou Servidor?',
        closeModal: false,
        buttons: ['Terminal', 'Servidor']
    }).then((confirmation) =>{
        if(!confirmation){
            swal({
                icon: 'warning',
                title: 'Tentando conexão com hosts da rede',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Por favor, aguarde...',
                closeModal: false,
                button: {text: 'Okay', closeModal: false}
            });

            function solicitaIp(){
                exec('arp -a', (err, stdout, stdin)=>{
                    const ips = stdout.split('\n');
                    for(ip of ips){
                        ip = ip.substring(0,20).trim();
                        if(ip !== '' && ip.length <= 15 && ip !== 'Endere�o IP'){
                            console.log(ip);
                            vetorIPs.push(ip);
                        }
                    }
                    testaComunicacao(vetorIPs[contadorIPs], 'terminal');

                    //EUNTROU EM LOOP POIS NÃO ATUALIZOUI O ARQUIVO COM O HOST



                });
                return;
                swal({
                    icon: 'warning',
                    title: 'Digite o IP da máquina Servidor',
                    closeOnClickOutside:false,
                    closeOnEsc: false,
                    content: 'input',
                    text: 'Você pode encontrar esta informação dentro da aba "Configurações > Informações do Sistema > IP Máquina no SH" da máquina Servidor',
                    button: {text:"Testar comunicação", closeModal: false}
                }).then((confirm)=>{
                    //if(!testaComunicacao(confirm, 'terminal')) solicitaIp();
                    testaComunicacao(confirm, 'terminal');
                });
            };
            solicitaIp();
        }else{
            verificaConfiguracaoDoMYSQL();
            //testaComunicacao('localhost', 'root');
        }
        
    });
}

function testaComunicacao(ip = 'localhost', usuario = 'root'){
    criaConexaoBanco(ip, usuario);
    return false;
}

verificaExistenciaBanco();

function estaLogado(){
    if(window.localStorage.getItem('manterLogado', 'false') === 'true') redirecionaUsuarioMenu();
}

/*var Helper = require(path.join(__dirname, '../../controller/Helper'));
Helper.verificaExistencia('cfghibridobd.txt', path.join('c:/siclop/'), 'arquivo');*/

let primeiraPassagemErroRefused = false;

let mysql = require('mysql2/promise');

function criaConexaoBanco(hostRemoto = 'localhost', usuario = 'root'){
    fs.writeFileSync('c:/siclop/hibrido/cfghibridobd.txt', (usuario + '|' + pass + '|' + database + '|' + hostRemoto + '|' + port),'utf-8');

    mysql.createConnection({
        host: hostRemoto,
        user     : usuario,
        password : pass,
        database : database
    }).then((conn)=>{
    
        //const syncBanco = require(path.join('../../database/sincronizaBanco'));
        //syncBanco(['LOGS', 'TB_CAD_FUNC', 'INFORSIS'], ['/tabelas/TB_LOGS','/tabelas/TB_CAD_FUNC','/tabelas/INFORSIS'], conn);
        try{ swal.close(); }catch(e){}
        //
        fs.writeFileSync(caminhoUtilProjeto + 'cfghibrido.txt', 'dbcriado','utf-8');
    
        let infosis = require(path.join('../../controller/cINFORSIS'));
    
        infosis.selectAll().then(dados => {
            if(dados.length === 0){
                swal({
                    icon: 'warning',
                    title: 'Dados inexistentes!',
                        text: 'Cadastre as informações do estabelecimento para utilizar o sistema.',
                    button: {
                        text: 'Ok, vamos nessa!'
                    }
                }).then(()=>{
                    document.location.assign("infoSis.html");
                });
            }else{
                estaLogado();
            }
        });
    
    }).catch((err)=>{
    
        console.log(JSON.stringify(err));
    
        if (err.code === 'ER_ACCESS_DENIED_ERROR'){    
            swal({
                icon: 'error',
                text: 'Falha na conexão com o banco de dados: configuração de usuário e/ou senha incorreta!\nContate o suporte SICLOP - (11)2841-6556'
            }).then(c=>{
                const {remote} = require('electron');
                remote.BrowserWindow.getFocusedWindow().close();
            });
            return;
        }
        
        if (err.code === 'ER_BAD_DB_ERROR'){
            require(path.join('../../database/criaBanco'));
            return;
        }

        if(hostRemoto === 'localhost' && err.code === 'ECONNREFUSED'){
            swal({
                icon: 'warning',
                title: 'Iniciando serviço MySQL',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Por favor, aguarde...',
                button: {text: 'Okay', closeModal: false}
            }).then(c=>{
                //const {remote} = require('electron');
                //remote.BrowserWindow.getFocusedWindow().close();
            });
            //Existem dois motivos para esse erro, ou a senha do usuario localhost está incorreta, ou o serviço do MYSQL está parado
            if(primeiraPassagemErroRefused === false){
                primeiraPassagemErroRefused = true;
                checaStatusServicoSQL(true);
            }else{
                swal({
                    icon: 'warning',
                    title: 'Senha do usuário do banco de dados incorreta',
                    closeOnClickOutside:false,
                    closeOnEsc: false,
                    text: 'Solicite correção ao suporte da SICLOP',
                    button: {text: 'Okay', closeModal: false}
                }).then(c=>{
                    const {remote} = require('electron');
                    remote.BrowserWindow.getFocusedWindow().close();
                });
            }
            return;
        }
    
        if(usuario === 'terminal' && (err.code === 'ECONNREFUSED' || err.code === 'EADDRNOTAVAIL')){
            contadorIPs++;
            if(vetorIPs[contadorIPs] !== undefined) testaComunicacao(vetorIPs[contadorIPs],'terminal');
            else swal({
                icon: 'warning',
                title: 'O programa não encontrou um servidor disponível na rede',
                closeOnClickOutside:false,
                closeOnEsc: false,
                text: 'Solicite ajuda ao suporte da SICLOP ou leia a ajuda',
                button: {text: 'Okay', closeModal: false}
            }).then(c=>{
                const {remote} = require('electron');
                remote.BrowserWindow.getFocusedWindow().close();
            });

            return;
        }
        //verificaExistenciaBanco();
        solicitaInfosBanco();
        return;
        swal({
            icon: 'error',
            text: 'Erro na conexão com o banco de dados: ' + err.message + '\nCódigo do erro: ' + err.code
        });
    
    });
}

async function esqueceuSenha(){
    var cCadFuncionario = require(path.join(__dirname + '../../../controller/cCadFunc'));
    swal({
        icon: 'warning',
        title: 'Digite seu usuário',
        closeOnClickOutside:false,
        content: 'input',
        buttons: ['Cancelar', 'Seguir'],
        closeModal: false
    }).then(async confirm=>{
        const username = confirm.toUpperCase();
        if(username){
            const dadosUsuario = (await cCadFuncionario.selectComLogin(username))[0];

            if(dadosUsuario) solicitaFraseDeSegurança(dadosUsuario, cCadFuncionario);
            else 
            swal({
               icon: 'error',
               title: 'Não há nenhum usuário com esse login',
               closeOnClickOutside: true,
               text: 'Por favor, tente novamente',
               button: {text: 'Ok', closeModal: true}
            });

        }
    }).catch(e=>{
       throw e;
    });
}

async function solicitaFraseDeSegurança(dadosUser, controller){
    swal({
        icon: 'warning',
        title: 'Digite a frase de segurança',
        closeOnClickOutside:false,
        content: 'input',
        buttons: ['Cancelar', 'Seguir'],
    }).then(confirm=>{
        if(dadosUser.frase_seguranca.toUpperCase() === confirm.toUpperCase()) solicitaNovaSenha(dadosUser, controller);
        else 
        swal({
            icon: 'error',
            title: 'Frase de segurança incompatível!',
            closeOnClickOutside: true,
            text: 'Por favor, tente novamente',
            button: {text: 'Ok', closeModal: true}
         });
    }).catch(e=>{
        throw e;
    });
}

async function solicitaNovaSenha(dadosUser, controller){
    swal({
        icon: 'success',
        title: 'Frase validada! Digite a nova senha:',
        closeOnClickOutside:false,
        content: 'input',
        buttons: ['Cancelar', 'Seguir'],
    }).then(confirm=>{
        if(confirm !== '') confirmaNovaSenha(dadosUser, controller, confirm.toUpperCase());
    }).catch(e=>{
        throw e;
    });
}

async function confirmaNovaSenha(dadosUser, controller, novaSenha){
    swal({
        icon: 'warning',
        title: 'Confirme a senha',
        closeOnClickOutside:false,
        content: 'input',
        buttons: ['Cancelar', 'Seguir'],
    }).then(confirm=>{
        if(novaSenha === confirm.toUpperCase()) atualizaSenhaUsuario(dadosUser, controller, confirm.toUpperCase());
        else {
            swal({
                icon: 'error',
                title: 'Senhas divergentes!',
                closeOnClickOutside: true,
                text: 'Por favor, tente novamente',
                buttons: ['Cancelar', 'Inserir nova senha']
            }).then(c=>{ if(c) solicitaNovaSenha(dadosUser, controller) });
        }
    }).catch(e=>{
        throw e;
    });
}

async function atualizaSenhaUsuario(dadosUser, controller, novaSenha){
    await controller.atualizaSenha(dadosUser.id_oper, novaSenha);
    swal({
       icon: 'success',
       title: 'Senha atualizada com sucesso!',
       closeOnClickOutside:true,
       text: 'Agora faça o login com a nova senha',
       button: {text: 'Ok', closeModal: true}
    });
}