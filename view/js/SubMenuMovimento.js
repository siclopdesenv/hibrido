const { ClientRequest } = require('http');
var Bind = require('mousetrap');
let { isArray } = require('util');
var Helper = require(path.join(__dirname + '../../../controller/Helper.js'));
var cCadPedido = require(path.join(__dirname + '../../../controller/cCadPedido.js'));
var cItemPedido = require(path.join(__dirname + '../../../controller/cItemPedido.js'));
const Pedido = require('../../controller/cCadPedido');
let dadosEstab = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));

window.sessionStorage.setItem('ultimoAtalho', '');

Bind.bind(['command+1', 'ctrl+1'], function () {
    if(!validaAtalho('ctrl+1')) return 0;
    abrirSearch();
});

Bind.bind(['command+2', 'ctrl+2'], function () {
    if(!validaAtalho('ctrl+2')) return 0;
    window.sessionStorage.setItem('carregouMesas', false)
    irParaCardapio('adicionar', 'Balcão', undefined, 'BALCÃO')
});

Bind.bind(['command+3', 'ctrl+3'], function () {
    if(!validaAtalho('ctrl+3')) return 0;
    carregaFrame("ReservaDeMesa")
});

Bind.bind(['command+4', 'ctrl+4'], function () {
    if(!validaAtalho('ctrl+4')) return 0;
    abrirModais('comanda');
});

Bind.bind(['command+5', 'ctrl+5'], function () {
    if(!validaAtalho('ctrl+5')) return 0;
    carregaFrame("frameSubMenuUltimo")
});

Bind.bind(['command+5', 'ctrl+6'], function () {
    if(!validaAtalho('ctrl+6')) return 0;
    carregaFrame("frameSubMenuCaixa")
});

function validaAtalho(atalho){
    console.warn(atalho);
    console.warn(window.sessionStorage.getItem('ultimoAtalho'));

    if(atalho !== window.sessionStorage.getItem('ultimoAtalho')){
        window.sessionStorage.setItem('ultimoAtalho', atalho);
        return true;
    }
    return false;
}

/* =================== VARIAVEIS DE AMBIENTE  =========================== */

inforsis = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
usuario = JSON.parse(window.localStorage.getItem('usuario'));

const { abrirInputPagamento, adicionaPedidoNaTela, fecharTelaPagamento, selecionarFormasPgto} = require(path.join(__dirname + '\\..\\js\\telaPagamento.js'))

const { SelecionaProdutos, 
        clickValor, 
        removeProduto, 
        adicionarQtd, 
        removerQtd, 
        enviaTexto, 
        finalizaPedido, 
        Retornar, 
        abrirIngredientes, 
        fecharModalIngrediente, 
        abrirItensExtras, 
        colocaItemExtra, 
        quantidade,
        fecharItensExtras,
        fecharModalItensExtras,
        irParaProximoSabor } = require(path.join(__dirname, '..\\html\\ModuloPedido\\Cardapio.js'));

const { guardaCliente, 
        salvaNovoEndereco, 
        listenFocusOutCep, 
        fecharModalCadastro, 
        modalTrocaEndereco, 
        abrirPedidoDelivery } = require(path.join(__dirname + '\\..\\js\\SubEncontrarCliente.js'));

var { irParaCardapioComanda, 
        buscaPedidoComanda, 
        cancelarPedidoComanda } = require(path.join(__dirname + '\\..\\js\\pedidoComanda.js'));

const { irParaCardapioBalcao } = require(path.join(__dirname + '\\..\\js\\PedidoBalcao.js'))
;

/* FUNÇÕES DE TELA */

window.sessionStorage.removeItem('itensDosPedidos');
window.sessionStorage.removeItem('itemDoPedidoMesa');
window.sessionStorage.removeItem('pedidoMesa');
window.sessionStorage.removeItem('pedidoTotais');

function abrirSearch(){
    document.getElementById('container-search').style.display = 'block';

    /*let btnOkSearch = document.getElementById('ok-search');
    
    btnOkSearch.addEventListener('click' , function (){
        window.localStorage.setItem('searchDelivery', document.getElementById('input-search').value.toUpperCase());
        carregaFrame('frameSubEncontrarCliente');
    })*/
}

function procuraCliente(){
    let informacaoCliente = document.getElementById('input-search').value.toUpperCase();

    //#region Verificação para transformar telefone (se foi digitado um) sem máscara em telefone com máscara
    const converitdoInteiro = parseInt(informacaoCliente);
    if(!isNaN(converitdoInteiro)){
        if(informacaoCliente.length === 9) informacaoCliente = informacaoCliente.substring(0,5) + '-' + informacaoCliente.substring(5,8);
        else if(informacaoCliente.length === 8) informacaoCliente = informacaoCliente.substring(0,4) + '-' + informacaoCliente.substring(4,7);
    }
    //#endregion

    window.localStorage.setItem('searchDelivery', informacaoCliente);
    carregaFrame('frameSubEncontrarCliente');
}

async function abrirModais(tela) {
    valorTroco = 0;
    if (tela === "delivery") {
        let produtos = await (require('../../controller/cCadProdutos.js').selectAll());
        if(produtos.length === 0) {
            swal({
               icon: 'warning',
               title: 'Você ainda não cadastrou produtos!',
               closeOnClickOutside:false,
               text: 'Deseja cadastrar agora?',
               buttons:['Não', 'Cadastrar produtos']
            }).then(confirm=>{
               if(confirm === true) carregaFrame(`frameCadProdutos`);
            }).catch(e=>{
               throw e;
            });
            return;
        }
        document.getElementById('container-search').style.display     = 'block';
        document.getElementById('input-search').focus();
        document.getElementById('input-search').addEventListener('keyup', (objKey)=>{
            if(objKey.key === 'Enter') document.getElementById('ok-search').click();
        });
        window.localStorage.setItem('cobrarTaxaEntregaPedido', 'true');
    } else if (tela === "cliente") {
        document.getElementById('container-cliente').style.display    = 'block';
    } else if (tela === "endereco") {
        document.getElementById('container-alterado').style.display   = 'block';
    }else if (tela === "itens-extras"){
        document.getElementById('itens-extras').style.display         = 'block';
    }else if (tela === "seleciona-item-extra"){
        document.getElementById('seleciona-item-extra').style.display = 'block';
    }else if (tela === "troca"){
        document.getElementById('container-troca-end').style.display  = 'block';
    }else if (tela === "comanda"){
        let produtos = await (require('../../controller/cCadProdutos.js').selectAll());
        if(produtos.length === 0) {
            swal({
               icon: 'warning',
               title: 'Você ainda não cadastrou produtos!',
               closeOnClickOutside:false,
               text: 'Deseja cadastrar agora?',
               buttons:['Não', 'Cadastrar produtos']
            }).then(confirm=>{
               if(confirm === true) carregaFrame(`frameCadProdutos`);
            }).catch(e=>{
               throw e;
            });
            return;
        }
        document.getElementById('container-comanda').style.display    = 'block';
        if(window.sessionStorage.getItem('pedidosTotais') === null || window.sessionStorage.getItem('pedidosTotais') === undefined){
            zerarVariaveis();
            selecionaPedidos();
        }

        setTimeout(() => {
            let campoComanda = document.getElementById('domComanda');
            campoComanda.addEventListener('focusout', buscaPedidoComanda);
        }, 1000);
    }
}

function fecharModais(tela) {
    if (tela === "delivery") {
        document.getElementById('container-search').style.display     = 'none';
    } else if (tela === "cliente") {
        document.getElementById('container-cliente').style.display    = 'none';
    } else if (tela === "endereco") {
        document.getElementById('container-alterado').style.display   = 'none';
    }else if (tela === "itens-extras"){
        document.getElementById('itens-extras').style.display         = 'none';
    }else if (tela === "seleciona-item-extra"){
        document.getElementById('seleciona-item-extra').style.display = 'none';
    }else if (tela === "troca"){
        document.getElementById('container-troca-end').style.display  = 'none';
    }else if (tela === "comanda"){
        document.getElementById('container-comanda').style.display    = 'none';
    }
}

function close_popup_delivery(){
    //zerar a variavel do atalho
    document.getElementById('container-search').style.display = 'none';
}

function abrirModalTroca(){
    fecharModais('troca');
    abrirModais('endereco');
}

function retornar(){
    if(clientes.length > 1){
        fecharModais('endereco');
    }else{
        if(tipoMenu === "0"){
            window.location.href = "../html/menuTradicional.html"
            setTimeout(() => {
                abrirSearch();
            }, 100);
        }else{
            carregaFrame('frameSubMenuMovimento', "SubMenu");
            setTimeout(() => {
                abrirSearch();
            }, 100);
        } 
    }
    
}
