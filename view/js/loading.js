let barraLoading = document.getElementById('barra-loading');
let segundos = 0;
var prog = 0;
var progresso;
let nome = JSON.parse(window.localStorage.usuario).nome;
let tpMenu = window.localStorage.tipoMenu;
let path = require('path');
let cConfImpressoras = require('../../controller/cConfImpressoras.js');

async function atualizaImpressoras(){
	cConfImpressoras.selectAll().then(async retorno=>{
		for(impressora of retorno){
			await cConfImpressoras.sincronizaPortas(impressora.portaLPT, impressora.nomePC, impressora.apelido);
		}
	});
}

atualizaImpressoras();

function progressoBarraLoading(prog){
	barraLoading.style.width = prog+"%";
	if(prog > 100){
		if(tpMenu == 1){
			window.location.href = "./menuPrincipal.html"; 
		}else{
			window.location.href = "./menuTradicional.html"; 
		}
	}
}

window.localStorage.setItem('fistLoad', true);

setInterval(function(){ segundos += 10;
	progressoBarraLoading(segundos);
}, 1000);
