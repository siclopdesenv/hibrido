var helper = require(path.join(__dirname + '../../../controller/Helper.js'));
var cPendura = require(path.join(__dirname + '../../../controller/cPendura.js'));
var cHistPgto = require(path.join(__dirname + '../../../controller/cHistPgto.js'));
var corpoTabela = document.getElementById('tBody');
var domValorRecebido = document.getElementById('valor_ja_recebido');
var domValorReceber = document.getElementById('valor_receber');
var domValorReceberAgora = document.getElementById('valor_receber_agora');
var domDataReceber = document.getElementById('dt_receber');
var domCliente = document.getElementById('cliente');
var domIdPedido = document.getElementById('id_pedido');
var btnCadastrar = document.getElementById('cadastrar');
var baixarPendura = false;
var idPedidoSelecionado = 0;

console.log('TELA DE PENDURAS');

document.getElementById('op1').addEventListener('click',()=>{listenerBtnOpcao('op1', 'op2', 'op3')});
document.getElementById('op2').addEventListener('click',()=>{listenerBtnOpcao('op2', 'op1', 'op3')});
document.getElementById('op3').addEventListener('click',()=>{listenerBtnOpcao('op3', 'op1', 'op2')});

document.getElementById('op1').click();

function listenerBtnOpcao(idBtnOpcaoMarcada, idDesativa1, idDesativa2){
    document.getElementById(idBtnOpcaoMarcada).style.backgroundColor = '#044D70';
    document.getElementById(idDesativa1).style.backgroundColor = '#101010';
    document.getElementById(idDesativa2).style.backgroundColor = '#101010';
    performaPesquisaEscolhida(idBtnOpcaoMarcada);
}

function performaPesquisaEscolhida(opcao){
    switch(opcao){
        case 'op1': recuperaTodasPenduras(); break;
        case 'op2': recuperaPendurasAgendadasParaPeriodo(); break;
        case 'op3': recuperaPendurasCriadasNoPeriodo(); break;
    }
}

async function recuperaTodasPenduras(){
    limpaTbody();
    let preparaConteudo = ``;
    const penduras = await cPendura.selectAll();

    function fabricaData(obj){
        return obj.getDate() + '/' + (obj.getMonth() + 1) + '/' + obj.getFullYear();
    }

    for(p of penduras){
        if(!p.recebido){
            preparaConteudo += `
            <tr onclick="selecionaPendura(${p.id_pendura})">
                <td class="primeiraTd">${fabricaData(p.dt_receber)}</td>
                <td>${p.cliente}</td>
                <td>R$ ${(p.valor_receber - p.valor_ja_recebido).toFixed(2)}</td>
            </tr>
            `;
        }
       
    }

    corpoTabela.innerHTML = preparaConteudo;
}

async function recuperaPendurasCriadasNoPeriodo(){
    limpaTbody();
    let preparaConteudo = `
    <tr onclick="teste">
        <td class="primeiraTd">op3</td>
        <td>op3</td>
        <td>op3</td>
    </tr>`;
    /*const penduras = await cPendura.selectAll();

    function fabricaData(obj){
        return obj.getDay() + '/' + (obj.getMonth() + 1) + '/' + obj.getFullYear();
    }

    for(p of penduras){
        preparaConteudo += `
            <tr>
                <td class="primeiraTd">${fabricaData(p.dt_receber)}</td>
                <td>${p.cliente}</td>
                <td>R$ ${(p.valor_receber - p.valor_ja_recebido).toFixed(2)}</td>
            </tr>
        `;
    }*/

    corpoTabela.innerHTML = preparaConteudo;
}

async function recuperaPendurasAgendadasParaPeriodo(){
    limpaTbody();
    let preparaConteudo = `
    <tr>
        <td class="primeiraTd">op2</td>
        <td>op2</td>
        <td>op2</td>
    </tr>`;
    /*const penduras = await cPendura.selectAll();

    function fabricaData(obj){
        return obj.getDay() + '/' + (obj.getMonth() + 1) + '/' + obj.getFullYear();
    }

    for(p of penduras){
        preparaConteudo += `
            <tr>
                <td class="primeiraTd">${fabricaData(p.dt_receber)}</td>
                <td>${p.cliente}</td>
                <td>R$ ${(p.valor_receber - p.valor_ja_recebido).toFixed(2)}</td>
            </tr>
        `;
    }*/

    corpoTabela.innerHTML = preparaConteudo;
}

async function selecionaPendura(idPendura){
    domValorRecebido.focus();
    btnCadastrar.value = idPendura;
    const pendura = await cPendura.select(idPendura);
    window.sessionStorage.setItem('penduraSelecionada', JSON.stringify(pendura));

    function fabricaDataPadraoCampo(){
        return pendura.dt_receber.getFullYear() + '-' + pendura.dt_receber.getMonth() + '-' + pendura.dt_receber.getDate();
    }

    domValorRecebido.value  = pendura.valor_ja_recebido;
    domValorReceber.value   = pendura.valor_receber;
    domDataReceber.value    = helper.converteDataBancoPDataCampo(pendura.dt_receber);
    domCliente.value        = pendura.cliente;
    domIdPedido.value       = pendura.id_pedido;
    idPedidoSelecionado     = pendura.id_pedido;

    document.getElementById('camadaModal').style.display = 'block';
}

function limpaTbody(){
    corpoTabela.innerHTML = '';
}

function salvarValorRecebidoPendura(penduraTotalemnteRecebida = false){
    var penduraSelecionada = JSON.parse(window.sessionStorage.getItem('penduraSelecionada'));
    var pendura = require(path.join(__dirname + '../../../controller/cPendura.js'));
    let valorRecebidoAgora = parseFloat(domValorReceberAgora.value.replace('.', '').replace('.', '').replace('.', '').replace(',', '.'));
    
    if(!valorRecebidoAgora) return;

    const valorRecebido = valorRecebidoAgora;
    valorRecebidoAgora += penduraSelecionada.valor_ja_recebido;

    let dataRecebimento = new Date();
    dataRecebimento.setHours(0);
    dataRecebimento.setMinutes(0);
    dataRecebimento.setSeconds(0);

    pendura.id_pendura = btnCadastrar.value;
    pendura.recebido            = penduraTotalemnteRecebida;
    pendura.dt_recebido         = new Date();
    pendura.valor_ja_recebido   = valorRecebidoAgora;

    pendura.update();

    //#region Registrando valor recebido no historico de pagamentos
    let data = new Date();
    data.setHours(0);
    data.setMinutes(0);
    data.setSeconds(0);
    let dataAgoraBanco = data.getFullYear() + '-' + (data.getMonth() + 1) + '-' + data.getDate();
    cHistPgto.nr_pedido = idPedidoSelecionado;
    cHistPgto.vl = valorRecebido;
    cHistPgto.qde_parcela = 1;
    cHistPgto.troco = 0;
    //cHistPgto.troco = (valorRecebidoAgora + penduraSelecionada.valor_ja_recebido) - penduraSelecionada.valor_receber;
    cHistPgto.dt_pedido = penduraSelecionada.tb_pedido.dt_pedido;
    cHistPgto.hr_pedido = penduraSelecionada.tb_pedido.hr_pedido;
    cHistPgto.id_fpgto = null;
    cHistPgto.dt_pgto = dataAgoraBanco;

    cHistPgto.insert();
    //#endregion

    addAlt('fechar');
    recuperaTodasPenduras();
}

btnCadastrar.addEventListener('click',()=>{
    salvarValorRecebidoPendura(baixarPendura);
});

domValorReceberAgora.addEventListener('keyup', ()=>{
    var penduraSelecionada = JSON.parse(window.sessionStorage.getItem('penduraSelecionada'));
    var valor2 = domValorReceberAgora.value !== '' ? parseFloat(domValorReceberAgora.value.replace('.', '').replace('.', '').replace('.', '').replace(',', '.')): 0.00;
    valor2 += penduraSelecionada.valor_ja_recebido;
    if(valor2 >= penduraSelecionada.valor_receber){
        btnCadastrar.innerHTML = 'Dar baixa';
        baixarPendura = true;
    }
    else {
        baixarPendura = false;
        btnCadastrar.innerHTML = 'Salvar';
    }
});