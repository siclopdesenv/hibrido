var $ = require("jquery");
var path = require('path');
var fs = require('fs');
window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));
let swal = require('sweetalert');
var Helper = require(path.join(__dirname + '/../../controller/Helper.js'));
var pjson = require('../../package.json');

var qtdItens = 1;
let idAntigo = "btn-aba-principal", controller, tbAux, resulPes, timeout, contProd;
var nomeTabAtual = "identificacao";

window.sessionStorage.setItem('carregouMesas', false) //fafzendo pra zerar isso toda vez que entrar no sistema

document.title = `Siclop Híbrido Beta v${pjson.version} - Olá ${JSON.parse(window.localStorage.getItem('usuario')).nome} !`;

function logout(){
	swal({
		icon: 'warning',
		text: 'Deseja realmente sair?',
		buttons:[
		'Sim',
		'Nao'
		]
	}).then(confirm =>{
		if(confirm === null) {
			window.localStorage.setItem('manterLogado', false);
			window.localStorage.setItem('fistLoad',true);
			window.location.assign(path.join(__dirname,'./telaLogin.html'));
		}
	});
}

function mudarCor(id){
	let cor;
	let corNav;
	switch(id){
		case 'corSiclop':
		cor = "linear-gradient(45deg, #2ea4d2 0%, #7d84e8 100%)";
		corNav = "linear-gradient(45deg, #7d84e8 0%, #2ea4d2 100%)";
		break;
		case 'corViva':
		cor = "#ffeb00";
		corNav = 'linear-gradient(45deg, #d39e00 0%, #f6ff00 100%)';		
		break;
		case 'corNatural':
		cor = "#28a745";
		corNav = 'linear-gradient(45deg, #28a745 0%, #2ea4d2 100%)';
		break;
		case 'corDark':
		cor = '#1d2124';
		corNav = 'linear-gradient(45deg, #1d2124cf 0%, #292323 100%)';
		break;
	}

	if(id == undefined && window.localStorage.getItem('corSistema') == "undefined"){
		document.getElementById('body-menu').style.background = 'linear-gradient(45deg, #2ea4d2 0%, #7d84e8 100%)';
	}else if(id != undefined){
		window.localStorage.setItem('corSistema', cor);
		window.localStorage.setItem('corNav', corNav);
	}

	if(window.localStorage.getItem('tipoMenu') == 1){
		document.getElementById('body-menu') ? document.getElementById('body-menu').style.background = window.localStorage.getItem('corSistema') : null;
	}else{
		document.getElementById('conteudoMain').style.background = window.localStorage.getItem('corSistema'); 
		document.getElementById('menuTrad').style.background = window.localStorage.getItem('corNav');
	}

}mudarCor();


function addItemModal(tipoGuia) {
	if (tipoGuia == 'formaPagamento')
		tbAux.addCampos('formasPagamento');
	else if (tipoGuia == 'funcoes')
		tbAux.addCampos('funcoes');
	else if (tipoGuia == 'terminais')
		addCamposTerminais('terminal');
	else if (tipoGuia == 'setor')
		tbAux.addCampos('setor');
	else if (tipoGuia == 'medida')
		tbAux.addCampos('unidadeMedida');
	else if (tipoGuia == 'destinoPedido') 
		tbAux.addCampos('destino')
	else if (tipoGuia == 'tpVenda') 
		tbAux.addCampos('tipoVenda')
	else if (tipoGuia == 'tpProduto')
		tbAux.addCampos('tipoProduto');
	else if (tipoGuia == 'tpLogradouro') 
		tbAux.addCampos('tipoLogradouro');
	else if (tipoGuia == 'cidade')
		tbAux.addCampos('cidade')
	else if (tipoGuia == 'estado')
		tbAux.addCampos('estado');
	
	qtdItens++;

}

function addAlt(acao,edicaoItem) {
	let abrirMod = document.getElementById('addAlt');
	let camadaModal = document.getElementById('camadaModal');	

	camadaModal.style.height = 'initial';
	camadaModal.style.display = 'block';

	if (acao == 'fechar') {
		let inputs = document.getElementsByTagName('input');
		for(var i = 0; i< inputs.length; i++){
			inputs[i].value = "";
		}
		camadaModal.style.display = 'none';
	}

	if (edicaoItem == true && camadaModal.parentNode) {
		camadaModal.parentNode.removeChild(camadaModal);
	}
}

function acoesAbaModais(idBtn){
	//elementos que irão mudar
	let dadosPrincipaisModal = document.getElementById("dados-principais-modal");
	let dadosSecundariosModal = document.getElementById("dados-secundarios-modal");
	let dadosTerceariosModal = document.getElementById("dados-tercearios-modal");

	let lbltitModal1 =  document.getElementById("lbl-aba-principal");
	let lbltitModal2 =  document.getElementById("lbl-aba-secundaria");
	let lbltitModal3 =  document.getElementById("lbl-aba-tercearia");

	if(idBtn == "btn-aba-principal"){
		document.getElementById(idAntigo).classList.remove("abas-cad-ativa");
		document.getElementById(idAntigo).classList.add("abas-info-sis");
		dadosPrincipaisModal.style.display = 'block';
	
		dadosSecundariosModal.style.display = 'none';
		dadosTerceariosModal.style.display = 'none';

		document.getElementById(idBtn).classList.remove("abas-info-sis");
		document.getElementById(idBtn).classList.add("abas-cad-ativa");

		lbltitModal2.classList.remove("titulo-modal-cad","titulo-modal-info-sis-ativa");
		lbltitModal3.classList.remove("titulo-modal-cad","titulo-modal-info-sis-ativa");
		lbltitModal1.classList.add("titulo-modal-cad","titulo-modal-info-sis-ativa");

		idAntigo = idBtn;
	}else if(idBtn == "btn-aba-secundaria"){
		document.getElementById(idAntigo).classList.remove("abas-cad-ativa");
		document.getElementById(idAntigo).classList.add("abas-info-sis");

		dadosPrincipaisModal.style.display = 'none';
		dadosTerceariosModal ? dadosTerceariosModal.style.display = 'none' : null;
		dadosSecundariosModal.style.display = 'block';

		document.getElementById(idBtn).classList.remove("abas-info-sis");
		document.getElementById(idBtn).classList.add("abas-cad-ativa");

		lbltitModal1.classList.remove("titulo-modal-cad","titulo-modal-info-sis-ativa");
		lbltitModal3 ? lbltitModal3.classList.remove("titulo-modal-cad","titulo-modal-info-sis-ativa") : null;
		lbltitModal2.classList.add("titulo-modal-cad","titulo-modal-info-sis-ativa");


		idAntigo = idBtn;
	}else if (idBtn == "btn-aba-tercearia"){
		document.getElementById(idAntigo).classList.remove("abas-cad-ativa");
		document.getElementById(idAntigo).classList.add("abas-info-sis");

		dadosSecundariosModal.style.display = 'none';
		dadosPrincipaisModal.style.display = 'none';
		dadosTerceariosModal.style.display = 'block';

		document.getElementById(idBtn).classList.remove("abas-info-sis");
		document.getElementById(idBtn).classList.add("abas-cad-ativa");

		lbltitModal1.classList.remove("titulo-modal-cad","titulo-modal-info-sis-ativa");
		lbltitModal2.classList.remove("titulo-modal-cad","titulo-modal-info-sis-ativa");
		lbltitModal3.classList.add("titulo-modal-cad","titulo-modal-info-sis-ativa");


		idAntigo = idBtn;
	}

}

// async function excluirItem(id){
// 	event.preventDefault();
// 	id = parseInt(id);

// 	swal('Tem certeza que deseja excluir ?',
// 	{
// 		icon: "warning",
// 		buttons: {
// 			cancelar:true,
// 			confirmar:true
// 		}
// 	}).then((acao)=>{		
// 		if(acao == "confirmar"){
// 			let Log = require(path.join(__dirname + '/../../controller/Logs.js'));
// 			let infoUser = JSON.parse(window.localStorage.getItem('usuario'));
// 			Log.descricao = "Item de ID:"+id+" deletado em "+nomeJs+" por: "+infoUser.operador;
// 			Log.id_operador = infoUser.id_oper;
// 			Log.insert();
// 			controller = require(path.join(__dirname + '/../../controller/'+nomeJs+'.js'));
// 			controller.delete(id);
// 			swal('Campo Deletado!!!');
// 			document.getElementById('tr'+id).innerHTML = '';
// 		}
// 	});
// }


async function excluirItem(id){
	event.preventDefault();
	id = parseInt(id);
	let qtdRegistros;
	controller = require(path.join(__dirname + '/../../controller/c'+nomeJs+'.js'));
	controller.buscaRelacoes(id).then(retorno=>{
		if((retorno.length !== 0 && retorno.length != undefined) || retorno > 0){
			if(Number.isInteger(retorno))
				qtdRegistros = retorno;
			else
				qtdRegistros = retorno.length;

			swal({
				icon:'warning',
				title:'Ação não permitida!',
				text: 'Existe(m) ' + qtdRegistros + ' registro(s) relacionado(s) com essa informação, por favor altere-o(s) antes de excluir este dado!'
			});
		}else{
			swal('Tem certeza que deseja excluir ?',
			{
				icon: "warning",
				buttons: ['SIM', 'NÃO']
			}).then(async (acao)=>{
				if(acao !== true){
					const retornoDelete = await controller.delete(id);
					if(retornoDelete !== 'NP'){
						swal('REGISTRO EXCLUÍDO COM SUCESSO!', {icon: "success"});
						document.getElementById('tr'+id).innerHTML = '';
					}
				}
			});
		}
	});	

}


async function editarItem(id, telaJaCarregada = false){
	if(telaJaCarregada === 'produtos' && cCadProdutos){
		id = parseInt(id);
		let resul = await cCadProdutos.select(id);
		carregaJs.selecionarItem(resul);
	}else{
		id = parseInt(id);
		controller = require(path.join(__dirname + '/../../controller/c'+nomeJs+'.js'));
		resul = await controller.select(id);
		carregaJs = require(path.join(__dirname + '/../js/' + nomeJs+'.js'));
		carregaJs.selecionarItem(resul);
	}
	
}

let pesquisaAnterior = '';

async function pesquisar(){
	let obj;
	let inputPes = document.getElementById('inputPesquisa');
	clearTimeout(timeout);
	timeout = setTimeout( async ()=>{
		resulPes = inputPes.value.trim();
		console.log(resulPes);

		if(resulPes.length === 0){
			carregaJs.carregaConteudoOriginal ? carregaJs.carregaConteudoOriginal() : carregaJs.selecionarTudo();
			return;
		}

		if(resulPes.length < 3){
			return;
		}

		controller = require(path.join(__dirname + '/../../controller/c'+nomeJs+'.js'));
		resul = await controller.like(resulPes);
		carregaJs = await require(path.join(__dirname + '/../js/' + nomeJs+'.js'));

		let tBody = document.getElementsByTagName('tbody')[0];
		tBody.innerHTML ="";
		obj = Object.assign({}, resul);
		for(pos in resul){
			carregaJs.insereItemNaTela(resul[pos],false, parseInt(pos), true);
		}
		setTimeout(()=>{
			carregaJs.innerPesquisa ? carregaJs.innerPesquisa() : null;
		},2000);
	},1000);
}

async function carregaFrame(nome, subMenu = null) {
	var conteudoMain = document.getElementById("conteudoMain");
	let vrframe = nome.search('frameSubMenu');
	let vrframeCases = nome.search('framSubMenuUltimo');
	let tipomenu = localStorage.getItem('tipoMenu');

	if (subMenu == null || subMenu == "modal") {
		conteudoMain.classList.remove("px-5","semi-transparente");
		conteudoMain.classList.add("col-11","bg-transparent","py-0", "pr-5");
	}else if (tipomenu === "0" && subMenu == "back"){
		document.location.href = "../html/menuTradicional.html"
	}else if (subMenu == "back" && tipomenu === "1") {

		setTimeout(function () {
			conteudoMain.classList.add("px-5");
			conteudoMain.classList.remove("bg-transparent");
			conteudoMain.classList.add("semi-transparente","py-0");
		}, 50);
	}

	$(function(){
		$("#conteudoMain").load(nome+'.html');
		setTimeout(function(){
			var barraHeader = document.getElementById('nav-tabContent');
			let tig;
			if (subMenu == false) {
				conteudoMain.classList.add("pl-4");
				barraHeader.classList.remove("col-12");
				barraHeader.classList.add("col-11","ml-5");

				tig = document.getElementById('tituloGrande');
				$('#tituloGrande').css('left', '5%');
				tig.classList.remove("col-md-6","col-lg-4","ml-4");
				tig.classList.add("col-4", "offset-2","ml-5");

				// tig.insertAdjacentHTML("afterend",`
				// 	<button id="sair" class="btn-sem-decoracao float-right" onclick="logout()">
				// 	<img class="mt-3 img-saida" src="../img/saida.png" alt="">
				// 	<center class="text-light"><h5>Sair</h5> </center>
				// 	</button>
				// 	`)
			}
			if(vrframe == -1 && nome != "frameConfDadosEstabelecimento"){
				tig = document.getElementById('tituloGrande');

				if(tig) tig.insertAdjacentHTML("afterend",`
					<button id="sair" class="btn-sem-decoracao float-right btn-sair-acima-main" onclick="logout()">
					<img class="mt-3 img-saida img-saida-personalizar" src="../img/saida.png" alt="">

					</button>
					`);

			}else{
				//let headerSubmenu = document.getElementById('header-sub-menu');
				//headerSubmenu.insertAdjacentHTML("afterend",`
			}


			if(nome != undefined && barraHeader != undefined){
			//if(nome != undefined && barraHeader != undefined){
				let frameAtual;
				let tblAuxFrame = nome.search('frameTblAux');
				let cadFrame = nome.search('frameCad');
				let confFrames = nome.search('frameConf');
				let confSubMenu = nome.search('frameSub')
				if(tblAuxFrame >= 0){
					frameAtual = 'frameSubMenuTblAux';
				}else if(cadFrame >= 0){
					frameAtual = 'frameSubMenuCadastros';
				}else if(confFrames >= 0){
					frameAtual = 'frameSubMenuConfiguracoes';
				}else if(confSubMenu >= 0){
					frameAtual = 'frameSubMenuMovimento';
				}

				var txt = `
				<!-- barra de pesquisa -->
				<div class="row mx-0">
				<div class="pl-3 col-md-8 col-lg-5 bg-transparent">
				<div class="row pt-3">
				<div class="col-5 border-right conf-breadcrumb">
				<div class="imgBack">
				<button class="btn-sem-decoracao" onclick="carregaFrame('${frameAtual}','back')">
				<img src="../img/backButton.svg"/>
				</button>
				</div>
				</div>
				<div class="col-4 border-right conf-breadcrumb">
				<button class="btn-sem-decoracao" onclick="voltarHome()">
				Tela Inicial
				</button>
				</div>

				<!--
				<div class="col-5 border-right conf-breadcrumb" aria-current="page">${nome}
				</div>
				-->
				</div>
				</div>
				<div class="offset-3 col-3 p-2">
				<div class="barra-pesquisa position-relative w-100 height-100 input-group">
				<input id="inputPesquisa" onkeyup="pesquisar()" type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="Pesquisar">
				</div>
				</div>
				<div id="btnNovo" class="btn-novo col-1 py-1 px-0 pr-1">
				</div>
				</div>
				<!-- fim -->`;

				if(nome != "frameConfDadosEstabelecimento"){
					titFrame = document.getElementById("titulo-frame");
					titFrame = `<div class="row linha-titulo-frame"> ${titFrame.innerHTML} </div>`;
					barraHeader.insertAdjacentHTML('afterbegin', txt);
					barraHeader.insertAdjacentHTML('afterbegin', titFrame);
				}else{
					var txt = `
					<!-- barra de pesquisa -->
					<div class="row mx-0">
					<div class="pl-3 col-md-8 col-lg-5 bg-transparent">
					<div class="row py-2">
					<div class="col-5 border-right conf-breadcrumb">
					<div class="imgBack">
					<button class="btn-sem-decoracao" onclick="carregaFrame('${frameAtual}','back')">
					<img src="../img/backButton.svg"/>
					</button>
					</div>
					</div>
					<div class="col-4 border-right conf-breadcrumb">
					<button class="btn-sem-decoracao" onclick="voltarHome()">
						Tela Inicial
					</button>
					</div>


					</div>
					</div>

					</div>
					<!-- fim -->`;
					barraHeader.insertAdjacentHTML('afterbegin', txt);
				}
				//barraHeader.appendChild(titFrame);

				let botaoSalvar = document.getElementById("addAlt");
				let divBtnNovo = document.getElementById("btnNovo");
				try {
					btnNovo.appendChild(botaoSalvar);
					botaoSalvar.classList.add('mt-1');
				} catch(e) {console.log(e);}
				
			}
		}, 80);

setTimeout(()=>{	
	if(nome.search('frameSubMenuCadastros') > -1){
		setTimeout(()=>{qtdRegistrosCad()},1000);
	}else if(vrframe == -1 || vrframeCases == -1){
		nomeJs = nome.replace('frame','');
		if(nomeJs.search("TblAux") == -1){
			setTimeout(()=>{
				if(nomeJs === 'SubMenuConfiguracoes' || nomeJs === 'SubMenuPenduras') return;
				carregaJs = require(path.join(__dirname + '/../js/' + nomeJs));
				if(nomeJs === 'SubMenuMovimento') require(path.join(__dirname, '../js/GerenciarCaixa.js'));
				if(carregaJs.selecionarTudo !== undefined) carregaJs.selecionarTudo();
				var descarregarJs =  require.resolve(path.join(__dirname + '/../js/' + nomeJs));
				delete require.cache[descarregarJs];
			}, 300);
		}
	}
},80);

});

tbAux = require(path.join(__dirname + '/../js/tbAuxiliares'));


setTimeout(()=>{
	if (nome == 'frameTblAuxFormaPagamento')
		tbAux.recuperaDadosBanco('FormasPagamento');
	else if (nome == 'frameTblAuxFuncoes')
		tbAux.recuperaDadosBanco('Funcoes');
	else if (nome == 'frameTblAuxTerminais')
		tbAux.recuperaDadosBanco('Terminal', true);
	else if (nome == 'frameTblAuxSetor')
		tbAux.recuperaDadosBanco('Setor');
	else if (nome == 'frameTblAuxMedida')
		tbAux.recuperaDadosBanco('UnidadeMedida');
	else if (nome == 'frameTblAuxDestinosPed')
		tbAux.recuperaDadosBanco('Destino');
	else if (nome == 'frameTblAuxTpVenda')
		tbAux.recuperaDadosBanco('TipoVenda');
	else if (nome == 'frameTblAuxTpProd')
		tbAux.recuperaDadosBanco('TipoProduto');
	else if (nome == 'frameTblAuxTpLogradouros')
		tbAux.recuperaDadosBanco('TipoLogradouro');
	else if (nome == 'frameTblAuxCidades')
		tbAux.recuperaDadosBanco('Cidade');
	else if (nome == 'frameTblAuxEstados')
		tbAux.recuperaDadosBanco('Estado');
	else if (nome == 'frameTblAuxNichos')
		tbAux.recuperaDadosBanco('Nichos');
	else if (nome == 'frameTblAuxNivelUsuario')
		tbAux.recuperaDadosBanco('NivelUsuario');
	else if (nome == 'frameTblAuxSituacaoCli')
		tbAux.recuperaDadosBanco('Situacao');
	else if (nome == 'frameTblAuxPeriodo')
		tbAux.recuperaDadosBanco('Periodo', false, true);
	else
			//console.log(nome);

		idRegistros = 0;
	},200);

	//Coloquei esse timemout acima para esperar todo o html ser carregado e depois chamar o método que preenche ele, isso evita pegar atributos antes deles serem criados, evitando assim manipular atributos nulos
}

function voltarHome(origem) {
	let tipoMenu = localStorage.getItem('tipoMenu');
	if(tipoMenu === "1"){
		document.location.replace("../html/menuPrincipal.html");
	}else if(tipoMenu === "2" || tipoMenu === "0"){
		if(origem === 'swalCaixa') {
			document.getElementById('dropMovimento').addEventListener('click',()=>{
				document.getElementById('navDropMovimento').style.display = 'none';
				swal({
				   icon: 'warning',
				   title: 'Ação não permitida',
				   closeOnClickOutside:true,
				   text: 'Você não pode entrar nas opções de movimento sem antes abrir um caixa! Se desejar abrir um caixa agora aperte "F5" no teclado',
				   buttons: ['Abrir um caixa', 'Sair']
				}).then(confirm => {
					if(!confirm) voltarHome();
				});
			});
			return;
		}
		document.location.replace("../html/menuTradicional.html");
	}
}

function proximo(tabAtual, proxTab) {
	if(proxTab != undefined){
		var objInputRequired;
		var verificar = true;
		tabAtual = !tabAtual ? nomeTabAtual : tabAtual.name;
		if(tabAtual == proxTab) return false;
		if(tabAtual == "identificacao"){
			objInputRequired = { 
				cfg_cnpj : document.getElementById("cfg_cnpj").value,
				cfg_nome : document.getElementById("cfg_nome").value,
				cfg_nome_fantasia : document.getElementById("cfg_nome_fantasia").value,
				cfg_fone1 : document.getElementById("cfg_fone1").value,
				cfg_ddd : document.getElementById("cfg_ddd").value,
				cfg_ie: document.getElementById('cfg_ie').value,
				cfg_im: document.getElementById('cfg_im').value,
				cfg_numero_local : document.getElementById("cfg_numero_local").value,
				cfg_id_logradouro : document.getElementById("cfg_cep_logradouro").value,
				cfg_nr_serie : document.getElementById("cfg_nr_serie").value,
				cfg_cod_lib : document.getElementById("cfg_cod_lib").value,
				cfg_id_nicho : document.getElementById("cfg_id_nicho").value,
				email : document.getElementById("email").value
			};

		}
		else if (tabAtual == "financeiro") {
			objInputRequired = { 
				cfg_porc_broto: document.getElementById("cfg_porc_broto").value,
				cfg_porc_media: document.getElementById("cfg_porc_media").value,
				cfg_porc_giga: document.getElementById("cfg_porc_giga").value
			};
			if(document.getElementById('cfg_formabroto').value === 'I'){
				objInputRequired = {};
			}
		}else if (tabAtual == "impressao") {
			/*objInputRequired = { 
				cfg_qde_cupom : document.getElementById("cfg_qde_cupom").value,
				apelido : document.getElementById('apelido').value,
				obs_impressora : document.getElementById('obs_impressora').value,
				ip_impressora : document.getElementById('ip_impressora').value,
				marca : document.getElementById('marca').value,
				modelo : document.getElementById('modelo').value,
				largura : document.getElementById('largura').value,
				salto : document.getElementById('salto').value
			};*/
			objInputRequired = { 
				cfg_qde_cupom : document.getElementById("cfg_qde_cupom").value
			};
		}else if (tabAtual == "primeiroImpressao") {
			objInputRequired = {
				cfg_qde_cupom : document.getElementById("cfg_qde_cupom").value,
				apelido : document.getElementById('apelido').value,
				obs_impressora : document.getElementById('obs_impressora').value,
				ip_impressora : document.getElementById('ip_impressora').value,
				marca : document.getElementById('marca').value,
				modelo : document.getElementById('modelo').value,
				largura : document.getElementById('largura').value,
				salto : document.getElementById('salto').value
			};
		}else if (tabAtual == "user") {
			objInputRequired = { 
				nome : document.getElementById("nome").value,
				operador : document.getElementById("operador").value,
				senha : document.getElementById("senha").value,
				senha2 : document.getElementById("senha2").value

			};
		}else{
			verificar = false;
		}


		if(verificar){
			for(var i in objInputRequired){
				if ( objInputRequired[i] == "") {

					swal({
						icon: 'warning',
						text: 'CAMPO "'+document.getElementById(i).name+'" FALTANDO!'
					})

					return false;
				};
			}
		}



		let atualTab = document.getElementById(tabAtual);
		let proximaTab = document.getElementById(proxTab);

		let navAtualTab = document.getElementById('nav-' + tabAtual),
		tipoAtualTab = document.getElementById(tabAtual+'-tab');

		navAtualTab.classList.remove('show','abas-info-sis-ativa');
		tipoAtualTab.classList.remove('active','titulo-modal-info-sis','titulo-modal-info-sis-ativa');

		atualTab.classList.remove('show');
		atualTab.classList.remove('active');
		document.getElementById('nav-' + tabAtual).style.background = '#e9ecefd9';

		proximaTab.classList.add('show');
		proximaTab.classList.add('active');

		let navProxTab = document.getElementById('nav-' + proxTab),
		tipoProxTab = document.getElementById(proxTab+'-tab');

		navProxTab.classList.add('abas-info-sis-ativa');
		tipoProxTab.classList.add('titulo-modal-info-sis','titulo-modal-info-sis-ativa');
		navProxTab.style.background = '#c9d6ff';
		//navProxTab

		event.preventDefault();
		nomeTabAtual = proxTab;
		botoesNav(tabAtual, proxTab);
	}
}

function botoesNav(tabAtual, proxTab){
	if(proxTab != undefined){
		let botaoSalvar = document.getElementById("botoes-"+proxTab+"-tab");
		let botaoSumir = document.getElementById("botoes-"+tabAtual+"-tab");
		let navBotoes = document.getElementById("nav-botoes");
		navBotoes.appendChild(botaoSalvar);
		botaoSalvar.style.display = 'block';
		botaoSumir.style.display = 'none';

		let botaoSalvar2 = document.getElementById("botoes-"+proxTab+"-tab-2");
		let botaoSumir2 = document.getElementById("botoes-"+tabAtual+"-tab-2");
		let navBotoes2 = document.getElementById("footer-botoes-nav");
		navBotoes2.appendChild(botaoSalvar2);
		botaoSalvar2.style.display = 'block';
		botaoSumir2.style.display = 'none';

	}else{
		let botaoSalvar = document.getElementById("botoes-home-tab");
		let navBotoes = document.getElementById("nav-botoes");
		navBotoes.appendChild(botaoSalvar);
	}

	//botaoSalvar.classList.add('mt-1');
}

function addCampos(fragment) {
	let cont = require(path.join(__dirname + '/../../controller/c' + fragment));
	var tbody = document.getElementById('listaAddItem');
	let id = 'id' + qtdItens;
	let save = 'save' + qtdItens;
	let nome = 'nome' + qtdItens;
	let tr = document.createElement('tr');
	tr.innerHTML = `<tr class="aparecendo"><th><center><button class="btn-sem-decoracao" id="${save}"><img src="../img/add-auxiliar.png"/></button></center></th><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="ID" id="${id}"></div> </td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="Nome" id="${nome}"></div></td></tr>`;
	tbody.appendChild(tr);

	document.getElementById(save).addEventListener('click', (e) => {
		e.preventDefault();
		cont.add(document.getElementById(id).value, document.getElementById(nome).value);
	});
}

function addCamposTerminais(fragment) {

	let cont = require(path.join(__dirname + '/../../controller/c' + fragment));

	var tbody = document.getElementById('listaAddItem');
	let save = 'save' + qtdItens;
	let idTerminal = 'terminal' + qtdItens;
	let codTerminal= 'cod' + qtdItens;
	let nome = 'nome' + qtdItens;
	//let tr = document.createElement('tr');
	//tr.innerHTML = `<tr id="trTemp${qtdItens}" class="aparecendo"><th><center><button class="btn-sem-decoracao" id="${save}" value="${qtdItens}" name="${nome}"><img src="../img/add-auxiliar.png"/></button></center></th><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="ID" maxlength="3" disabled=""></div> </td><td><div class="input-group input-group-sm"><input type="text" class="form-control" maxlength="3" placeholder="ID Tipo Terminal" id="${idTerminal}"></div></td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="Descrição" maxlength="15" id="${nome}"></div> </td></tr>`;
	//tr.innerHTML = `<tr id="trTemp${qtdItens}" class="aparecendo"><th><center><button class="btn-sem-decoracao" id="${save}" value="${qtdItens}" name="${nome}"><img src="../img/add-auxiliar.png"/></button></center></th><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="ID" maxlength="3" disabled=""></div> </td><td><div class="input-group input-group-sm"><input type="text" class="form-control" maxlength="3" placeholder="ID Tipo Terminal" id="${idTerminal}"></div></td><td><div class="input-group input-group-sm"><input type="text" class="form-control" placeholder="Descrição" maxlength="15" id="${nome}"></div> </td></tr>`;
	var htmlTerminal = `
	<tr id="trTemp${qtdItens}" class="aparecendo">
		<th>
			<center>
				<button class="btn-sem-decoracao" id="${save}" value="${qtdItens}" name="${nome}">
					<img src="../img/add-auxiliar.png"/>
				</button>
			</center>
		</th>
		<td>
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" maxlength="3" placeholder="ID Tipo Terminal" id="${idTerminal}">
			</div>
		</td>
		<td>
			<div class="input-group input-group-sm">
				<input type="text" class="form-control" placeholder="Descrição" maxlength="15" id="${nome}">
			</div>
		</td>
	</tr>`;
	tbody.insertAdjacentHTML("beforeend",htmlTerminal);
	document.getElementById(save).addEventListener('click', (e) => {
		e.preventDefault();
		var frag = "tbAuxiliares";
		let vTerminal = require(path.join(__dirname +"/../js/"+frag));
		vTerminal.addCamposTerminais(frag, e.currentTarget.value);
	});
}

//Verifica Internet
let conexaoComInternet = true;
setInterval(()=>{
	if(!Helper.verificaInternet() && conexaoComInternet){
		conexaoComInternet  = false;
		swal({
			title:"Sem Conexão com a Internet!",
			text: `Alguns recursos ficarão inacessíveis até o retorno da internet, como: \n 
			S@T, consulta de endereços pela internet, entre outros...
			`,
			icon: 'warning'
		})
		console.log('sem conexao')
	}else if(!conexaoComInternet && Helper.verificaInternet()){
		swal({
			title:"Conexão com a Internet retomada!",
			text: "Recursos Inacessíveis sem internet, agora podem ser acessados novamente",
			icon: 'success'
		});
		conexaoComInternet = true;
		console.log('conexao ativa')
	}
}, 10000)

async function primeiroCarregamento(){
	if(JSON.parse(window.localStorage.getItem('fistLoad')) == true){

		let menuPrinc = document.getElementById("menu");
		let msgIni = document.getElementById("msgInicial");
		menuPrinc.style.display = 'none';
		msgIni.style.display 	= 'block';

		let nome = JSON.parse(window.localStorage.usuario).nome;
		let numeroMsg = Math.floor((Math.random() * 527) + 1);
		
		nome = nome.substring(0,1).toUpperCase() + nome.substring(1,nome.length).toLowerCase();
		
		msgIni.insertAdjacentHTML("afterbegin",`
			<center class="aguarde-mensagem-ola" style="margin-top: 23%;" id="">
			<h3 id="mostraNome" style="color:white;">
			Olá ${nome}, ${msgHorario()} <br>
			"${(await require(path.join(__dirname + '/../../controller/cPensamento.js')).select(numeroMsg)).frase}"
			<br> <h4 class="vamos-comecar">Vamos começar?</h4>
			</h3>
			</center>`);
		setTimeout(()=>{
			msgIni.style.display = 'none';
			menuPrinc.style.display = 'block';
		},10000)

		window.localStorage.setItem('fistLoad', false);
	}
}primeiroCarregamento();

function msgHorario(){
	let d = new Date().getHours();
	let msg;
	if(d > 17) {
		msg = "boa noite!"
	}else if(d >= 12){
		msg = "boa tarde!"
	}else if(d >= 3){
		msg = "bom dia!"
	}
	return msg;
}

async function qtdRegistrosCad(){
	try {
		let qtdProd   = document.getElementById("qtdProdutos"),
		qtdGrupos 	  = document.getElementById("qtdGrupos"),
		qtdClientes   = document.getElementById("qtdClientes"),
		qtdAdicionais = document.getElementById("qtdAdicionais"),
		qtdEnderecos  = document.getElementById("qtdEnderecos"),
		qtdFuncionaios= document.getElementById("qtdFuncionaios");

		let cCadProdutos = require(path.join(__dirname + '/../../controller/cCadProdutos.js')),
		cCadGrupos 		 = require(path.join(__dirname + '/../../controller/cCadGrupos.js')),
		cCadClientes 	 = require(path.join(__dirname + '/../../controller/cCadClientes.js')),
		cCadItensExtras  = require(path.join(__dirname + '/../../controller/cCadItensExtras.js')),
		cCadFunc = require(path.join(__dirname + '/../../controller/cCadFunc.js')),
		cCadEnderecos 	 = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));

		let
		countCadProd  = await cCadProdutos.countAll(),
		countCadGrupo = await cCadGrupos.countAll(),
		countCadCli   = await cCadClientes.countAll(),
		countCadIeEx  = await cCadItensExtras.countAll(),
		countCadEnd = await cCadEnderecos.countAll(),
		countCadFunc  = await cCadFunc.countAll();

		await countCadProd > 0 ? qtdProd.innerHTML = "QTD: "+countCadProd : qtdProd.innerHTML =  "QTD: 0";
		await countCadGrupo > 0 ? qtdGrupos.innerHTML = "QTD: "+countCadGrupo : qtdGrupos.innerHTML =  "QTD: 0";
		await countCadCli > 0 ? qtdClientes.innerHTML = "QTD: "+countCadCli : qtdClientes.innerHTML =  "QTD: 0";
		await countCadIeEx > 0 ? qtdAdicionais.innerHTML = "QTD: "+countCadIeEx : qtdAdicionais.innerHTML =  "QTD: 0";
		await countCadEnd > 0 ? qtdEnderecos.innerHTML = "QTD: "+countCadEnd : qtdEnderecos.innerHTML =  "QTD: 0";
		await countCadFunc > 0 ? qtdFuncionarios.innerHTML = "QTD: "+countCadFunc : qtdFuncionarios.innerHTML =  "QTD: 0";



	} catch(e) {	

	}	

	// window.localStorage.getItem('fistLoad') == "true")

}


module.exports = {
	alterar : (par)=>{addAlt(par)}
}
;
