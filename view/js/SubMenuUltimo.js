var path = require('path');
var Helper = require(path.join(__dirname + '../../../controller/Helper.js'));
/* ================ CONTROLLERS ================ */

var cCadPedido = require(path.join(__dirname + "../../../controller/cCadPedido.js"))
var CadEnderecos = require(path.join(__dirname + "../../../controller/cCadEnderecos.js"))

/* ============================================= */

/* ================ VARIAVEIS GLOBAIS ================ */

let pedidoLast = [];
let pedidosTotais = window.sessionStorage.getItem('pedidosTotais');
let itensDosPedidos = window.sessionStorage.getItem('itensDosPedidos');
let pedidosTotaisUltimo = [];
let itensDosPedidosUltimo = []


/* =================================================== */

function selecionarTudo() {
    let pedidos = cCadPedido.selectAll();
    let pedidosTotais = [];
    pedidosTotaisUltimo = [];
    itensDosPedidosUltimo = [];
    pedido = {}
    
    pedido = {
        produtos: [],
        itex: [],
        valores: [],
        valoresCalculados: [],
        qtds: [],
    };

    document.getElementById('inputPesquisa') ? document.getElementById('inputPesquisa').style.display = 'none' : null;
    
    pedidos.then(data => {
        let pedidoData = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < pedidoData.length; i++) {
            if (pedidoData[i].status_pedido !== "CANCELADO"  && pedidoData[i].status_pedido !== "RECEBIDO") {
                pedidosTotais.push(pedidoData[i]);
                selecionaLogByPedido(pedidoData[i])
                itensDosPedidos = [];
            }
        }
        selecionaPedidosUltimo();
    });
}

async function selecionaPedidosUltimo() {
    let pedidos = cCadPedido.selectAll();
    await pedidos.then(data => {
        let pedidoData = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < pedidoData.length; i++) {
            pedidosTotaisUltimo.push(pedidoData[i]);
            //pedidosTotais.push(pedidoData[i]);
            window.sessionStorage.setItem('pedidosTotais', JSON.stringify(pedidosTotaisUltimo))
        }
    });
    selecionaItensDoPedidoUltimo();
}

async function selecionaItensDoPedidoUltimo() {
    let itensPedido = cItemPedido.selectProdutosItens();
    await itensPedido.then(data => {
        let itensPedidoData = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < itensPedidoData.length; i++) {
            itensDosPedidosUltimo.push(itensPedidoData[i]);
            //itensDosPedidos.push(itensPedidoData[i]);
            window.sessionStorage.setItem('itensDosPedidos', JSON.stringify(itensDosPedidosUltimo));
        }
    });

    /*window.sessionStorage.removeItem('itensDosPedidos');
    window.sessionStorage.removeItem('pedidosTotais');*/
}

function selecionaLogByPedido(pedidoAtual) {

    cCadPedido.trocaRelacao("HASONE")
    //let pedido = [];
    let logData = cCadPedido.selectJoinLogradouros(pedidoAtual.id_pedido);

    cCadPedido.trocaRelacao("BELONGSTO")

    logData.then(data => {
        let pedidoJoin = JSON.parse(JSON.stringify(data));
        pedidoLast.push(pedidoJoin);

        insereItemNaTela(pedidoJoin);
    })


}

async function insereItemNaTela(retorno, stringify) {
    if (stringify != true) {
        retorno = JSON.parse(JSON.stringify(retorno));
    }

    ////console.log(retorno);
    //let Logradouro = await CadEnderecos.select(retorno.id_logradouro);
    //let nomeId;
    //nomeId = (Logradouro.tba_Tp_Logr.descricao ? Logradouro.tba_Tp_Logr.descricao : ' ') + ' ' + Logradouro.rua;

    const tipoEntrega = retorno.tipo_entrega === 'ENTREGA' ? 'DELIVERY': retorno.tipo_entrega;

    let tbody = document.getElementById('listaPedidos');
        tbody.innerHTML += `
        <tr id="tr${retorno.id_pedido}">
            <td class="border-right txt-num" id="cod${retorno.id_pedido}">${retorno.id_pedido}</td>
            <td class="border-right txt-label" id="nome${retorno.id_pedido}">${retorno.cliente}</td>
            <td class="border-right txt-label" id="tipo${retorno.id_pedido}">${retorno.tipo_entrega}</td>
            <td class="border-right txt-label" id="preco${retorno.id_pedido}">${Helper.valorEmReal(retorno.vl_total)}</td>
            <td>
                <ul class="nav nav-tabs">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Opções
                        </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#" onclick = "irParaRecebimento('${tipoEntrega}', 'ULTIMO', ${retorno.id_pedido});">Receber</a></li>
                        <li><a class="dropdown-item" href="#" onclick = "irParaFechamento('Fechamento', 'Ultimo', ${retorno.id_pedido})">Consultar</a></li>
                        <li><a class="dropdown-item" href="#" onclick = "irParaCardapio('Editar', 'Ultimo', ${retorno.id_pedido}, '${retorno.tipo_entrega}')">Editar</a></li>
                        <li><a class="dropdown-item" href="#" onclick = "irParaFechamento('Fechamento', 'Ultimo', ${retorno.id_pedido})">Visualizar/2ª Via</a></li>
                        <li><a class="dropdown-item" href="#" onclick = "cancelarPedido('${retorno.tipo_entrega}', '${retorno.numero_mesa}', 'Ultimo', ${retorno.id_pedido})">Cancelar</a></li>
                    </ul>
                    </li>
                </ul>
            </td>
        </tr>`;
}

function refazerTabela(){
    selecionarTudo();
}

module.exports = {
    selecionarTudo: selecionarTudo,
    refazerTabela:  refazerTabela,
}