var path = require('path');
var fs = require('fs');
var os = require('os');
const Swal = require('sweetalert');
window.$ = window.jQuery = require(path.join(__dirname, '..\\jquery\\jquery-3.4.1.min.js'));

let objCidades = {};
let objUFs = {};

let conteudoMegaTxt = fs.readFileSync(__dirname + '\\..\\..\\database\\ceps.txt', 'utf-8');
const InfoSis = require(path.join(__dirname + '/../../controller/cINFORSIS.js'));
const cCadFunc = require(path.join(__dirname + '/../../controller/cCadFunc.js'));
const cGrupos = require(path.join(__dirname + '/../../controller/cCadGrupos.js'));
const cTGrupos = require(path.join(__dirname + '/../../controller/cTGrupos.js'));
const cConfImpressoras = require(path.join(__dirname + '/../../controller/cConfImpressoras.js'));
const cTipoLogradouro = require(path.join(__dirname + '/../../controller/cTipoLogradouro.js'));
let con = require(path.join(__dirname + '/../../database/connection'));
const modelTB_LOGRADOUROS = require(path.join(__dirname + '/../../database/model/principais/mTB_LOGRADOUROS.js'));
const modelTBA_MUNICIPIOS = require(path.join(__dirname + '/../../database/model/auxiliares/mTBA_MUNICIPIOS.js'));
const modelTBA_UF = require(path.join(__dirname + '/../../database/model/auxiliares/mTBA_UF.js'));
const helperCampoCEP = require(path.join(__dirname + '/../../helpers/helperCampoCep.js'));
modelTB_LOGRADOUROS.init(con);
modelTBA_MUNICIPIOS.init(con);
modelTBA_UF.init(con);

const campoQtdMesas = document.getElementById('qde_mesas');
campoQtdMesas.value = '10';

campoQtdMesas.addEventListener('focusout',()=>{
	const qtdDigitada = parseInt(campoQtdMesas.value);
	
	if(qtdDigitada % 5 > 0) campoQtdMesas.value = (parseInt(qtdDigitada / 5) * 5) + 5;
	else campoQtdMesas.value = qtdDigitada;
});

//#region Busca as UFs para poder associar aos endereços
modelTBA_UF.findAll().then(result=>{
	for(uf of result) objUFs[uf.uf.trim().toUpperCase()] = uf.id_uf;
});
//#endregion

//#region Verifica conexao com internet
Helper.verificaInternet(true);
window.addEventListener('offline', ()=>{ Helper.verificaInternet(true) });
//#endregion


//#region Preenchendo campos com dados padrão
document.getElementById('cfg_porc_broto').value = '65';
document.getElementById('cfg_porc_media').value = '80';
document.getElementById('cfg_porc_giga').value = '25';

document.getElementById('cfg_taxa_servico').value = '5,00';
document.getElementById('cfg_frete_basico').value = '3,00';
//#endregion

//#region ------- INSERINDO E TRABALHANDO COM OS CAMPOS COM MÁSCARA ---------
let campoCPF = document.getElementById('cpf');
let campoCNPJ = document.getElementById('cfg_cnpj');

campoCPF.addEventListener('keyup', Helper.maskCPF);
campoCPF.addEventListener('focusout', ()=>{
	let valor = campoCPF.value.replace('.','').replace('.','').replace('.','').replace('-','');
	if(valor.length < 11 || !Helper.validaCPF(valor)) {
		campoCPF.style.borderColor = '#f90000'
	}
	else campoCPF.style.borderColor = '#00FF00'
});

campoCNPJ.addEventListener('keyup', Helper.maskCPFCNPJ);
campoCNPJ.addEventListener('focusout', ()=>{
	let valor = campoCNPJ.value.replace('.','').replace('.','').replace('.','').replace('-','').replace('/','');
	if(valor.length !== 11 && valor.length < 14) campoCNPJ.style.borderColor = '#f90000';
	else if(Helper.validaCPF(valor) || Helper.validaCNPJ(valor))campoCNPJ.style.borderColor = '#00FF00';
	else campoCNPJ.style.borderColor = '#f90000';
});

let campoEmail = document.getElementById('email');
campoEmail.addEventListener('focusout', ()=>{
	var vCampoEmail = campoEmail.value;
	Helper.isEmail(vCampoEmail) ? campoEmail.style.borderColor = '#f90000' : campoEmail.style.borderColor = '#00FF00'
});

document.getElementById('cfg_ddd').addEventListener('keyup', ()=>{Helper.controlaTamanhoCampo(document.getElementById('cfg_ddd'), 2);});
document.getElementById('fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('fone2').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('cfg_fone1').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('cfg_fone2').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('cfg_fone3').addEventListener('keyup', Helper.phoneMaskSemDDD);
document.getElementById('senha').addEventListener('keyup', listenConfirmaSenha);
document.getElementById('senha2').addEventListener('keyup', listenConfirmaSenha);

function listenConfirmaSenha(){
	const senhaDigi	= document.getElementById('senha').value;
	const senhaConfirm	= document.getElementById('senha2').value;
	senhaDigi !== senhaConfirm ? document.getElementById('senha2').style.borderColor = '#f90000' : document.getElementById('senha2').style.borderColor = '#00FF00';
}
//#endregion

let formaPizzaAnterior = 'P';

//#region ---------- Preenchendo as combobox com dados do banco --------------
let comboSetor = document.getElementById('id_setor');
let comboNivel = document.getElementById('id_nivel');
let comboFuncao = document.getElementById('id_funcao');
let comboNicho = document.getElementById('cfg_id_nicho');
let comboImpressora = document.getElementById('cfg_id_impressora');

const cSetor 		= require(path.join(__dirname + '/../../controller/cSetor.js'));
const cNivelUsuario = require(path.join(__dirname + '/../../controller/cNivelUsuario.js'));
const cFuncao		= require(path.join(__dirname + '/../../controller/cFuncoes.js'));
//const cLogradouro	= require(path.join(__dirname + '/../../controller/cFuncoes.js'));
const cNicho		= require(path.join(__dirname + '/../../controller/cNichos.js'));
const cImpressora	= require(path.join(__dirname + '/../../controller/cConfImpressoras.js'));

preencheCombo(comboSetor, cSetor);
preencheCombo(comboNivel, cNivelUsuario);
preencheCombo(comboFuncao, cFuncao);
preencheCombo(comboNicho, cNicho);
//preencheCombo(comboImpressora, cImpressora);

function preencheCombo(combo, controller){
	controller.selectAll().then(dados=>{
		dados = JSON.parse(JSON.stringify(dados));

		dados.forEach(registro => {
			combo.innerHTML += `<option value="${Object.values(registro)[0]}">${Object.values(registro)[1]}</option>`
			// console.log(Object.values(registro)[1]);
		});

		if(combo === comboNivel || combo === comboFuncao) combo.value = 3;
	})
}

function preencheComboLogradouro(combo, controller){
	document.getElementById('cfg_id_logradouro').innerHTML += `<option value="1">Rua de teste</option>`;
	document.getElementById('id_logradouro').innerHTML += `<option value="1">Rua de teste</option>`;
}
//#endregion

//#region Logradouros
let campoLogradouroCEP = document.getElementById('cfg_cep_logradouro');
let iniciouMigracao = false;

campoLogradouroCEP.addEventListener('keyup', eventoCampoCep);


let enderecos = conteudoMegaTxt.split('\n');

/*let tipoLogr = {
	R:{
		desc: 'RUA'
	},
	Av:{
		desc: 'AVENIDA'
	},
	Tr:{
		desc: 'TRAVESSA'
	},
	Pc:{
		desc: 'PRAÇA'
	},
	Vd:{
		desc: 'VIADUTO'
	},
	Al:{
		desc: 'ALAMEDA'
	},
	Lg:{
		desc: 'LARGO'
	}
};*/
let tipoLogr = {
	"R": {
		id_tplogr : 1,
		"descricao": "Rua"
	},
	"AV": {
		id_tplogr : 2,
		"descricao": "Avenida"
	},
	"AC": {
		id_tplogr : 3,
		"descricao": "Acesso"
	},
	"AL": {
		id_tplogr : 4,
		"descricao": "Alameda"
	},
	"AT": {
		id_tplogr : 5,
		"descricao": "Alto"
	},
	"AR": {
		id_tplogr : 6,
		"descricao": "Área"
	},
	"BL": {
		id_tplogr : 7,
		"descricao": "Balão"
	},
	"Bc": {
		id_tplogr : 8,
		"descricao": "Beco"
	},
	"BV": {
		id_tplogr : 9,
		"descricao": "Boulevard"
	},
	"Bos": {
		id_tplogr : 10,
		"descricao": "Bosque"
	},
	"C": {
		id_tplogr : 11,
		"descricao": "Cais"
	},
	"Cc": {
		id_tplogr : 12,
		"descricao": "Calçada"
	},
	"Cam": {
		id_tplogr : 13,
		"descricao": "Caminho"
	},
	"Clu": {
		id_tplogr : 14,
		"descricao": "Clube"
	},
	"Col": {
		id_tplogr : 15,
		"descricao": "Colónia"
	},
	"CJ": {
		id_tplogr : 16,
		"descricao": "Conjunto"
	},
	"Con": {
		id_tplogr : 17,
		"descricao": "Condomínio"
	},
	"Cor": {
		id_tplogr : 18,
		"descricao": "Corredor"
	},
	"Ent": {
		id_tplogr : 19,
		"descricao": "Entrada"
	},
	"Esc": {
		id_tplogr : 20,
		"descricao": "Escada/Escadaria"
	},
	"Estacionamento": {
		id_tplogr : 21,
		"descricao": "Estacionamento"
	},
	"ET": {
		id_tplogr : 22,
		"descricao": "Estrada"
	},
	"FV": {
		id_tplogr : 23,
		"descricao": "Favela"
	},
	"GL": {
		id_tplogr : 24,
		"descricao": "Galeria"
	},
	"GJ": {
		id_tplogr : 25,
		"descricao": "Granja"
	},
	"IL": {
		id_tplogr : 26,
		"descricao": "Ilha"
	},
	"JD": {
		id_tplogr : 27,
		"descricao": "Jardim"
	},
	"LD": {
		id_tplogr : 28,
		"descricao": "Ladeira"
	},
	"LG": {
		id_tplogr : 29,
		"descricao": "Largo"
	},
	"LT": {
		id_tplogr : 30,
		"descricao": "Loteamento"
	},
	"MT": {
		id_tplogr : 31,
		"descricao": "Monte"
	},
	"MR": {
		id_tplogr : 32,
		"descricao": "Morro"
	},
	"PQ": {
		id_tplogr : 33,
		"descricao": "Parque"
	},
	"PA": {
		id_tplogr : 34,
		"descricao": "Pátio"
	},
	"PS": {
		id_tplogr : 35,
		"descricao": "Passagem"
	},
	"Passeio": {
		id_tplogr : 36,
		"descricao": "Passeio"
	},
	"PT": {
		id_tplogr : 37,
		"descricao": "Ponte"
	},
	"PC": {
		id_tplogr : 38,
		"descricao": "Praça"
	},
	"PR": {
		id_tplogr : 39,
		"descricao": "Praia"
	},
	"Q": {
		id_tplogr : 40,
		"descricao": "Quadra"
	},
	"RC": {
		id_tplogr : 41,
		"descricao": "Recanto"
	},
	"Ret": {
		id_tplogr : 42,
		"descricao": "Retorno"
	},
	"RD": {
		id_tplogr : 43,
		"descricao": "Rodovia"
	},
	"RT": {
		id_tplogr : 44,
		"descricao": "Rotatória"
	},
	"TM": {
		id_tplogr : 45,
		"descricao": "Terminal"
	},
	"TR": {
		id_tplogr : 46,
		"descricao": "Travessa"
	},
	"Trecho": {
		id_tplogr : 47,
		"descricao": "Trecho"
	},
	"TV": {
		id_tplogr : 48,
		"descricao": "Trevo"
	},
	"TN": {
		id_tplogr : 49,
		"descricao": "Túnel"
	},
	"VR": {
		id_tplogr : 50,
		"descricao": "Vereda"
	},
	"VL": {
		id_tplogr : 51,
		"descricao": "Vale"
	},
	"VD": {
		id_tplogr : 52,
		"descricao": "Viaduto"
	},
	"VIA": {
		id_tplogr : 53,
		"descricao": "Via"
	},
	"VI": {
		id_tplogr : 54,
		"descricao": "VI"
	}
};

function eventoCampoCep(){
	Helper.controlaTamanhoCampo(campoLogradouroCEP, 8);
	let cepDigitado = campoLogradouroCEP.value;

	if(cepDigitado.length === 8){
		if(conteudoMegaTxt.indexOf(cepDigitado) !== -1) {
			//Pegando o conteudo da linha em que o cep foi encontrado
			const conteudoLinhaEndereco = conteudoMegaTxt.substring(conteudoMegaTxt.indexOf(cepDigitado), conteudoMegaTxt.indexOf(cepDigitado) + 150).split('\n')[0];
			const dadosEndereco = conteudoLinhaEndereco.split(String.fromCharCode(9));
			document.getElementById('cfg_nome_rua').value 	= dadosEndereco[3];
			document.getElementById('cfg_bairro').value 	= dadosEndereco[2];
			document.getElementById('cfg_cidade').value 	= dadosEndereco[1].split('/')[0];
			document.getElementById('cfg_uf').value 		= dadosEndereco[1].split('/')[1];
			campoLogradouroCEP.style.borderColor = '#00FF00'
		}else zeraCampos();
		
	}else zeraCampos();

	function zeraCampos(){
		campoLogradouroCEP.style.borderColor = '#f90000';
		document.getElementById('cfg_nome_rua').value 	= '';
		document.getElementById('cfg_bairro').value 	= '';
		document.getElementById('cfg_cidade').value 	= '';
		document.getElementById('cfg_uf').value 		= '';
	}
}

async function migrarEnderecos(cepEstabelecimento = ''){
	/*
	Abaixo pegando uma região abaixo e uma região acima, ou seja:
	Se o CEP for 04233220 vai pegar:
	041XXXXX
	042XXXXX
	043XXXXX
	*/

	let regiaoCep = '';
	let regiaoAntes = parseInt(cepEstabelecimento.substring(2,3)) - 1;
	let regiaoDepois = parseInt(cepEstabelecimento.substring(2,3)) + 1;

	regiaoAntes 	= cepEstabelecimento.substring(0,2) + (regiaoAntes < 0 ? 9 : (regiaoAntes > 9 ? 0 : regiaoAntes)).toString();
	regiaoCep 		= cepEstabelecimento.substring(0,3);
	regiaoDepois 	= cepEstabelecimento.substring(0,2) + (regiaoDepois < 0 ? 9 : (regiaoDepois > 9 ? 0 : regiaoDepois)).toString();

	await migraBairrosECidades(regiaoCep, regiaoAntes, regiaoDepois, cepEstabelecimento);
	//Jogar tudo daqui pra baixo para dentro de uma função independente para ser chamada depois de migrar bairros e cidades
}

async function migraBairrosECidades(regiaoEstabelecimento, regiaoAnterior, regiaoPosterior, cepEstabelecimento){
	let listaCidades = [];

	for(let i = 0; i < enderecos.length; i++){
		endereco = enderecos[i].split(String.fromCharCode(9));
		let cepAtual = endereco[0];
		let regiaoCepTxt = cepAtual.substring(0,3);
		if(regiaoCepTxt === regiaoAnterior || regiaoCepTxt === regiaoEstabelecimento || regiaoCepTxt === regiaoPosterior){
			listaCidades.push(endereco[1].split('/')[0]);
		}
		if(endereco[0] === '99980974'){
			let listaCidadesFiltradas = [... new Set(listaCidades)];

			for(let i = 0; i<listaCidadesFiltradas.length; i++){
				let cidade = listaCidadesFiltradas[i].trim().toUpperCase();
				objCidades[cidade] = await modelTBA_MUNICIPIOS.create({
					descricao : cidade.substring(0,15)
				});
				objCidades[cidade] = objCidades[cidade].dataValues;
				if(i === (listaCidadesFiltradas.length - 1)){
					await migraEnderecosPorCep(regiaoEstabelecimento, regiaoAnterior, regiaoPosterior, cepEstabelecimento);		
				}
			}
		}
	}
}
//Jogar essas funções um local independente
async function migraEnderecosPorCep(regiaoCep, regiaoAntes, regiaoDepois, cepEstabelecimento){
	Swal({
		icon : 'warning',
		title: 'Adicionando endereços próximos a você, por favor aguarde...',
		text : 'Adicionando Logradouros',
		closeOnClickOutside : false,
		closeOnEsc: false,
		button: {
			text: "Okay",
			closeModal: false,
		}
	});
	let limiteInsert = 0;
	let endInseridos = 0;
	let vetorDeEnderecos = [];

	for(let i = 0; i < enderecos.length; i++){
		endereco = enderecos[i].split(String.fromCharCode(9));
		let cepAtual = endereco[0];
		let regiaoCepTxt = cepAtual.substring(0,3);
		if(regiaoCepTxt === regiaoAntes || regiaoCepTxt === regiaoCep || regiaoCepTxt === regiaoDepois){
			try{
				endInseridos ++;
				//Trazer do banco as UFs para pegar o id depois
				let tipoLogradouroEncontrado = tipoLogr[endereco[3].substring(0,2).trim().toUpperCase()];
				tipoLogradouroEncontrado = tipoLogradouroEncontrado ? tipoLogradouroEncontrado : {id_tplogr : 1, descricao : 'RUA'}
				const idTipoLogradouro = tipoLogradouroEncontrado.id_tplogr;
				//let title = tipoLogr[endereco[3].substring(0,2).trim().toUpperCase()].descricao;
				vetorDeEnderecos.push(
					{
						titulo: ' ',
						rua: endereco[3].substring(3, 38).split(' - ')[0].trim().toUpperCase(),
						bairro: endereco[2].substring(0,20).trim().toUpperCase(),
						CEP: cepAtual,
						range: '10',
						frete: 0.00,
						id_tplogr: idTipoLogradouro ? idTipoLogradouro : 1,
						id_uf: objUFs[(endereco[1].split('/')[1]).trim().toUpperCase()],
						id_municipio: objCidades[(endereco[1].split('/')[0]).trim().toUpperCase()].id_municipio
					}
				);
				limiteInsert ++;

				if(limiteInsert === 1000 || endereco[0] === '99980974'){
					console.log('bateuLimite');
					await modelTB_LOGRADOUROS.bulkCreate(vetorDeEnderecos).then(retorno=>{
						limiteInsert = 0 ;
						vetorDeEnderecos = [];
						console.log('inseriu');
					});
				}

				document.getElementsByClassName('swal-text')[0].innerHTML = endereco[3];
							
			}catch(e){
				console.error(endereco);
				console.error(e);
			}
		}
		if(endereco[0] === '99980974'){
			await modelTB_LOGRADOUROS.bulkCreate(vetorDeEnderecos).then( async (retorno)=>{
				limiteInsert = 0 ;
				vetorDeEnderecos = [];
				console.log('inseriu');
				InfoSis.cfg_id_logradouro = (await modelTB_LOGRADOUROS.findOne({ where: { cep: cepEstabelecimento }})).id_logradouro;
				cCadFunc.id_logradouro = InfoSis.cfg_id_logradouro;
				Swal({icon:'success', title: endInseridos + ' Logradouros inseridos com sucesso!'});
			});
		}
	}
}

function existeCEP(cepDigitado){
	if(conteudoMegaTxt.indexOf(cepDigitado) !== -1) return true;
	return false;
}

//#endregion

//------------Verifica o nicho do estabelecimento
let tpNicho = 1;
let mP;
if(tpNicho == 1){
	mP = document.getElementById('moduloPizza').style.display = 'flex';
}
let itemSelecionado = document.querySelector('#cfg_id_nicho');
itemSelecionado.addEventListener('change', (event) => {
	tpNicho = event.target.value;
	if(tpNicho == 3){
		mP = document.getElementById('moduloPizza').style.display = 'flex';
	}else{
		mP = document.getElementById('moduloPizza').style.display = 'none';
	}
});

//---------------------------------------------------------------------

//------------------------porcentagem de pizzas
let porcPizza = "I";
/*if(porcPizza == "I"){
	document.getElementById("pzBr").style.display = 'none';
	document.getElementById("pzMd").style.display = 'none';
	document.getElementById("pzGr").style.display = 'none';
}*/
let pizzaCombSelec = document.getElementById("cfg_formabroto");
pizzaCombSelec.addEventListener('change',(event)=>{
	porcPizza = event.target.value;
	setTimeout(() => {
		if(porcPizza === "P"){
			document.getElementById('valuesPizzas').style.display = "block";
			document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (%):";
			document.getElementById("pzMediaSpan").innerText 	= "Pizza Média (%):";
			document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (% a mais):";
		}else if(porcPizza === "F"){
			document.getElementById('valuesPizzas').style.display = "block";
			document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (Menos):";
			document.getElementById("pzMediaSpan").innerText	= "Pizza Média (Menos):";
			document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (Mais):";
		}else if(porcPizza === "I"){
			document.getElementById("valuesPizzas").style.display = "none";
		}

		if(porcPizza === "I") {
			Swal({
				icon: 'warning',
				title: 'Forma individual de cobrança',
				closeOnClickOutside:false,
				closeOnEsc: false,
				text: 'Selecionando esta maneira de cobrar as pizzas você deverá entrar no cadastro e salvar o valor do tamanho de cada uma delas!',
				buttons: ['Cancelar', 'Manter decisão']
			}).then((confirmation)=>{
				if(!confirmation){
					pizzaCombSelec.value = formaPizzaAnterior;
					if(formaPizzaAnterior === "P"){
						document.getElementById('valuesPizzas').style.display = "block";
						document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (%):";
						document.getElementById("pzMediaSpan").innerText 	= "Pizza Média (%):";
						document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (% a mais):";
					}else if(formaPizzaAnterior === "F"){
						document.getElementById('valuesPizzas').style.display = "block";
						document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (Menos):";
						document.getElementById("pzMediaSpan").innerText	= "Pizza Média (Menos):";
						document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (Mais):";
					}
				}
			});
		}else{
			formaPizzaAnterior = pizzaCombSelec.value;
		}

	}, 100);
});
//---------------------------------

var btnSalvar = document.getElementById('btnCadastrarEstab');

const apelido = document.getElementById(`apelido`);
const nomePC = document.getElementById(`nomePC`);
const obs_impressora = document.getElementById(`obs_impressora`);
const ip_impressora = document.getElementById(`ip_impressora`);
const marca = document.getElementById(`marca`);
const modelo = document.getElementById(`modelo`);
const largura = document.getElementById(`largura`);
const salto = document.getElementById(`salto`);

nomePC.value = os.hostname();
apelido.value = 'IMPREDE';
obs_impressora.value = 'IMPRESSORA PRINCIPAL';
ip_impressora.value = '192.168.0.100';
largura.value = '48';
salto.value = '3';

async function salvar(){
	const apelidoValue = apelido.value;
	const nomePCValue = nomePC.value;
	const obs_impressoraValue = obs_impressora.value;
	const ip_impressoraValue = ip_impressora.value;
	const marcaValue = marca.value;
	const modeloValue = modelo.value;
	const larguraValue = largura.value;
	const saltoValue = salto.value;

	cConfImpressoras.apelido = apelidoValue;
	cConfImpressoras.nomePC = nomePCValue;
	cConfImpressoras.obs_impressora = obs_impressoraValue;
	cConfImpressoras.ip_impressora = ip_impressoraValue;
	cConfImpressoras.marca = marcaValue;
	cConfImpressoras.modelo = modeloValue;
	cConfImpressoras.largura = larguraValue;
	cConfImpressoras.salto = saltoValue;
	cConfImpressoras.portaLPT = '4';

	event.preventDefault();
	
	let senhaCorreta;
	
	//lista de campos dados primeiro usuario (cadpess)
	cCadFunc.nome = document.getElementById('nome').value.toUpperCase();
	cCadFunc.operador = document.getElementById('operador').value.toUpperCase();

	cCadFunc.senha = document.getElementById('senha').value.toUpperCase();
	let senha2 = document.getElementById('senha2').value.toUpperCase();
	if(document.getElementById('senha').value.toUpperCase() != senha2){
		swal({
			title: 'Senhas Diferentes!',
			text: 'AS SENHAS DEVEM SER EXATAMENTE IGUAIS!',
			button: 'Okay'
		}).then(()=>{
			return false
		});
	} else senhaCorreta = true
	cCadFunc.rg = document.getElementById('rg').value;
	cCadFunc.cpf = document.getElementById('cpf').value;
	cCadFunc.fone1 = document.getElementById('fone1').value.replace('-','');
	cCadFunc.fone2 = document.getElementById('fone2').value.replace('-','');
	cCadFunc.ddd = document.getElementById('ddd').value;
	cCadFunc.numero_casa = parseInt(document.getElementById('numero_casa').value);
	cCadFunc.complemento = document.getElementById('complemento').value.toUpperCase();
	cCadFunc.comissao = parseFloat(document.getElementById('comissao').value);
	cCadFunc.dt_adm = Helper.dataAtualEua();
	//cCadFunc.id_logradouro = parseInt(document.getElementById('id_logradouro').value);
	//cCadFunc.id_logradouro = InfoSis.cfg_id_logradouro;
	cCadFunc.id_setor = parseInt(document.getElementById('id_setor').value);
	cCadFunc.id_nivel = parseInt(document.getElementById('id_nivel').value);
	cCadFunc.id_funcao = parseInt(document.getElementById('id_funcao').value);
	cCadFunc.dt_nasc = "1999-01-01";
	cCadFunc.compl_res = "-";
	cCadFunc.num_res = "0";
	cCadFunc.salario = 0;
	cCadFunc.cnh = "0";
	cCadFunc.dt_valid_cnh = "2022-01-01";
	cCadFunc.banco = 0;
	cCadFunc.agencia = "-";
	cCadFunc.conta_banco = "-";
	cCadFunc.frase_seguranca = document.getElementById('frase_seguranca') != '' ? document.getElementById('frase_seguranca').value.substring(0,30) : 'SICLOP';


	let cfg_frete_basico = document.getElementById('cfg_frete_basico').value + '';
	cfg_frete_basico = parseFloat(cfg_frete_basico.replace('.','').replace(',','.'));
	let cfg_taxa_servico = document.getElementById('cfg_taxa_servico').value + '';
	cfg_taxa_servico = parseFloat(cfg_taxa_servico.replace('.','').replace(',','.'));
	let cfg_porc_broto = document.getElementById('cfg_porc_broto').value + '';
	cfg_porc_broto == ""? cfg_porc_broto = 0 :cfg_porc_broto = parseFloat(cfg_porc_broto.replace('.','').replace(',','.'));
	let cfg_porc_media = document.getElementById('cfg_porc_media').value + '';
	cfg_porc_media == ""? cfg_porc_media = 0 : cfg_porc_media = parseFloat(cfg_porc_media.replace('.','').replace(',','.'));
	let cfg_porc_giga = document.getElementById('cfg_porc_giga').value + '';
	cfg_porc_giga == ""? cfg_porc_giga = 0 : cfg_porc_giga = parseFloat(cfg_porc_giga.replace('.','').replace(',','.'));
	

	//lista de campos Dados cadastros (info sis)
	InfoSis.cfg_nome = document.getElementById('cfg_nome').value.toUpperCase();
	InfoSis.cfg_fone1 = document.getElementById('cfg_fone1').value.replace('-','');
	InfoSis.cfg_fone2 = document.getElementById('cfg_fone2').value.replace('-','');
	InfoSis.cfg_fone3 = document.getElementById('cfg_fone3').value.replace('-','');
	InfoSis.cfg_ie = document.getElementById('cfg_ie').value.toUpperCase();
	InfoSis.cfg_im = document.getElementById('cfg_im').value.toUpperCase();
	InfoSis.cfg_nome_fantasia = document.getElementById('cfg_nome_fantasia').value.toUpperCase();
	InfoSis.cfg_numero_local = parseInt(document.getElementById('cfg_numero_local').value);
	InfoSis.cfg_complemento = document.getElementById('cfg_complemento').value.toUpperCase();
	InfoSis.cfg_cnpj = document.getElementById('cfg_cnpj').value;
	InfoSis.cfg_nr_serie = document.getElementById('cfg_nr_serie').value;
	InfoSis.cfg_cod_lib = document.getElementById('cfg_cod_lib').value;
	InfoSis.cfg_pzcombinada = document.getElementById('cfg_pzcombinada').value;
	InfoSis.cfg_formabroto = document.getElementById('cfg_formabroto').value;
	InfoSis.cfg_ddd = document.getElementById('cfg_ddd').value;
	InfoSis.email = document.getElementById('email').value;
	InfoSis.rateio = parseInt(document.getElementById('rateio') ? document.getElementById('rateio').value : '1');

	
	InfoSis.cfg_frete_basico =  cfg_frete_basico;
	InfoSis.cfg_taxa_servico =  cfg_taxa_servico;	

	InfoSis.cfg_qde_cupom = parseInt(document.getElementById('cfg_qde_cupom').value);
	InfoSis.cfg_separa_cupom = document.getElementById('cfg_separa_cupom').value;
	InfoSis.cfg_codigo_cupom = document.getElementById('cfg_codigo_cupom').value;
	InfoSis.cfg_contr_pagto = document.getElementById('cfg_contr_pagto').value;
	InfoSis.cfg_contr_estq = document.getElementById('cfg_contr_estq').value;
	InfoSis.cfg_promo_dia = document.getElementById('cfg_promo_dia').value;
	InfoSis.cfg_modulo_movel = document.getElementById('cfg_modulo_movel').value;
	InfoSis.cfg_modulo_web = document.getElementById('cfg_modulo_web').value;
	InfoSis.cfg_modulo_fiscal = document.getElementById('cfg_modulo_fiscal').value;
	InfoSis.cfg_contr_motoq = document.getElementById('cfg_contr_motoq').value;
	InfoSis.cfg_baixa_aqui = document.getElementById('cfg_baixa_aqui').value;
	InfoSis.cfg_repete_item = document.getElementById('cfg_repete_item').value;
	InfoSis.cfg_separa_periodo = document.getElementById('cfg_separa_periodo').value;
	InfoSis.cfg_cod_barra = document.getElementById('cfg_cod_barra').value;
	InfoSis.cfg_faz_delivery = document.getElementById('cfg_faz_delivery').value;
	InfoSis.cfg_id_nicho = parseInt(document.getElementById('cfg_id_nicho').value);
	//InfoSis.id_impressora = parseInt(document.getElementById('cfg_id_impressora').value);
	InfoSis.cfg_canhoto_cupom = document.getElementById('cfg_canhoto_cupom').value;
	InfoSis.cfg_preco_cupom = 1;

	var valor2 = campoCNPJ.value.replace('.','').replace('.','').replace('.','').replace('-','').replace('/','');
	//colocar aqui a verificação de CPF também
	if(!Helper.validaCNPJ(valor2) && !Helper.validaCPF(valor2)) {
		swal({
			title: 'CNPJ Inválido!',
			text: 'O CNPJ ESTÁ INCOERENTE!',
			button: 'Okay'
		});
		campoFaltando = true;
	}

	let tpMenu = document.getElementById('tpMenu').value;
	window.localStorage.setItem('tipoMenu', tpMenu);

	if(tpNicho != 3){
		InfoSis.cfg_porc_broto =  1;
		InfoSis.cfg_porc_media =  1;
		InfoSis.cfg_porc_giga = 1;
	}else{
		InfoSis.cfg_porc_broto =  cfg_porc_broto;
		InfoSis.cfg_porc_media =  cfg_porc_media;
		InfoSis.cfg_porc_giga = cfg_porc_giga;
	}

	//verifica se campo esta vazio
	var conteudoNotNull = {
		nome: cCadFunc.nome,
		operador: cCadFunc.operador,
		senha: cCadFunc.senha,
		rg: cCadFunc.rg,
		cpf: cCadFunc.cpf,
		fone1: cCadFunc.fone1,
		fone2: cCadFunc.fone2,
		ddd: cCadFunc.ddd,
		numero_casa: cCadFunc.numero_casa,
		complemento: cCadFunc.complemento,
		comissao: cCadFunc.comissao,
		cfg_ddd: InfoSis.cfg_ddd,
		cfg_nome: InfoSis.cfg_nome,
		email: InfoSis.email,
		cfg_ie: InfoSis.cfg_ie,
		cfg_im: InfoSis.cfg_im,
		cfg_nome_fantasia: InfoSis.cfg_nome_fantasia,
		cfg_cod_barra: InfoSis.cfg_cod_barra,
		cfg_cnpj: InfoSis.cfg_cnpj,
		cfg_fone1: InfoSis.cfg_fone1,
		cfg_cod_lib: InfoSis.cfg_cod_lib,
		cfg_nr_serie: InfoSis.cfg_nr_serie,
		//cfg_frete_basico: InfoSis.cfg_frete_basico,
		//cfg_numero_local: InfoSis.cfg_numero_local,
		cfg_qde_cupom: InfoSis.cfg_qde_cupom,
		cfg_id_nicho: InfoSis.cfg_id_nicho,
		apelido:apelidoValue,
		obs_impressora:obs_impressoraValue,
		ip_impressora:ip_impressoraValue,
		marca:marcaValue,
		modelo:modeloValue,
		largura:larguraValue,
		salto:saltoValue
	};

	InfoSis.qde_mesas = parseInt(document.getElementById('qde_mesas').value);

	let campoFaltando = false;

	let n = 1;

	for(var i in conteudoNotNull){
		if(conteudoNotNull[i] == '' || conteudoNotNull[i] == undefined){
			campoFaltando = true;
			let footerCont = document.getElementById('alerta');
			footerCont.innerHTML += `<div class="alert alert-danger tag" role="alert">falta campo ${i}</div>`;
		}
		n++;
	}

	//insere no banco se não tiver faltando nada

	/*let objsLogradouros = [];
	let objLogradouro = {
		titulo:         '',
		rua:            '',
		bairro:         '',
		CEP:            '',
		range:          '',
		frete:          '',
		id_tsocial:     '',
		id_tplogr:      '',
		id_uf:          '',
		id_municipio:   ''
	};*/

	if(campoFaltando == false && senhaCorreta){

		//#region Ativa Migração de endereços
		let valor = campoLogradouroCEP.value;
		if(valor.length < 8 || isNaN(parseInt(valor)) || !existeCEP(valor)) campoLogradouroCEP.style.borderColor = '#f90000';
		else{
			campoLogradouroCEP.style.borderColor = '#00FF00';
			iniciouMigracao !== true ? await migrarEnderecos(valor) : null;
			iniciouMigracao = true;
		}
		//#endregion

		//#region Migrando categorias padrão
		let dadosGruposPadrao = await cTGrupos.select(InfoSis.cfg_id_nicho);
		dadosGruposPadrao = JSON.parse(JSON.stringify(dadosGruposPadrao));
		//#endregion

		InfoSis.id_logradouro = 1;
		//cCadFunc.frase_seguranca = ;
		cConfImpressoras.insert().then(async (retorno)=>{
			InfoSis.id_impressora = retorno.id_impressora;

			let contGrupo = 1;
			for(d of dadosGruposPadrao){
				cGrupos.cod_grupo = contGrupo;
				cGrupos.descricao = d.desc_grupo.substring(0,20);
				cGrupos.cupom_separado = undefined;
				cGrupos.totaliza = undefined;
				cGrupos.id_cat_padrao = 1;
				cGrupos.id_impressora = retorno.id_impressora;
				await cGrupos.insert();
				contGrupo++;
			}

			InfoSis.insert().then(()=>{
				cCadFunc.insert().then(()=>{
					swal(
					{
						icon: 'success',
						title: 'Tudo certo!',
						text: 'Todos os dados do estabelecimento foram salvos corretamente!',
						button: 'Vamos lá!'
					}
					).then(()=>{
						document.location.assign(path.join(__dirname,'./telaLogin.html'))
					});
				});
			}).catch((error)=>{
				console.log(error);
			});
		});
	}else{
		setTimeout(()=>{
			for(var l = 0; l < n; ){
				document.getElementsByClassName("tag")[0].remove();
				l++;
			}
		},3000)
		if(senhaCorreta == false){
			swal({
				title: "Senha Diferente!!!",
				text: "Coloque senhas iguais para prosseguir."
			})
		}
		event.preventDefault();
	}
}

async function leArquivoPreConfig(){
	if(fs.existsSync('c:/siclop/precfghibrido.txt')){
		const conteudoArquivoPreCfg = fs.readFileSync('c:/siclop/precfghibrido.txt', 'utf8');
		const linhasArquivo = conteudoArquivoPreCfg.split(String.fromCharCode(10));

		for(linha of linhasArquivo){
			const infos = linha.split(':')
			const nomeCampo = infos[0].trim();
			const valorCampo = infos[1].trim();
			try{
				document.getElementById(nomeCampo).value = valorCampo;
				if(nomeCampo === 'cfg_cep_logradouro') eventoCampoCep();
			}catch(e){
				console.log('Campo não encontrado = ' + nomeCampo);
			}
		}

	}
}

leArquivoPreConfig();
/*document.getElementById("cfg_formabroto").addEventListener('change', () => {
	let comboBoxCategoria = document.getElementById("cfg_formabroto");
	console.log(comboBoxCategoria);
	for(let i = 0; i < comboBoxCategoria.options.length; i++){
		for([key, value] of Object.entries(comboBoxCategoria.options)){
			if(value.text === "PORCENTAGEM"){
				document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (%):";
				document.getElementById("pzMediaSpan").innerText 	= "Pizza Média (%):";
				document.getElementById("pzGigaSpan").innerText 	= "Pizza Giga (%):";			
			}else if(value.text === "VALOR FIXO"){
				document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (-):";
				document.getElementById("pzMediaSpan").innerText	= "Pizza Broto (-):";
				document.getElementById("pzGigaSpan").innerText 	= "Pizza Broto (+):";
			}else if(value.text === "INDIVIDUAL"){
				document.getElementById("pzBrotoSpan").innerText 	= "Pizza Broto (R$):";
				document.getElementById("pzMediaSpan").innerText	= "Pizza Broto (R$):";
				document.getElementById("pzGigaSpan").innerText 	= "Pizza Broto (R$):";
			}
		}
	}
})*/