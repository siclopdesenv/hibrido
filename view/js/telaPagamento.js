//#region Importação das Controllers
var cFPGTO = require(path.join(__dirname + '../../../controller/cFGPTO.js'));
var cHistPgto = require(path.join(__dirname + '../../../controller/cHistPgto.js'));
var cHistVenda = require(path.join(__dirname + '../../../controller/cHistVenda.js'));
var cCadEnderecos = require(path.join(__dirname + '/../../controller/cCadEnderecos.js'));
var cCadFunc = require(path.join(__dirname + '/../../controller/cCadFunc.js'));
var cPendura = require(path.join(__dirname + '/../../controller/cPendura.js'));
//#endregion

//#region Variaveis de ambiente
    let pedidoDeMesa                ;
    let itemPedidoMesa              ;
    let pedidoReceb                 ; 
    var dadosEstabelecimento        ; 
    let primeiroSabor           = 0 ;
    let segundoSabor            = 0 ;
    let terceiroSabor           = 0 ;
    let quartoSabor             = 0 ;
    let itemNormal              = 0 ;
    let valorTotal              = 0 ;
    let entrandoObjSabores      = 0 ;
    let body                    = "";
    let adicionaPagamentoXML    = "";
    let logradouroFormat        = "";
    let complementoXml          = "";
    let nomeItemForComparar     = [];
    let qtdsItens               = [];
    let arrayTiposItensExtras   = [];
    let nomeItemForSearch       = [];
    let qtdsItensForSearch      = [];
    let formasPagamento         = [];
    let ObjSaboresMesas         = {
        0: 'PRIMEIRO SABOR',
        1: 'SEGUNDO SABOR',
        2: 'TERCEIRO SABOR',
        3: 'QUARTO SABOR',
    }
    var objTamanhos           = {
        'nn': '(NORMAL)',
        'br': 'BROTO',
        'md': 'MÉDIA',
        'gi': 'GIGANTE',
    }
//#endregion

//#region Funções da tela de pagamento

function zerarAmbientes(){
    window.sessionStorage.removeItem('itemDoPedidoMesa');
    window.sessionStorage.removeItem('pedidoMesa');
    primeiroSabor           = 0 ;
    segundoSabor            = 0 ; 
    terceiroSabor           = 0 ;
    quartoSabor             = 0 ;
    itemNormal              = 0 ;
    valorTotal              = 0 ;
    entrandoObjSabores      = 0 ;
    adicionaPagamentoXML    = "";
    body                    = "";
    complementoXml          = "";
    logradouroFormat        = "";
    nomeItemForComparar     = [];
    qtdsItens               = [];
    arrayTiposItensExtras   = [];
    nomeItemForSearch       = [];
    qtdsItensForSearch      = [];
    formasPagamento         = [];
}

async function selecionarFormasPgto(){
    pedidoDeMesa    = JSON.parse(window.sessionStorage.getItem('pedidoMesa'));
    itemPedidoMesa  = JSON.parse(window.sessionStorage.getItem('itemDoPedidoMesa'));
    dadosEstabelecimento  = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
    pedidoReceb     = document.getElementById('ItensPedidoReceb')
    document.getElementById("paraDia").setAttribute("min", Helper.dataAtualEua(true));

    let fpgto = cFPGTO.selectAll();
    await fpgto.then(data => {
        let actuallyData = JSON.parse(JSON.stringify(data));
        for(formaPag of actuallyData){
            formasPagamento.push(formaPag);
        }
    })

    procuraLogradouroBancoDeDados();
    preencherTelaComFormasDePagamento(pedidoDeMesa.tipo_entrega);
}

function focusoutInputsFormasPgto(dom){
    console.log(dom.value);
}

let idFormaCredito = undefined;

function preencherTelaComFormasDePagamento(tipoPedido){
    let adicionaFPGTO = document.getElementById('adicionaFPGTO');

    let htmlEntregador = '';

    //Inserindo campo de selecionar ou não entregador/garçom
    if(dadosEstabelecimento.cfg_contr_motoq){
        if(tipoPedido.toUpperCase() === 'ENTREGA') {
            htmlEntregador += `
            <div class="cont-celulas-receb">
                <div class="celula-1">
                    <span class="label-celula-receb">ENTREGADOR: </span>
                </div>
                <div class="celula-2 col-2 col-atendente">
                    <select id="ENTREGADOR" class="custom-select select-atendente">
                    </select>
                </div>
            </div>
        `;
        //<input id="ENTREGADOR" type="text" class="form-control" aria-label="Sizing example input" name="Código" onkeyup="Helper.maskMoeda(this,event)" aria-describedby="inputGroup-sizing-default" placeholder="">
        } else if(tipoPedido.toUpperCase() === 'MESA'){
            htmlEntregador += `
            <div class="cont-celulas-receb">
                <div class="celula-1">
                    <span class="label-celula-receb">GARÇOM: </span>
                </div>
                <div class="celula-2 col-2 col-atendente">
                    <select id="GARCOM" class="custom-select select-atendente">
                    </select>
                </div>
            </div>
            `;
        }
    
    }
    
    let htmlPadrao = `
        <div class="cont-celulas-receb">
            <div class="celula-1">
                <span class="label-celula-receb">DESCONTO: </span>
            </div>
            <div class="celula-2 col-5">
                <input id="DESCONTO" type="text" class="recebimentos form-control" aria-label="Sizing example input"
                    name="DESCONTO" onkeyup="Helper.maskMoeda(this,event)" aria-describedby="inputGroup-sizing-default" placeholder="">
            </div>
        </div>

        <div class="cont-celulas-receb">
            <div class="celula-1">
                <span class="label-celula-receb">GORJETA: </span>
            </div>
            <div class="celula-2 col-5">
                <input id="GORJETA" type="text" class="recebimentos form-control" aria-label="Sizing example input"
                    name="GORJEJTA" onkeyup="Helper.maskMoeda(this,event)" aria-describedby="inputGroup-sizing-default" placeholder="">
            </div>
        </div>
    `

    for(i in formasPagamento){
        if(formasPagamento[i].descricao === 'CRÉDITO'){
            adicionaPagamentoXML += `
                <div class="cont-celulas-receb">
                    <div class="celula-1">
                        <span class="label-celula-receb">${formasPagamento[i].descricao}: </span>
                    </div>
                    <div class="celula-2 col-5">
                        <input id="${formasPagamento[i].id_fpgto}" type="text" class="recebimentos form-control" aria-label="Sizing example input"
                            name="${formasPagamento[i].descricao}" onkeyup="Helper.maskMoeda(this,event)" aria-describedby="inputGroup-sizing-default" placeholder="">
                    </div>
                </div>
                <div class="cont-celulas-receb" id="divParcelas">
                    <div class="celula-1">
                        <span class="label-celula-receb">PARCELAS: </span>
                    </div>
                    <div class="celula-2 col-5">
                        <input id="parcelas" type="number" class="form-control" aria-label="Sizing example input"
                            name="parcelas" aria-describedby="inputGroup-sizing-default" placeholder="" min="1">
                    </div>
                </div>
            `
            idFormaCredito = formasPagamento[i].id_fpgto;
            continue;
        }
        adicionaPagamentoXML += `
            <div class="cont-celulas-receb">
                    <div class="celula-1">
                        <span class="label-celula-receb">${formasPagamento[i].descricao}: </span>
                    </div>
                    <div class="celula-2 col-5">
                        <input id="${formasPagamento[i].id_fpgto}" type="text" class="recebimentos form-control" aria-label="Sizing example input"
                            name="${formasPagamento[i].descricao}" onkeyup="Helper.maskMoeda(this,event)" aria-describedby="inputGroup-sizing-default" placeholder="">
                    </div>
                </div>
        `
    }

    adicionaFPGTO.innerHTML = htmlEntregador;
    adicionaFPGTO.innerHTML += adicionaPagamentoXML;
    adicionaFPGTO.innerHTML += htmlPadrao;

    if(tipoPedido.toUpperCase() === 'ENTREGA') preencheComboFuncionario(document.getElementById('comboFuncionario'), undefined, 'ENTREGADOR'); else if(tipoPedido.toUpperCase() === 'MESA' || tipoPedido.toUpperCase() === 'COMANDA' || tipoPedido.toUpperCase() === 'BALCÃO') preencheComboFuncionario(document.getElementById('comboFuncionario'), undefined, 'GARÇOM');

    function listenerFocusOutCredito(){
        if(dadosEstabelecimento.id_nicho === 3) return;
        if(document.getElementById(idFormaCredito).value !== '' && parseFloat(document.getElementById(idFormaCredito).value) !== 0) document.getElementById('divParcelas').style.display = 'block';
        else document.getElementById('divParcelas').style.display = 'none';
    }

    function impedeZeroENegativoParcelas(){
        if(document.getElementById('parcelas').value === '' || document.getElementById('parcelas').value < 1) document.getElementById('parcelas').value = 1;
    }

    if(idFormaCredito){
        document.getElementById('parcelas').value = 1;
        document.getElementById('divParcelas').style.display='none';
        document.getElementById(idFormaCredito).addEventListener('focusout', listenerFocusOutCredito);
        document.getElementById('parcelas').addEventListener('focusout', impedeZeroENegativoParcelas);
    }
    criarEventos();
}

async function procuraLogradouroBancoDeDados(){
    let logradouro = await cCadEnderecos.select(pedidoDeMesa.id_logradouro);
    logradouroFormat = logradouro.tba_Tp_Logr.descricao + " " + logradouro.rua + " Nº " + pedidoDeMesa.num_endereco + " " + pedidoDeMesa.comp_endereco;

    preencherInformacoesDoPedido();
}

function criarEventos(){
    const btnAceitar = document.getElementById('btnFecharPagamento');
    btnAceitar.addEventListener('click', listenerBtnAceitar);
    let querySelector = document.querySelectorAll('.recebimentos');
    for(let i = 0; i < querySelector.length; i++) {
        querySelector[i].addEventListener('focusout', function() {atualizaValorAReceber(querySelector)}, false);
    }
}

function preencherInformacoesDoPedido(){
    document.getElementById('nrPedido').value           = pedidoDeMesa.nr_pedido;
    document.getElementById('nrPedido').disabled        = true;
    document.getElementById('dtPedido').value           = Helper.converteDataBancoPDataString(pedidoDeMesa.dt_pedido);
    document.getElementById('dtPedido').disabled        = true;
    document.getElementById('horas').value              = pedidoDeMesa.hr_pedido;
    document.getElementById('horas').disabled           = true;
    document.getElementById('cliente').value            = pedidoDeMesa.cliente;
    document.getElementById('cliente').disabled         = true;
    document.getElementById('taxaEntrega').disabled     = true;
    document.getElementById('acrescimo').value          = String(pedidoDeMesa.acrescimo.toFixed(2)).replace('.', ',');
    document.getElementById('acrescimo').disabled       = true;
    document.getElementById('desconto').value           = String(pedidoDeMesa.desconto.toFixed(2)).replace('.', ',');
    document.getElementById('desconto').disabled        = true;
    document.getElementById('soma').value               = String((pedidoDeMesa.vl_total - pedidoDeMesa.acrescimo).toFixed(2)).replace('.',',');
    document.getElementById('totPago').value            = String(pedidoDeMesa.vl_total.toFixed(2)).replace('.',',');

    if(pedidoDeMesa.tipo_entrega === "ENTREGA"){
        document.getElementById('fone').value = pedidoDeMesa.telefone;
        document.getElementById('fone').disabled = true;
        setTimeout(() => {
            document.getElementById('endereco').value = logradouroFormat;
        }, 100);
        document.getElementById('grid-altera-margem').style.height = "133%";
        //document.getElementById('container-recebidos-id').style.marginTop = "-6.5rem";
        document.getElementById('taxaEntrega').value = String(pedidoDeMesa.frete.toFixed(2)).replace('.', ',');
    } else if (pedidoDeMesa.tipo_entrega === "MESA"){
        document.getElementById('mesa').value = pedidoDeMesa.numero_mesa;
        document.getElementById('mesa').disabled = true;
        document.getElementById('grid-altera-margem').style.height = "120%";
        document.getElementById('taxaEntrega').value = "0,00";
    } else if (pedidoDeMesa.tipo_entrega === "COMANDA"){
        document.getElementById('comanda').value = pedidoDeMesa.numero_mesa;
        document.getElementById('comanda').disabled = true;
        document.getElementById('grid-altera-margem').style.height = "120%";
        document.getElementById('taxaEntrega').value = "0,00";
    } else if (pedidoDeMesa.tipo_entrega === "BALCÃO"){
        document.getElementById('fone').value = pedidoDeMesa.telefone;
        document.getElementById('fone').disabled = true;
        document.getElementById('grid-altera-margem').style.height = "120%";
        document.getElementById('taxaEntrega').value = "0,00";
    }
}

function abrirInputPagamento(tipoPagamento){
    let toggle = document.getElementById(tipoPagamento + 'ck');
    if(toggle.checked === true){
        document.getElementById(tipoPagamento).style.display = "block";
    } else if (toggle.checked === false){
        document.getElementById(tipoPagamento).style.display = "none";
    }
}

function somaTotal(inputsPagamento){
    let somatoria = 0;
    for(input of inputsPagamento){
        if(input.value !== ''){
            const valorInseridoCampo = parseFloat(input.value.replace('.','').replace('.','').replace(',','.'));
            somatoria += valorInseridoCampo;
        }
    }

    return somatoria;

    /*let somandoValores = parseFloat(document.getElementById('totPago').value.replace(',', '.') === "" ? 0.00 : document.getElementById('totPago').value.replace(',', '.'));

    if (tpPag === 'PENDURAR'){
        somandoValores -= parseFloat(document.getElementById(tpPag).value.replace(',', '.') === "" ? 0.00 : document.getElementById(tpPag).value.replace(',', '.'));

        document.getElementById('totalReceb').value = somandoValores.toFixed(2);
        document.getElementById('receber').value = document.getElementById(tpPag).value;
    }*/
}

function atualizaValorAReceber(inputsPagamento){
    const somatoriaPagamentos = somaTotal(inputsPagamento);
    document.getElementById('totalReceb').value = String(somatoriaPagamentos.toFixed(2)).replace('.',',');
    document.getElementById('receber').value = String((pedidoDeMesa.vl_total - somatoriaPagamentos).toFixed(2)).replace('.',',');
}

function adicionaPedidoNaTela(){
    for(let k = 0; k < itemPedidoMesa.length; k++){
        if (itemPedidoMesa[k].complemento.search('/') > 0) {
            let itensExtrasArray = itemPedidoMesa[k].complemento.split('/');
            for (j of itensExtrasArray) {
                let separandoItensEQtds = j.split('x');
                if (separandoItensEQtds[0] !== "") {
                    nomeItemForSearch.push(separandoItensEQtds[0]);
                    qtdsItensForSearch.push(parseInt(separandoItensEQtds[1]));
                }
            }
            nomeItemForComparar.push(nomeItemForSearch);
            qtdsItens.push(qtdsItensForSearch);
            nomeItemForSearch = [];
            qtdsItensForSearch = [];
        }

        let complements = nomeItemForComparar[0];

        for (complementoText in complements) {
            if (complements[complementoText].search('1') > -1) {
                primeiroSabor++;
            } else if (complements[complementoText].search('2') > -1) {
                segundoSabor++;
            } else if (complements[complementoText].search('3') > -1) {
                terceiroSabor++;
            } else if (complements[complementoText].search('4') > -1) {
                quartoSabor++;
            } else if (complements[complementoText].search("-") === -1) {
                itemNormal++;
            }
        }

        arrayTiposItensExtras.push(primeiroSabor);
        arrayTiposItensExtras.push(segundoSabor);
        arrayTiposItensExtras.push(terceiroSabor);
        arrayTiposItensExtras.push(quartoSabor);
        arrayTiposItensExtras.push(itemNormal);

        if (complements !== undefined) {
            for (montandoItemExtras of arrayTiposItensExtras) {
                for (let contador = 0; contador < montandoItemExtras; contador++) {
                    controleLabel = montandoItemExtras;
                    if (complements[0] !== undefined) {
                        if (montandoItemExtras >= 1 && complements[0].search('-') > -1) {

                            complementoXml += `
                            ${montandoItemExtras === controleLabel ? '<b style = "font-size: 0.7rem">' + ObjSaboresMesas[entrandoObjSabores] + '</b>' : ""}
                            <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0].substring(2)} x ${qtdsItens[0][0]}</p>
                        `

                            complements.splice(0, 1);
                            qtdsItens[0].splice(0, 1);

                            controleLabel--;
                        } else {

                            if (montandoItemExtras >= 1) {
                                complementoXml += `
                                <p style = "font-size: 0.6rem; margin-bottom: 0px">${complements[0]} x ${qtdsItens[0][0]}</p>
                            `

                                complements.splice(0, 1);
                                qtdsItens[0].splice(0, 1);
                                controleLabel--;
                            }

                        }
                    }
                }

                entrandoObjSabores++;
            }

        }

        body += `
            <tr class = "table-row-receb" id = "tr-mesa-${k}">
                <td style = "text-align: center;" id = "table-cell">${itemPedidoMesa[k].qtd_prod}</td>
                <td style = "font-size: 0.7rem;" id = "table-cell">${itemPedidoMesa[k].desc_produto} <p style = "font-weight: 800; font-size: 0.7rem; margin-bottom: 0px">${objTamanhos[itemPedidoMesa[k].tam_escolhido] !== "(NORMAL)" ? "Tamanho: " + objTamanhos[itemPedidoMesa[k].tam_escolhido] : ''}</p> ${complementoXml}</td>
                <td id = "table-cell">${Helper.valorEmReal((itemPedidoMesa[k].vl_unit + itemPedidoMesa[k].acrescimo) * itemPedidoMesa[k].qtd_prod)}</td>
            </tr>
        `

        valorTotal = valorTotal + ((itemPedidoMesa[k].vl_unit + itemPedidoMesa[k].acrescimo) * itemPedidoMesa[k].qtd_prod);

        primeiroSabor = 0
        segundoSabor = 0
        terceiroSabor = 0
        quartoSabor = 0
        itemNormal = 0
        complementoXml = "";
        nomeItemForComparar = [];
        qtdsItens = [];
        arrayTiposItensExtras = [];

    }

    pedidoReceb.innerHTML += body;

}

function fecharTelaPagamento(){
    zerarAmbientes();
    document.getElementById('container-receb').innerHTML = "";
    document.getElementById('container-receb').style.display = "none";
    document.getElementById('listaPedidos') ? document.getElementById('listaPedidos').innerHTML = "" : null;
    refazerTabela();
    if(pedidoDeMesa.tipo_entrega === 'MESA'){
        carregaFrame('ReservaDeMesa');
    }
}

async function listenerBtnAceitar(){
    let pagamentosSelecionados = [];
    let pagamentosEspeciaisSelecionados = [];
    let querySelector = document.querySelectorAll('.recebimentos');
    let dataPedidoSelecionado = new Date (pedidoDeMesa.dt_pedido);
    dataPedidoSelecionado.setDate(dataPedidoSelecionado.getDate()+1);
    dataPedidoSelecionado.setHours(0);
    dataPedidoSelecionado.setMinutes(0);
    dataPedidoSelecionado.setSeconds(0);

    for(dom of querySelector){
        const valor = parseFloat(dom.value.replace('.','').replace('.','').replace(',','.'));
        if(valor) {
            if(parseInt(dom.id)){
                let data = new Date();
                data.setHours(0);
                data.setMinutes(0);
                data.setSeconds(0);
                let dataAgoraBanco = data.getFullYear() + '-' + (data.getMonth() + 1) + '-' + data.getDate();
                if(idFormaCredito === parseInt(dom.id)){
                    pagamentosSelecionados.push(
                        {
                            nr_pedido     : pedidoDeMesa.id_pedido,
                            vl            : valor,
                            qde_parcela   : document.getElementById('parcelas').value,
                            troco         : 0,
                            dt_pedido     : dataPedidoSelecionado,
                            hr_pedido     : pedidoDeMesa.hr_pedido,
                            id_fpgto      : dom.id,
                            dt_pgto       : dataAgoraBanco
                        }
                    );
                    continue;
                }
                pagamentosSelecionados.push(
                    {
                        nr_pedido     : pedidoDeMesa.id_pedido,
                        vl            : valor,
                        qde_parcela   : 1,
                        troco         : 0,
                        dt_pedido     : dataPedidoSelecionado,
                        hr_pedido     : pedidoDeMesa.hr_pedido,
                        id_fpgto      : dom.id,
                        dt_pgto       : dataAgoraBanco
                    }
                );
            }else
                pagamentosEspeciaisSelecionados.push(
                    {
                        nr_pedido     : pedidoDeMesa.id_pedido,
                        vl            : valor,
                        id_fpgto      : dom.id,
                        dt_pgto       : dom.id === 'PENDURAR' ? document.getElementById('paraDia').value : null
                    }
                );
        }
    }

    if(pagamentosSelecionados.length > 0 || pagamentosEspeciaisSelecionados.length > 0) {
        let somatoriaValoresPagamento = retornaValoresSomadosArray(pagamentosSelecionados, 0);
        const somatoriaValoresPagamentosEspeciais = retornaValoresSomadosArray(pagamentosEspeciaisSelecionados, 0);
        somatoriaValoresPagamento += somatoriaValoresPagamentosEspeciais;

        if(somatoriaValoresPagamento > pedidoDeMesa.vl_total){

            const continuarExec = await swal({
                icon: 'warning',
                title: 'Valor excedido!',
                closeOnClickOutside:false,
                text: 'O valor das formas de pagamento excederam o valor do pedido em R$' + (somatoriaValoresPagamento - pedidoDeMesa.vl_total).toFixed(2) + ', confirma o valor inserido?',
                buttons: ['Não', 'Sim']
            });

            if(!continuarExec) return 0;
             
            await swal({
               icon: 'warning',
               title: 'Valor excedido!',
               closeOnClickOutside:false,
               text: 'O valor das formas de pagamento excederam o valor do pedido em R$' + (somatoriaValoresPagamento - pedidoDeMesa.vl_total).toFixed(2) + ', registrar como troco ou gorjeta?',
               buttons: ['Gorjeta', 'Troco']
            }).then(result=>{
                
                const valorTroco = (somatoriaValoresPagamento - pedidoDeMesa.vl_total)
                let registrouTroco = false;
                for(let i = 0; i < pagamentosSelecionados.length; i++){
                    if(pagamentosSelecionados[i].descricao === 'DINHEIRO') {
                        pagamentosSelecionados[i].troco = valorTroco;
                        registrouTroco = true;
                        break;
                    }
                }
                if(!registrouTroco) pagamentosSelecionados[0].troco = valorTroco;

            }).catch(ex=>{
                throw ex;
            });

        }else if(somatoriaValoresPagamento < pedidoDeMesa.vl_total){

            await swal({
                icon: 'warning',
                title: 'Valor insuficiente!',
                closeOnClickOutside:false,
                text: 'Ainda faltam R$' + String((pedidoDeMesa.vl_total - somatoriaValoresPagamento).toFixed(2)).replace('.',',') + ' para chegar ao valor total do pedido.',
                button: 'Voltar'
            });
    
            return;

        }

        cHistPgto.insertBulk(pagamentosSelecionados).then(response => {
            atualizaDescontoPedidoERegistraPendura(pedidoDeMesa.id_pedido, pagamentosEspeciaisSelecionados);
        }).catch((e)=>{
            throw e;
        });
    }
    else swal({icon:'error',title:'Por favor, preencha uma forma de pagamento'});
    
}

async function atualizaDescontoPedidoERegistraPendura(idPedido, pagamentosEspeciais){
    const objDesconto = pagamentosEspeciais.filter(p => p.id_fpgto === 'DESCONTO'? p.vl : null )[0];
    if(objDesconto) await cCadPedido.atualizaValorDesconto(idPedido, objDesconto.vl);
    
    const objPendura = pagamentosEspeciais.filter(p => p.id_fpgto === 'PENDURAR'? p.vl : null )[0];
    if(objPendura) await salvaPendura(objPendura, idPedido);

    atualizaStatusPedido(idPedido);
}

async function salvaPendura(objPendura, idPedido){
    let dataCriacaoPendura = new Date ();
    dataCriacaoPendura.setDate(dataCriacaoPendura.getDate());
    dataCriacaoPendura.setHours(0);
    dataCriacaoPendura.setMinutes(0);
    dataCriacaoPendura.setSeconds(0);

    const caixa = JSON.parse(window.localStorage.getItem('caixaAtual'));
    const operador = JSON.parse(window.localStorage.getItem('usuario'));
    cPendura.dt_receber = objPendura.dt_pgto;
    cPendura.valor_receber = objPendura.vl;
    cPendura.cliente = pedidoDeMesa.cliente;
    cPendura.id_pedido = idPedido;
    cPendura.id_oper = operador.id_oper;
    cPendura.id_caixa = caixa.id_caixa;
    cPendura.valor_ja_recebido = 0.00;
    cPendura.dt_criacao = dataCriacaoPendura;

    await cPendura.insert();
}

function atualizaStatusPedido(idPedido){

    cCadPedido.atualizaStatusPedido(idPedido, 'RECEBIDO').then(confirm=>{
        registrarVendaNoHistorico();
    }).catch((e)=>{
        throw e;
    });
    
}

function registrarVendaNoHistorico(){
    const caixa = JSON.parse(window.localStorage.getItem('caixaAtual'));
    const operador = JSON.parse(window.localStorage.getItem('usuario'));

    cHistVenda.nr_pedido    = pedidoDeMesa.nr_pedido;
    cHistVenda.data         = pedidoDeMesa.dt_pedido;
    cHistVenda.hora         = pedidoDeMesa.hr_pedido;
    cHistVenda.vl_pedido    = pedidoDeMesa.vl_total;
    cHistVenda.cliente      = pedidoDeMesa.cliente;
    cHistVenda.obs          = 'OBS';
    cHistVenda.id_pedido    = pedidoDeMesa.id_pedido;
    cHistVenda.id_oper      = operador.id_oper;
    cHistVenda.id_caixa     = caixa.id_caixa;

    cHistVenda.id_func_atend = null;

    if(dadosEstabelecimento.cfg_contr_motoq){
        if(pedidoDeMesa.tipo_entrega.toUpperCase() === 'ENTREGA'){
            const entregadorSelecionado = document.getElementById('ENTREGADOR');
            if(entregadorSelecionado) cHistVenda.id_func_atend = parseInt(entregadorSelecionado.value);
        }else if (pedidoDeMesa.tipo_entrega.toUpperCase() === 'MESA'){
            const garcomSelecionado = document.getElementById('GARÇOM');
            if(garcomSelecionado) cHistVenda.id_func_atend = parseInt(garcomSelecionado.value);
        }
    }

    cHistVenda.insert().then(result=>{
        /*window.sessionStorage.removeItem('pedidosTotais');
        window.sessionStorage.removeItem('itensDosPedidos');*/
        swal({
           icon: 'success',
           title: 'Pedido recebido e registrado',
           closeOnClickOutside:true,
           text: 'Sucesso',
           button: {text: 'Okay', closeModal: true}
        });
        fecharTelaPagamento();
    }).catch(ex=>{
        throw ex;
    });
}

function retornaValoresSomadosArray(array, posicao){
    if(array[posicao] === undefined) return 0;
    return array[posicao].vl + retornaValoresSomadosArray(array, posicao + 1);
}
//#endregion

async function preencheComboEntregador(combo, id) {
	cCadFunc.selectMotoqueiros().then(dados => {
		if(combo.id === 'id_grupo') combo.insertAdjacentHTML('beforeend', `<option value="0"></option>`);
		dados = JSON.parse(JSON.stringify(dados));
        dados = [{id:null, descricao: "S/ Entregador", tba_Funco:{descricao:'ENTREGADOR'}}, ...dados];
		dados.forEach(registro => {
            if(registro.tba_Funco.descricao === 'ENTREGADOR'){
                if (id != undefined && id == Object.values(registro)[0]) {
                    combo.insertAdjacentHTML('afterbegin', `<option value="${Object.values(registro)[0]}" selected>${Object.values(registro)[1]}</option>`);
                } else {
                    combo.insertAdjacentHTML('beforeend', `<option value="${Object.values(registro)[0]}">${Object.values(registro)[1]}</option>`);
                }
            }
		});
	})
}

async function preencheComboGarcom(combo, id) {
    if(!combo && !id) return;
	cCadFunc.selectMotoqueiros().then(dados => {
		if(combo.id === 'id_grupo') combo.insertAdjacentHTML('beforeend', `<option value="0"></option>`);
		dados = JSON.parse(JSON.stringify(dados));
        dados = [{id:null, descricao: "S/ Garçom", tba_Funco:{descricao:'GARÇOM'}}, ...dados];
		dados.forEach(registro => {
            if(registro.tba_Funco.descricao === 'GARÇOM'){
                if (id != undefined && id == Object.values(registro)[0]) {
                    combo.insertAdjacentHTML('afterbegin', `<option value="${Object.values(registro)[0]}" selected>${Object.values(registro)[1]}</option>`);
                } else {
                    combo.insertAdjacentHTML('beforeend', `<option value="${Object.values(registro)[0]}">${Object.values(registro)[1]}</option>`);
                }
            }
		});
	})
}

async function preencheComboFuncionario(combo, id, tipoFunc) {
    if(!combo && !id) return;

	cCadFunc.selectMotoqueiros().then(dados => {
        document.getElementById('spanLabelCombo').innerHTML = tipoFunc;
		if(combo.id === 'id_grupo') combo.insertAdjacentHTML('beforeend', `<option value="0"></option>`);
		dados = JSON.parse(JSON.stringify(dados));
        dados = [{id:null, descricao: `S/ ${tipoFunc}`, tba_Funco:{descricao:tipoFunc}}, ...dados];

        let conteudoCombo = '';
		dados.forEach(registro => {
            if(registro.tba_Funco.descricao === tipoFunc) conteudoCombo += `<option value="${Object.values(registro)[0]}" ${ id != undefined && id == Object.values(registro)[0] ? 'selected' : ''}>${Object.values(registro)[1]}</option>`;
		});
        combo.innerHTML = conteudoCombo;
	})
}

module.exports = {
    abrirInputPagamento  : abrirInputPagamento,
    adicionaPedidoNaTela : adicionaPedidoNaTela,
    fecharTelaPagamento  : fecharTelaPagamento,
    selecionarFormasPgto : selecionarFormasPgto,
    focusoutInputsFormasPgto : focusoutInputsFormasPgto
}