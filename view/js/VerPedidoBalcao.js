function selecionarTudo(){
    let cCadCliente = require(path.join(__dirname + '/../../controller/cCadClientes.js'));
    let listaCliente = cCadCliente.recuperaTodosClientesByLogradouro();
    listaCliente.then(data => {
        let actuallyData = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < actuallyData.length; i++) {
            if(actuallyData[i].tipo_entrega === "BALCÃO"){
                insereItemNaTela(actuallyData[i], true, i);
            }
        }
    });
}

async function insereItemNaTela(retorno,stringify, posicaoVetor){
	if(stringify != true){
		retorno = JSON.parse(JSON.stringify(retorno));
	}

	let tbody = document.getElementById('listaClienteDelivery');
	tbody.innerHTML += `
	<tr id="tr${retorno.id_pedido}">							
		<td  class="border-right" scope="row">
		    <center>
                ${retorno.id_pedido}
		    </center>
		</td>
		<td class="border-right txt-label">${retorno.id_pedido}</td>
		<td class="border-right txt-label" id="nome${retorno.id_pedido}">${retorno.cliente}</td>
        <td class="border-right txt-label">
            <button onclick = "guardaCliente(${posicaoVetor})">
                <img src = "../Imagens/order.png">
                Realizar pedido
            </button>
        </td>
	</tr>`;
}


module.exports = {
	selecionarTudo: selecionarTudo,
	insereItemNaTela: insereItemNaTela,
};