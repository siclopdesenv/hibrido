try{
	let ctx;
	let lista;
	let tamanhoLista;
}catch(ex){
	console.log(ex);
}

var Chart = require('chart.js');
//ctxQtdCliente = context. o contexto é necessario para sabermos se o grafico sera em 2d ou 3d
// var chartVendas = new Chart(ctx, {
// 		type: "line",
// 		data: {
// 			labels: ["jan","fev","mar","abr","mai","jun","jul"],
// 			datasets: [{
// 					label: "Porcentagem de quão trouxa voce foi em 2019",
// 					data: [5,7,9,8,3,6,10],
// 					borderWidth: 2,
// 					borderColor: "rgba(77,166,250,0.90)", 
// 			}] 
// 		}

// });

var ctxQtdCliente = document.getElementById('qtdCliente');
var ctxVendas = document.getElementById('vendas');
var ctxDespesas = document.getElementById('despesas');


var chartQtdCliente = new Chart(ctxQtdCliente, {
	"type":"bar",
	"data": {"labels":
	["January","February","March","April","May","June","July"],
	"datasets":[
	{"label":"Clientes",
	"data":[65,59,80,81,56,55,40],
	"fill":false,"backgroundColor":[
	"rgba(255, 99, 132, 0.2)",
	"rgba(255, 159, 64, 0.2)",
	"rgba(255, 205, 86, 0.2)",
	"rgba(75, 192, 192, 0.2)",
	"rgba(54, 162, 235, 0.2)"
	,"rgba(153, 102, 255, 0.2)"
	,"rgba(201, 203, 207, 0.2)"]
	,"borderColor":["rgb(255, 99, 132)"
	,"rgb(255, 159, 64)"
	,"rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"]
	,"borderWidth":1}]},"options":{"scales":{"yAxes":[{"ticks":{"beginAtZero":true}}]}}
});

var chartVendas = new Chart(ctxVendas, {
	"type":"line",
	"data":
		{"labels":
			["January","February","March","April","May","June","July"]
			,"datasets":				
				[{"label":"My First Dataset",
				"data":[65,59,80,81,56,55,40],
				"fill":false,
				"borderColor":"rgb(75, 192, 192)",
				"lineTension":0.1}]
			},"options":{}
		}
	);

var chartDespesas = new Chart(ctxDespesas,{
	"type":"doughnut"
	,"data":{"labels":
	["Comidas","outros","Funcionários"]
	,"datasets":
		[{"label":"My First Dataset","data":[50,10,40],
			"backgroundColor":
				["rgb(255, 99, 132)","rgb(54, 162, 235)","rgb(255, 205, 86)"]}]}
});


