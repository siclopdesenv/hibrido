//#region Importações de bibliotecas e de arquivos JS;
const { isEmptyObject, isArray } = require("jquery");
const { criaConteudo } = require("../../js/Impressao");

let cCadProdutos = require("../../../controller/cCadProdutos");
const swal = require("sweetalert");
//#endregion

let informacoesPedidoEdicao = null;

//#region BINDS PARA TELA

Bind.bind('f1', function () {
    bindsPorcoes(1);
});

Bind.bind('f2', function () {
    bindsPorcoes(2);
});

Bind.bind('f3', function () {
    bindsPorcoes(3);
});

Bind.bind('f4', function () {
    bindsPorcoes(4);
});

Bind.bind('f9', function () {
    abrirItensExtras();
});

//#endregion

//#region Variaveis Ambiente         
let screenWidth = window.innerWidth;
let screenHeight = window.innerHeight;
let produtosEscolhidos = [];
let linhasPintadas = [];
let itensExtras = [];
let produtos = [];
let qtdSabores = [];
let itensExtrasParaPzComb = [];
let tamanhoAnterior = "";
let conteudoHTML1 = "";
let tipoOperacaoSA = "";
let TelaAnterior = "";
let tipoPedido = "";
pedidoAntesDaEdicao = "";
tbody = "";
let flagTamanho = false;
let valorSomaDosItens = 0;
let codPedido = 0;
let valorObj = 0;
let posicaoDoItemExtraNoSabor = 0;
let posicaoVetorItPed = 0;
let vTipo = 1;
let vTipoItex = 1;
let itensDosPedidosMesa = window.sessionStorage.getItem('itemDoPedidoMesa');
var pedidosTotais;
var valorTroco = 0;
inforsis = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
usuario = JSON.parse(window.localStorage.getItem('usuario'));

pedido = {
    produtos: [],
    itex: [],
    valores: [],
    valoresCalculados: [],
    qtds: [],
};

itensExtrasModel = {
    nomeItem: [],
    qtdsItens: [],
}

var objTamanhos = {
    'nn': 'NORMAL',
    'br': 'BROTO',
    'md': 'MÉDIA',
    'gi': 'GIGANTE',
}

const objAtalhos = {
    'f2': () => { return document.getElementById('f2'); },
    'f3': () => { return document.getElementById('f3'); },
    'f4': () => { return document.getElementById('f4'); },
}

let objProximoSaborPizza = {
    0: 'PRIMEIRO SABOR',
    1: 'SEGUNDO SABOR',
    2: 'TERCEIRO SABOR',
    3: 'QUARTO SABOR',
}

//#endregion

//#region FUNÇÕES E INTERAÇÕES REFERENTES AOS PEDIDOS

//objAtalhos['f2']().addEventListener('click', ()=>{bindsPorcoes(2)})
//objAtalhos['f3']().addEventListener('click', ()=>{bindsPorcoes(3)})
//objAtalhos['f4']().addEventListener('click', ()=>{bindsPorcoes(4)})


var onDomIsRendered = function(domString) {
    return new Promise(function(resolve, reject) {
        function waitUntil() {
        setTimeout(function() {
            if($(domString).length > 0){
            resolve($(domString));
            }else {
            waitUntil();
            }
        }, 100);
        }
        //start the loop
        waitUntil();
    });
};

onDomIsRendered("#f2").then((element)=>{objAtalhos['f2']().addEventListener('click', ()=>{bindsPorcoes(2)})});
onDomIsRendered("#f3").then((element)=>{objAtalhos['f3']().addEventListener('click', ()=>{bindsPorcoes(3)})});
onDomIsRendered("#f4").then((element)=>{objAtalhos['f4']().addEventListener('click', ()=>{bindsPorcoes(4)})});


window.onresize = function () {
    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;

    if (screenWidth < 1024) {
        estiloResponsivoParaElementosInalterados(1024, tipoPedido)
    } else if (screenWidth > 1280) {
        estiloResponsivoParaElementosInalterados(1280)
    }
}

function bindsPorcoes(valorVTipo) {
    const atalhos = ['f2', 'f3', 'f4'];

    for (atalho of atalhos) objAtalhos[atalho]().style.backgroundColor = '#066896';

    if(valorVTipo !== 1) document.getElementById('f' + valorVTipo).style.backgroundColor = "#4B0082";

    vTipo = valorVTipo;
    produtosEscolhidos = [];
    flagTamanho = false;
    desfazLinhasPintadas();
}

function adicionaEventListenerTroco(){
    document.getElementById('labelTroco').addEventListener('click', solicitaInfoTroco);
}

async function solicitaInfoTroco(){
    //Procurar algum lugar na tela para informar isso
    swal({
       icon: 'warning',
       title: 'Troco para:',
       closeOnClickOutside:false,
       content: 'input',
       buttons: ['Cancelar', 'Salvar']
    }).then(confirm=>{
        if(confirm){ //Posteriormente verificar se o valor digitado é maior que o valor do pedido
            let valorDigitado = document.getElementsByClassName('swal-content__input')[0].value;
            valorDigitado = valorDigitado.replace('.','').replace('.','').replace(',','.');
            valorTroco = parseFloat(valorDigitado);
            document.getElementById('labelTroco').innerHTML = 'TROCO P/ R$' + valorDigitado;
        }
    }).catch(e=>{
       throw e;
    });

    setTimeout(()=>{document.getElementsByClassName('swal-content__input')[0].addEventListener('keyup', function(event){Helper.maskMoeda(this, event)})}, 1000);

}

function definindoValorDaVariavelTipo(valorTipo) {
    vTipoItex = valorTipo;
}

async function gerandoNrPedido() {
    let pedidos = require(path.join(__dirname + '/../../../controller/cCadPedido.js'));
    let dataAtual = Helper.dataAtualEua(true);

    dataAtual = dataAtual;

    let query = await pedidos.recuperaNrPedido(dataAtual);
    let resultQuery;
    if (query[0] !== undefined && query.length !== 0) {
        resultQuery = query[0].nr_pedido;
        return resultQuery;
    } else {
        return resultQuery = 0;
    }

}

function zerarCamposComanda() {
    document.getElementById('domComanda').value = "";
    document.getElementById('domCli').value = "";
    document.getElementById('adicionarItensPedido').innerHTML = "";
}

function SelecionaProdutos(tipoOperacao, telaAtual, tipoPed, infoPedidoEdicao = null) {
    adicionaEventListenerTroco();
    //document.getElementById('menu').style.display = "none";
    estiloResponsivoParaElementosInalterados(screenWidth, tipoPed, tipoOperacao)

    informacoesPedidoEdicao = infoPedidoEdicao;
    tipoOperacaoSA = tipoOperacao;
    TelaAnterior = telaAtual;
    tipoPedido = tipoPed;
    produtos = [];
    tbody = "";
    let cCadProdutos = require(path.join(__dirname + '/../../../controller/cCadProdutos.js'));
    let listaProd = inforsis.cfg_contr_estq ? cCadProdutos.selectOrderByNomeQtdDisponivel() : cCadProdutos.selectOrderByNome();
    listaProd.then(data => {
        let actuallyData = JSON.parse(JSON.stringify(data));
        for (let i = 0; i < actuallyData.length; i++) {
            mostrarTodosOsProdutos(actuallyData[i], true, i, tipoOperacao)
        };

        selecionaItens();

        setTimeout(() => {
            if (tipoOperacao === "Editar") {
                setTimeout(() => {
                    adicionarPedidoAoArray(tipoOperacaoSA);
                }, 1000);
                listaProd = null;
            } else {
                listaProd = null;
            }
        }, 100);
    });

}

function SelecionaProdutosLikeBy(pesquisa) {
    let cCadprodutos = require(path.join(__dirname + '/../../../controller/cCadProdutos.js'));
    let listaProd = cCadprodutos.like(pesquisa);
    listaProd.then(data => {
        let listaRefinada = JSON.parse(JSON.stringify(data));
        tbody = "";
        for (let i = 0; i < listaRefinada.length; i++) mostrarTodosOsProdutos(listaRefinada[i], true, i);
    })

    listaProd = null;

}

function SelecionaProdutosCodBarras(pesquisa) {
    let cCadprodutos = require(path.join(__dirname + '/../../../controller/cCadProdutos.js'));
    let listaProd = cCadprodutos.selectByCodBarras(pesquisa);
    listaProd.then(data => {
        let produtoEncontrado = JSON.parse(JSON.stringify(data));
        if(produtoEncontrado){
            produtos = [produtoEncontrado];
            clickValor(0, 'nn');
            //tbody = "";
        }
    })

    listaProd = null;
}

function enviaTexto() {
    document.getElementById('table-prod').innerHTML = "";
    let pesquisa = document.getElementById('buscaProd').value;
    if(pesquisa.length === 5 && !isNaN(parseInt(pesquisa)))
        SelecionaProdutosCodBarras(pesquisa);
    else
        SelecionaProdutosLikeBy(pesquisa);
}

function Retornar() {
    if (window.localStorage.getItem('tipoMenu') === "1") {
        document.getElementById('menu').style.display = 'block'
        if (clientes.length > 1) {
            document.getElementById('container-cliente').style.display = "none";
            fecharModais('endereco')

            pedido = {
                produtos: [],
                itex: [],
                valores: [],
                valoresCalculados: [],
                qtds: [],
            };

            pedidoAntesDaEdicao = "";

            produtos = []

            window.sessionStorage.removeItem('itensDosPedidos');
            window.sessionStorage.removeItem('itemDoPedidoMesa');
            window.sessionStorage.removeItem('pedidoMesa');
            window.sessionStorage.removeItem('pedidosTotais');

        } else {
            voltarSubMenu(1);

            pedido = {
                produtos: [],
                itex: [],
                valores: [],
                valoresCalculados: [],
                qtds: [],
            };

            pedidoAntesDaEdicao = "";

            produtos = []

            window.sessionStorage.removeItem('itensDosPedidos');
            window.sessionStorage.removeItem('itemDoPedidoMesa');
            window.sessionStorage.removeItem('pedidoMesa');
            window.sessionStorage.removeItem('pedidosTotais');

        }
    } else if (window.localStorage.getItem('tipoMenu') === "0") {
        window.location.href = "../html/menuTradicional.html";
        document.getElementById('menuTrad').style.display = "block";

        pedido = {
            produtos: [],
            itex: [],
            valores: [],
            valoresCalculados: [],
            qtds: [],
        };

        pedidoAntesDaEdicao = "";

        produtos = []

        window.sessionStorage.removeItem('itensDosPedidos');
        window.sessionStorage.removeItem('itemDoPedidoMesa');
        window.sessionStorage.removeItem('pedidoMesa');
        window.sessionStorage.removeItem('pedidosTotais');

    }
}

function goBack(telaAtual) {

    //zerarVariaveis();

    if (telaAtual === "Reserva") {
        if (tipoMenu === "0") {

            window.sessionStorage.removeItem('itensDosPedidos');
            window.sessionStorage.removeItem('itemDoPedidoMesa');
            window.sessionStorage.removeItem('pedidoMesa');
            window.sessionStorage.removeItem('pedidosTotais');

            carregaFrame("ReservaDeMesa");

            pedidoAntesDaEdicao = "";

            produtos = []

        } else if (tipoMenu === "1") {
            window.sessionStorage.removeItem('itensDosPedidos');
            window.sessionStorage.removeItem('itemDoPedidoMesa');
            window.sessionStorage.removeItem('pedidoMesa');
            window.sessionStorage.removeItem('pedidosTotais');

            carregaFrame('ReservaDeMesa', 'SubMenu');

            pedidoAntesDaEdicao = "";

            produtos = []
        }
    }
}

function clickValor(posicaoProdutoVetor, tamanho, linhaProduto) {
    const produtoClickado = JSON.parse(JSON.stringify(produtos[posicaoProdutoVetor]));
    const itensExtraClikado = JSON.parse(JSON.stringify(itensExtrasModel));

    if(linhaProduto !== undefined) linhaProduto = document.getElementById(linhaProduto);
    produtoClickado.tamanhoEscolhido = tamanho;

    if (produtoClickado.Grupo.descricao === 'PIZZA' || produtoClickado.Grupo.descricao === "PIZZAS") {
        if (produtoClickado[tamanho] === "0,00") return;

        if (flagTamanho === false) {
            tamanhoAnterior = tamanho;
            flagTamanho = true;
        }

        if (tamanhoAnterior === tamanho) {
            produtosEscolhidos.push(produtoClickado);
            itensExtrasParaPzComb.push(itensExtraClikado);

            if (produtosEscolhidos.length === vTipo) {
                if(!pedido.produtos) pedido.produtos = [];
                
                pedido.produtos.push(produtosEscolhidos);
                pedido.itex.push(itensExtrasParaPzComb);
                produtosEscolhidos = [];
                itensExtrasParaPzComb = [];
                vTipo = 1;
                flagTamanho = false;
                bindsPorcoes(1);
                desfazLinhasPintadas();
                preencherNotaTela();

                if (document.getElementById('buscaProd').value !== "") {
                    document.getElementById('buscaProd').value = "";
                    enviaTexto();
                }

            } else {
                linhaProduto.style.backgroundColor = "#1f65ff";
                linhasPintadas.push(linhaProduto);
            }
        } else {
            swal({
                icon: "warning",
                text: 'Você deve escolher um produto do tamanho: ' + objTamanhos[tamanhoAnterior] + "!",
                button: {
                    text: 'Entendido!'
                }
            });
        }

    } else {
        pedido.produtos.push(produtoClickado);
        pedido.itex.push(JSON.parse(JSON.stringify(itensExtrasModel)));
        //produtosEscolhidos = [];
        //vTipo = 1;
        //flagTamanho = false;
        bindsPorcoes(1);
        desfazLinhasPintadas();
        preencherNotaTela();

        if (document.getElementById('buscaProd').value !== "") {
            document.getElementById('buscaProd').value = "";
            enviaTexto();
        }
    }

    linhaProduto = [];

}

function desfazLinhasPintadas() {
    //objAtalhos['f1']().style.backgroundColor = '#066896';
    
    for (element of linhasPintadas) element.style.backgroundColor = '#044D70';
}

function preencherNotaTela(flagEdit) {
    //let adicionaHTML = document.getElementById("table-order");
    let adicionaHTML = document.getElementById("corpoOrder");
    let adicionaSomaItens = document.getElementById("somaDosItens");
    let adicionaValorTotal = document.getElementById("valorTotal");
    itensExtras = [];
    //let taxaEntrega = 2;
    valorSomaDosItens = 0;

    let flagPedidoAdd = true;

    if (pedido.produtos.length === 0) {
        adicionaHTML.innerHTML = conteudoHTML1;
        adicionaSomaItens.innerHTML = 'R$ 0,00';
        adicionaValorTotal.innerHTML = clienteSelecionado.Logradouro ? clienteSelecionado.Logradouro.frete ? Helper.valorEmReal(clienteSelecionado.Logradouro.frete) : 'R$ 0,00' : 'R$ 0,00';
        //adicionaValorTotal.innerHTML = Helper.valorEmReal(0.00);
    }

    for (let i = 0; i < pedido.produtos.length; i++) {
        let produto = pedido.produtos[i];
        if (isArray(produto)) {
            //Se sim, é uma pizza
            let itEx = pedido.itex[i];
            let nomeCompletoPizza = '';
            let valorPizza = 0;
            let maiorValor = 0;
            let codigoPizzas = '';
            let acrescimoTotal = 0;
            let complementoXml = "";

            for (pizza of produto) {
                codigoPizzas += pizza.cod_prod + '/';
                nomeCompletoPizza += produto.length > 1 ? '1/' + produto.length + ' ' + pizza.nome_prod + '<br>' : pizza.nome_prod + '<br>';
                pizza[pizza.tamanhoEscolhido] > maiorValor ? maiorValor = pizza[pizza.tamanhoEscolhido] : null;
                valorPizza += pizza[pizza.tamanhoEscolhido];
            }

            valorPizza = dadosEstab.cfg_pzcombinada === 'P' ? maiorValor : valorPizza / produto.length;
            pedido.qtds[i] = pedido.qtds[i] ? pedido.qtds[i] : 1;
            pedido.valores[i] = valorPizza;
            pedido.valoresCalculados[i] ? null : pedido.valoresCalculados[i] = pedido.valores[i] * pedido.qtds[i];
            valorPizza = valorPizza * pedido.qtds[i];

            codigoPizzas = codigoPizzas.substr(-20, (codigoPizzas.length - 1))

            for (k in itEx) {
                let arrayDeItensExtras = itEx[k];
                for (j in arrayDeItensExtras) {
                    let position = arrayDeItensExtras[j].length;

                    if (arrayDeItensExtras[j].length !== 0 && arrayDeItensExtras[j].length > 1 && j === "nomeItem") {
                        for (z in arrayDeItensExtras[j]) {
                            acrescimoTotal += (arrayDeItensExtras.qtdsItens[z] * arrayDeItensExtras[j][z].acrescimo);
                            complementoXml += `
                                ${arrayDeItensExtras[j].length === position ? "<b style = 'font-size: 0.7rem'>" + objProximoSaborPizza[k] + "</b>" : ""}
                                <p style = "font-size: 0.6rem; margin-bottom: 0px">${arrayDeItensExtras[j][z].descricao} x${arrayDeItensExtras.qtdsItens[z]}</p>
                            `;

                            position--;
                        }


                    } else if (arrayDeItensExtras[j].length !== 0 && arrayDeItensExtras[j].length === 1 && j === "nomeItem") {
                        acrescimoTotal += (arrayDeItensExtras.qtdsItens[0] * arrayDeItensExtras[j][0].acrescimo);
                        complementoXml += `
                            ${arrayDeItensExtras[j].length === 1 ? "<b style = 'font-size: 0.7rem'>" + objProximoSaborPizza[k] + "</b>" : ""}
                            <p style = "font-size: 0.6rem; margin-bottom: 0px">${arrayDeItensExtras[j][0].descricao} x${arrayDeItensExtras.qtdsItens[0]}</p>
                        `;
                    } else if (arrayDeItensExtras[j].length === 0) {
                        flagPedidoAdd = false;
                    }
                }
            }

            conteudoHTML1 += `
            <div class="linhaDetalheProduto">
                <div class="colQtd colQtdDetalhe">
                    <img class="btnAddMinus" src = "../Imagens/remove.png" onclick = "removerQtd(${i})"/>
                        <span class = "tdPreencherNota" id = "qtd-${i}">${pedido.qtds[i]}</span>
                    <img class="btnAddMinus" src = "../Imagens/plus.png" onclick = "adicionarQtd(${i})"/>
                </div>
                
                <div class = "colDesc colDescDetalhe"><p style = "font-size: 0.8rem">${nomeCompletoPizza}</p> <p style = "font-weight: 800; font-size: 0.6rem; margin-bottom: 0px">${objTamanhos[produto[0].tamanhoEscolhido] === 'NORMAL' ? '' : objTamanhos[produto[0].tamanhoEscolhido]}</p> ${complementoXml}</div>
                <div class = "colVal colValDetalhe" id = "valor${i}">
                    ${Helper.valorEmReal((acrescimoTotal * pedido.qtds[i]) + (pedido.valores[i] * pedido.qtds[i]))}
                </div>
                <div class="colOperacoesDetalhe">
                    <img class="btnAddMinus" src = "../Imagens/lapis.png" onclick = "colocaItemExtra(${i}, ${produto.length})" style = "cursor: pointer; margin-left: 10px; margin-bottom: 5px; width:24px; heigth:24px;"/>
                    <img class="btnAddMinus" src = "../Imagens/delete.png" onclick = "removeProduto(${i})" style = "cursor: pointer; margin-left: 10px; margin-bottom: 5px;"/>
                </div>
            </div>
                
            `;

            pedido.valoresCalculados[i] = (acrescimoTotal * pedido.qtds[i]) + (pedido.valores[i] * pedido.qtds[i]);
            valorSomaDosItens += (pedido.valores[i] * pedido.qtds[i]);
            valorTotalSomado(null, flagEdit);

        } else {
            //Se não é um produto comum
            let itEx = pedido.itex[i];
            pedido.qtds[i] = pedido.qtds[i] ? pedido.qtds[i] : 1;
            pedido.valores[i] = produto.vl_venda1;
            pedido.valoresCalculados[i] ? null : pedido.valoresCalculados[i] = pedido.valores[i];
            valorSomaDosItens += produto.vl_venda1 * pedido.qtds[i];

            let acrescimoTotal = 0;
            let complementoXml = "";

            for (arrayItens in itEx.nomeItem) {
                acrescimoTotal += (itEx.qtdsItens[arrayItens] * itEx.nomeItem[arrayItens].acrescimo);
                complementoXml += `
                    <p style = "font-size: 0.6rem; margin-bottom: 0px">${itEx.nomeItem[arrayItens].descricao} x${itEx.qtdsItens[arrayItens]}</p>
                `;
            }

            conteudoHTML1 += `
            <div class="linhaDetalheProduto">
                <div class="colQtd colQtdDetalhe">
                    <img class="btnAddMinus" src = "../Imagens/remove.png" onclick = "removerQtd(${i})"/>
                        <span class = "tdPreencherNota" id = "qtd-${i}">${pedido.qtds[i]}</span>
                    <img class="btnAddMinus" src = "../Imagens/plus.png" onclick = "adicionarQtd(${i})"/>
                </div>
                
                <div class = "colDesc colDescDetalhe"> <p>${produto.nome_prod}</p> ${complementoXml}</div>
                <div class = "colVal colValDetalhe" id = "valor${i}">
                    ${Helper.valorEmReal((acrescimoTotal * pedido.qtds[i]) + (produto.vl_venda1 * pedido.qtds[i]))}
                </div>
                <div class="colOperacoesDetalhe">
                    <img class="btnAddMinus" src = "../Imagens/lapis.png" onclick = "colocaItemExtra(${i}, '1')" style = "cursor: pointer; margin-left: 10px; margin-bottom: 5px; width:24px; heigth:24px;"/>
                    <img class="btnAddMinus" src = "../Imagens/delete.png" onclick = "removeProduto(${i})" style = "cursor: pointer; margin-left: 10px; margin-bottom: 5px;"/>
                </div>
            </div>
            `;

            pedido.valoresCalculados[i] = (acrescimoTotal * pedido.qtds[i]) + (produto.vl_venda1 * pedido.qtds[i]);

        }

        valorTotal = valorSomaDosItens;

        conteudoSomaItens = Helper.valorEmReal(valorSomaDosItens);

        adicionaHTML.innerHTML = conteudoHTML1;

        adicionaSomaItens.innerHTML = conteudoSomaItens;
        adicionaValorTotal.innerHTML = Helper.valorEmReal(valorTotal);
        valorTotalSomado(null, flagEdit);

    }

    conteudoHTML1 = '';
}

function qtd(operacao, posicaoItemVetor) {
    if (operacao === '-') pedido.qtds[posicaoItemVetor] -= 1; else pedido.qtds[posicaoItemVetor] += 1;

    if (pedido.qtds[posicaoItemVetor] === 0) return removeProduto(posicaoItemVetor);

    let itEx = pedido.itex[posicaoItemVetor];
    let acrescimoTotal = 0;

    if (isArray(itEx)) {
        for (arrayItens in itEx) {
            for (entries in itEx[arrayItens].nomeItem) {
                acrescimoTotal += (itEx[arrayItens].nomeItem[entries].acrescimo * itEx[arrayItens].qtdsItens[entries]);
            }
        }
    } else {
        for (arrayItens in itEx.nomeItem) {
            acrescimoTotal += (itEx.qtdsItens[arrayItens] * itEx.nomeItem[arrayItens].acrescimo);
        }
    }

    pedido.valoresCalculados[posicaoItemVetor] = (acrescimoTotal * pedido.qtds[posicaoItemVetor]) + (pedido.valores[posicaoItemVetor] * pedido.qtds[posicaoItemVetor]);

    let qtdComprada = document.getElementById("qtd-" + posicaoItemVetor);

    let alterarValor = document.getElementById(`valor${posicaoItemVetor}`);

    let qtdAtual = pedido.qtds[posicaoItemVetor];
    let valorNovo = pedido.valoresCalculados[posicaoItemVetor];

    qtdComprada.innerHTML = qtdAtual;
    alterarValor.innerHTML = `${Helper.valorEmReal(valorNovo)}`;
}

async function adicionarQtd(posicaoItemVetor) {
    if(inforsis.cfg_contr_estq && !isArray(pedido.produtos[posicaoItemVetor])){
        const prod = await cCadProdutos.selectQtdItem(pedido.produtos[posicaoItemVetor].id_prod);
        if(prod.contr_estq === true && (pedido.qtds[posicaoItemVetor] + 1) > prod.qde_estq){
            swal({
               icon: 'warning',
               title: 'Quantidade excedente',
               closeOnClickOutside:true,
               text: 'Você já selecionou a quantidade total disponível em estoque',
               button: {text: 'Okay', closeModal: true}
            });
            return; //Se controlar estoque para esse item e a quantidade somada for maior que a dísponível no estoque, nem soma a nova quantidade
        }
    }
    qtd('+', posicaoItemVetor);
    valorTotalSomado(posicaoItemVetor, 'adicionar');
}

function removerQtd(posicaoItemVetor) {
    qtd('-', posicaoItemVetor);
    valorTotalSomado(posicaoItemVetor, 'adicionar');
}

function valorTotalSomado(posicaoItemVetor, flagEdit) {
    let valorTotalLabel = document.getElementById("valorTotal");
    let somaDosItens = document.getElementById("somaDosItens");
    let taxaEntrega = document.getElementById('taxaEntrega');

    let itex = pedido.itex;

    let valorTotal = 0;
    let qtdsProdutos = [];
    let acrescimoTotal = 0;

    posicaoVetorItPed = posicaoItemVetor;

    let valoresCalculado = pedido.valoresCalculados;

    for (valor in valoresCalculado) {
        valorTotal = valorTotal + (valoresCalculado[valor]);
    }

    for (qtdsForVector of pedido.qtds) {
        qtdsProdutos.push(qtdsForVector);
    }

    for (valorAcres of itex) {
        if (isArray(valorAcres)) {
            for (pzCombItex of valorAcres) {
                if (pzCombItex.nomeItem.length > 1) {
                    let arrayItens = pzCombItex.nomeItem;
                    for (posicaoItemVetor in arrayItens) {
                        acrescimoTotal += qtdsProdutos[posicaoItemVetor] * (arrayItens[posicaoItemVetor].acrescimo * pzCombItex.qtdsItens[posicaoItemVetor]);
                        qtdsTotalEx = (qtdsProdutos[posicaoItemVetor] * pzCombItex.qtdsItens[posicaoItemVetor]);
                    }
                } else if (pzCombItex.nomeItem.length === 1) {
                    acrescimoTotal += qtdsProdutos[0] * (pzCombItex.nomeItem[0].acrescimo * pzCombItex.qtdsItens[0]);
                    qtdsTotalEx = (qtdsProdutos[0] * pzCombItex.qtdsItens[0]);
                }
            }
        } else {
            if (valorAcres.nomeItem.length > 1) {
                let arrayItens = valorAcres.nomeItem;
                for (posicaoItemVetor in arrayItens) {
                    acrescimoTotal += qtdsProdutos[posicaoItemVetor] * (arrayItens[posicaoItemVetor].acrescimo * valorAcres.qtdsItens[posicaoItemVetor]);
                    qtdsTotalEx = (qtdsProdutos[posicaoItemVetor] * valorAcres.qtdsItens[posicaoItemVetor]);
                }
            } else if (valorAcres.nomeItem.length === 1) {
                acrescimoTotal += qtdsProdutos[0] * (valorAcres.nomeItem[0].acrescimo * valorAcres.qtdsItens[0]);
                qtdsTotalEx = (qtdsProdutos[0] * valorAcres.qtdsItens[0]);
            }
        }
    }

    if (flagEdit === "Adicionar" && tipoPedido === "Balcao") {
        let valorTotalSom = 0;
        for (valorUnitario in pedido.valores) {
            valorTotalSom = valorTotalSom + (pedido.valores[valorUnitario]);
        }

        valorTotalSom += acrescimoTotal;

        valorTotalLabel.innerHTML = "";
        somaDosItens.innerHTML = "";

        valorTotalLabel.innerHTML = Helper.valorEmReal(valorTotalSom);
        somaDosItens.innerHTML = Helper.valorEmReal(valorTotalSom);
        taxaEntrega.innerHTML = Helper.valorEmReal(0);

    } else if (flagEdit === "Adicionar" || tipoPedido === "Delivery") {

        let valorTotalSom = 0;
        for (valorUnitario in pedido.valores) {
            valorTotalSom = valorTotalSom + (pedido.valores[valorUnitario]);
        }

        valorTotalSom += acrescimoTotal;

        valorTotalLabel.innerHTML = "";
        somaDosItens.innerHTML = "";

        valorTotalLabel.innerHTML = Helper.valorEmReal(valorTotal);
        somaDosItens.innerHTML = Helper.valorEmReal(valorTotal);
        
        const tipoEndereco = window.localStorage.getItem('tipoEndPed');
        let valorTaxaDeEntrega = 0;
        if(tipoEndereco === '1') valorTaxaDeEntrega = (clienteSelecionado.Logradouro === undefined ? 0 : (clienteSelecionado.Logradouro.frete !== 0 ? clienteSelecionado.Logradouro.frete : inforsis.cfg_frete_basico));
        else if(tipoEndereco === '2') {
            const objNovoEndereco = JSON.parse(window.localStorage.getItem('enderecoAlterado'));
            valorTaxaDeEntrega = (objNovoEndereco === undefined ? 0 : (objNovoEndereco.frete !== 0 ? objNovoEndereco.frete : inforsis.cfg_frete_basico));
        }

        taxaEntrega.innerHTML = Helper.valorEmReal(valorTaxaDeEntrega);

    } else if (flagEdit === "Adicionar" && tipoPedido === "Mesa") {

        let valorTotalSom = 0;
        for (valorUnitario in pedido.valores) {
            valorTotalSom = valorTotalSom + (pedido.valores[valorUnitario]);
        }

        valorTotalSom += acrescimoTotal;

        valorTotalLabel.innerHTML = "";
        somaDosItens.innerHTML = "";

        valorTotalLabel.innerHTML = Helper.valorEmReal(valorTotalSom);
        somaDosItens.innerHTML = Helper.valorEmReal(valorTotalSom);
        taxaEntrega.innerHTML = Helper.valorEmReal(0);

    } else if (flagEdit === "Adicionar" && tipoPedido === "Comanda") {

        let valorTotalSom = 0;
        for (valorUnitario in pedido.valores) {
            valorTotalSom = valorTotalSom + (pedido.valores[valorUnitario]);
        }

        valorTotalSom += acrescimoTotal;

        valorTotalLabel.innerHTML = "";
        somaDosItens.innerHTML = "";

        valorTotalLabel.innerHTML = Helper.valorEmReal(valorTotalSom);
        somaDosItens.innerHTML = Helper.valorEmReal(valorTotalSom);
        taxaEntrega.innerHTML = Helper.valorEmReal(0);

    } else {
        somaDosItens.innerHTML = Helper.valorEmReal(valorTotal);

        if(window.localStorage.getItem('cobrarTaxaEntregaPedido') === 'true'){

            if(!tipoPedido || tipoPedido === 'ENTREGA'){
                if (informacoesPedidoEdicao !== null && clienteSelecionado.Logradouro === undefined){
                    clienteSelecionado = {};
                    clienteSelecionado.Logradouro = {};
                    clienteSelecionado.Logradouro.frete = informacoesPedidoEdicao.frete;
                }
    
                const tipoEndereco = window.localStorage.getItem('tipoEndPed');
                let valorTaxaDeEntrega = 0;
                if(tipoEndereco === '1') valorTaxaDeEntrega = (clienteSelecionado.Logradouro === undefined ? 0 : (clienteSelecionado.Logradouro.frete !== 0 ? clienteSelecionado.Logradouro.frete : inforsis.cfg_frete_basico));
                else if(tipoEndereco === '2') {
                    const objNovoEndereco = JSON.parse(window.localStorage.getItem('enderecoAlterado'));
                    valorTaxaDeEntrega = (objNovoEndereco === undefined ? 0 : (objNovoEndereco.frete !== 0 ? objNovoEndereco.frete : inforsis.cfg_frete_basico));
                }
                valorTotal = valorTotal + valorTaxaDeEntrega;
                clienteSelecionado.Logradouro ? clienteSelecionado.Logradouro.frete = valorTaxaDeEntrega : null;
                taxaEntrega.innerHTML = Helper.valorEmReal(valorTaxaDeEntrega);
            }

        }

        valorTotalLabel.innerHTML = Helper.valorEmReal((valorTotal));
    }

    acrescimoTotal = 0;
    posicaoVetorItPed = 0;
}

function removeProduto(posicaoItemVetor) {

    pedido.produtos.splice(posicaoItemVetor, 1);
    pedido.valores.splice(posicaoItemVetor, 1);
    pedido.itex.splice(posicaoItemVetor, 1);
    pedido.valoresCalculados.splice(posicaoItemVetor, 1);
    pedido.qtds.splice(posicaoItemVetor, 1);

    conteudoHTML1 = '';

    preencherNotaTela();
    valorTotalSomado();
}

function mostraCarregamento(){
    document.getElementById('divCarregamento').style.display = 'flex';
}

function escondeCarregamento(){
    document.getElementById('divCarregamento').style.display = 'none';
}

async function finalizaPedido(tipoPed) {
    //mostraCarregamento();
    if(pedido.produtos.length <= 0) {
        swal({
            icon: "warning",
            text: "Nenhum produto escolhido! Por favor escolha um produto para finalizar o pedido.",
            button: {
                text: 'Entendido!'
            }
        });
        //escondeCarregamento();
        return;
    }
    tipoPedido !== undefined ? tipoPed = tipoPedido : tipoPed;
    pedidosTotais = [];
    itensDosPedidos = [];
    let acrescimoTotal = 0;
    let itexForAcrs = pedido.itex;

    let dataFormatada;
    let horaMinuto = "";

    var data = new Date();
    let hora = data.getHours();
    let minutes = data.getMinutes();

    dataFormatada = Helper.dataAtualEua(true);
    horaMinuto = hora + ":" + minutes;

    //document.getElementById('btnAceitar').disabled = true;

    let nrPedido = 1 + await gerandoNrPedido();

    if (tipoOperacaoSA !== "Editar") {

        //rodar um for verificando se todas as quantidades estão dispiníveis
        if(inforsis.cfg_contr_estq){
            let prodsIndisponiveis = [];
            for (let q = 0; q < pedido.produtos.length; q++) {
                const prodAtual = pedido.produtos[q];
                if(!isArray(prodAtual) && prodAtual.contr_estq === true){
                    const prodSelecionado = await cCadProdutos.select(prodAtual.id_prod);
                    if(pedido.qtds[q] > prodSelecionado.qde_estq) prodsIndisponiveis.push(prodSelecionado);
                }
            }
    
            if(prodsIndisponiveis.length > 0){
                let nomesMensagem = '';
                for(item of prodsIndisponiveis) nomesMensagem += item.nome_prod + ', ';
    
                swal({
                   icon: 'warning',
                   title: 'Estoque insuficiente',
                   closeOnClickOutside:false,
                   text: prodsIndisponiveis.length === 1 ? 'O item ' + nomesMensagem + 'está indisponível na quantidade selecionada!' : 'Os itens ' + nomesMensagem + 'estão indisponíveis nas quantidades selecionadas!',
                   button: {text: 'Voltar', closeModal: true}
                });
                //escondeCarregamento();
                return;
            }
        }

        let valorTotal = 0;

        inforsis = JSON.parse(window.localStorage.getItem('dadosEstabelecimento'));
        usuario = JSON.parse(window.localStorage.getItem('usuario'));
        enderecoAlterado = JSON.parse(window.localStorage.getItem('enderecoAlterado'));
        //clienteSelecionado = JSON.parse(window.localStorage.getItem('clienteSelecionado'));

        tipoEndPed = window.localStorage.getItem('tipoEndPed');
        pedidoMesa = window.sessionStorage.getItem('pedidoMesa'); // VERIFICAR ISSO DEPOIS PRA VER SE NÃO VAI CAUSAR REPETIÇÕES E TALS

        for (let i = 0; i < pedido.valoresCalculados.length; i++) {
            valorTotal += pedido.valoresCalculados[i];
        }

        for (valorAcres in itexForAcrs) {
            if (isArray(itexForAcrs[valorAcres])) {
                for (posicaoItemVetor in itexForAcrs[valorAcres]) {
                    let arrayItens = itexForAcrs[valorAcres][posicaoItemVetor];
                    for (sabores in arrayItens) {
                        for (teste in arrayItens[sabores]) {
                            if (sabores === "nomeItem") {
                                acrescimoTotal += (arrayItens[sabores][teste].acrescimo * arrayItens.qtdsItens[teste]);
                            }
                        }
                    }
                }
            } else {
                let arrayItens = itexForAcrs[valorAcres].nomeItem;
                for (posicaoItemVetor in arrayItens) {
                    acrescimoTotal += (arrayItens[posicaoItemVetor].acrescimo * itexForAcrs[valorAcres].qtdsItens[posicaoItemVetor]);
                }
            }
        }

        //verificar  aqui o tipo do pedido
        //se for entrega 

        cCadPedido.nr_pedido = nrPedido;
        cCadPedido.qde_total_itens = pedido.produtos.length;
        cCadPedido.desconto = 0.00
        cCadPedido.acrescimo = acrescimoTotal
        //cCadPedido.tx_serv = inforsis.cfg_taxa_servico
        cCadPedido.nr_nfiscal = 123456
        cCadPedido.cpf_cnpj = inforsis.cfg_cnpj
        cCadPedido.dt_pedido = dataFormatada;
        cCadPedido.hr_pedido = horaMinuto;
        cCadPedido.supervisor = usuario.id_oper
        cCadPedido.obs = ''
        cCadPedido.id_infor_trm = 1
        cCadPedido.id_oper = 1
        //Acima é padrão para todos os pedidos

        if(tipoPed === 'Delivery'){
            cCadPedido.cliente = clienteSelecionado.nome_cli;//
            
            cCadPedido.telefone = clienteSelecionado.fone1;
            cCadPedido.tipo_entrega = "ENTREGA";
            cCadPedido.numero_mesa = "";
            cCadPedido.comp_endereco = clienteSelecionado.compl_res;
            cCadPedido.status_pedido = "PENDENTE";

            if (tipoEndPed === "1") {
                const valorTaxaDeEntrega = (clienteSelecionado.Logradouro === undefined ? 0 : (clienteSelecionado.Logradouro.frete !== 0 ? clienteSelecionado.Logradouro.frete : inforsis.cfg_frete_basico));
                cCadPedido.frete = valorTaxaDeEntrega;
                cCadPedido.id_logradouro = clienteSelecionado.Logradouro.id_logradouro;
                cCadPedido.num_endereco = clienteSelecionado.num_res;
            } else if (tipoEndPed === "2") {
                const valorTaxaDeEntrega = (enderecoAlterado === undefined ? 0 : (enderecoAlterado.frete !== 0 ? enderecoAlterado.frete : inforsis.cfg_frete_basico));
                cCadPedido.frete = valorTaxaDeEntrega;
                cCadPedido.id_logradouro = enderecoAlterado.id_logradouro;
                cCadPedido.num_endereco = enderecoAlterado.num_res;
            }

        }else if(tipoPed === 'Mesa'){

            cCadPedido.cliente = document.getElementById('nomeCli').innerHTML === "" ? "MESA " + document.getElementById('mesaCli').innerHTML : 'MESA ' + document.getElementById('mesaCli').innerHTML + ' ' + document.getElementById('nomeCli').innerHTML;//dif
            cCadPedido.frete = 0.00//
            //cCadPedido.tx_serv = 0.00//dif
            cCadPedido.tx_serv = inforsis.cfg_taxa_servico//dif
            cCadPedido.id_logradouro = inforsis.id_logradouro;//dif
            cCadPedido.num_endereco = 0;
            cCadPedido.tipo_entrega = "MESA"//dif
            cCadPedido.numero_mesa = document.getElementById('mesaCli').innerHTML;//dif
            cCadPedido.comp_endereco = "";//dif
            cCadPedido.status_pedido = "OCUPADO";//dif

        } else if(tipoPed === 'Balcao'){
            cCadPedido.cliente = "BALCÃO " + document.getElementById('domBalcao').value.toUpperCase();
            cCadPedido.frete = 0.00;
            cCadPedido.tx_serv = inforsis.cfg_taxa_servico;
            cCadPedido.telefone = document.getElementById('domTelefone').value === "" ? "" : document.getElementById('domTelefone').value;
            cCadPedido.id_logradouro = inforsis.id_logradouro;
            cCadPedido.num_endereco = ""
            cCadPedido.tipo_entrega = "BALCÃO";
            cCadPedido.numero_mesa = "";
            cCadPedido.comp_endereco = "",
            cCadPedido.status_pedido = "PENDENTE";
        } else if(tipoPed === 'Comanda'){
            cCadPedido.cliente = "COMANDA " + document.getElementById('mesaCli').innerText + ' ' + document.getElementById('nomeCli').innerText.toUpperCase();
            cCadPedido.frete = 0.00;
            cCadPedido.tx_serv = inforsis.cfg_taxa_servico;
            cCadPedido.telefone = "";
            cCadPedido.id_logradouro = inforsis.id_logradouro;
            cCadPedido.num_endereco = "";
            cCadPedido.tipo_entrega = "COMANDA";
            cCadPedido.numero_mesa = document.getElementById('mesaCli').innerText;
            cCadPedido.comp_endereco = "",
            cCadPedido.status_pedido = "PENDENTE";
        }

        cCadPedido.vl_total = valorTotal + cCadPedido.frete;
        //cCadPedido.troco = valorTroco; adicionar esse campo no banco de dados

        if(pedidoMesa === null || pedidoMesa === ''){
            await cCadPedido.insert().then(async data => {
                inserirItensPedido(data, tipoPed);
            }).catch(e => {
                console.log(e);
            });
        }else{
            await updateValorTotalPedido(JSON.parse(pedidoMesa).id_pedido, JSON.parse(window.sessionStorage.getItem('pedidoMesa')), true);
            await inserirItensPedido(pedidoMesa, tipoPed);
        }
    } else {
        EditarPedido(JSON.parse(window.sessionStorage.getItem('pedidoMesa')));
        window.sessionStorage.removeItem('pedidoMesa');
    }

    window.localStorage.setItem('pedidoMesa', "");
    arrayDosItensPedidos = [];
    itensDosPedidosMesa = [];

    window.sessionStorage.removeItem('itensDosPedidos');
    window.sessionStorage.removeItem('itemDoPedidoMesa');
    window.sessionStorage.removeItem('pedidosTotais');

    zerarVariaveis();

    arrayDosItensPedidos = [];
    itensDosPedidosMesa = [];
    escondeCarregamento();
}

async function inserirItensPedido(data, tipoPed){
    if(typeof data === 'string') data = JSON.parse(data);
    let conjuntoProdutosInseridos = [];

    //aqui
    for (let k = 0; k < pedido.produtos.length; k++) {

        let produto = pedido.produtos[k];
        //let itex = pedido.itex[k];
        if (isArray(produto)) {

            let nomeCompletoPizza = '';
            let maiorValor = 0;
            let codigoPizzas = '';
            let tamanhoEscolhido = '';
            let itex = pedido.itex[k];
            let complementoCompleto = "";
            let acrescimo = 0.00;
            let positionObj = 1;


            for (pizza of produto) {
                codigoPizzas += pizza.cod_prod + '/';
                nomeCompletoPizza += produto.length > 1 ? '1/' + produto.length + ' ' + pizza.nome_prod + '<br>' : pizza.nome_prod + '<br>';
                pizza[pizza.tamanhoEscolhido] > maiorValor ? maiorValor = pizza[pizza.tamanhoEscolhido] : null;
                tamanhoEscolhido = pizza.tamanhoEscolhido;
            }

            for (complementoItEx in itex) {
                for (positionItEx in itex[complementoItEx]) {
                    if (positionItEx !== "qtdsItens" && itex[complementoItEx][positionItEx].length > 1) {

                        let arrayItensExtras = itex[complementoItEx][positionItEx];

                        for (innerArray in arrayItensExtras) {
                            complementoCompleto += positionObj + "-" + arrayItensExtras[innerArray].descricao;
                            complementoCompleto += "x" + itex[complementoItEx].qtdsItens[innerArray] + "/";
                            acrescimo = acrescimo + (itex[complementoItEx].qtdsItens[innerArray] * arrayItensExtras[innerArray].acrescimo);
                        }

                        positionObj++;

                    } else if (positionItEx !== "qtdsItens" && itex[complementoItEx][positionItEx].length === 1) {

                        let arrayItensExtras = itex[complementoItEx][positionItEx];

                        for (innerArray in arrayItensExtras) {
                            complementoCompleto += positionObj + "-" + arrayItensExtras[innerArray].descricao;
                            complementoCompleto += "x" + itex[complementoItEx].qtdsItens[innerArray] + "/";
                            acrescimo = acrescimo + (itex[complementoItEx].qtdsItens[innerArray] * arrayItensExtras[innerArray].acrescimo);
                        }

                        positionObj++;
                    } else if (positionItEx !== "qtdsItens" && itex[complementoItEx][positionItEx].length === 0) {
                        positionObj++;
                    }
                }
            }

            cItemPedido.id_prod = codigoPizzas;
            cItemPedido.cod_pedido = data.id_pedido;
            cItemPedido.desc_produto = nomeCompletoPizza;
            cItemPedido.qtd_prod = pedido.qtds[k];
            cItemPedido.tam_escolhido = tamanhoEscolhido;
            cItemPedido.vl_unit = pedido.valores[k];
            cItemPedido.desconto = 0.00;
            cItemPedido.acrescimo = acrescimo;
            cItemPedido.complemento = complementoCompleto;

        } else {

            let itex = pedido.itex[k];
            let complementoCompleto = "";
            let acrescimo = 0.00;

            for (var complementoItEx in itex) {
                for (var positionItEx in itex[complementoItEx]) {
                    if (complementoItEx !== "qtdsItens") {
                        complementoCompleto += itex[complementoItEx][positionItEx].descricao;
                        complementoCompleto += "x" + itex.qtdsItens[positionItEx] + "/";
                        acrescimo = acrescimo + (itex.qtdsItens[positionItEx] * itex[complementoItEx][positionItEx].acrescimo);
                    }
                }
            }


            cItemPedido.id_prod = pedido.produtos[k].cod_prod;
            cItemPedido.cod_pedido = data.id_pedido;
            cItemPedido.desc_produto = pedido.produtos[k].nome_prod;
            cItemPedido.qtd_prod = pedido.qtds[k];
            cItemPedido.tam_escolhido = pedido.produtos[k].tamanhoEscolhido;
            cItemPedido.vl_unit = pedido.valores[k];
            cItemPedido.desconto = 0.00;
            cItemPedido.acrescimo = acrescimo;
            cItemPedido.complemento = complementoCompleto;

            complementoCompleto = "";
            acrescimo = 0.00;

            //Por enquanto controle de estoque só pra quem não é pizza
            if(inforsis.cfg_contr_estq){
                const qtdAtualProdCad = await cCadProdutos.select(cItemPedido.id_prod);
                const qtdAtualizada = qtdAtualProdCad.qde_estq - cItemPedido.qtd_prod;
                await cCadProdutos.updateQtdEstoque(cItemPedido.id_prod, qtdAtualizada);
            }
        }

        //if controle estoque ativo para o item, remover a quantidade
        let itemPed = await cItemPedido.insert();
        itemPed.DadosProduto = itemPed.id_prod.toString().indexOf('/') !== -1 ? { Grupo: { descricao: 'PIZZAS' } } : await cCadProdutos.recuperaGrupoPedido(itemPed.id_prod);
        conjuntoProdutosInseridos.push({ ...itemPed.dataValues, ...itemPed.DadosProduto });
        produtos = [];

    }

    data.troco = valorTroco;

    const opcaoImpressao = 'IP'; // PODE SER NI (NAO IMPRIMIR) | IP (IMPIRMIR COM PERGUNTA) | IA (IMPRIMIR AUTOMÁTICO)
    if(opcaoImpressao === 'IP') imprimirOuNao(data, tipoPed, conjuntoProdutosInseridos);
    else if(opcaoImpressao === 'IA') criaConteudo(pedidoMesa, tipoPed, conjuntoProdutosInseridos);
    //else continua;

    pedido = {
        produtos: [],
        itex: [],
        valores: [],
        valoresCalculados: [],
        qtds: [],
    };

    produtos = [];

    document.getElementById('container-mesa') ? document.getElementById('container-mesa').innerHTML = "" : null;
    selecionaPedidos();//verificar tbm a necessidade disso

    //isso abaixo tem que ter
    if (window.localStorage.getItem('tipoMenu') === "1") {

        document.getElementById('menu').style.display = 'block'
        voltarSubMenu(1)
        window.sessionStorage.removeItem('pedidoMesa');

    } else if (window.localStorage.getItem('tipoMenu') === "0") {
        window.location.href = "../html/menuTradicional.html"
        document.getElementById('menuTrad').style.display = "block";
        window.sessionStorage.removeItem('pedidoMesa');
    }

}

async function EditarPedido(dadosPedido) {
    let pedidoEditado = {}
    let excluido = 0;
    if (pedidoAntesDaEdicao !== JSON.stringify(pedido)) {
        pedidoEditado = JSON.parse(pedidoAntesDaEdicao);

        if (pedidoEditado.length !== 0 && pedidoEditado.length !== undefined && pedidoEditado.length !== null) {
            for (pedidoEdit of pedidoEditado.produtos) {
                if (isArray(pedidoEdit)) {
                    for (pizzas of pedidoEdit) {
                        if (excluido === 0) {
                            codPedido = pizzas.cod_pedido;
                            await cItemPedido.deleteByCod(pizzas.cod_pedido).then(retorno => {
                                if ((retorno !== 0 && retorno !== undefined) || retorno > 0) {
                                    console.log('EXCLUIDO');
                                }
                            });
                            excluido++;
                        } else {
                            excluido = 0;
                        }
                    }

                    excluido = 0;
                } else {
                    codPedido = pedidoEdit.cod_pedido;
                    await cItemPedido.deleteByCod(pedidoEdit.cod_pedido).then(retorno => {
                        if ((retorno !== 0 && retorno !== undefined) || retorno > 0) {
                            console.log('EXCLUIDO');
                        }
                    });
                }
            }
        } else {
            codPedido = pedidoMesa.id_pedido;
            await cItemPedido.deleteByCod(pedidoMesa.id_pedido).then(retorno => {
                if ((retorno !== 0 && retorno !== undefined) || retorno > 0) {
                    console.log('EXCLUIDO');
                }
            });
        }
        //#region Atualizando informações na controller do pedido
        //cCadPedido.nr_pedido = codPedido;
        cCadPedido.nr_pedido = 23;
        cCadPedido.qde_total_itens = pedido.produtos.length;
        cCadPedido.desconto = 0.00
        //cCadPedido.acrescimo = acrescimoTotal
        cCadPedido.acrescimo = 0;
        cCadPedido.tx_serv = inforsis.cfg_taxa_servico;
        cCadPedido.nr_nfiscal = 123456;
        cCadPedido.cpf_cnpj = inforsis.cfg_cnpj;
        cCadPedido.supervisor = usuario.id_oper;
        cCadPedido.obs = '';
        cCadPedido.id_infor_trm = 1;
        cCadPedido.id_oper = 1;
        //Acima é padrão para todos os pedidos
        //#endregion

        updateValorTotalPedido(codPedido, dadosPedido);

    } else {
        pedidoEditado = JSON.parse(pedidoAntesDaEdicao);

        if (pedidoEditado.length !== 0 && pedidoEditado.length !== undefined && pedidoEditado.length !== null) {
            for (pedidoEdit of pedidoEditado.produtos) {
                if (isArray(pedidoEdit)) {
                    for (pizzas of pedidoEdit) {
                        if (excluido === 0) {
                            codPedido = pizzas.cod_pedido;
                            await cItemPedido.deleteByCod(pizzas.cod_pedido).then(retorno => {
                                if ((retorno !== 0 && retorno !== undefined) || retorno > 0) {
                                    console.log('EXCLUIDO');
                                }
                            });
                            excluido++;
                        } else {
                            excluido = 0;
                        }
                    }

                    excluido = 0;
                } else {
                    codPedido = pedidoEdit.cod_pedido;
                    await cItemPedido.deleteByCod(pedidoEdit.cod_pedido).then(retorno => {
                        if ((retorno !== 0 && retorno !== undefined) || retorno > 0) {
                            console.log('EXCLUIDO');
                        }
                    });
                }
            }
        } else {
            codPedido = pedidoMesa.id_pedido;
            await cItemPedido.deleteByCod(pedidoMesa.id_pedido).then(retorno => {
                if ((retorno !== 0 && retorno !== undefined) || retorno > 0) {
                    console.log('EXCLUIDO');
                }
            });
        }




        updateValorTotalPedido(codPedido, dadosPedido);
    }

    arrayDosItensPedidos = [];
    itensDosPedidosMesa = [];

}

async function updateValorTotalPedido(codigoPedido, dadosPedido, ehAdicaoDeProduto = false) {
    let valoresCalculados = pedido.valoresCalculados;
    let novoValorTotal = 0;

    let acrescimoTotal = 0;

    let itexAcresUpdate = pedido.itex;


    for (vlCalc of valoresCalculados) {
        novoValorTotal += vlCalc;
    }

    novoValorTotal += dadosPedido.frete;

    ehAdicaoDeProduto ? novoValorTotal += parseFloat(window.sessionStorage.getItem('valorTotalPedidoPAdicao')) : null;

    /*for (valorAcres of itexAcresUpdate) {
        if (valorAcres.nomeItem.length > 1) {
            let arrayItens = valorAcres.nomeItem;
            for (posicaoItemVetor in arrayItens) {
                acrescimoTotal += (arrayItens[posicaoItemVetor].acrescimo * valorAcres.qtdsItens[posicaoItemVetor]);
            }
        } else if (valorAcres.nomeItem.length === 1) {
            acrescimoTotal += (valorAcres.nomeItem[0].acrescimo * valorAcres.qtdsItens[0]);
        }

    }*/

    for (valorAcres in itexAcresUpdate) {
        if (isArray(itexAcresUpdate[valorAcres])) {
            for (posicaoItemVetor in itexAcresUpdate[valorAcres]) {
                let arrayItens = itexAcresUpdate[valorAcres][posicaoItemVetor];
                for (sabores in arrayItens) {
                    for (teste in arrayItens[sabores]) {
                        if (sabores === "nomeItem") {
                            acrescimoTotal += (arrayItens[sabores][teste].acrescimo * arrayItens.qtdsItens[teste]);
                        }
                    }
                }
            }
        } else {
            let arrayItens = itexAcresUpdate[valorAcres].nomeItem;
            for (posicaoItemVetor in arrayItens) {
                acrescimoTotal += (arrayItens[posicaoItemVetor].acrescimo * itexAcresUpdate[valorAcres].qtdsItens[posicaoItemVetor]);
            }
        }
    }

    //novoValorTotal += acrescimoTotal;
    cCadPedido.vl_total = novoValorTotal;
    cCadPedido.id_pedido = codigoPedido;

    //As informações do pedido na controller não estão sendo atualizadas (está com os dados do pedido feito anteriormente), ou preencher todas elas aqui novamente, ou remover os campos da query de update
    cCadPedido.update().then(retorno => {
        if(!ehAdicaoDeProduto) inserirItemAposExclusao(codPedido, dadosPedido);
        if (retorno > 0) {
            console.log("ATUALIZADO VALOR TOTAL");
        } else {
            console.log("NÃO ATUALIZADO VALOR TOTAL");
        }
    });
}

async function inserirItemAposExclusao(codigoPedido, dadosPedido) {
    dadosPedido = await cCadPedido.select(codigoPedido);
    //let codPedido = 0;
    swal('Deseja editar este pedido? ', {
        icon: "warning",
        buttons: {
            cancelar: true,
            confirmar: true
        }
    }).then(async (acao) => {
        if (acao === "confirmar") {

            let conjuntoProdutosInseridos = [];

            if (pedido.produtos.length !== 0) {
                for (let k = 0; k < pedido.produtos.length; k++) {
                    let produto = pedido.produtos[k];
                    if (isArray(produto)) {

                        let nomeCompletoPizza = '';
                        let maiorValor = 0;
                        let codigoPizzas = '';
                        let tamanhoEscolhido = '';
                        let complemento = "";
                        let itex = pedido.itex[k];
                        let complementoCompleto = "";
                        let acrescimo = 0.00;
                        let positionObj = 1;


                        for (pizza of produto) {
                            codigoPizzas += pizza.cod_prod + '/';
                            nomeCompletoPizza += produto.length > 1 ? '1/' + produto.length + ' ' + pizza.nome_prod + '<br>' : pizza.nome_prod + '<br>';
                            pizza[pizza.tamanhoEscolhido] > maiorValor ? maiorValor = pizza[pizza.tamanhoEscolhido] : null;
                            tamanhoEscolhido = pizza.tamanhoEscolhido;
                        }

                        for (complementoItEx in itex) {
                            for (positionItEx in itex[complementoItEx]) {
                                if (positionItEx !== "qtdsItens" && itex[complementoItEx][positionItEx].length > 1) {

                                    let arrayItensExtras = itex[complementoItEx][positionItEx];

                                    for (innerArray in arrayItensExtras) {
                                        complementoCompleto += positionObj + "-" + arrayItensExtras[innerArray].descricao;
                                        complementoCompleto += "x" + itex[complementoItEx].qtdsItens[innerArray] + "/";
                                        acrescimo = acrescimo + (itex[complementoItEx].qtdsItens[innerArray] * arrayItensExtras[innerArray].acrescimo);
                                    }

                                    positionObj++;

                                } else if (positionItEx !== "qtdsItens" && itex[complementoItEx][positionItEx].length === 1) {

                                    let arrayItensExtras = itex[complementoItEx][positionItEx];

                                    for (innerArray in arrayItensExtras) {
                                        complementoCompleto += positionObj + "-" + arrayItensExtras[innerArray].descricao;
                                        complementoCompleto += "x" + itex[complementoItEx].qtdsItens[innerArray] + "/";
                                        acrescimo = acrescimo + (itex[complementoItEx].qtdsItens[innerArray] * arrayItensExtras[innerArray].acrescimo);
                                    }

                                    positionObj++;
                                } else if (positionItEx !== "qtdsItens" && itex[complementoItEx][positionItEx].length === 0) {
                                    positionObj++;
                                }
                            }
                        }

                        cItemPedido.id_prod = codigoPizzas;
                        cItemPedido.cod_pedido = codigoPedido;
                        cItemPedido.desc_produto = nomeCompletoPizza;
                        cItemPedido.qtd_prod = pedido.qtds[k];
                        cItemPedido.tam_escolhido = tamanhoEscolhido;
                        cItemPedido.vl_unit = pedido.valores[k];
                        cItemPedido.desconto = 0.00;
                        cItemPedido.acrescimo = acrescimo;
                        cItemPedido.complemento = complementoCompleto;

                    } else {
                        let itex = pedido.itex[k];
                        let complementoCompleto = "";
                        let acrescimo = 0.00;

                        for (var complementoItEx in itex) {
                            for (var positionItEx in itex[complementoItEx]) {
                                if (complementoItEx !== "qtdsItens") {
                                    complementoCompleto += itex[complementoItEx][positionItEx].descricao;
                                    complementoCompleto += "x" + itex.qtdsItens[positionItEx] + "/";
                                    acrescimo = acrescimo + (itex.qtdsItens[positionItEx] * itex[complementoItEx][positionItEx].acrescimo);
                                }
                            }
                        }



                        cItemPedido.id_prod = pedido.produtos[k].cod_prod;
                        cItemPedido.cod_pedido = codigoPedido;
                        cItemPedido.desc_produto = pedido.produtos[k].nome_prod;
                        cItemPedido.qtd_prod = pedido.qtds[k];
                        cItemPedido.tam_escolhido = pedido.produtos[k].tamanhoEscolhido;
                        cItemPedido.vl_unit = pedido.valores[k];
                        cItemPedido.desconto = 0.00;
                        cItemPedido.acrescimo = acrescimo;
                        cItemPedido.complemento = complementoCompleto;

                    }

                    /*let itemPed = await cItemPedido.insert();
                    itemPed.DadosProduto = itemPed.id_prod.toString().indexOf('/') !== -1 ? { Grupo: { descricao: 'PIZZAS' } } : await cCadProdutos.recuperaGrupoPedido(itemPed.id_prod);
                    conjuntoProdutosInseridos.push({ ...itemPed.dataValues, ...itemPed.DadosProduto });
                    produtos = [];

                    criaConteudo(data, 'MESA', conjuntoProdutosInseridos);*/

                    complementoCompleto = "";
                    acrescimo = 0.00;

                    //MUDAR AQUI

                    let itemPed = await cItemPedido.insert();
                    itemPed.DadosProduto = itemPed.id_prod.toString().indexOf('/') !== -1 ? { Grupo: { descricao: 'PIZZAS' } } : await cCadProdutos.recuperaGrupoPedido(itemPed.id_prod);
                    conjuntoProdutosInseridos.push({ ...itemPed.dataValues, ...itemPed.DadosProduto });
                    produtos = [];

                }

                setTimeout(() => {
                    Swel.fire({
                        icon: 'success',
                        text: 'Pedido editado com sucesso!'
                    }).then((result) => {
                        if (window.localStorage.getItem('tipoMenu') === "1") {
                            if (TelaAnterior === "Reserva") {
                                zerarVariaveis();

                                document.getElementById('container-mesa').innerHTML = "";
                                document.getElementById('container-mesa').style.display = 'none';
                                document.getElementById('buttons-container').innerHTML = "";
                                document.getElementById('adiciona-pedido').innerHTML = "";
                                document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                document.getElementById('domCliente').value = "";
                                document.getElementById('domMesa').value = "";
                                // document.getElementById('menuTrad').style.display = "block";


                                selecionarTudo();
                            } else if (TelaAnterior === "Ultimo") {
                                carregaFrame('frameSubMenuUltimo');
                                window.sessionStorage.removeItem('pedidoTotais');
                                window.sessionStorage.removeItem('itensDosPedidos');
                                selecionaPedidos();
                            } else if (TelaAnterior === "Comanda") {
                                //carregaFrame('frameSubMenuMovimento', "SubMenu");
                                window.sessionStorage.removeItem('pedidoTotais');
                                window.sessionStorage.removeItem('itensDosPedidos');
                                document.getElementById('container-mesa').innerHTML = "";
                                document.getElementById('container-mesa').style.display = "";
                                document.getElementById('adicionarItensPedido').innerHTML = "";

                                setTimeout(() => {
                                    abrirModais('comanda');
                                }, 10);

                                selecionaPedidos();
                                setTimeout(() => {
                                    buscaPedidoComanda();
                                }, 200);
                            } else if (TelaAnterior === "Fechamento") {
                                if (tipoPedido === "Mesa") {
                                    document.getElementById('container-mesa').innerHTML = "";
                                    // document.getElementById('menuTrad').style.display = "block";
                                    selecionaPedidos();
                                    setTimeout(() => {
                                        irParaCardapio('VoltandoFechamento', 'Fechamento', undefined, tipoPedido)
                                    }, 1000);
                                } else if (tipoPedido === "Comanda") {
                                    document.getElementById('container-mesa').innerHTML = "";
                                    // document.getElementById('menuTrad').style.display = "block";
                                    selecionaPedidos();
                                    setTimeout(() => {
                                        irParaCardapio('VoltandoFechamento', 'Fechamento', undefined, tipoPedido)
                                    }, 1000);
                                }
                            }
                        } else if (window.localStorage.getItem('tipoMenu') === "0") {
                            if (TelaAnterior === "Reserva") {
                                zerarVariaveis();

                                document.getElementById('container-mesa').innerHTML = "";
                                document.getElementById('container-mesa').style.display = 'none';
                                document.getElementById('buttons-container').innerHTML = "";
                                document.getElementById('adiciona-pedido').innerHTML = "";
                                document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                                document.getElementById('valorTotalMesaBtns').innerHTML = "";
                                document.getElementById('domCliente').value = "";
                                document.getElementById('domMesa').value = "";
                                document.getElementById('menuTrad').style.display = "flex";

                                selecionarTudo();
                            } else if (TelaAnterior === "Ultimo") {
                                carregaFrame('frameSubMenuUltimo');
                                window.sessionStorage.removeItem('pedidoTotais');
                                window.sessionStorage.removeItem('itensDosPedidos');
                                document.getElementById('menuTrad').style.display = "flex";
                                selecionaPedidos();
                            } else if (TelaAnterior === "Comanda") {
                                //carregaFrame('frameSubMenuMovimento', "SubMenu");
                                window.sessionStorage.removeItem('pedidoTotais');
                                window.sessionStorage.removeItem('itensDosPedidos');
                                document.getElementById('container-mesa').innerHTML = "";
                                document.getElementById('container-mesa').style.display = "";
                                document.getElementById('adicionarItensPedido').innerHTML = "";
                                document.getElementById('menuTrad').style.display = "flex";

                                setTimeout(() => {
                                    abrirModais('comanda');
                                }, 10);

                                selecionaPedidos();
                                setTimeout(() => {
                                    buscaPedidoComanda();
                                }, 200);
                            } else if (TelaAnterior === "Fechamento") {
                                if (tipoPedido === "Mesa") {
                                    document.getElementById('container-mesa').innerHTML = "";
                                    document.getElementById('menuTrad').style.display = "flex";
                                    selecionaPedidos();
                                    setTimeout(() => {
                                        irParaCardapio('VoltandoFechamento', 'Fechamento', undefined, tipoPedido)
                                    }, 1000);
                                } else if (tipoPedido === "Comanda") {
                                    document.getElementById('container-mesa').innerHTML = "";
                                    document.getElementById('menuTrad').style.display = "flex";
                                    selecionaPedidos();
                                    setTimeout(() => {
                                        irParaCardapio('VoltandoFechamento', 'Fechamento', undefined, tipoPedido)
                                    }, 1000);
                                }
                            }
                        }
                    })

                }, 100);

                const opcaoImpressao = 'IP'; // PODE SER NI (NAO IMPRIMIR) | IP (IMPIRMIR COM PERGUNTA) | IA (IMPRIMIR AUTOMÁTICO)
                if(opcaoImpressao === 'IP') imprimirOuNao(dadosPedido, dadosPedido.tipo_entrega, conjuntoProdutosInseridos);
                else if(opcaoImpressao === 'IA') criaConteudo(dadosPedido, dadosPedido.tipo_entrega, conjuntoProdutosInseridos);
                //imprimirOuNao(dadosPedido, 'MESA', conjuntoProdutosInseridos, false, true);

                pedido = {
                    produtos: [],
                    itex: [],
                    valores: [],
                    valoresCalculados: [],
                    qtds: [],
                };

                pedidoAntesDaEdicao = "";
                pedidoEditado = {};
                produtos = [];

            } else {
                Swel.fire({
                    icon: 'success',
                    text: 'Pedido editado com sucesso!'
                }).then((result) => {
                    if (window.localStorage.getItem('tipoMenu') === "1") {
                        if (TelaAnterior === "Reserva") {
                            zerarVariaveis();

                            document.getElementById('container-mesa').innerHTML = "";
                            document.getElementById('container-mesa').style.display = 'none';
                            document.getElementById('buttons-container').innerHTML = "";
                            document.getElementById('adiciona-pedido').innerHTML = "";
                            document.getElementById('somaDosItensMesaBtns').innerHTML = "";
                            document.getElementById('valorTotalMesaBtns').innerHTML = "";
                            document.getElementById('domCliente').value = "";
                            document.getElementById('domMesa').value = "";

                            pedido = {
                                produtos: [],
                                itex: [],
                                valores: [],
                                valoresCalculados: [],
                                qtds: [],
                            };

                            selecionarTudo();
                        } else if (TelaAnterior === "Ultimo") {
                            carregaFrame('frameSubMenuUltimo');
                            window.sessionStorage.removeItem('pedidoTotais');
                            window.sessionStorage.removeItem('itensDosPedidos');
                            selecionaPedidos();
                        } else if (TelaAnterior === "Comanda") {
                            //carregaFrame('frameSubMenuMovimento', "SubMenu");
                            window.sessionStorage.removeItem('pedidoTotais');
                            window.sessionStorage.removeItem('itensDosPedidos');
                            document.getElementById('container-mesa').innerHTML = "";
                            document.getElementById('container-mesa').style.display = "";
                            document.getElementById('adicionarItensPedido').innerHTML = "";

                            setTimeout(() => {
                                abrirModais('comanda');
                            }, 10);

                            selecionaPedidos();
                            setTimeout(() => {
                                buscaPedidoComanda();
                            }, 200);
                        } else if (TelaAnterior === "Fechamento") {
                            if (tipoPedido === "Mesa") {
                                document.getElementById('container-mesa').innerHTML = "";
                                selecionaPedidos();
                                setTimeout(() => {
                                    irParaCardapio('VoltandoFechamento', 'Fechamento', undefined, tipoPedido)
                                }, 1000);
                            } else if (tipoPedido === "Comanda") {
                                document.getElementById('container-mesa').innerHTML = "";
                                selecionaPedidos();
                                setTimeout(() => {
                                    irParaCardapio('VoltandoFechamento', 'Fechamento', undefined, tipoPedido)
                                }, 1000);
                            }
                        }
                    } else if (window.localStorage.getItem('tipoMenu') === "0") {
                        window.location.href = "../html/menuTradicional.html"
                        document.getElementById('menuTrad').style.display = "block";
                        window.sessionStorage.removeItem('pedidoTotais');
                        window.sessionStorage.removeItem('itensDosPedidos');
                    }
                })

                pedidoAntesDaEdicao = "";
                pedidoEditado = {};
                produtos = [];


            }
        }else{
            document.getElementById('btnAceitar').disabled = false;
        }
    });

    window.sessionStorage.removeItem('itensDosPedidos');
    window.sessionStorage.removeItem('itemDoPedidoMesa');
    //window.sessionStorage.removeItem('pedidoMesa');
    window.sessionStorage.removeItem('pedidosTotais');

}

function gerarXMLManual(clienteSelecionado, produtos, pedido, entrega = false) {
    produtos = produtos.produtos;

    let conteudoGeral = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <CFe>`;

    conteudoGeral += `
    <infCFe versaoDadosEnt="0.07">
        <ide>
            <CNPJ></CNPJ>
            <signAC></signAC>
            <numeroCaixa></numeroCaixa>
        </ide>
        <emit>
            <CNPJ>${dadosEstab.cfg_cnpj}</CNPJ>
            <IE>${dadosEstab.cfg_ie}</IE>
            <IM>${dadosEstab.cfg_im}</IM>
            <cRegTribISSQN>3</cRegTribISSQN>
            <indRatISSQN>N</indRatISSQN>
        </emit>
        ${entrega ? `<entrega><xLogr>${clienteSelecionado.Logradouro.rua}</xLogr><nro>${clienteSelecionado.num_res}</nro><xCpl>${clienteSelecionado.compl_res}</xCpl><xBairro>${clienteSelecionado.Logradouro.bairro}</xBairro><xMun>${clienteSelecionado.Logradouro.id_municipio}</xMun><UF>${clienteSelecionado.Logradouro.id_uf}</UF></entrega>` : ''}
        <dest>
            <xNome>${pedido.cliente}</xNome>
            <cnpj>${clienteSelecionado.cnpj ? clienteSelecionado.cnpj : ''}</cnpj>
            <cpf>${clienteSelecionado.cpf ? clienteSelecionado.cpf : ''}</cpf>
        </dest>`;

    let conteudoProdutos = ``;

    for (let i = 0; i < produtos.length; i++) {
        conteudoProdutos += `
        <det nItem="${i + 1}">
            <prod>
                <cProd>${produtos[i].cod_prod}</cProd>
                ${produtos[i].cod_barr ? `<cEan>${produtos[i].cod_barr}</cEan>` : '<cEan/>'}
                ${produtos[i].ncm ? `<NCM>${produtos[i].ncm}</NCM>` : '<NCM/>'}
                <xProd>${produtos[i].nome_prod}</xProd>
                ${produtos[i].cfop ? `<CFOP>${produtos[i].cfop}</CFOP>` : '<CFOP/>'}
                <uCom>${produtos[i].id_umed + 'UN'}</uCom>
                <qCom>${produtos[i].quantidade}</qCom>
                <vUnCom>${produtos[i].vl_venda1.toFixed(2).toString().replace('.', ',')}</vUnCom>
                <indRegra>A</indRegra>
            </prod>
            <imposto>
                <ICMS>
                <ICMSSN102>
                    <Orig>0</Orig>
                    <CSOSN>500</CSOSN>
                </ICMSSN102>
                </ICMS>
                <PIS>
                <PISSN>
                    <CST>49</CST>
                </PISSN>
                </PIS>
                <COFINS>
                <COFINSSN>
                    <CST>49</CST>
                </COFINSSN>
                </COFINS>
            </imposto>
        </det>`;
    }

    conteudoGeral += conteudoProdutos;

    conteudoGeral += `
            <pgto>
            <MP>
                <cMP>1</cMP>
                <vMP>${(pedido.vl_total + clienteSelecionado.Logradouro.frete).toFixed(2).toString().replace('.', ',')}</vMP>
            </MP>
            </pgto>
        </infCFe>
    </CFe>
    `;

    fs.writeFileSync('c:/SICLOP/testeXMLManual.xml', conteudoGeral);

}

function mostrarTodosOsProdutos(retorno, stringify, contador, tipoOperacao) {

    if (stringify != true) {
        retorno = JSON.parse(JSON.stringify(retorno));
    }

    const valorNormal = Helper.valorEmReal(retorno.vl_venda1);
    let valorBroto = 0.00;
    let valorMedia = 0.00;
    let valorGiga = 0.00;

    if (retorno.Grupo.descricao === "PIZZA" || retorno.Grupo.descricao === "PIZZAS") {
        switch (dadosEstab.cfg_formabroto) {
            case "P": valorBroto = (retorno.vl_venda1 * (dadosEstab.cfg_porc_broto / 100)); break;
            case "F": valorBroto = retorno.vl_venda1 - dadosEstab.cfg_porc_broto; break;
            case "I": valorBroto = retorno.vl_broto; break;
        }

        switch (dadosEstab.cfg_formabroto) {
            case "P": valorMedia = (retorno.vl_venda1 * (dadosEstab.cfg_porc_media / 100)); break;
            case "F": valorMedia = retorno.vl_venda1 - dadosEstab.cfg_porc_media; break;
            case "I": valorMedia = retorno.vl_media; break;
        }

        switch (dadosEstab.cfg_formabroto) {
            case "P": valorGiga = retorno.vl_venda1 + (retorno.vl_venda1 * (dadosEstab.cfg_porc_giga / 100)); break;
            case "F": valorGiga = retorno.vl_venda1 - dadosEstab.cfg_porc_giga; break;
            case "I": valorGiga = retorno.vl_giga; break;
        }
    }

    retorno.nn = retorno.vl_venda1;
    retorno.br = valorBroto;
    retorno.md = valorMedia;
    retorno.gi = valorGiga;

    if (retorno.Grupo.descricao !== "INGREDIENTES") {

        produtos.push(retorno);

        const posicaoProdutoVetor = produtos.length - 1;
        tbody += `
        <div class="linhaDadosCardapio linhaDetalheCardapio" id = "rowMenu${contador}">
            <div class = "colDadosCardapio colDadosCardapioCod" id = "table-content" name = "nn" onclick = "clickValor(${posicaoProdutoVetor}, 'nn', 'rowMenu${contador}')">
                <p id = "codigo">${retorno.cod_prod}</p>
            </div>
            <div id = "table-content" class="colDadosCardapio colDadosCardapioDesc" name = "nn" onclick = "clickValor(${posicaoProdutoVetor}, 'nn', 'rowMenu${contador}')">
                <p id = "descProd">${retorno.nome_prod}</p>
            </div>
            <div class="colDadosCardapio colDadosCardapioNormal" name = "nn" onclick = "clickValor(${posicaoProdutoVetor}, 'nn', 'rowMenu${contador}')" id = "table-content">
                <p>${Helper.valorEmReal(valorNormal)}</p>
            </div>
            <div class="colDadosCardapio colDadosCardapioBroto" name = "br" onclick = "clickValor(${posicaoProdutoVetor}, 'br', 'rowMenu${contador}')" id = "table-content">
                <p>${Helper.valorEmReal(valorBroto) === Helper.valorEmReal(0) && retorno.Grupo.descricao !== 'PIZZAS' ? "" : Helper.valorEmReal(valorBroto)}</p>
            </div>
            <div class="colDadosCardapio colDadosCardapioMedia" name = "md" onclick = "clickValor(${posicaoProdutoVetor}, 'md', 'rowMenu${contador}')" id = "table-content">
                <p>${Helper.valorEmReal(valorMedia) === Helper.valorEmReal(0) && retorno.Grupo.descricao !== 'PIZZAS' ? "" : Helper.valorEmReal(valorMedia)}</p>
            </div>
            <div class="colDadosCardapio colDadosCardapioGiga" name = "gi" onclick = "clickValor(${posicaoProdutoVetor}, 'gi', 'rowMenu${contador}')" id = "table-content">
                <p>${Helper.valorEmReal(valorGiga) === Helper.valorEmReal(0) && retorno.Grupo.descricao !== 'PIZZAS' ? "" : Helper.valorEmReal(valorGiga)}</p>
            </div>

            <div class="colDadosCardapio colDadosCardapioMaisInfo" id = "table-content">
                <button class = "btnRedondoMaisInfo" class="textoValorProduto" onclick = "abrirIngredientes(${retorno.cod_prod});">?</button>
            </div>
        </div>`;
    }

    let adicionarHTML = document.getElementById("table-prod");

    adicionarHTML.innerHTML = tbody;

}

function adicionarPedidoAoArray(flagEdit) {
    let itensPedido = {};
    let arrayCodigos = [];
    let tamanhoEscolhidos = [];
    let idItemPed = [];
    let idPedido = [];
    let pizzaCombinada = [];
    let codigoPizzas;
    let recebeModelItensExtras = [];
    let arrayDeQuantidadeInalterado = [];
    let arrayDeItensExtrasInalterado = [];

    let qtdsItens = [];
    let qtdsItensForSearch = [];
    let nomeItemForSearch = [];
    let nomeItemForComparar = [];
    let nomeItem = [];
    let itensExtrasPzComb = [];

    let primeiroSabor = 0;
    let segundoSabor = 0;
    let terceiroSabor = 0;
    let quartoSabor = 0;
    let cont = 0;

    itensPedido = arrayDosItensPedidos;

    for (itPed of itensPedido) {
        if (itPed.id_prod.search('/') > 0) {
            codigoPizzas = itPed.id_prod.substr(-20, (itPed.id_prod.length - 1))
            let concatCodigos = codigoPizzas.split('/');
            arrayCodigos.push(concatCodigos);
            for (s = 0; s < arrayCodigos.length; s++) {
                for (z = 0; z < arrayCodigos[s].length; z++) {
                    recebeModelItensExtras.push(JSON.parse(JSON.stringify(itensExtrasModel)));
                }
            }

            //nomeItem.push('PzComb');

            concatCodigos = [];
            codigoPizzas = "";
        } else {
            arrayCodigos.push(itPed.id_prod);
            pedido.itex.push(JSON.parse(JSON.stringify(itensExtrasModel)));
        }

        idItemPed.push(itPed.id_item);
        idPedido.push(itPed.cod_pedido);
        tamanhoEscolhidos.push(itPed.tam_escolhido);
        recebeModelItensExtras.length !== 0 ? pedido.itex.push(recebeModelItensExtras) : console.log(recebeModelItensExtras);
        pedido.valoresCalculados.push((itPed.vl_unit * itPed.qtd_prod));
        pedido.valores.push(itPed.vl_unit);
        pedido.qtds.push(itPed.qtd_prod);

        recebeModelItensExtras = [];

    }

    for (itPed of itensPedido) {
        if (itPed.complemento.search('/') > 0) {
            let itensExtrasArray = itPed.complemento.split('/');
            for (i of itensExtrasArray) {
                let separandoItensEQtds = i.split('x');
                if (separandoItensEQtds[0] !== "") {
                    nomeItemForSearch.push(separandoItensEQtds[0]);
                    qtdsItensForSearch.push(parseInt(separandoItensEQtds[1]));
                }
            }

            nomeItemForComparar.push(nomeItemForSearch);
            qtdsItens.push(qtdsItensForSearch);

            nomeItemForSearch = [];
            qtdsItensForSearch = [];
        } else {
            nomeItemForComparar.push("");
            qtdsItens.push(0);
            arrayDeQuantidadeInalterado.push(0);
        }

    }

    for (searchPositionSabor of nomeItemForComparar) {
        if (isArray(searchPositionSabor)) {
            for (k in searchPositionSabor) {
                if (searchPositionSabor[k].search('1') > -1) {
                    primeiroSabor++;
                } else if (searchPositionSabor[k].search('2') > -1) {
                    segundoSabor++;
                } else if (searchPositionSabor[k].search('3') > -1) {
                    terceiroSabor++;
                } else if (searchPositionSabor[k].search('4') > -1) {
                    quartoSabor++;
                }
            }
        }
    }

    qtdSabores.push(primeiroSabor);
    qtdSabores.push(segundoSabor);
    qtdSabores.push(terceiroSabor);
    qtdSabores.push(quartoSabor);

    for (procuraItem in nomeItemForComparar) {
        for (arr in nomeItemForComparar[procuraItem]) {
            if (arr !== "reverse") {
                if (nomeItemForComparar[procuraItem][arr].search('-') > -1) {
                    for (itex in itensExtras) {
                        if (nomeItemForComparar[procuraItem][arr].substring(2) === itensExtras[itex].descricao) {
                            itensExtrasPzComb.push(itensExtras[itex]);
                        }
                    }
                } else {
                    for (itex in itensExtras) {
                        if (nomeItemForComparar[procuraItem][arr] === itensExtras[itex].descricao) {
                            nomeItem.push(itensExtras[itex]);
                        }
                    }
                }
            }
        }

        if (isArray(pedido.itex[procuraItem])) {
            for (entrandoSabores in qtdSabores) {
                if (qtdSabores[entrandoSabores] > 0 && pedido.itex[procuraItem][entrandoSabores] !== undefined) {
                    if (qtdSabores[entrandoSabores] > 0) {
                        for (contador = 0; contador < qtdSabores[entrandoSabores]; contador++) {
                            pedido.itex[procuraItem][entrandoSabores].nomeItem.push(itensExtrasPzComb[procuraItem]);
                            pedido.itex[procuraItem][entrandoSabores].qtdsItens.push(qtdsItens[procuraItem][procuraItem]);

                            itensExtrasPzComb.splice(procuraItem, 1);
                            qtdsItens[procuraItem].splice(procuraItem, 1);


                        }
                    }
                }
            }
        } else {
            pedido.itex[procuraItem].qtdsItens = qtdsItens[procuraItem];
            pedido.itex[procuraItem].nomeItem = nomeItem;
        }


        if (isArray(pedido.itex[procuraItem])) {
            if (itensExtrasPzComb.length === 1) {
                if (isArray(arrayDeQuantidadeInalterado[procuraItem])) {
                    for (entrandoDentroArray in arrayDeQuantidadeInalterado[procuraItem]) {
                        pedido.valoresCalculados[procuraItem] += itensExtrasPzComb[0] !== undefined ? pedido.qtds[procuraItem] * (arrayDeQuantidadeInalterado[procuraItem][entrandoDentroArray] * arrayDeItensExtrasInalterado[procuraItem][entrandoDentroArray].acrescimo) : 0;
                    }
                }
            } else if (itensExtrasPzComb.length > 1) {
                for (arrayAcrescimo in nomeItem) {
                    pedido.valoresCalculados[procuraItem] += pedido.qtds[procuraItem] * (arrayDeQuantidadeInalterado[procuraItem][arrayAcrescimo] * itensExtrasPzComb[arrayAcrescimo].acrescimo);
                }
            } else if (itensExtrasPzComb.length === 0) {
                pedido.valoresCalculados[procuraItem];
            }

        } else {
            if (nomeItem.length === 1) {
                pedido.valoresCalculados[procuraItem] += nomeItem[0] !== undefined ? pedido.qtds[procuraItem] * (qtdsItens[procuraItem] * nomeItem[0].acrescimo) : 0;
            } else if (nomeItem.length > 1) {
                for (arrayAcrescimo in nomeItem) {
                    pedido.valoresCalculados[procuraItem] += pedido.qtds[procuraItem] * (qtdsItens[procuraItem][arrayAcrescimo] === undefined ? 0 : qtdsItens[procuraItem][arrayAcrescimo] * nomeItem[procuraItem].acrescimo);
                }
            } else if (nomeItem.length === 0) {
                pedido.valoresCalculados[procuraItem];
            }
        }

        nomeItem = [];
    }

    for (cod of arrayCodigos) {

        if (isArray(cod)) {
            for (pizzaComb of cod) {
                for (prod of produtos) {
                    if (parseInt(pizzaComb) === prod.cod_prod) {
                        prod.tamanhoEscolhido = tamanhoEscolhidos[cont];
                        prod.id_item = idItemPed[cont];
                        prod.cod_pedido = idPedido[cont];
                        pizzaCombinada.push(prod);
                    }
                }
            }

            pedido.produtos.push(pizzaCombinada);
            pizzaCombinada = [];

        } else {
            for (prod in produtos) {
                if (parseInt(cod) === produtos[prod].cod_prod) {

                    produtos[prod].tamanhoEscolhido = tamanhoEscolhidos[cont];
                    produtos[prod].id_item = idItemPed[cont];
                    produtos[prod].cod_pedido = idPedido[cont];
                    pedido.produtos.push(produtos[prod]);

                }
            }
        }

        cont++;
    }

    qtdSabores = [];
    pedidoAntesDaEdicao = JSON.stringify(pedido);
    preencherNotaTela(flagEdit);

}

function abrirIngredientes(idProduto) {
    document.getElementById('container-ingrediente').style.display = "block";

    let valorBroto = 0.00;
    let valorMedia = 0.00;
    let valorGiga = 0.00;

    for (let i = 0; i < produtos.length; i++) {

        if (produtos[i].Grupo.descricao === "PIZZA" || produtos[i].Grupo.descricao === "PIZZAS") {
            switch (dadosEstab.cfg_formabroto) {
                case "P": valorBroto = (produtos[i].vl_venda1 * (dadosEstab.cfg_porc_broto / 100)); break;
                case "F": valorBroto = produtos[i].vl_venda1 - dadosEstab.cfg_porc_broto; break;
                case "I": valorBroto = produtos[i].vl_broto; break;
            }

            switch (dadosEstab.cfg_formabroto) {
                case "P": valorMedia = (produtos[i].vl_venda1 * (dadosEstab.cfg_porc_media / 100)); break;
                case "F": valorMedia = produtos[i].vl_venda1 - dadosEstab.cfg_porc_media; break;
                case "I": valorMedia = produtos[i].vl_media; break;
            }

            switch (dadosEstab.cfg_formabroto) {
                case "P": valorGiga = produtos[i].vl_venda1 + (produtos[i].vl_venda1 * (dadosEstab.cfg_porc_giga / 100)); break;
                case "F": valorGiga = produtos[i].vl_venda1 - dadosEstab.cfg_porc_giga; break;
                case "I": valorGiga = produtos[i].vl_giga; break;
            }
        }

        if (dadosEstab.cfg_contr_estq === true) {
            if (produtos[i].cod_prod === idProduto && produtos[i].Grupo.descricao === "PIZZAS" || produtos[i].Grupo.descricao === "PIZZA") {
                document.getElementById('precosPizzas').style.display = "block";
                document.getElementById('controleEstoque').style.display = "block";
                document.getElementById('modalIng-nome').innerHTML = produtos[i].nome_prod;
                document.getElementById('modalIng-descricao').innerHTML = produtos[i].descricao.toUpperCase();
                document.getElementById('modalIng-valor').innerHTML = Helper.valorEmReal(produtos[i].vl_venda1);
                document.getElementById('modalIng-disponivel').innerHTML = produtos[i].qde_estq;
                document.getElementById('modalIng-broto').innerHTML = Helper.valorEmReal(valorBroto);
                document.getElementById('modalIng-normal').innerHTML = Helper.valorEmReal(valorMedia);
                document.getElementById('modalIng-giga').innerHTML = Helper.valorEmReal(valorGiga);
            } else if (produtos[i].cod_prod === idProduto && produtos[i].Grupo.descricao !== "PIZZAS" && produtos[i].Grupo.descricao !== "PIZZA") {
                document.getElementById('controleEstoque').style.display = "block";
                document.getElementById('modalIng-nome').innerHTML = produtos[i].nome_prod;
                document.getElementById('modalIng-descricao').innerHTML = produtos[i].descricao.toUpperCase();
                document.getElementById('modalIng-valor').innerHTML = Helper.valorEmReal(produtos[i].vl_venda1);
                document.getElementById('modalIng-disponivel').innerHTML = produtos[i].qde_estq;
                document.getElementById('precosPizzas').style.display = "none";
            }
        } else {
            if (produtos[i].cod_prod === idProduto && produtos[i].Grupo.descricao === "PIZZAS" || produtos[i].Grupo.descricao === "PIZZA") {
                document.getElementById('precosPizzas').style.display = "block";
                document.getElementById('modalIng-nome').innerHTML = produtos[i].nome_prod;
                document.getElementById('modalIng-descricao').innerHTML = produtos[i].descricao.toUpperCase();
                document.getElementById('modalIng-valor').innerHTML = Helper.valorEmReal(produtos[i].vl_venda1);
                document.getElementById('controleEstoque').style.display = "none";
                document.getElementById('modalIng-broto').innerHTML = Helper.valorEmReal(valorBroto);
                document.getElementById('modalIng-normal').innerHTML = Helper.valorEmReal(valorMedia);
                document.getElementById('modalIng-giga').innerHTML = Helper.valorEmReal(valorGiga);
            } else if (produtos[i].cod_prod === idProduto && produtos[i].Grupo.descricao !== "PIZZAS" && produtos[i].Grupo.descricao !== "PIZZA") {
                document.getElementById('modalIng-nome').innerHTML = produtos[i].nome_prod;
                document.getElementById('modalIng-descricao').innerHTML = produtos[i].descricao.toUpperCase();
                document.getElementById('modalIng-valor').innerHTML = Helper.valorEmReal(produtos[i].vl_venda1);
                document.getElementById('controleEstoque').style.display = "none";
                document.getElementById('precosPizzas').style.display = "none";
            }
        }
    }
}

function fecharModalIngrediente() {
    document.getElementById('container-ingrediente').style.display = 'none';
}

function abrirItensExtras() {
    document.getElementById("container-itex").style.display = "block";
    let containerPedido = document.getElementById('container-pedido');
    let tbody = "";

    for (let i = 0; i < pedido.produtos.length; i++) {
        let produto = pedido.produtos[i];
        let itEx = pedido.itex[i];
        if (isArray(produto)) {
            //Se sim, é uma pizza
            let nomeCompletoPizza = '';
            let valorPizza = 0;
            let maiorValor = 0;
            let codigoPizzas = '';
            let acrescimoTotal = 0;
            let complementoXml = "";

            //partesPizza = produto.length;

            for (pizza of produto) {
                codigoPizzas += pizza.cod_prod + '/';
                nomeCompletoPizza += produto.length > 1 ? '1/' + produto.length + ' ' + pizza.nome_prod + '<br>' : pizza.nome_prod + '<br>';
                pizza[pizza.tamanhoEscolhido] > maiorValor ? maiorValor = pizza[pizza.tamanhoEscolhido] : null;
                valorPizza += pizza[pizza.tamanhoEscolhido];
            }

            valorPizza = dadosEstab.cfg_pzcombinada === 'P' ? maiorValor : valorPizza / produto.length;
            pedido.qtds[i] = pedido.qtds[i] ? pedido.qtds[i] : 1;
            pedido.valores[i] = valorPizza;
            //pedido.valoresCalculados[i] ? null : pedido.valoresCalculados[i] = pedido.valores[i] * pedido.qtds[i];
            valorPizza = valorPizza * pedido.qtds[i];

            codigoPizzas = codigoPizzas.substr(-20, (codigoPizzas.length - 1))

            for (arrayItens in itEx.nomeItem) {
                acrescimoTotal += (itEx.qtdsItens[arrayItens] * itEx.nomeItem[arrayItens].acrescimo);
                pedido.valoresCalculados[i] = pedido.qtds[i] * (pedido.valores[i] + (itEx.qtdsItens[arrayItens] * itEx.nomeItem[arrayItens].acrescimo));
                complementoXml += `
                    <p style = "font-size: 0.6rem; margin-bottom: 0px">${itEx.nomeItem[arrayItens].descricao} x ${itEx.qtdsItens[arrayItens]}</p>
                `;
            }

            //nomeCompletoPizza += ' - ' +
            tbody += `
                <tr id = "listaProd${i}">
                    <td style = "text-align: left; padding-left: 20px;">
                        <span class = "text-bold" id = "qtd-${i}">${pedido.qtds[i]}</span>
                    </td>
                    <td class = "text-bold" style = "text-align: left;">${nomeCompletoPizza} <p style = "font-weight: 800; font-size: 0.7rem; margin-bottom: 0px;">${objTamanhos[produto[0].tamanhoEscolhido] === 'NORMAL' ? '' : objTamanhos[produto[0].tamanhoEscolhido]}</p>${complementoXml}</td>
                    <td class = "text-bold" id = "valor${i}">
                        ${Helper.valorEmReal(pedido.valoresCalculados[i])}
                    </td>
                    <td>
                        <button style = "width: 100px; font-weight: 800;" onclick = "colocaItemExtra('${i}', '${produto.length}')">Editar</button>
                    </td>
                </tr>
                
            `;

        } else {
            //Se não é um produto comum
            pedido.qtds[i] = pedido.qtds[i] ? pedido.qtds[i] : 1;
            pedido.valores[i] = produto.vl_venda1;
            pedido.valoresCalculados[i] ? null : pedido.valoresCalculados[i] = pedido.valores[i];

            let acrescimoTotal = 0;
            let complementoXml = "";

            for (arrayItens in itEx.nomeItem) {
                acrescimoTotal += (itEx.qtdsItens[arrayItens] * itEx.nomeItem[arrayItens].acrescimo);
                pedido.valoresCalculados[i] = pedido.qtds[i] * (pedido.valores[i] + (itEx.qtdsItens[arrayItens] * itEx.nomeItem[arrayItens].acrescimo));
                complementoXml += `
                    <p style = "font-size: 0.6rem; margin-bottom: 0px">${itEx.nomeItem[arrayItens].descricao} x ${itEx.qtdsItens[arrayItens]}</p>
                `;
            }

            tbody += `
                <tr id = "listaProd${i}">
                    <td style = "text-align: left; padding-left: 20px;">
                        <span class = "text-bold" id = "qtd-${i}">${pedido.qtds[i]}</span>
                    </td>
                    <td class = "text-bold" style = "text-align: left;">${produto.nome_prod} ${complementoXml}</td>
                    <td class = "text-bold" id = "valor${i}">
                        ${Helper.valorEmReal(pedido.valoresCalculados[i])}
                    </td>
                    <td>
                        <button style = "width: 100px; font-weight: 800;" onclick = "colocaItemExtra('${i}', '1')">Editar</button>
                    </td>
                </tr>
                
            `;

        }
    }


    containerPedido.innerHTML = tbody;

}

function colocaItemExtra(posicaoVetor, tamanhoVetorItex) {
    tamanhoVetorItex = parseInt(tamanhoVetorItex);
    if (tamanhoVetorItex > 1) {

        let conteudoListaPedido = document.getElementById('container-itens-extras');
        let conteudoModPedido = fs.readFileSync(__dirname + '\\..\\TeleItemExtra\\telaItemExtraPizzas.html', 'utf-8');

        document.getElementById('container-itens-extras').style.display = "block";
        document.getElementById('container-itex').style.display = "none";

        setTimeout(() => {
            document.getElementById('button-itens-extras').innerHTML = "Continuar";
            document.getElementById("saborAtual").innerHTML = objProximoSaborPizza[valorObj];
            if (screenWidth < 1024) {
                document.getElementById('box-itemExtra').style.height = "480px";
            }
        }, 100);

        conteudoListaPedido.innerHTML = conteudoModPedido;
        posicaoVetorItPed = posicaoVetor;

        definindoValorDaVariavelTipo(tamanhoVetorItex)
        vTipoItex--;
        selectItensExtras();

    } else if (tamanhoVetorItex === 1) {

        let conteudoListaPedido = document.getElementById('container-itens-extras');
        let conteudoModPedido = fs.readFileSync(__dirname + '\\..\\TeleItemExtra\\telaItemExtra.html', 'utf-8');

        document.getElementById('container-itens-extras').style.display = "block";
        document.getElementById('container-itex').style.display = "none";

        setTimeout(() => {
            if (screenWidth < 1024) {
                document.getElementById('box-itemExtra').style.height = "480px";
            }
        }, 100);

        conteudoListaPedido.innerHTML = conteudoModPedido;
        posicaoVetorItPed = posicaoVetor;

        definindoValorDaVariavelTipo(tamanhoVetorItex)
        selectItensExtras();

    }
}

async function selecionaItens() {
    let CadItensExtras = require(path.join(__dirname + '/../../../controller/cCadItensExtras.js'));
    let listaItex = CadItensExtras.selectAll();

    await listaItex.then(data => {
        let actuallyData = JSON.parse(JSON.stringify(data));
        for (itensExtra of actuallyData) {
            itensExtras.push(itensExtra);
        };
    });
}

async function selectItensExtras() {
    let CadItensExtras = require(path.join(__dirname + '/../../../controller/cCadItensExtras.js'));
    let listaItex = CadItensExtras.selectAll();

    await listaItex.then(data => {
        let actuallyData = JSON.parse(JSON.stringify(data));
        for (itensExtra of actuallyData) {
            itensExtras.push(itensExtra);
        };
    });

    adicionarItemExtraTela();
}

function adicionarItemExtraTela() {
    let tbody = "";
    let listaItensExtras = document.getElementById('itens-extras-lista');

    let itEx = pedido.itex[posicaoVetorItPed];
    let testeArray = [];
    let arrayItexPed = [];
    let qtdTotais = [];
    let qtdsPedido = [];

    if (itensExtras.length > 15) {
        listaItensExtras.style.overflowY = "scroll";
    }

    for (teste of itensExtras) {
        testeArray.push(JSON.stringify(teste));
    }

    if (isArray(itEx)) {
        for (arr in itEx[posicaoDoItemExtraNoSabor].nomeItem) {
            arrayItexPed.push(JSON.stringify(itEx[posicaoDoItemExtraNoSabor].nomeItem[arr]));
            qtdsPedido.push(itEx[posicaoDoItemExtraNoSabor].qtdsItens[arr]);

        }
    } else {
        for (arr in itEx.nomeItem) {
            arrayItexPed.push(JSON.stringify(itEx.nomeItem[arr]));
            qtdsPedido.push(itEx.qtdsItens[arr]);
        }
    }

    for (let i = 0; i < arrayItexPed.length; i++) {
        const posDoItemNoTotais = testeArray.indexOf(arrayItexPed[i]);
        qtdTotais[posDoItemNoTotais] = qtdsPedido[i]
    }

    for (let j = 0; j < testeArray.length; j++) {

        const item = JSON.parse(testeArray[j]);
        qtdTotais[j] ? null : qtdTotais[j] = 0;
        const qtdItem = qtdTotais[j];

        tbody += `
            <tr style = "height: 30px; border-width: 1px; border-style: solid; border-color: gray; background-color: gainsboro;">
                <td style = "text-align: left; height: 30px; padding-left: 10px; font-weight: 600;">${item.descricao}</td>
                <td style = "height: 30px; text-align: left;">${Helper.valorEmReal(item.acrescimo)}</td>
                <td style = "height: 30px;">
                    <img class="btnAddMinus" onclick = "quantidade('-', '${j}')" src = "../Imagens/remove.png" style = "width: 20px; height: 20px;"/>
                        <span id = "qtd-itex-${j}" style = "font-weight: 800;">${qtdItem}</span>
                    <img class="btnAddMinus" onclick = "quantidade('+', '${j}')" src = "../Imagens/plus.png" style = "width: 20px; height: 20px;"/>
                </td>
            </tr>
        `;
    };


    listaItensExtras.innerHTML = tbody;

}

function quantidade(operacao, posicaoItemVetor) {
    let qtdAtualItex = parseInt(document.getElementById('qtd-itex-' + posicaoItemVetor).innerHTML);

    if (operacao === "+") {
        if (isArray(pedido.itex[posicaoVetorItPed])) {
            let ItensExtrasComArray = pedido.itex[posicaoVetorItPed][posicaoDoItemExtraNoSabor];

            if (qtdAtualItex < 1) {
                ItensExtrasComArray.nomeItem.push(itensExtras[posicaoItemVetor]);

                if (ItensExtrasComArray.qtdsItens !== 0) {
                    ItensExtrasComArray.qtdsItens.push(1);
                } else {
                    ItensExtrasComArray.qtdsItens = [];
                    ItensExtrasComArray.qtdsItens.push(1);
                }

                qtdAtualItex = + 1;
                document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = qtdAtualItex;

            } else {

                let nomeItem = ItensExtrasComArray.nomeItem;
                //let ItemExtraAdd = ItensExtrasComArray.nomeItem;

                for (var positionItEx in nomeItem) {
                    if (nomeItem[positionItEx].id_extra === itensExtras[posicaoItemVetor].id_extra) {
                        ItensExtrasComArray.qtdsItens[positionItEx] = ItensExtrasComArray.qtdsItens[positionItEx] + 1;

                        document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = ItensExtrasComArray.qtdsItens[positionItEx];
                    }
                }
            }


        } else {

            if (qtdAtualItex < 1) {
                pedido.itex[posicaoVetorItPed].nomeItem.push(itensExtras[posicaoItemVetor]);
                if (pedido.itex[posicaoVetorItPed].qtdsItens !== 0) {
                    pedido.itex[posicaoVetorItPed].qtdsItens.push(1);
                } else {
                    pedido.itex[posicaoVetorItPed].qtdsItens = [];
                    pedido.itex[posicaoVetorItPed].qtdsItens.push(1);
                }
                qtdAtualItex = + 1;
                document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = qtdAtualItex;

            } else {
                let nomeItem = pedido.itex[posicaoVetorItPed].nomeItem;
                let ItemExtraAdd = itensExtras[posicaoItemVetor].id_extra;

                for (var positionItEx in nomeItem) {
                    if (nomeItem[positionItEx].id_extra === ItemExtraAdd) {
                        pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx] = pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx] + 1;

                        document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx];
                    }
                }
            }

        }

    } else {
        if (isArray(pedido.itex[posicaoVetorItPed])) {

            let tamanhoDoArrayItensExtras = pedido.itex[posicaoVetorItPed].length;
            let ItensExtrasComArray = pedido.itex[posicaoVetorItPed][posicaoDoItemExtraNoSabor];

            for (i in ItensExtrasComArray) {
                if (qtdAtualItex === 1 && i === 'nomeItem') {
                    let entrieItex = pedido.itex[posicaoVetorItPed][posicaoDoItemExtraNoSabor];
                    //let ItemExtraAdd = itensExtras[posicaoItemVetor].id_extra;

                    for (var positionItEx in entrieItex) {
                        for (arrayDosItensDaPizza in entrieItex[positionItEx]) {
                            if (entrieItex[positionItEx][arrayDosItensDaPizza].id_extra === itensExtras[posicaoItemVetor].id_extra) {
                                entrieItex.qtdsItens[arrayDosItensDaPizza] = entrieItex.qtdsItens[arrayDosItensDaPizza] - 1;

                                document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = entrieItex.qtdsItens[arrayDosItensDaPizza];
                                removerItensExtrasDoArray(arrayDosItensDaPizza, posicaoDoItemExtraNoSabor);
                            }
                        }
                    }

                } else if (i !== 'nomeItem') {

                    let nomeItem = ItensExtrasComArray.nomeItem;
                    //let ItemExtraAdd = ItensExtrasComArray.id_extra;

                    for (var positionItEx in nomeItem) {
                        if (nomeItem[positionItEx].id_extra === itensExtras[posicaoItemVetor].id_extra) {
                            ItensExtrasComArray.qtdsItens[positionItEx] = ItensExtrasComArray.qtdsItens[positionItEx] - 1;

                            document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = ItensExtrasComArray.qtdsItens[positionItEx];
                        }
                    }
                }
            }

        } else {

            if (qtdAtualItex === 1) {
                let nomeItem = pedido.itex[posicaoVetorItPed].nomeItem;
                //let ItemExtraAdd = itensExtras[posicaoItemVetor].id_extra;

                for (var positionItEx in nomeItem) {
                    if (nomeItem[positionItEx].id_extra === itensExtras[posicaoItemVetor].id_extra) {
                        pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx] = pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx] - 1;

                        document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx];
                        removerItensExtrasDoArray(positionItEx)
                    }
                }

            } else {
                let nomeItem = pedido.itex[posicaoVetorItPed].nomeItem;
                //let ItemExtraAdd = itensExtras[posicaoItemVetor].id_extra;

                for (var positionItEx in nomeItem) {
                    if (nomeItem[positionItEx].id_extra === itensExtras[posicaoItemVetor].id_extra) {
                        pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx] = pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx] - 1;

                        document.getElementById("qtd-itex-" + posicaoItemVetor).innerHTML = pedido.itex[posicaoVetorItPed].qtdsItens[positionItEx];
                    }
                }
            }

        }

    }

}

function irParaProximoSabor() {
    if (vTipoItex !== 0) {
        valorObj++;
        vTipoItex--;
        posicaoDoItemExtraNoSabor++;
        document.getElementById('saborAtual').innerHTML = objProximoSaborPizza[valorObj];
        document.getElementById('itens-extras-lista').innerHTML = "";
        adicionarItemExtraTela();
    } else {
        fecharItensExtras();
        vTipoItex = 1;
        valorObj = 0;
        posicaoDoItemExtraNoSabor = 0;
    }
}

function removerItensExtrasDoArray(posicaoVetor, posicaoItemPzComb) {
    if (isArray(pedido.itex[posicaoVetorItPed])) {
        for (j in pedido.itex[posicaoVetorItPed][posicaoItemPzComb].nomeItem) {
            pedido.itex[posicaoVetorItPed][posicaoItemPzComb].nomeItem.splice(posicaoVetor);
            pedido.itex[posicaoVetorItPed][posicaoItemPzComb].qtdsItens.splice(posicaoVetor);
        }
    } else {
        pedido.itex[posicaoVetorItPed].nomeItem.splice(posicaoVetor);
        pedido.itex[posicaoVetorItPed].qtdsItens.splice(posicaoVetor);
    }
}

function fecharItensExtras() {
    valorTotalSomado(posicaoVetorItPed, 'Adicionar');
    document.getElementById('itens-extras-lista').innerHTML = "";
    document.getElementById('container-itens-extras').innerHTML = "";
    document.getElementById('container-itens-extras').style.display = "none";
    //document.getElementById('table-order').innerHTML = "";
    document.getElementById('corpoOrder').innerHTML = "";
    conteudoHTML1 = "";
    itensExtras = [];
    preencherNotaTela();
}

function fecharModalItensExtras() {
    document.getElementById('container-itex').style.display = "none";
    document.getElementById('container-pedido').innerHTML = "";
}

function estiloResponsivoParaElementosInalterados(tpMonitor, pedido, operacao) {
    if (tpMonitor <= 1024 && (pedido === "BALCÃO" || pedido === "Balcao") && operacao !== "Editar") {

        document.getElementById('domBalcao').style.width = "200px";
        document.getElementById('domTextCli') ? document.getElementById('domTextCli').style.marginRight = "1px" : null;
        document.getElementById('domTextTel').style.marginRight = "1px";
        document.getElementById('domTextCli').style.marginLeft = "8px";
        document.getElementById('domTextTel').style.marginLeft = "8px";
        document.getElementById('domTelefone').style.width = "123px";
        document.getElementById('domBalcao').style.fontSize = "0.7rem";
        document.getElementById('domTelefone').style.fontSize = "0.7rem";
        document.getElementById('labels-Tot-soma').style.fontSize = "0.7rem";
        document.getElementById('labels-Tot-entrega').style.fontSize = "0.7rem";
        document.getElementById('labels-Tot-total').style.fontSize = "0.7rem";
        document.getElementById('labels-Tot-entrega').style.marginRight = "10px";
        document.getElementById('labels-Tot-total').style.marginRight = "10px";
        document.getElementById('labels-Tot-soma').style.marginRight = "10px";
        document.getElementById('row-atalhos').style.paddingLeft = "0px";
        document.getElementById('buscaProd').style.width = "320px";
        document.getElementById('buscaProd').style.fontSize = "1rem";
        document.getElementById('buscaProd').style.marginTop = "7%";
        document.getElementById('domTextCli').style.marginLeft = "79px";
        document.getElementById('row-atalhos').style.paddingLeft = "0px";

    } else if (tpMonitor > 1024 && (pedido === "BALCÃO" || pedido === "Balcao") && operacao !== "Editar") {

        document.getElementById('domBalcao').style.width = "200px";
        document.getElementById('domTextCli').style.marginRight = "1px";
        document.getElementById('domTextTel').style.marginRight = "1px";
        document.getElementById('domTextCli').style.marginLeft = "8px";
        document.getElementById('domTextTel').style.marginLeft = "8px";
        document.getElementById('domTelefone').style.width = "130px";
        document.getElementById('domBalcao').style.width = "100%";
        document.getElementById('domBalcao').style.fontSize = "1rem";
        document.getElementById('domTelefone').style.fontSize = "1rem";
        document.getElementById('labels-Tot-soma').style.fontSize = "1rem";
        document.getElementById('labels-Tot-entrega').style.fontSize = "1rem";
        document.getElementById('labels-Tot-total').style.fontSize = "1rem";
        document.getElementById('labels-Tot-soma').style.marginRight = "30px";
        document.getElementById('labels-Tot-entrega').style.marginRight = "30px";
        document.getElementById('labels-Tot-total').style.marginRight = "30px";
        document.getElementById('labels-Tot-soma').style.fontSize = "1rem";
        document.getElementById('row-atalhos').style.paddingLeft = "0px";
        document.getElementById('search-Cardapio-id').style.marginTop = "2%";
        document.getElementById('search-Cardapio-id').style.paddingLeft = "6%";
        document.getElementById('search-Cardapio-id').style.paddingRight = "19%";
        document.getElementById('buscaProd').style.width = "400px";
        document.getElementById('buscaProd').style.fontSize = "1rem";
        document.getElementById('domTextCli').style.marginLeft = "0px";
        document.getElementById('row-atalhos').style.paddingLeft = "42px";

    } else if (tpMonitor <= 1024 && operacao === "Editar") {

        document.getElementById('InfoPed').style.marginLeft = "0px";
        document.getElementById('buscaProd').style.width = "350px";
        document.getElementById('buscaProd').style.paddingRight = "18%";

    } else if (tpMonitor > 1024 && operacao === "Editar") {

        document.getElementById('InfoPed').style.marginLeft = "0px";
        document.getElementById('buscaProd').style.width = "350px";

    }
}

function imprimirOuNao(pedido, tipoPedido, conjuntoProds, segundaVia = false, ehEdicao = false) {
    swal({
        text: "Deseja imprimir esse pedido?",
        icon: "warning",
        buttons: ['Não', 'Sim']
    }).then(async (resp) => {
        console.log(resp);
        if (resp === true) {
            criaConteudo(pedido, tipoPedido, conjuntoProds, segundaVia, ehEdicao);
        }
    })
}

//#endregion

module.exports = {
    SelecionaProdutos: SelecionaProdutos,
    clickValor: clickValor,
    removeProduto: removeProduto,
    adicionarQtd: adicionarQtd,
    removerQtd: removerQtd,
    enviaTexto: enviaTexto,
    finalizaPedido: finalizaPedido,
    Retornar: Retornar,
    abrirIngredientes: abrirIngredientes,
    fecharModalIngrediente: fecharModalIngrediente,
    abrirItensExtras: abrirItensExtras,
    colocaItemExtra: colocaItemExtra,
    quantidade: quantidade,
    fecharItensExtras: fecharItensExtras,
    fecharModalItensExtras: fecharModalItensExtras,
    irParaProximoSabor: irParaProximoSabor,
    adicionaEventListenerTroco: adicionaEventListenerTroco
}
