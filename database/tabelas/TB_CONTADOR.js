const Sequelize     = require('sequelize');
const connection    = require('../connection');

const tbContador = connection.define('TB_CONTADOR',{
    id_contador: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nome: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    nome_contabilidade: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    email: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    email2: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    enviar_xml_email: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbContador.sync({force: false});

module.exports = tbContador;