const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Setor         = require('../tabelas_auxiliares/TBA_SETOR');
const Funcoes       = require('../tabelas_auxiliares/TBA_FUNCOES');
const Nivel         = require('../tabelas_auxiliares/TBA_NIVEL_USU');
const TipoConta     = require('../tabelas_auxiliares/TBA_TIPO_CONTA');
const CatCNH        = require('../tabelas_auxiliares/TBA_CAT_CNH');
const Logradouros   = require('./TB_LOGRADOUROS');

const cadFunc = connection.define('TB_CAD_FUNC',{
    id_oper: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    operador: {
        type: Sequelize.STRING(15),
        allowNull: false
    },
    senha: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    frase_seguranca:{
        type: Sequelize.STRING(30),
        allowNull: false
    },
    nome: {
        type: Sequelize.STRING(40),
        allowNull: false
    },
    rg: {
        type: Sequelize.STRING(12),
        allowNull: false
    },
    cpf: {
        type: Sequelize.STRING(14),
        allowNull: false
    },
    ddd: {
        type: Sequelize.STRING(3),
        allowNull: false
    },
    fone1: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    fone2: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    dt_adm: {
        type: Sequelize.DATE,
        allowNull: false
    },
    dt_nasc: {
        type: Sequelize.DATE,
        allowNull: false
    },
    num_res: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    compl_res: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    comissao: {
        type: Sequelize.FLOAT(10),
        allowNull: false
    },
    salario: {
        type: Sequelize.FLOAT(10),
        allowNull: false
    },
    cnh: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    dt_valid_cnh: {
        type: Sequelize.DATE,
        allowNull: false
    },
    banco: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    agencia: {
        type: Sequelize.STRING(6),
        allowNull: false
    },
    conta_banco: {
        type: Sequelize.STRING(10),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

cadFunc.belongsTo(Nivel,        {foreignKey: 'id_nivel'});
cadFunc.belongsTo(Setor,        {foreignKey: 'id_setor'});
cadFunc.belongsTo(Funcoes,      {foreignKey: 'id_funcao'});
cadFunc.belongsTo(Logradouros,  {foreignKey: 'id_logradouro'});
cadFunc.belongsTo(TipoConta,    {foreignKey: 'id_tipo_conta'}); //corrente - poupança
cadFunc.belongsTo(CatCNH,       {foreignKey: 'id_cat'}); // A - B - AB

cadFunc.sync({force: false});

module.exports = cadFunc;