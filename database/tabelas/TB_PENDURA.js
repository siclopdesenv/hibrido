const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Caixa         = require('./TB_CAIXA');
const CadFunc       = require('./TB_CAD_FUNC');
const Pedido        = require('./TB_PEDIDO');

const Pendura = connection.define('TB_PENDURA',{
    id_pendura: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    dt_criacao: {
        type: Sequelize.DATE,
        allowNull: true
    },
    dt_receber: {
        type: Sequelize.DATE,
        allowNull: true
    },
    valor_receber: {
        type: Sequelize.FLOAT(8),
        allowNull: false
    },
    cliente: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    recebido: {
        type: Sequelize.BOOLEAN,
        allowNull: true
    },
    valor_ja_recebido: {
        type: Sequelize.FLOAT(8),
        allowNull: false
    },
    dt_recebido: {
        type: Sequelize.DATE,
        allowNull: true
    }
}, {timestamps : false, freezeTableName: true});

Pendura.belongsTo(CadFunc,       {foreignKey: 'id_oper'});
Pendura.belongsTo(Caixa,         {foreignKey: 'id_caixa'});
Pendura.belongsTo(Pedido,     {foreignKey: 'id_pedido'});

Pendura.sync({
    timestamps : false,
    freezeTableName: true
});

module.exports = Pendura;