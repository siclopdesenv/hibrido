const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Pedido        = require('./TB_PEDIDO');
const CadFunc       = require('./TB_CAD_FUNC');
const Caixa         = require('./TB_CAIXA');

// cliente pode ser: Balcao, Mesa X, Cliente Y

const HistVenda = connection.define('TB_HIST_VENDA',{
    id_hist_venda: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nr_pedido: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    data: {
        type: Sequelize.DATE,
        allowNull: false
    },
    hora: {
        type: Sequelize.STRING(5),
        allowNull: false
    },
    vl_pedido: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    cliente: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    obs: {
        type: Sequelize.TEXT,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

HistVenda.belongsTo(Pedido,     {foreignKey: 'id_pedido'});
HistVenda.belongsTo(CadFunc,    {foreignKey: 'id_oper'});
HistVenda.belongsTo(CadFunc,    {foreignKey: 'id_func_atend'});
HistVenda.belongsTo(Caixa,      {foreignKey: 'id_caixa'});

HistVenda.sync({force: false});

module.exports = HistVenda;