const Sequelize     = require('sequelize');
const connection    = require('../connection');
const CadPet        = require('./TB_CAD_PET');
const CadFunc       = require('./TB_CAD_FUNC');

//Histórico de atendimento dos PETs

const historicoATD = connection.define('HISTORICO_ATD',{
    id_atd: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(200),
        allowNull: false
    },
    dt_atd: {
        type: Sequelize.DATE,
        allowNull: false
    },
    hr_atd: {
        type: Sequelize.STRING(5),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

historicoATD.belongsTo(CadPet, {foreignKey:  'id_pet'});
historicoATD.belongsTo(CadFunc, {foreignKey: 'id_oper'});

historicoATD.sync({force: false});

module.exports = historicoATD;