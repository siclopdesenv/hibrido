const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Cliente       = require('./TB_CLIENTE');

const cadPet = connection.define('TB_CAD_PET',{
    id_pet: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nome: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    dt_nasc: {
        type: Sequelize.DATE,
        allowNull: false
    },
    peso: {
        type: Sequelize.FLOAT(7),
        allowNull: false
    },
    raca: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    cor_pelo: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    genero: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    obs_geral: {
        type: Sequelize.STRING(200),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

cadPet.belongsTo(Cliente, {foreignKey: 'id_cli'});

cadPet.sync({force: false});

module.exports = cadPet;