const Sequelize     = require('sequelize');
const connection    = require('../connection');
const RedeSocial    = require('../tabelas_auxiliares/TBA_REDE_SOCIAL');
const TpTAB         = require('../tabelas_auxiliares/TBA_TP_TAB');
const TpFone        = require('../tabelas_auxiliares/TBA_TP_FONE')

//O id_pertencente pode ser do CLIENTE, FORNECEDOR, FUNCIONÁRIO, CONTADOR ETC, mas não é FK

const Fone = connection.define('FONE',{
    id_fone: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    ddd: {
        type: Sequelize.STRING(3),
        allowNull: false
    },
    fone: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    id_pertencente: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Fone.belongsTo(RedeSocial,  {foreignKey: 'id_rede_social'});
Fone.belongsTo(TpTAB,       {foreignKey: 'id_tp_tab'});
Fone.belongsTo(TpFone,      {foreignKey: 'id_tp_fone'});

Fone.sync({force: false});

module.exports = Fone;