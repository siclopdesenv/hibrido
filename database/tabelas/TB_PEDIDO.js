const Sequelize     = require('sequelize');
const connection    = require('../connection');
const INFORTRM      = require('./INFORTRM');
const CadFunc       = require('./TB_CAD_FUNC');

// cliente pode ser: Balcao, Mesa X, Cliente Y
// supervisor (guarda id_oper (sem relacionamento))

const Pedido = connection.define('TB_PEDIDO',{
    id_pedido: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nr_pedido: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    cliente: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    telefone: {
        type: Sequelize.STRING(11),
        allowNull: true
    },
    qde_total_itens: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    desconto: {
        type: Sequelize.FLOAT(9),
    },
    acrescimo: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    vl_total: {
        type: Sequelize.FLOAT(9),
        allowNull: true
    },
    frete: {
        type: Sequelize.FLOAT(9),
        allowNull: true
    },
    tx_serv: {
        type: Sequelize.FLOAT(9),
        allowNull: true
    },
    nr_nfiscal: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    cpf_cnpj: {
        type: Sequelize.STRING(20),
        allowNull: true
    },
    dt_pedido: {
        type: Sequelize.DATEONLY,
        allowNull: true
    },
    hr_pedido: {
        type: Sequelize.STRING(5),
        allowNull: true
    },
    supervisor: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    obs: {
        type: Sequelize.TEXT,
        allowNull: true
    },
    num_endereco: {
        type: Sequelize.STRING(10),
        allowNull: true
    },
    comp_endereco: {
        type: Sequelize.STRING(40),
        allowNull: true
    },
    tipo_entrega: {
        type: Sequelize.STRING(10),
        allowNull: true
    },
    numero_mesa: {
        type: Sequelize.STRING(10),
        allowNull: true
    },
    status_pedido: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    id_logradouro: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Pedido.belongsTo(INFORTRM,  {foreignKey: 'id_infor_trm'});
Pedido.belongsTo(CadFunc,   {foreignKey: 'id_oper'});

Pedido.sync({force: false});

module.exports = Pedido;