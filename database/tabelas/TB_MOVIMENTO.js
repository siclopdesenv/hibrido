const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Localpedido   = require('../tabelas_auxiliares/TBA_LOCAL_PEDIDO');
const Produto       = require('./TB_PRODUTOS');
const TipoMov       = require('../tabelas_auxiliares/TBA_TIPO_MOV');

const Movimento = connection.define('MOVIMENTO',{
    id_mov: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    dt_mov: {
        type: Sequelize.DATE,
        allowNull: true
    },
    hr_mov: {
        type: Sequelize.STRING(5),
        allowNull: false
    },
    nr_venda: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    qde_prod: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    vl_prod: {
        type: Sequelize.FLOAT(10),
        allowNull: false
    },
    vl_desconto: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    vl_acresc: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    obs_prod: {
        type: Sequelize.STRING(40),
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Movimento.belongsTo(Localpedido,    {foreignKey: 'id_local'});
Movimento.belongsTo(Produto,        {foreignKey: 'id_prod'});
Movimento.belongsTo(TipoMov,        {foreignKey: 'id_tp_mov'});

Movimento.sync({force: false});

module.exports = Movimento;