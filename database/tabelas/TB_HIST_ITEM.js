const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Pedido        = require('./TB_PEDIDO');
const Produto       = require('./TB_PRODUTOS');

const HistItem = connection.define('TB_HIST_ITEM',{
    id_hist_item: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    qde: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    vl_unit: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    desconto: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    acrescimo: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    complemento: {
        type: Sequelize.TEXT,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

HistItem.belongsTo(Pedido,  {foreignKey: 'id_pedido'});
HistItem.belongsTo(Produto, {foreignKey: 'id_prod'});

HistItem.sync({force: false});

module.exports = HistItem;