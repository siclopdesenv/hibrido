const Sequelize     = require('sequelize');
const connection    = require('../connection');
const TpLogr        = require('../tabelas_auxiliares/TBA_TP_LOGR');
const UF            = require('../tabelas_auxiliares/TBA_UF');
const Municipio     = require('../tabelas_auxiliares/TBA_MUNICIPIOS');
const TSocial       = require('../tabelas_auxiliares/TBA_TSOCIAL');

const Logradouro = connection.define('LOGRADOURO',{
    id_logradouro: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    titulo: {
        type: Sequelize.STRING(35),
        allowNull: true
    },
    rua: {
        type: Sequelize.STRING(35),
        allowNull: false
    },
    bairro: {
        type: Sequelize.STRING(20),
        allowNull: true
    },
    CEP: {
        type: Sequelize.STRING(9),
        allowNull: false
    },
    range: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    frete: {
        type: Sequelize.FLOAT(8),
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Logradouro.belongsTo(TSocial,   {foreignKey: 'id_tsocial'});
Logradouro.belongsTo(TpLogr,    {foreignKey: 'id_tplogr'});
Logradouro.belongsTo(UF,        {foreignKey: 'id_uf'});
Logradouro.belongsTo(Municipio, {foreignKey: 'id_municipio'});

Logradouro.sync({force: false});

module.exports = Logradouro;