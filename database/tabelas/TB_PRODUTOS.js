const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Fiscal        = require('./TB_GRUPO');
const CadFunc       = require('./TB_CAD_FUNC');
const TpProd        = require('../tabelas_auxiliares/TBA_TP_PROD');
const UniMed        = require('../tabelas_auxiliares/TBA_UNID_MED');

const Produto = connection.define('PRODUTOS',{
    id_prod: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    sub_cod: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    cod_original: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    cod_prod: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true
    },
    cod_barr: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    nome_prod: {
        type: Sequelize.STRING(40),
        allowNull: false
    },
    descricao: {
        type: Sequelize.STRING(150),
        allowNull: false
    },
    vl_venda1: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    vl_venda2: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    vl_venda3: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    vl_broto: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    vl_media: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    vl_giga: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    qde_estq: {
        type: Sequelize.FLOAT(8),
        allowNull: true
    },
    qde_min: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    contr_estq: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    corredor: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    prateleira: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    dt_valid: {
        type: Sequelize.DATE,
        allowNull: true
    },
    dt_cad: {
        type: Sequelize.DATE,
        allowNull: false
    },
    dt_alt: {
        type: Sequelize.DATE,
        allowNull: false
    },
    dia_semana: {
        type: Sequelize.STRING(7),
        allowNull: false
    },
    comissao: {
        type: Sequelize.FLOAT(10),
        allowNull: true
    },
    ncm: {
        type: Sequelize.STRING(20),
        allowNull: true
    },
    origem: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    cfop: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    icms: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    csticms: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    bcicms: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    pis: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    cstpis: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    bcpis: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    cofins: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    cstcofins: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    bccofins: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    porc_mun: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    porc_est: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    porc_fed: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    cest: {
        type: Sequelize.STRING(30),
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Produto.belongsTo(Fiscal,   {foreignKey: 'id_grupo'});
Produto.belongsTo(TpProd,   {foreignKey: 'id_tpprod'});
Produto.belongsTo(UniMed,   {foreignKey: 'id_umed'});
Produto.belongsTo(CadFunc,  {foreignKey: 'id_oper'});

Produto.sync({force: false});

module.exports = Produto;