const Sequelize     = require('sequelize');
const connection    = require('../connection');
const FPGTO         = require('../tabelas_auxiliares/TBA_FPGTO');

const HistPgto = connection.define('TB_HIST_PGTO',{
    id_hist_pgto: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nr_pedido: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    vl: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    qde_parcela: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    troco: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    dt_pedido: {
        type: Sequelize.DATE,
        allowNull: false
    },
    hr_pedido: {
        type: Sequelize.STRING(5),
        allowNull: false
    },
    dt_pgto: {
        type: Sequelize.DATE,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

HistPgto.belongsTo(FPGTO, {foreignKey: 'id_fpgto'});

HistPgto.sync({force: false});

module.exports = HistPgto;