const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Produto       = require('./TB_PRODUTOS');

//Caracteristicas do produto

const tbImagem = connection.define('TB_IMAGEM',{
    id_imagem: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    cod_qr: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    imagem: {
        type: Sequelize.STRING(50),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbImagem.belongsTo(Produto, {foreignKey: 'id_prod'});

tbImagem.sync({force: false});

module.exports = tbImagem;