const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Situacao      = require('../tabelas_auxiliares/TBA_SITUACAO');
const FormaPgto     = require('../tabelas_auxiliares/TBA_FPGTO');
const Logradouro    = require('./TB_LOGRADOUROS');
const CadFunc       = require('./TB_CAD_FUNC');

const Cliente = connection.define('CLIENTE',{
    id_cli: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    cod_cli: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true
    },
    nome_cli: {
        type: Sequelize.STRING(40),
        allowNull: false
    },
    apelido: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    num_res: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    compl_res: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    num_com: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    compl_com: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    referencia: {
        type: Sequelize.STRING(50),
        allowNull: true
    },
    credito: {
        type: Sequelize.FLOAT(8),
        allowNull: true
    },
    desconto: {
        type: Sequelize.FLOAT(5),
        allowNull: true
    },
    dt_cad: {
        type: Sequelize.DATE,
        allowNull: false
    },
    dt_alt: {
        type: Sequelize.DATE,
        allowNull: false
    },
    dt_nasc: {
        type: Sequelize.DATE,
        allowNull: true
    },
    ddd_cli: {
        type: Sequelize.STRING(3),
        allowNull: false
    },
    fone1: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    fone2: {
        type: Sequelize.STRING(10),
        allowNull: true
    },
    fone3: {
        type: Sequelize.STRING(10),
        allowNull: true
    }    
}, {
    timestamps : false,
    freezeTableName: true
});

Cliente.belongsTo(Situacao,     {foreignKey: 'id_situacao'});
Cliente.belongsTo(FormaPgto,    {foreignKey: 'id_fpgto'});
Cliente.belongsTo(CadFunc,      {foreignKey: 'id_oper'}); //Operador que atendeu este cliente
Cliente.belongsTo(Logradouro,   {foreignKey: 'id_logradouro'});

Cliente.sync({force: false});

module.exports = Cliente;