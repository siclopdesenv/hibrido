const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Periodo       = require('../tabelas_auxiliares/TBA_PERIODO');
const TpTrm         = require('../tabelas_auxiliares/TBA_TP_TRM');
const CadFunc       = require('./TB_CAD_FUNC');

const Caixa = connection.define('CAIXA',{
    id_caixa: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    dt_caixa: {
        type: Sequelize.DATE,
        allowNull: false
    },
    fundo_caixa: {
        type: Sequelize.FLOAT(10),
        allowNull: false
    },
    hr_abre: {
        type: Sequelize.STRING(5),
        allowNull: false
    },
    hr_fecha: {
        type: Sequelize.STRING(5),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Caixa.belongsTo(Periodo,    {foreignKey: 'id_periodo'});
Caixa.belongsTo(TpTrm,      {foreignKey: 'id_tpterminal'});
Caixa.belongsTo(CadFunc,    {foreignKey: 'id_oper'});

Caixa.sync({force: false});

module.exports = Caixa;