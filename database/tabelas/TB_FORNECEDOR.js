const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Logradouro    = require('./TB_LOGRADOUROS');
const Fpgto         = require('../tabelas_auxiliares/TBA_FPGTO');

// 'contato' é o nome da pessoa na empresa fornecedora.

const Fornecedor = connection.define('FORNECEDOR',{
    id_fornecedor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nome: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    produto_forn: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    contato: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Fornecedor.belongsTo(Logradouro,    {foreignKey: 'id_logradouro'});
Fornecedor.belongsTo(Fpgto,         {foreignKey: 'id_fpgto'});

Fornecedor.sync({force: false});

module.exports = Fornecedor;