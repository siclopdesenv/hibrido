const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Fatura        = require('./TB_FATURA');

const tbRecebidos = connection.define('TB_RECEBIDOS',{
    id_recebido: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    vl_recebido: {
        type: Sequelize.FLOAT(10),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbRecebidos.belongsTo(Fatura,   {foreignKey: 'id_fat'});

tbRecebidos.sync({force: false});

module.exports = tbRecebidos;