const Sequelize     = require('sequelize');
const connection    = require('../connection');
const TpTerminal    = require('../tabelas_auxiliares/TBA_TP_TRM');

// ip_trm (ip do terminal)
// id_tpterminal (como opera: servidor ou terminal)

const inforTRM = connection.define('INFORTRM',{
    id_infor_trm: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    realiza_baixa: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    abre_caixa: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    fecha_caixa: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    nome_trm: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    ip_trm: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    sit_trm: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

inforTRM.belongsTo(TpTerminal, {foreignKey: 'id_tpterminal'});

inforTRM.sync({force: false});

module.exports = inforTRM;