const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Nicho         = require('../tabelas_auxiliares/TBA_NICHOS');
const Logradouro    = require('./TB_LOGRADOUROS');
const Impressoras   = require('./TB_IMPRESSORA');
const RegTributario = require('../tabelas_auxiliares/TBA_REG_TRIBUTARIO');

const InforSis = connection.define('INFORSIS', {
    cfg_nome: {
        type: Sequelize.STRING(90),
        allowNull: false
    },
    cfg_nome_fantasia: {
        type: Sequelize.STRING(90),
        allowNull: false
    },
    cfg_ddd:{
        type: Sequelize.STRING(3),
        allowNull: false
    },
    cfg_fone1: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    cfg_fone2: {
        type: Sequelize.STRING(10),
        allowNull: true
    },
    cfg_fone3: {
        type: Sequelize.STRING(10),
        allowNull: true
    },
    cfg_numero_local: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    cfg_complemento: {
        type: Sequelize.STRING(50),
        allowNull: true
    },
    cfg_cnpj: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    cfg_nr_serie: {
        type: Sequelize.STRING(8),
        allowNull: false
    },
    cfg_cod_lib: {
        type: Sequelize.STRING(6),
        allowNull: false
    },
    cfg_pzcombinada: {
        type: Sequelize.CHAR,
        allowNull: true
    },
    cfg_formabroto: {
        type: Sequelize.CHAR,
        allowNull: true
    },
    cfg_porc_broto: {
        type: Sequelize.FLOAT(6),
        allowNull: true
    },
    cfg_porc_media: {
        type: Sequelize.FLOAT(6),
        allowNull: true
    },
    cfg_porc_giga: {
        type: Sequelize.FLOAT(6),
        allowNull: true
    },
    cfg_frete_basico: {
        type: Sequelize.FLOAT(7),
        allowNull: true
    },
    cfg_taxa_servico: {
        type: Sequelize.FLOAT(6),
        allowNull: true
    },
    cfg_qde_cupom: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    cfg_separa_cupom: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_codigo_cupom: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_preco_cupom: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_canhoto_cupom: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_contr_pagto: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_contr_estq: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_promo_dia: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_modulo_movel: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_modulo_web: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_modulo_fiscal: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_contr_motoq: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_baixa_aqui: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_repete_item: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_separa_periodo: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_cod_barra: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_faz_delivery: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cfg_ie: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    cfg_im: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    rateio: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING(60),
        allowNull: false
    },
    qde_mesas: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

InforSis.belongsTo(Logradouro,   {foreignKey: 'id_logradouro'});
InforSis.belongsTo(Nicho,        {foreignKey: 'id_nicho'});
InforSis.belongsTo(Impressoras,  {foreignKey: 'id_impressora'});
InforSis.belongsTo(RegTributario,{foreignKey: 'id_regime_tributario'});

InforSis.sync({force: false});

module.exports = InforSis;