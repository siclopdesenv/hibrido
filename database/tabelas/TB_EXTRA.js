const Sequelize     = require('sequelize');
const connection    = require('../connection');

const Extra = connection.define('EXTRA',{
    id_extra: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(60),
        allowNull: false
    },
    acrescimo: {
        type: Sequelize.FLOAT(6),
        allowNull: false
    },
    dividir: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Extra.sync({force: false});

module.exports = Extra;