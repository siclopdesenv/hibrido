const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Pedido        = require('./TB_PEDIDO');

const ItemPedido = connection.define('TB_ITEM_PEDIDO',{
    id_item: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    id_prod: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    qtd_prod: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    desc_produto: {
        type: Sequelize.STRING(250),
        allowNull: false
    },
    tam_escolhido: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    vl_unit: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    desconto: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    acrescimo: {
        type: Sequelize.FLOAT(9),
        allowNull: false
    },
    complemento: {
        type: Sequelize.TEXT,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

ItemPedido.belongsTo(Pedido,   {foreignKey: 'cod_pedido'});

ItemPedido.sync({force: false});

module.exports = ItemPedido;