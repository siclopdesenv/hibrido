const Sequelize     = require('sequelize');
const connection    = require('../connection');

const Impressora = connection.define('IMPRESSORA',{
    id_impressora: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    apelido: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    nomePC: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    portaLPT: {
        type: Sequelize.STRING(6),
        allowNull: false
    },
    obs_impressora: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    ip_impressora: {
        type: Sequelize.STRING(15),
        allowNull: false
    },
    marca: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    modelo: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    largura: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    salto: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Impressora.sync({force: false});

module.exports = Impressora;