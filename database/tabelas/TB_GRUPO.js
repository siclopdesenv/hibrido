const Sequelize     = require('sequelize');
const connection    = require('../connection');
const CatFiscPadrao = require('../tabelas_auxiliares/TBT_CAT_FISCAL_PADRAO');
const Impressora    = require('./TB_IMPRESSORA');

const Grupo = connection.define('GRUPO',{
    id_grupo: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(20),
        allowNull: false
    },
    cod_grupo: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true
    },
    cupom_separado: {
        type: Sequelize.STRING(25),
        allowNull: true
    },
    totaliza: {
        type: Sequelize.STRING(25),
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Grupo.belongsTo(CatFiscPadrao, {foreignKey: 'id_cat_padrao'});
Grupo.belongsTo(Impressora,    {foreignKey: 'id_impressora'});

Grupo.sync({force: false});

module.exports = Grupo;