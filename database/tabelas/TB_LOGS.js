const Sequelize     = require('sequelize');
const connection    = require('../connection');
const CadFunc       = require('./TB_CAD_FUNC');

const Logs = connection.define('LOGS',{
    id_log: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(60),
        allowNull: false
    },
    data: {
        type: Sequelize.DATE,
        allowNull: false
    },
    hora: {
        type: Sequelize.STRING(5),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Logs.belongsTo(CadFunc, {foreignKey: 'id_oper'});

Logs.sync({force: false});

module.exports = Logs;