const Sequelize     = require('sequelize');
const connection    = require('../connection');
const Caixa         = require('./TB_CAIXA');
const Localpedido   = require('../tabelas_auxiliares/TBA_LOCAL_PEDIDO');
const CadFunc       = require('./TB_CAD_FUNC');
const TpTerm        = require('../tabelas_auxiliares/TBA_TP_TRM');

const Fatura = connection.define('FATURA',{
    id_fat: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    dt_fat: {
        type: Sequelize.DATE,
        allowNull: true
    },
    hr_fat: {
        type: Sequelize.STRING(5),
        allowNull: false
    },
    nr_venda: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    cupom_fiscal: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    cliente: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    desconto: {
        type: Sequelize.FLOAT(8),
        allowNull: false
    },
    troco: {
        type: Sequelize.FLOAT(8),
        allowNull: false
    }
}, {timestamps : false, freezeTableName: true});

Fatura.belongsTo(TpTerm,        {foreignKey: 'id_tpterminal'});
Fatura.belongsTo(Localpedido,   {foreignKey: 'id_local'});
Fatura.belongsTo(CadFunc,       {foreignKey: 'id_oper'});
Fatura.belongsTo(Caixa,         {foreignKey: 'id_caixa'});

Fatura.sync({
    timestamps : false,
    freezeTableName: true
});

module.exports = Fatura;