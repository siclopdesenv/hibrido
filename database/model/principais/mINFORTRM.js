const { Model, DataTypes } = require('sequelize');

// ip_trm (ip do terminal)
// id_tpterminal (como opera: servidor ou terminal)

class inforTRM extends Model {
    static init(connection) {
        super.init({
            id_infor_trm: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            realiza_baixa: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            abre_caixa: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            fecha_caixa: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            nome_trm: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            ip_trm: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            sit_trm: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            id_tpterminal: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });
    }

}

module.exports = inforTRM;