const { Model, DataTypes } = require('sequelize');

class Extra extends Model{
    static init (connection){
        super.init({
            id_extra: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            },
            acrescimo: {
                type: DataTypes.FLOAT(6),
                allowNull: false
            },
            dividir: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
}

module.exports = Extra;