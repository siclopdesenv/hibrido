const { Model, DataTypes } = require('sequelize');

// cliente pode ser: Balcao, Mesa X, Cliente Y
// supervisor (guarda id_oper (sem relacionamento)) 

class tb_pedido extends Model{
    static init (connection){
        super.init({
            id_pedido: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            nr_pedido: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cliente: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            telefone: {
                type: DataTypes.STRING(11),
                allowNull: true
            },
            qde_total_itens: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            desconto: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            acrescimo: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            vl_total: {
                type: DataTypes.FLOAT(9),
                allowNull: true
            },
            frete: {
                type: DataTypes.FLOAT(9),
                allowNull: true
            },
            tx_serv: {
                type: DataTypes.FLOAT(9),
                allowNull: true
            },
            nr_nfiscal: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cpf_cnpj: {
                type: DataTypes.STRING(20),
                allowNull: true
            },
            dt_pedido: {
                type: DataTypes.DATEONLY,
                allowNull: true
            },
            hr_pedido: {
                type: DataTypes.STRING(5),
                allowNull: true
            },
            supervisor: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            obs: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            id_infor_trm: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_oper: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            num_endereco: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            comp_endereco: {
                type: DataTypes.STRING(40),
                allowNull: true
            },
            tipo_entrega: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            numero_mesa: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            status_pedido: {
                type: DataTypes.STRING(15),
                allowNull: true
            },
            id_logradouro: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
}



module.exports = tb_pedido;