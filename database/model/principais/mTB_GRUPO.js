const { Model, DataTypes } = require('sequelize');

class Grupo extends Model{
    static init (connection){
        super.init({
            id_grupo: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(20),
                allowNull: false
            },
            cod_grupo: {
                type: DataTypes.INTEGER,
                allowNull: false,
                unique: true
            },
            cupom_separado: {
                type: DataTypes.STRING(25),
                allowNull: true
            },
            totaliza: {
                type: DataTypes.STRING(25),
                allowNull: true
            },
            id_cat_padrao: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_impressora: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
}

module.exports = Grupo;