const { Model, DataTypes } = require('sequelize');

//O id_pertencente pode ser do CLIENTE, FORNECEDOR, FUNCIONÁRIO, CONTADOR ETC, mas não é FK

class Fone extends Model {
    static init(connection) {
        super.init({
            id_fone: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            ddd: {
                type: DataTypes.STRING(3),
                allowNull: false
            },
            fone: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            id_pertencente: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_rede_social: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_tp_tab: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_tp_fone: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = Fone;