const { Model, DataTypes } = require('sequelize');

class tbContador extends Model {
    static init(connection) {
        super.init({
            id_contador: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            nome: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            nome_contabilidade: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            email: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            email2: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            enviar_xml_email: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tbContador;