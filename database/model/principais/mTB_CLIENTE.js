const { Model, DataTypes } = require('sequelize');

class Cliente extends Model{
    static init (connection){
        super.init({
            id_cli: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            cod_cli: {
                type: DataTypes.INTEGER,
                allowNull: false,
                unique: true
            },
            nome_cli: {
                type: DataTypes.STRING(40),
                allowNull: false
            },
            apelido: {
                type: DataTypes.STRING(15),
                allowNull: true
            },
            num_res: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            compl_res: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            num_com: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            compl_com: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            referencia: {
                type: DataTypes.STRING(50),
                allowNull: true
            },
            credito: {
                type: DataTypes.FLOAT(8),
                allowNull: true
            },
            desconto: {
                type: DataTypes.FLOAT(5),
                allowNull: true
            },
            dt_cad: {
                type: DataTypes.DATE,
                allowNull: false
            },
            dt_alt: {
                type: DataTypes.DATE,
                allowNull: false
            },
            dt_nasc: {
                type: DataTypes.DATE,
                allowNull: true
            },
            id_situacao : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            id_fpgto : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            id_oper : {
                type : DataTypes.INTEGER,
                allowNull : true
            },
            id_logradouro : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            ddd_cli: {
                type: DataTypes.STRING(3),
                allowNull: false
            },
            fone1: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            fone2: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            fone3: {
                type: DataTypes.STRING(10),
                allowNull: true
            }    
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = Cliente;