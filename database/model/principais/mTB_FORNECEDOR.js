const { Model, DataTypes } = require('sequelize');

// 'contato' é o nome da pessoa na empresa fornecedora.

class Fornecedor extends Model {
    static init(connection) {
        super.init({
            id_fornecedor: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            nome: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            produto_forn: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            contato: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            id_logradouro: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_fpgto: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = Fornecedor;