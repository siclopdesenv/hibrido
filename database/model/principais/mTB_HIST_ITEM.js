const { Model, DataTypes } = require('sequelize');

class HistItem extends Model{
    static init (connection){
        super.init({
            id_hist_item: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            qde: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            vl_unit: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            desconto: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            acrescimo: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            complemento: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            id_pedido: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_prod: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    static insert(qde, vl_unit, desconto, acrescimo, complemento, obs, id_pedido, id_prod){
        this.create({
            qde:            qde,
            vl_unit:        vl_unit,
            desconto:       desconto,
            acrescimo:      acrescimo,
            complemento:    complemento,
            obs:            obs,
            id_pedido:      id_pedido,
            id_prod:        id_prod
        });
    }
}

module.exports = HistItem;