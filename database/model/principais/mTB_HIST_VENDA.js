const { Model, DataTypes } = require('sequelize');

// cliente pode ser: Balcao, Mesa X, Cliente Y

class tb_hist_venda extends Model{
    static init (connection){
        super.init({
            id_hist_venda: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            nr_pedido: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            data: {
                type: DataTypes.DATE,
                allowNull: false
            },
            hora: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            vl_pedido: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            cliente: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            obs: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            id_pedido: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_oper: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_func_atend: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_caixa: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    static insert(nr_pedido, data, hora, vl_pedido, cliente, obs, id_pedido, id_oper, id_caixa){
        this.create({
            nr_pedido:  nr_pedido,
            data:       data,
            hora:       hora,
            vl_pedido:  vl_pedido,
            cliente:    cliente,
            obs:        obs,
            id_pedido:  id_pedido,
            id_oper:    id_oper,
            id_caixa:   id_caixa
        });
    }
}

module.exports = tb_hist_venda;