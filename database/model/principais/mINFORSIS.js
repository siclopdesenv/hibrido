const { Model, DataTypes } = require('sequelize');

class inforsis extends Model {
    static init(connection) {
        super.init({
            cfg_nome: {
                type: DataTypes.STRING(90),
                allowNull: false
            },
            cfg_nome_fantasia: {
                type: DataTypes.STRING(90),
                allowNull: false
            },
            cfg_ddd:{
                type: DataTypes.STRING(3),
                allowNull: false
            },
            cfg_fone1: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            cfg_fone2: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            cfg_fone3: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            cfg_numero_local: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cfg_complemento: {
                type: DataTypes.STRING(50),
                allowNull: true
            },
            cfg_cnpj: {
                type: DataTypes.STRING(20),
                allowNull: false
            },
            cfg_nr_serie: {
                type: DataTypes.STRING(8),
                allowNull: false
            },
            cfg_cod_lib: {
                type: DataTypes.STRING(6),
                allowNull: false
            },
            cfg_pzcombinada: {
                type: DataTypes.CHAR,
                allowNull: true
            },
            cfg_formabroto: {
                type: DataTypes.CHAR,
                allowNull: true
            },
            cfg_porc_broto: {
                type: DataTypes.FLOAT(6),
                allowNull: true
            },
            cfg_porc_media: {
                type: DataTypes.FLOAT(6),
                allowNull: true
            },
            cfg_porc_giga: {
                type: DataTypes.FLOAT(6),
                allowNull: true
            },
            cfg_frete_basico: {
                type: DataTypes.FLOAT(7),
                allowNull: true
            },
            cfg_taxa_servico: {
                type: DataTypes.FLOAT(6),
                allowNull: true
            },
            cfg_qde_cupom: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            cfg_separa_cupom: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_codigo_cupom: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_preco_cupom: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_canhoto_cupom: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_contr_pagto: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_contr_estq: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_promo_dia: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_modulo_movel: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_modulo_web: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_modulo_fiscal: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_contr_motoq: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_baixa_aqui: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_repete_item: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_separa_periodo: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_cod_barra: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_faz_delivery: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cfg_ie: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            cfg_im: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            rateio: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            email: {
                type: DataTypes.STRING(60),
                allowNull: false
            },
            qde_mesas: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_logradouro: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_nicho: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_impressora: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_regime_tributario: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });
    }

}

module.exports = inforsis;