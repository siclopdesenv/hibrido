const { Model, DataTypes } = require('sequelize');

class Produtos extends Model{
    static init (connection){
        super.init({
            id_prod: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            sub_cod: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cod_original: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cod_prod: {
                type: DataTypes.INTEGER,
                allowNull: false,
                unique: true
            },
            cod_barr: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            nome_prod: {
                type: DataTypes.STRING(40),
                allowNull: false
            },
            descricao: {
                type: DataTypes.STRING(150),
                allowNull: false
            },
            vl_venda1: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            vl_venda2: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            vl_venda3: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            vl_broto: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            vl_media: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            vl_giga: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            qde_estq: {
                type: DataTypes.FLOAT(8),
                allowNull: true
            },
            qde_min: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            contr_estq: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            corredor: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            prateleira: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            dt_valid: {
                type: DataTypes.DATE,
                allowNull: true
            },
            dt_cad: {
                type: DataTypes.DATE,
                allowNull: false
            },
            dt_alt: {
                type: DataTypes.DATE,
                allowNull: false
            },
            dia_semana: {
                type: DataTypes.STRING(7),
                allowNull: false
            },
            comissao: {
                type: DataTypes.FLOAT(10),
                allowNull: false
            },
            ncm: {
                type: DataTypes.STRING(20),
                allowNull: true
            },
            origem: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cfop: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            icms: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            csticms: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            bcicms: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            pis: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            cstpis: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            bcpis: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            cofins: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            cstcofins: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            bccofins: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            porc_mun: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            porc_est: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            porc_fed: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            cest: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            id_grupo : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            id_tpprod : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            id_umed : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            id_oper : {
                type : DataTypes.INTEGER,
                allowNull : true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

       
}

module.exports = Produtos;