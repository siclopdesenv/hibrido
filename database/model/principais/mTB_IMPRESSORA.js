const { Model, DataTypes } = require('sequelize');

class Impressora extends Model{
    static init (connection){
        super.init({
            id_impressora: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            apelido: {
                type: DataTypes.STRING(20),
                allowNull: false
            },
            nomePC: {
                type: DataTypes.STRING(20),
                allowNull: false
            },
            portaLPT: {
                type: DataTypes.STRING(6),
                allowNull: false
            },
            obs_impressora: {
                type: DataTypes.STRING(20),
                allowNull: false
            },
            ip_impressora: {
                type: DataTypes.STRING(15),
                allowNull: false
            },
            marca: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            modelo: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            largura: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            salto: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
    
}

module.exports = Impressora;