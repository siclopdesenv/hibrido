const { Model, DataTypes } = require('sequelize');

class Logs extends Model{
    static init (connection){
        super.init({
            id_log: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(60),
                allowNull: false
            },
            data: {
                type: DataTypes.DATE,
                allowNull: false
            },
            hora: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            id_oper: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
    
}

module.exports = Logs;