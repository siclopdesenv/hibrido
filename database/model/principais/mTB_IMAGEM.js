const { Model, DataTypes } = require('sequelize');

//Caracteristicas do produto

class tbImagem extends Model {
    static init(connection) {
        super.init({
            id_imagem: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            cod_qr: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            imagem: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            id_prod: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tbImagem;