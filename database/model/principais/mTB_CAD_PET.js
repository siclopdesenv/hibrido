const { Model, DataTypes } = require('sequelize');

class tb_cad_pet extends Model {
    static init(connection) {
        super.init({
            id_pet: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            nome: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            dt_nasc: {
                type: DataTypes.DATE,
                allowNull: false
            },
            peso: {
                type: DataTypes.FLOAT(7),
                allowNull: false
            },
            raca: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            cor_pelo: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            genero: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            obs_geral: {
                type: DataTypes.STRING(200),
                allowNull: false
            },
            id_cli: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tb_cad_pet;