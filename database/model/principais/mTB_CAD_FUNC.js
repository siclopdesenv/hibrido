const { Model, DataTypes } = require('sequelize');

class tb_cad_func extends Model {
    static init(connection) {
        super.init({
            id_oper: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            operador: {
                type: DataTypes.STRING(15),
                allowNull: false
            },
            senha: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            frase_seguranca:{
                type: DataTypes.STRING(30),
                allowNull: false
            },
            nome: {
                type: DataTypes.STRING(40),
                allowNull: false
            },
            rg: {
                type: DataTypes.STRING(12),
                allowNull: false
            },
            cpf: {
                type: DataTypes.STRING(14),
                allowNull: false
            },
            ddd: {
                type: DataTypes.STRING(3),
                allowNull: false
            },
            fone1: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            fone2: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            dt_adm: {
                type: DataTypes.DATE,
                allowNull: false
            },
            dt_nasc: {
                type: DataTypes.DATE,
                allowNull: false
            },
            num_res: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            compl_res: {
                type: DataTypes.STRING(50),
                allowNull: false
            },
            comissao: {
                type: DataTypes.FLOAT(10),
                allowNull: false
            },
            salario: {
                type: DataTypes.FLOAT(10),
                allowNull: false
            },
            cnh: {
                type: DataTypes.STRING(20),
                allowNull: false
            },
            dt_valid_cnh: {
                type: DataTypes.DATE,
                allowNull: false
            },
            banco: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            agencia: {
                type: DataTypes.STRING(6),
                allowNull: false
            },
            conta_banco: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            id_nivel: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_setor: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_funcao: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_logradouro: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_tipo_conta: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_cat: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }

    static async validaLogin(login, senha){
        return await this.findAll({
            where: {
                operador: login,
                senha : senha
            }
        })
    }
    
}

module.exports = tb_cad_func;