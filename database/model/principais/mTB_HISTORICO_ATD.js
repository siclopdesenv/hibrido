const { Model, DataTypes } = require('sequelize');

class HistoricoATD extends Model {
    static init(connection) {
        super.init({
            id_atd: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(200),
                allowNull: false
            },
            dt_atd: {
                type: DataTypes.DATE,
                allowNull: false
            },
            hr_atd: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            id_pet: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_oper: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = HistoricoATD;