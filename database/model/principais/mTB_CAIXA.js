const { Model, DataTypes } = require('sequelize');
 
class Caixa extends Model{
    static init (connection){
        super.init({
            id_caixa: {
                type: DataTypes.INTEGER,
                    allowNull: false,
                    primaryKey: true,
                    autoIncrement: true
            },
            dt_caixa: {
                type: DataTypes.DATE,
                    allowNull: false
            },
            fundo_caixa: {
                type: DataTypes.FLOAT(10),
                    allowNull: false
            },
            hr_abre: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            hr_fecha: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            id_periodo : {
                type : DataTypes.INTEGER,
                allowNull : true
            },
            id_tpterminal : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            id_oper : {
                type : DataTypes.INTEGER,
                allowNull : false
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    static insert(id, dt_caixa, fundo_caixa, hr_abre, hr_fecha, id_periodo, id_tpterminal, id_oper){
        this.create({
            id_caixa    : id,
            dt_caixa    : dt_caixa,
            fundo_caixa : fundo_caixa,
            hr_abre     : hr_abre,
            hr_fecha    : hr_fecha,
            id_periodo  : id_periodo,
            id_tpterminal : id_tpterminal,
            id_oper : id_oper
        });
    }

}

module.exports = Caixa;