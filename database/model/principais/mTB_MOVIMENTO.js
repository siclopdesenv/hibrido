const { Model, DataTypes } = require('sequelize');

class Movimento extends Model{
    static init (connection){
        super.init({
            id_mov: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            dt_mov: {
                type: DataTypes.DATE,
                allowNull: true
            },
            hr_mov: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            nr_venda: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            qde_prod: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            vl_prod: {
                type: DataTypes.FLOAT(10),
                allowNull: false
            },
            vl_desconto: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            vl_acresc: {
                type: DataTypes.FLOAT(10),
                allowNull: true
            },
            obs_prod: {
                type: DataTypes.STRING(40),
                allowNull: true
            },
            id_local: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_prod: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_tp_mov: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    static insert(dt_mov, hr_mov, nr_venda, qde_prod, vl_prod, vl_desconto, vl_acresc, obs_prod, id_local, id_prod, id_tp_mov){
        this.create({
            dt_mov      : dt_mov,
            hr_mov      : hr_mov,
            nr_venda    : nr_venda,
            qde_prod    : qde_prod,
            vl_prod     : vl_prod,
            vl_desconto : vl_desconto,
            vl_acresc   : vl_acresc,
            obs_prod    : obs_prod,
            id_local    : id_local,
            id_prod     : id_prod,
            id_tp_mov   : id_tp_mov
        });
    }
}

module.exports = Movimento;