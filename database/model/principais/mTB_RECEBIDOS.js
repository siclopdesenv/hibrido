const { Model, DataTypes } = require('sequelize');

class tbRecebidos extends Model {
    static init(connection) {
        super.init({
            id_recebido: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            vl_recebido: {
                type: DataTypes.FLOAT(10),
                allowNull: false
            },
            id_fat: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tbRecebidos;