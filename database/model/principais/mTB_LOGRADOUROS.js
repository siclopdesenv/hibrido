const { Model, DataTypes } = require('sequelize');

class Logradouro extends Model{
    static init (connection){
        super.init({
            id_logradouro: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            titulo: {
                type: DataTypes.STRING(35),
                allowNull: true
            },
            rua: {
                type: DataTypes.STRING(35),
                allowNull: false
            },
            bairro: {
                type: DataTypes.STRING(20),
                allowNull: true
            },
            CEP: {
                type: DataTypes.STRING(9),
                allowNull: false
            },
            range: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            frete: {
                type: DataTypes.FLOAT(8),
                allowNull: true
            },
            id_tsocial: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_tplogr: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_uf: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_municipio: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
    
}

module.exports = Logradouro;