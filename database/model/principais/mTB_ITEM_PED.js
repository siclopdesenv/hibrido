const { Model, DataTypes } = require('sequelize');

class tb_item_pedido extends Model{
    static init (connection){
        super.init({
            id_item: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            id_prod: {
                type: DataTypes.STRING(15),
                allowNull: true
            },
            qtd_prod: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            desc_produto: {
                type: DataTypes.STRING(250),
                allowNull: false
            },
            tam_escolhido: {
                type: DataTypes.STRING(15),
                allowNull: true
            },
            vl_unit: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            desconto: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            acrescimo: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            complemento: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            cod_pedido: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    static insert(qtd_prod, vl_unit, desconto, acrescimo, complemento, id_prod, cod_pedido){
        this.create({
            qtd_prod:       qtd_prod,
            vl_unit:        vl_unit,
            desconto:       desconto,
            acrescimo:      acrescimo,
            complemento:    complemento,
            id_prod:        id_prod,
            cod_pedido:     cod_pedido
        });
    }
}

module.exports = tb_item_pedido;