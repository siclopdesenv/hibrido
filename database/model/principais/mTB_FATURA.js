const { Model, DataTypes } = require('sequelize');

class Fatura extends Model{
    static init (connection){
        super.init({
            id_fat: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            dt_fat: {
                type: DataTypes.DATE,
                allowNull: true
            },
            hr_fat: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            nr_venda: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cupom_fiscal: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            cliente: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            desconto: {
                type: DataTypes.FLOAT(8),
                allowNull: false
            },
            troco: {
                type: DataTypes.FLOAT(8),
                allowNull: false
            },
            id_tpterminal: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_local: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_oper: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            id_caixa: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    static insert(dt_fat, hr_fat, nr_venda, cupom_fiscal, cliente, desconto, troco, id_tpterminal, id_local, id_oper, id_caixa){
        this.create({
            dt_fat          : dt_fat,
            hr_fat          : hr_fat,
            nr_venda        : nr_venda,
            cupom_fiscal    : cupom_fiscal,
            cliente         : cliente,
            desconto        : desconto,
            troco           : troco,
            id_tpterminal   : id_tpterminal,
            id_local        : id_local,
            id_oper         : id_oper,
            id_caixa        : id_caixa
        });
    }
}

module.exports = Fatura;