const { Model, DataTypes } = require('sequelize');

// cliente pode ser: Balcao, Mesa X, Cliente Y

class tb_pendura extends Model{
    static init (connection){
        super.init({
            id_pendura: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            dt_criacao: {
                type: DataTypes.DATE,
                allowNull: true
            },
            dt_receber: {
                type: DataTypes.DATE,
                allowNull: true
            },
            valor_receber: {
                type: DataTypes.FLOAT(8),
                allowNull: false
            },
            cliente: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            recebido: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            valor_ja_recebido: {
                type: DataTypes.FLOAT(8),
                allowNull: false
            },
            dt_recebido: {
                type: DataTypes.DATE,
                allowNull: true
            },
            id_pedido: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_oper: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            id_caixa: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
    
}

module.exports = tb_pendura;