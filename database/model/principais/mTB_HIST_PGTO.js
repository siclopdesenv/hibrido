const { Model, DataTypes } = require('sequelize');

class tb_hist_pgto extends Model{
    static init (connection){
        super.init({
            id_hist_pgto: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            nr_pedido: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            vl: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            qde_parcela: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            troco: {
                type: DataTypes.FLOAT(9),
                allowNull: false
            },
            dt_pedido: {
                type: DataTypes.DATE,
                allowNull: false
            },
            hr_pedido: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            dt_pgto: {
                type: DataTypes.DATE,
                allowNull: false
            },
            id_fpgto: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    static insert(nr_pedido, vl, qde_parcela, troco, dt_pedido, hr_pedido, id_fpgto){
        this.create({
            nr_pedido:      nr_pedido,
            vl:             vl,
            qde_parcela:    qde_parcela,
            troco:          troco,
            dt_pedido:      dt_pedido,
            hr_pedido:      hr_pedido,
            id_fpgto:       id_fpgto
        });
    }
}

module.exports = tb_hist_pgto;