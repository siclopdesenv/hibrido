const { Model, DataTypes } = require('sequelize');

//tipo conta banco
//descricao ex: (corrente, poupança)

class tba_tipo_Conta extends Model {
    static init(connection) {
        super.init({
            id_tipo_conta: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tba_tipo_Conta;
