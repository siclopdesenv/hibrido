const { Model, DataTypes } = require('sequelize');

//descricao (Telegram, Whats, Facebook)

class tba_rede_Social extends Model {
    static init(connection) {
        super.init({
            id_rede_social: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tba_rede_Social;
