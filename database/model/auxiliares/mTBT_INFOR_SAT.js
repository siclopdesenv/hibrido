const { Model, DataTypes } = require('sequelize');

//A fk 'id_siclop' é para pegar a chave ac

class tbt_Infor_SAT extends Model {
    static init(connection) {
        super.init({
            id_sat: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            marca: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            modelo: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            cod_ativacao: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            cod_paginas: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            tipo_driver: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            versao_xml: {
                type: DataTypes.STRING(10),
                allowNull: false
            },
            caminho_dll: {
                type: DataTypes.STRING(60),
                allowNull: false
            },
            pasta_txt: {
                type: DataTypes.STRING(60),
                allowNull: false
            },
            gerar_sat_automatico: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            sat_autonomo: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            id_siclop: {
                type: DataTypes.INTEGER,
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });
    }

}

module.exports = tbt_Infor_SAT;