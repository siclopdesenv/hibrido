const { Model, DataTypes } = require('sequelize');

class tbt_Infor_SICLOP extends Model {
    static init(connection) {
        super.init({
            id_siclop: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            nome_fantasia: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            CNPJ: {
                type: DataTypes.STRING(30),
                allowNull: true
            },
            num_caixa: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            chave_ac: {
                type: DataTypes.STRING(60),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });
    }

}

module.exports = tbt_Infor_SICLOP;