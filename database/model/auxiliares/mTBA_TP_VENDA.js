const { Model, DataTypes } = require('sequelize');

class tba_Tp_Venda extends Model{
    static init (connection){
        super.init({
            id_tpvenda: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_Tp_Venda;