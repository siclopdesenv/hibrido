const { Model, DataTypes } = require('sequelize');

class tba_Municipios extends Model{
    static init (connection){
        super.init({
            id_municipio: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
}

module.exports = tba_Municipios;
