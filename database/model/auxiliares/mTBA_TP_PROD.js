const { Model, DataTypes } = require('sequelize');

class tba_Tp_Prod extends Model{
    static init (connection){
        super.init({
            id_tpprod: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            },
            habilitado: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
    
}

module.exports = tba_Tp_Prod;