const { Model, DataTypes } = require('sequelize');

class tba_Pensamento extends Model{
    static init (connection){
        super.init({
            id_pensamento: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            frase: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            autor: {
                type: DataTypes.STRING(50),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }
}

module.exports = tba_Pensamento;
