const { Model, DataTypes } = require('sequelize');

//contém os dados fiscais das categorias que vem por padrão quando instala o SISTEMA.

class tbt_cat_Fiscal_Padrao extends Model {
    static init(connection) {
        super.init({
            id_cat_padrao: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            dados_fiscais: {
                type: DataTypes.STRING(100),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tbt_cat_Fiscal_Padrao;