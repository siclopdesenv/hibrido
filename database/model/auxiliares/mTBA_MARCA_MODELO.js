const { Model, DataTypes } = require('sequelize');

//Marca ou modelo de produto
//O 'cod_original' é o código que o produto recebeu quando foi fabricado e não o código dado pela loja.

class tba_marca_modelo extends Model{
    static init (connection){
        super.init({
            id_mm: {
                type: DataTypes.INTEGER,
                allowNull: true,
                primaryKey: true,
                autoIncrement: true
            },
            cod_original: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    
}

module.exports = tba_marca_modelo;
