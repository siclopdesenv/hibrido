const { Model, DataTypes } = require('sequelize');

class tba_Tp_Logr extends Model{
    static init (connection){
        super.init({
            id_tplogr: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_Tp_Logr;