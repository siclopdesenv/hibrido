const { Model, DataTypes } = require('sequelize');

class tba_Local_pedido extends Model{
    static init (connection){
        super.init({
            id_local: {
                type: DataTypes.INTEGER,
                allowNull: true,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            },
            habilitado: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

    
}

module.exports = tba_Local_pedido;
