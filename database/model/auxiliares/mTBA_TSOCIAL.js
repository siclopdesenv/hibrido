const { Model, DataTypes } = require('sequelize');

//Exemplo: PADRE, CAPITÃO, SARGENTO ETC

class tba_TSocial extends Model {
    static init(connection) {
        super.init({
            id_tsocial: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tba_TSocial;