const { Model, DataTypes } = require('sequelize');

class tba_Unid_Medida extends Model{
    static init (connection){
        super.init({
            id_unid_medida: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            sigla: {
                type: DataTypes.STRING(2),
                allowNull: false
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_Unid_Medida;