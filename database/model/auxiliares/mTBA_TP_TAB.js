const { Model, DataTypes } = require('sequelize');

class tba_tp_TAB extends Model {
    static init(connection) {
        super.init({
            id_tp_tab: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tba_tp_TAB;
