const { Model, DataTypes } = require('sequelize');

class tba_Fpgto extends Model{
    static init (connection){
        super.init({
            id_fpgto: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: true
            },
            cod_fpgto: {
                type: DataTypes.STRING(2),
                allowNull: true
            },
            habilitado: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_Fpgto;