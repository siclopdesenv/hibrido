const { Model, DataTypes } = require('sequelize');

// descricao ex: (trocado, venda simples, cancelado)

class tba_tipo_Mov extends Model {
    static init(connection) {
        super.init({
            id_tp_mov: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tba_tipo_Mov;
