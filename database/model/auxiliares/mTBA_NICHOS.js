const { Model, DataTypes } = require('sequelize');

class tba_Nichos extends Model{
    static init (connection){
        super.init({
            id_nicho: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(40),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_Nichos;