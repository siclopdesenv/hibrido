const { Model, DataTypes } = require('sequelize');

class tbt_grupos extends Model{
    static init (connection){
        super.init({
            id_tbt_grupos: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            id_nicho: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            desc_nicho: {
                type: DataTypes.STRING(60),
                allowNull: false
            },
            desc_grupo: {
                type: DataTypes.STRING(60),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tbt_grupos;