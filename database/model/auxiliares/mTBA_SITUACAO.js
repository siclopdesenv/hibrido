const { Model, DataTypes } = require('sequelize');

class tba_Situacao extends Model{
    static init (connection){
        super.init({
            id_situacao: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_Situacao;