const { Model, DataTypes } = require('sequelize');

//Tipo de telefone: residencial, comercial, recado, vendas etc

class tba_Tp_Fone extends Model {
    static init(connection) {
        super.init({
            id_tp_fone: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tba_Tp_Fone;
