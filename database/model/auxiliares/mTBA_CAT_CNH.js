const { Model, DataTypes } = require('sequelize');

//descricao ex: (A, B, ETC)

class tba_cat_CNH extends Model {
    static init(connection) {
        super.init({
            id_cat: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            sigla: {
                type: DataTypes.STRING(2),
                allowNull: false
            },
            descricao: {
                type: DataTypes.STRING(60),
                allowNull: false
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });

    }
    
}

module.exports = tba_cat_CNH;
