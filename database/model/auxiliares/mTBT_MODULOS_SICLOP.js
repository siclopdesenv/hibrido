const { Model, DataTypes } = require('sequelize');

class tbt_modulos_siclop extends Model{
    static init (connection){
        super.init({
            id_modulo: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            nome: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            ativo: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tbt_modulos_siclop;