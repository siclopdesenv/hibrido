const { Model, DataTypes } = require('sequelize');

class tba_UF extends Model{
    static init (connection){
        super.init({
            id_uf: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            uf: {
                type: DataTypes.STRING(2),
                allowNull: false
            },
            nome_uf: {
                type: DataTypes.STRING(30),
                allowNull: false
            }
        },{
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_UF;