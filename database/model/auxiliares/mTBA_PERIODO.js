const { Model, DataTypes } = require('sequelize');

class tba_Periodo extends Model{
    static init (connection){
        super.init({
            id_periodo: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            descricao: {
                type: DataTypes.STRING(15),
                allowNull: false
            },
            hora_inicio: {
                type: DataTypes.STRING(5),
                allowNull: false
            },
            hora_termino: {
                type: DataTypes.STRING(5),
                allowNull: false
            }
        }, {
            sequelize : connection,
            timestamps : false,
            freezeTableName: true
        });
    }

}

module.exports = tba_Periodo;