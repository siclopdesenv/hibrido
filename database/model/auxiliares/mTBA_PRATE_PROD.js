const { Model, DataTypes } = require('sequelize');

//Prateleira de produtos

class tba_prate_Prod extends Model {
    static init(connection) {
        super.init({
            id_prate: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            descricao: {
                type: DataTypes.STRING(30),
                allowNull: false
            },
            num: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        }, {
            sequelize: connection,
            timestamps: false,
            freezeTableName: true
        });
    }

}

module.exports = tba_prate_Prod;