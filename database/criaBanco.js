//tabelas auxiliares
const swal = require('sweetalert');
const fs = require('fs');
const ip = require('ip');
const os = require('os');
let connection ;
let qtdTbTotais = 58;
let qtdTbAuxiliares = 32;
let intervaloTbunica = '';
let intervaloTbAux = '';
let intervaloTbPrinc = '';

let checkTab1 = document.getElementById('checkTab1');
let checkTab2 = document.getElementById('checkTab2');
let checkTab3 = document.getElementById('checkTab3');

let labelConjTab1 = document.getElementById('labelConjTab1');
let labelConjTab2 = document.getElementById('labelConjTab2');
let labelConjTab3 = document.getElementById('labelConjTab3');

let linhaCheckTb1 = document.getElementById('linhaCheckTb1');
let linhaCheckTb2 = document.getElementById('linhaCheckTb2');
let linhaCheckTb3 = document.getElementById('linhaCheckTb3');

/*swal({
    icon: 'warning',
    title: 'Criando Banco de Dados',
    closeOnClickOutside:false,
    closeOnEsc: false,
    text: 'Por favor, aguarde, essa operação vai demorar cerca de 1 minuto',
    button: {text: 'Okay', closeModal: false}
});*/

let swal_text = document.getElementsByClassName('swal-text')[0];

criaAuxiliares();

async function criaAuxiliares(){
    //atualizaMensagemAlert('1/3 - Criando tabelas auxiliares');
    atualizaLabelCheck(labelConjTab1, 'Criando primeiro conjunto de tabelas');
    focoLinhaInstalacao(linhaCheckTb1);

    const cfgHibrido = require('fs').readFileSync('c:/siclop/hibrido/cfghibridobd.txt','utf-8').split('|');
    const user = cfgHibrido[0];
    const pass = cfgHibrido[1];
    const database = cfgHibrido[2];
    const host = cfgHibrido[3];
    const port = cfgHibrido[4];

    const mysql = require('mysql2/promise');

    await mysql.createConnection({
        host     : host,
        user     : user,
        password : pass
    }).then((con) => {
        con.query('CREATE DATABASE IF NOT EXISTS bdsiclop;').then(() => {
            connection = require('./connection');
        });
    });

    await require('./connection'                        );
    await require('./tabelas_auxiliares/TBA_MUNICIPIOS' );
    await require('./tabelas_auxiliares/TBA_NIVEL_USU'  );
    await require('./tabelas_auxiliares/TBA_UF'         );
    await require('./tabelas_auxiliares/TBA_LOCAL_PEDIDO');
    await require('./tabelas_auxiliares/TBA_FPGTO'      );
    await require('./tabelas_auxiliares/TBA_FUNCOES'    );
    await require('./tabelas_auxiliares/TBA_NICHOS'     );
    await require('./tabelas_auxiliares/TBT_GRUPOS');
    await require('./tabelas_auxiliares/TBT_MODULOS_SICLOP');
    await require('./tabelas_auxiliares/TBA_PERIODO'    );
    await require('./tabelas_auxiliares/TBA_SETOR'      );
    await require('./tabelas_auxiliares/TBA_SITUACAO'   );
    await require('./tabelas_auxiliares/TBA_TP_PROD'    );
    await require('./tabelas_auxiliares/TBA_TP_LOGR'    );
    await require('./tabelas_auxiliares/TBA_TP_TRM'     );
    await require('./tabelas_auxiliares/TBA_TP_VENDA'   );
    await require('./tabelas_auxiliares/TBA_UNID_MED'   );
    await require('./tabelas_auxiliares/TBA_REDE_SOCIAL');
    await require('./tabelas_auxiliares/TBA_TSOCIAL'    );
    await require('./tabelas_auxiliares/TBA_MARCA_MODELO');
    await require('./tabelas_auxiliares/TBA_TP_TAB'     );
    await require('./tabelas_auxiliares/TBA_CAT_CNH'    );
    await require('./tabelas_auxiliares/TBA_TIPO_CONTA' );
    await require('./tabelas_auxiliares/TBA_TIPO_MOV'   );
    await require('./tabelas_auxiliares/TBA_CORREDOR_PROD');
    await require('./tabelas_auxiliares/TBA_PRATE_PROD' );
    //await require('./tabelas_auxiliares/TBT_INFOR_SAT');
    intervaloTbunica = setInterval(async ()=>{
        try{
            connection.query('SELECT * FROM TBT_INFOR_SICLOP;').then(()=>{ require('./tabelas_auxiliares/TBT_INFOR_SAT'); console.log('CRIOU A TABELA');}).catch((e)=>{console.log('deu erro');});
        }catch(e){
            console.log('Não existe');
        }
    },3000);
    await require('./tabelas_auxiliares/TBT_INFOR_SICLOP');
    await require('./tabelas_auxiliares/TBA_TP_FONE'    );
    await require('./tabelas_auxiliares/TBA_REG_TRIBUTARIO');
    await require('./tabelas_auxiliares/TBA_PENSAMENTO' );
    await require('./tabelas_auxiliares/TBT_CAT_FISCAL_PADRAO');
    //atualizaMensagemAlert('1/3 - Criando tabelas auxiliares (31/32)');
    atualizaLabelCheck(labelConjTab1, 'Criando primeiro conjunto de tabelas (31/32)');

    let criouAuxiliares = false;
    intervaloTbAux = setInterval(()=>{
        if(!criouAuxiliares){
            connection.query('show tables;').then(tabelas=>{
                console.log(tabelas[0].length);
                if(tabelas[0].length === qtdTbAuxiliares) {
                    //atualizaMensagemAlert('1/3 - Criando tabelas auxiliares (32/32)');
                    atualizaLabelCheck(labelConjTab1, 'Criando primeiro conjunto de tabelas (32/32)');
                    focoLinhaInstalacao(linhaCheckTb1, false);
                    checkTab1.checked = true;
                    criaTabelasPrincipais();
                    criouAuxiliares = true;
                }else{
                    console.log('Não criou todas as auxiliares');
                }
            });
        }
    },5000);
    
}

let modelTBA_MUNICIPIOS;
let modelTBA_LOCAL_PEDIDO;
let modelTBA_UF;
let modelTBA_FPGTO;
let modelTBA_FUNCOES;
let modelTBA_NICHOS;
let modelTBT_GRUPOS;
let modelTBT_MODULOS_SICLOP;
let modelTBA_NIVEL_USU;
let modelTBA_PERIODO;
let modelTBA_SETOR;
let modelTBA_SITUACAO;
let modelTBA_TP_LOGR;
let modelTBA_TP_PROD;
let modelTBA_TP_TRM;
let modelTBA_TP_VENDA;
let modelTBA_UNID_MED;
let modelTBA_CAT_CNH;
let modelTBA_REDE_SOCIAL;
let modelTBA_TIPO_CONTA;
let modelTBA_TIPO_MOV;
let modelTBA_TSOCIAL;
let modelTBA_MARCA_MODELO;
let modelTBA_CORREDOR_PROD;
let modelTBA_PRATE_PROD;
let modelTBT_INFOR_SAT;
let modelTBT_INFOR_SICLOP;
let modelTBA_REG_TRIBUTARIO;
let modelTBA_TP_FONE;
let modelTBA_PENSAMENTO;
let modelTBT_CAT_FISCAL_PADRAO;

let modelTB_CAD_FUNC;
let modelTB_LOGRADOUROS;
let modelTB_IMPRESSORA;
let model_INFORTRM;


async function criaTabelasPrincipais(){
    clearInterval(intervaloTbunica);
    clearInterval(intervaloTbAux);
    //atualizaLabelCheck(labelConjTab2, 'Criando segundo conjunto de tabelas 0/26');
    atualizaLabelCheck(labelConjTab2, 'Criando segundo conjunto de tabelas (0/26)');
    focoLinhaInstalacao(linhaCheckTb2);

    //#region Criando as tabelas principais
    require('./tabelas/TB_LOGRADOUROS'  );
    require('./tabelas/TB_IMPRESSORA'   );
    require('./tabelas/TB_FONE'         );
    require('./tabelas/TB_CONTADOR'     );
    require('./tabelas/TB_EXTRA'        );
    require('./tabelas/INFORTRM'        );
    require('./tabelas/TB_HIST_PGTO'    );
    atualizaLabelCheck(labelConjTab2, 'Criando segundo conjunto de tabelas (7/26)');

    setTimeout(() => {
        //Aguardar a criação das primeiras tabelas
        require('./tabelas/TB_CAD_FUNC'     );
        require('./tabelas/TB_CLIENTE'      );
        require('./tabelas/TB_FORNECEDOR'   );
        require('./tabelas/TB_GRUPO'        );
        require('./tabelas/INFORSIS'        );
        atualizaLabelCheck(labelConjTab2, 'Criando segundo conjunto de tabelas (12/26)');
        
        setTimeout(() => {
            //Aguardar a criação das tabelas do timeout anterior
            require('./tabelas/TB_CAD_PET'      );
            require('./tabelas/TB_CAIXA'        );
            require('./tabelas/TB_LOGS'         );
            require('./tabelas/TB_PEDIDO'       );
            atualizaLabelCheck(labelConjTab2, 'Criando segundo conjunto de tabelas (16/26)');

            setTimeout(() => {
                require('./tabelas/TB_FATURA'       );
                require('./tabelas/TB_HISTORICO_ATD');
                require('./tabelas/TB_PRODUTOS'     );
                require('./tabelas/TB_HIST_VENDA'   );
                atualizaLabelCheck(labelConjTab2, 'Criando segundo conjunto de tabelas (20/26)');
                
                setTimeout(() => {
                    require('./tabelas/TB_MOVIMENTO'    );
                    require('./tabelas/TB_IMAGEM'       );
                    require('./tabelas/TB_RECEBIDOS'    );
                    require('./tabelas/TB_ITEM_PED'     );
                    require('./tabelas/TB_HIST_ITEM'    );
                    require('./tabelas/TB_PENDURA'      );
                    // atualizaLabelCheck(labelConjTab2, 'Criando segundo conjunto de tabelas 26/26');
                    // atualizaMensagemAlert('2/3 - Preparando dados para inserção...');
                    focoLinhaInstalacao(linhaCheckTb2, false);
                    checkTab2.checked = true;
                    atualizaLabelCheck('2/3 - Criando Tabelas Principais 26/26');
                }, 22000);
            }, 22000);
        }, 22000);
    }, 22000);
    //#endregion

    let criouPrincipais = false;
    intervaloTbPrinc = setInterval(()=>{
        if(!criouPrincipais){
            connection.query('show tables;').then(tabelas=>{
                console.warn(tabelas[0]);
                console.log(tabelas[0].length);
                if(tabelas[0].length === qtdTbTotais){
                    carregaModais();
                    console.log('CRIOU TODAS AS TABELAS');
                    criouPrincipais = true;
                }
            });
        }
    },10000);
}

// region Preenchendo as tables auxiliares

async function carregaModais(){
    
    let con                 = require('./connection');

    //#region Models Auxiliares
    modelTBA_MUNICIPIOS     = require('./model/auxiliares/mTBA_MUNICIPIOS');
    modelTBA_MUNICIPIOS.init(con);

    modelTBA_LOCAL_PEDIDO   = require('./model/auxiliares/mTBA_LOCAL_PEDIDO');
    modelTBA_LOCAL_PEDIDO.init(con);

    modelTBA_UF             = require('./model/auxiliares/mTBA_UF');
    modelTBA_UF.init(con);

    modelTBA_FPGTO          = require('./model/auxiliares/mTBA_FPGTO');
    modelTBA_FPGTO.init(con);

    modelTBA_FUNCOES        = require('./model/auxiliares/mTBA_FUNCOES');
    modelTBA_FUNCOES.init(con);

    modelTBA_NICHOS         = require('./model/auxiliares/mTBA_NICHOS');
    modelTBA_NICHOS.init(con);

    modelTBT_GRUPOS         = require('./model/auxiliares/mTBT_GRUPOS');
    modelTBT_GRUPOS.init(con);

    modelTBT_MODULOS_SICLOP = require('./model/auxiliares/mTBT_MODULOS_SICLOP');
    modelTBT_MODULOS_SICLOP.init(con);

    modelTBA_NIVEL_USU      = require('./model/auxiliares/mTBA_NIVEL_USU');
    modelTBA_NIVEL_USU.init(con);

    modelTBA_PERIODO        = require('./model/auxiliares/mTBA_PERIODO');
    modelTBA_PERIODO.init(con);

    modelTBA_SETOR          = require('./model/auxiliares/mTBA_SETOR');
    modelTBA_SETOR.init(con);

    modelTBA_SITUACAO       = require('./model/auxiliares/mTBA_SITUACAO');
    modelTBA_SITUACAO.init(con);

    modelTBA_TP_LOGR        = require('./model/auxiliares/mTBA_TP_LOGR');
    modelTBA_TP_LOGR.init(con);

    modelTBA_TP_PROD        = require('./model/auxiliares/mTBA_TP_PROD');
    modelTBA_TP_PROD.init(con);

    modelTBA_TP_TRM         = require('./model/auxiliares/mTBA_TP_TRM');
    modelTBA_TP_TRM.init(con);

    modelTBA_TP_VENDA       = require('./model/auxiliares/mTBA_TP_VENDA');
    modelTBA_TP_VENDA.init(con);

    modelTBA_UNID_MED       = require('./model/auxiliares/mTBA_UNID_MED');
    modelTBA_UNID_MED.init(con);

    modelTBA_CAT_CNH        = require('./model/auxiliares/mTBA_CAT_CNH');
    modelTBA_CAT_CNH.init(con);

    modelTBA_REDE_SOCIAL    = require('./model/auxiliares/mTBA_REDE_SOCIAL');
    modelTBA_REDE_SOCIAL.init(con);

    modelTBA_TIPO_CONTA     = require('./model/auxiliares/mTBA_TIPO_CONTA');
    modelTBA_TIPO_CONTA.init(con);

    modelTBA_TIPO_MOV       = require('./model/auxiliares/mTBA_TIPO_MOV');
    modelTBA_TIPO_MOV.init(con);

    modelTBA_TSOCIAL        = require('./model/auxiliares/mTBA_TSOCIAL');
    modelTBA_TSOCIAL.init(con);

    modelTBA_MARCA_MODELO   = require('./model/auxiliares/mTBA_MARCA_MODELO');
    modelTBA_MARCA_MODELO.init(con);

    modelTBA_CORREDOR_PROD  = require('./model/auxiliares/mTBA_CORREDOR_PROD');
    modelTBA_CORREDOR_PROD.init(con);

    modelTBA_PRATE_PROD     = require('./model/auxiliares/mTBA_PRATE_PROD');
    modelTBA_PRATE_PROD.init(con);

    modelTBT_INFOR_SAT      = require('./model/auxiliares/mTBT_INFOR_SAT');
    modelTBT_INFOR_SAT.init(con);

    modelTBT_INFOR_SICLOP   = require('./model/auxiliares/mTBT_INFOR_SICLOP');
    modelTBT_INFOR_SICLOP.init(con);

    model_INFORTRM          = require('./model/principais/mINFORTRM');
    model_INFORTRM.init(con);

    modelTBA_REG_TRIBUTARIO = require('./model/auxiliares/mTBA_REG_TRIBUTARIO');
    modelTBA_REG_TRIBUTARIO.init(con);

    modelTBA_TP_FONE        = require('./model/auxiliares/mTBA_TP_FONE');
    modelTBA_TP_FONE.init(con);

    modelTBA_PENSAMENTO     = require('./model/auxiliares/mTBA_PENSAMENTO');
    modelTBA_PENSAMENTO.init(con);
    
    modelTBT_CAT_FISCAL_PADRAO = require('./model/auxiliares/mTBT_CAT_FISCAL_PADRAO');
    modelTBT_CAT_FISCAL_PADRAO.init(con);
    //#endregion

    //#region Models Principais
    modelTB_CAD_FUNC        = require('./model/principais/mTB_CAD_FUNC');
    modelTB_CAD_FUNC.init(con);

    modelTB_LOGRADOUROS     = require('./model/principais/mTB_LOGRADOUROS');
    modelTB_LOGRADOUROS.init(con);

    modelTB_IMPRESSORA      = require('./model/principais/mTB_IMPRESSORA');
    modelTB_IMPRESSORA.init(con);

    console.log("Tabelas principais criadas !!!");
    //#endregion

    await preencheTabelasAuxiliares();
    console.log("Tabelas auxiliares criadas !!!");
}


async function preencheTabelasAuxiliares() {
    await atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão (0/26)');
    focoLinhaInstalacao(linhaCheckTb3);

    let contadorGeral = 0;
    clearInterval(intervaloTbPrinc);
    //await atualizaMensagemAlert('3/3 - Preenchendo tabelas auxiliares com dados padrão');

    let listaLocalPedido = ["DELIVERY", "BALCÃO", "RETIRADA", "MESA"];
    for (let i = 0; i < listaLocalPedido.length; i++) {
        await modelTBA_LOCAL_PEDIDO.create({
            descricao : listaLocalPedido[i],
            habilitado: true
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaUF         = [ "AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"];
    let listaNomeUF     = [ "Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goiás", "Maranhão", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia", "Roraima", "Santa Catarina", "São Paulo", "Sergipe", "Tocantins"];
    for (let i = 0; i < listaUF.length; i++) {
        await modelTBA_UF.create({
            uf: listaUF[i],
            nome_uf : listaNomeUF[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');})
    }

    let listaFpgto      = ["DINHEIRO", "CHEQUE",  "VALE ALIMENTAÇÃO", "VALE REFEIÇÃO",   "DÉBITO" , "CRÉDITO", "CRÉDITO LOJA","VALE PRESENTE", "VALE COMBUSTÍVEL", "BOLETO BANCÁRIO", "SEM PAGAMENTO (p/ NFe)", "OUTROS"];
    let listaCodFpgto   = ["01"      ,  "02"   ,  "10"              , "11"            ,  "04"     , "03"     , "05"          , "12"          , "13"              , "15"             , "90"                             , "99"];
    for (let i = 0; i < listaFpgto.length; i++) {
        await modelTBA_FPGTO.create({
            descricao   : listaFpgto[i],
            cod_fpgto   : listaCodFpgto[i],
            habilitado  : true
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaFuncoes = [ "CAIXA", "MANOBRISTA", "GERENTE", "ADMINISTRADOR", "ENTREGADOR", "GARÇOM", "VENDEDOR", "PIZZAIOLO" ];
    for (let i = 0; i < listaFuncoes.length; i++) {
        await modelTBA_FUNCOES.create({
            descricao : listaFuncoes[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaNichos = [ "Água/Gás", "Barraca volante de alimentos", "Pizzaria/Esfiharia/China/Etc", "Truck Food", "Açaí/Sorveteria", "Auto/Moto Peças", "Bolos/Doces/Salgados", "Borracharia", "Conveniência (1.99)", "Material para Construção", "Mercearia", "Padaria", "Roupas/Sapatos", "Bares", "Casa Noturna", "Churrascaria", "Restaurante", "Pet", "Hotel/Motel", "Academia", "S@T SICLOP"];
    for (let i = 0; i < listaNichos.length; i++) {
        await modelTBA_NICHOS.create({
            descricao : listaNichos[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaIdNicho    = ["1","1","1","1","1","1","1","1","1","1","1","2","2","2","2","2","2","2","2","2","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","3","4","4","4","4","4","4","4","4","4","5","5","5","5","5","5","5","5","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","6","7","7","7","7","7","7","7","7","7","7","7","7","8","8","8","8","8","8","8","8","8","8","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","9","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","10","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","11","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","12","13","13","13","13","13","13","13","13","13","13","13","13","13","13","13","13","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","14","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","15","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","16","17","17","17","17","17","17","17","17","17","17","17","17","17","17","17","17","17","17","17","17","17","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","18","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","19","20","20","20","20","20","20","20","21"];
    let listaDescNicho  = ["Água/Gás","Água/Gás","Água/Gás","Água/Gás","Água/Gás","Água/Gás","Água/Gás","Água/Gás","Água/Gás","Água/Gás","Água/Gás","Barraca","Barraca","Barraca","Barraca","Barraca","Barraca","Barraca","Barraca","Barraca","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","PIZZARIA/Afins","Truck Food","Truck Food","Truck Food","Truck Food","Truck Food","Truck Food","Truck Food","Truck Food","Truck Food","Açaí/Sorveteria","Açaí/Sorveteria","Açaí/Sorveteria","Açaí/Sorveteria","Açaí/Sorveteria","Açaí/Sorveteria","Açaí/Sorveteria","Açaí/Sorveteria","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Auto/Moto Peças","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Bolos/Doces/Salgados","Borracharia","Borracharia","Borracharia","Borracharia","Borracharia","Borracharia","Borracharia","Borracharia","Borracharia","Borracharia","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Conveniência","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Materiais Construção","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Mercearia","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Padaria","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Roupas/Calçados","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Bares","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Casa Noturna","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Churrascaria","Restalrante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Restaurante ","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Pets","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Hotelaria/Motel","Academias","Academias","Academias","Academias","Academias","Academias","Academias","S@T SICLOP"];
    let listaDescGrupo  = ["ACESSORIOS","GLP 2","GLP 5","GLP 13","GLP 20","GLP 45","GLP 90","GALAO 5","GALAO 10","GALAO 20 ","FILTROS","LANCHES","MASSAS","OUTROS","PORCOES","REFEICOES","REFRIGERANTES","SALGADOS","SOBREMESAS","SUCOS","BEBIDA QUENTES","BEIRUTES","CALZONES","ESFIHAS","FOGAZZAS","INGREDIENTES","LANCHES","MASSAS","OUTROS","PIZZAS","PORCOES","REFEICOES","REFRIGERANTES","SALGADOS","SOBREMESAS","SUCOS","LANCHES","MASSAS","OUTROS","PORCOES","REFEICOES","REFRIGERANTES","SALGADOS","SOBREMESAS","SUCOS","LANCHES","OUTROS","PALITO","SALGADOS","SORVETE BOLA","SORVETE KILO","SORVETE POTE","SUCOS","ACESSORIOS","ADESIVOS","ANTENAS","CALCADOS","CAPACETE","ELETRICA","FERRAGEM","FERRAMENTAS","FILTROS","FREIOS","FUNILARIA","LAMPADAS","LANTERNAGEM","LATARIA","MONTAGEM","OLEO / LUBRIFICANTES","OUTROS","PECAS","PNEU","PORCAS /PARAFUSOS","RETROVISORES","SERVICO","SOM","TAPECARIA","TINTAS","VESTIMENTA","VIDROS","BOLOS","BOLOS FESTA","COMBOS","KITIS","LANCHES","MASSAS","OUTROS","PORCOES","REFRIGERANTES","SALGADOS","SOBREMESAS","SUCOS","ACESSORIOS","CAMERAS","FERRAMENTAS","KITIS","MONTAGEM","OUTROS","PNEU NOVO","PNEU USADO","REMENDOS","SERVICOS","ACESSORIOS","AGUA","ARTESANATOS","CALCADOS","CAMA /MESA /BANHO","CARVAO","CERVEJAS","CIGARROS","DOCES","LANCHES","MASSAS","OUTROS","PORCOES","REFEICOES","REFRIGERANTES","SALGADOS","SOBREMESAS","SUCOS","VESTUARIOS","ACABAMENTO","ACESSORIOS","ALVENARIA","AREIAS","AZULEJOS","BANHEIRAS","BLOCOS","CAIXAS D´AGUA","CANOS","CHUVEIROS","CIMENTO","CONEXOES","DECORACAO","ELETRICA","ELETRODOMESTICOS","ESGOTO","FECHADURAS","FERRAGEM","FERRAMENTAS","GESSO","HIDRAULICA","ILUMINACAO","JANELAS","JARDINAGEM","LONAS","LOUCAS","MADEIRAME","MARMORARIA","MOVEIS","OUTROS","PEDRAS","PORCAS /ARAFUSOS","PORTAS","TELHAS","TIJOLOS","TINTAS /VERNIZES","TOMADAS /INTERRUPTORES","TORNEIRAS","UTENCILIOS","VEDACAO","ACOUGUE","AGUA GALAO","BEBIDAS","BISCOITOS","BOLOS SABORES","BRINQUEDOS","CAMA /MESA /BANHO","CEREAIS","CERVEJAS","CHOCOLATES","CIGARROS","COPA","DECORACAO","DIVERSOS","DOCES","ELETRONICOS","EMBALAGENS","ENLATADOS","FERRAMENTAS","FESTAS","FRIOS","GAS","HIGIENE PESSOAL","HORTIFRUT","ILUMINACAO","LANCHONETE","MASSAS","MATERIAL ESCOLAR","MATERIAL LIMPEZA","MATINAIS","PADARIA","PAPELARIA","PERFUMARIA","PETS","REFRIGERANTES","SALGADOS","SUCOS","TEMPEROS","TOMADAS /INTERRUPTORES","UTILIDADES","AGUA GALAO","BEBIDAS","BISCOITOS","BOLOS SABORES","CEREAIS","CERVEJAS","CHOCOLATES","CIGARROS","COPA","DIVERSOS","DOCES","ELETRONICOS","ENLATADOS","FRIOS","GAS","HIGIENE PESSOAL","HORTIFRUT","ILUMINACAO","LANCHONETE","LEITE","MASSAS","MATERIAL ESCOLAR","MATERIAL LIMPEZA","MATINAIS","PADARIA","PAPELARIA","PERFUMARIA","PETS","PIZZAS","REFRIGERANTES","SALGADOS","SUCOS","TEMPEROS","UTILIDADES","ACESSORIOS","BIJUTERIAS","CALCADOS FEMININOS","CALCADOS INFANTIS","CALCADOS MASCULINO","CALCADOS UNISEX","CAMA /MESA /BANHO","CINTOS","COUROS","OUTROS","PERFUMARIA","VESTUARIO ESPORTIVO","VESTUARIO FEMININO","VESTUARIO INFANTIL","VESTUARIO MASCULINO","VESTUARIO UNISEX","ACOUGUE","AGUA GALAO","BEBIDAS","BISCOITOS","BOLOS SABORES","BRINQUEDOS","CAMA /MESA /BANHO","CEREAIS","CERVEJAS","CHOCOLATES","CIGARROS","COPA","DECORACAO","DIVERSOS","DOCES","DRINKS","ELETRONICOS","EMBALAGENS","ENLATADOS","FERRAMENTAS","FESTAS","FICHAS /JOGOS","FRIOS","GAS","HIGIENE PESSOAL","HORTIFRUT","ILUMINACAO","LANCHONETE","MASSAS","MATERIAL ESCOLAR","MATERIAL LIMPEZA","MATINAIS","PADARIA","PAPELARIA","PERFUMARIA","PETS","REFRIGERANTES","SALGADOS","SUCOS","TEMPEROS","UTILIDADES","ACESSORIOS","BEBIDAS","CERVEJAS","CIGARROS","COPA","DIVERSOS","DOCES","DRINKS","ESTACIONAMENTO","FESTAS","FICHAS /JOGOS","HIGIENE PESSOAL","LANCHONETE","PERFUMARIA","PROGRAMAS","REFRIGERANTES","SALGADOS","SUCOS","TRANSPORTE","BEBIDAS","BRINQUEDOS","CERVEJAS","CHOCOLATES","CIGARROS","DIVERSOS","DOCES","DRINKS","ELETRONICOS","ESTACIONAMENTO","FESTAS","FICHAS /JOGOS","LANCHONETE","MASSAS","REFEICAO A KILO","REFEICAO A LA CARTE","REFRIGERANTES","SALGADOS","SELFIE SERVICE","SUCOS","UTILIDADES","BEBIDAS","BRINQUEDOS","CERVEJAS","CHOCOLATES","CIGARROS","DIVERSOS","DOCES","DRINKS","ELETRONICOS","ESTACIONAMENTO","FESTAS","FICHAS /JOGOS","LANCHONETE","MASSAS","REFEICAO A KILO","REFEICAO A LA CARTE","REFRIGERANTES","SALGADOS","SELFIE SERVICE","SUCOS","UTILIDADES","ACESSORIOS","BANHO /TOSA","CIRURGIAS","COLEIRAS","CONSULTAS","FILHOTES /PETS","GAIOLAS","GARDEM","HOSPEDAGEM","MEDICAMENTOS","PERFUMES /ODORIZANTES","PET TAXI","RACAO AVES","RACAO CANINA","RACAO FELINA","RACAO ROEDORES","REPELENTES","SACARIAS","VACINAS","VENENOS","VERMIFUGOS","ACESSORIOS","ARTESANATOS","BRINQUEDOS","CAFÉ DA MANHA /TARDE","CAMA /MESA /BANHO","CERVEJAS","CHOCOLATES","CIGARROS","COPA","DIVERSOS","DOCES","DRINKS","ELETRONICOS","ESTACIONAMENTO","FESTAS","FICHAS /JOGOS","HIGIENE PESSOAL","HOSPEDAGEM","LANCHONETE","OUTROS DIVERSOS","PERFUMARIA","REFEICAO A KILO","REFEICAO A LA CARTE","REFEICAO SELFIE SERVICE","REFRIGERANTES","SALGADOS","SPA /LAZER","SUCOS","TRANSPORTE","UTILIDADES","ACESSORIOS","ISOTONICOS","LANCHONETE","MENSALIDADE","REFRIGERANTES","SUPLEMENTOS","VESTUARIO","IDEM AO NICHO"];
    let conjuntoDadosInsertTbtGrupos = [];
    for (let i = 0; i < listaIdNicho.length; i++) {
        conjuntoDadosInsertTbtGrupos.push({
            id_nicho    : listaIdNicho[i],
            desc_nicho  : listaDescNicho[i],
            desc_grupo  : listaDescGrupo[i]
        });
        contadorGeral++;
        if(i === (listaIdNicho.length - 1)) {
            await modelTBT_GRUPOS.bulkCreate(conjuntoDadosInsertTbtGrupos).then(dados=>{atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
        }
    }

    let listaModulos = ["Comanda Fácil", "LinkPed", "OlhoNoRango", "Robô", "S@T SICLOP"];
    for (let i = 0; i < listaModulos.length; i++) {
        await modelTBT_MODULOS_SICLOP.create({
            nome : listaModulos[i],
            ativo: false
        });
    }

    let listaNivelUsu = [ "OPERACIONAL", "GERENCIAL", "ADMINISTRADOR" ];
    for (let i = 0; i < listaNivelUsu.length; i++) {
        await modelTBA_NIVEL_USU.create({
            descricao : listaNivelUsu[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaPeriodo = [ "MANHÃ", "TARDE", "NOITE" ];
    let listaPeriodoHorarios = [ "07:00|13:00", "13:01|18:00", "18:01|06:59" ];
    for (let i = 0; i < listaPeriodo.length; i++) {
        await modelTBA_PERIODO.create({
            descricao : listaPeriodo[i],
            hora_inicio : listaPeriodoHorarios[i].split('|')[0],
            hora_termino : listaPeriodoHorarios[i].split('|')[1]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaSetor = [ "COMERCIAL", "CAIXA", "ADMNISTRATIVO", "ESTOQUE" ];
    for (let i = 0; i < listaSetor.length; i++) {
        await modelTBA_SETOR.create({
            descricao : listaSetor[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaSituacao = [ "ATIVO", "INATIVO" ];
    for (let i = 0; i < listaSituacao.length; i++) {
        await modelTBA_SITUACAO.create({
            descricao : listaSituacao[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaTpLogr = ["Rua", "Avenida", "Acesso", "Alameda", "Alto", "Área", "Balão", "Beco", "Boulevard", "Bosque", "Cais", "Calçada", "Caminho", "Clube", "Colónia", "Conjunto", "Condomínio", "Corredor", "Entrada", "Escada/Escadaria", "Estacionamento", 
     "Estrada", "Favela", "Galeria", "Granja", "Ilha", "Jardim", "Ladeira", "Largo", "Loteamento", "Monte", "Morro", "Parque", 
     "Pátio", "Passagem", "Passeio", "Ponte", "Praça", "Praia", "Quadra", "Recanto", "Retorno", "Rodovia", "Rotatória", 
     "Terminal", "Travessa", "Trecho", "Trevo", "Túnel", "Vereda", "Vale", "Viaduto", "Via", "Vi"];
    let conjuntoDadosInsertTpLogr = [];
    for (let i = 0; i < listaTpLogr.length; i++) {
        conjuntoDadosInsertTpLogr.push({
            descricao : listaTpLogr[i].toUpperCase()
        });
        contadorGeral++;
        if((listaTpLogr.length - 1) === i) {
            await modelTBA_TP_LOGR.bulkCreate(conjuntoDadosInsertTpLogr).then(dados=>{atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
        }
    }

    let listaTpProd = [ "SERVIÇO", "PRODUTO", "PIZZA" ];
    for (let i = 0; i < listaTpProd.length; i++) {
        await modelTBA_TP_PROD.create({
            descricao : listaTpProd[i],
            habilitado : true
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaTBA_TP_TRM = [ "CAIXA", "VENDAS", "ORÇAMENTO", "ADMINISTRATIVO", "TOTEM", "SERVIDOR", "MOBILE"];
    for (let i = 0; i < listaTBA_TP_TRM.length; i++) {
        await modelTBA_TP_TRM.create({
            descricao : listaTBA_TP_TRM[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaTpVenda = [ "A VISTA", "A PRAZO" ];
    for (let i = 0; i < listaTpVenda.length; i++) {
        await modelTBA_TP_VENDA.create({
            descricao : listaTpVenda[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaSiglaUnidMed   = ["UN"     , "KG"   , "LT"   , "DZ"   , "BL"   , "CX"   , "MT"   , "PC"  , "PT"    , "RM"]
    let listaUnidMed        = ["Unidade", "Quilo", "Litro", "Dúzia", "Bloco", "Caixa", "Metro", "Peça", "Pacote", "Resma"];
    for (let i = 0; i < listaUnidMed.length; i++) {
        await modelTBA_UNID_MED.create({
            sigla: listaSiglaUnidMed[i],
            descricao : listaUnidMed[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaSiglaCNH   = ["A"   , "B"    , "C"       , "D"                  , "E"              , "AB"          , "AC"             , "AD"                        , "AE"];
    let listaCatCNH     = ["MOTO", "CARRO", "CAMINHÃO", "ÔNIBUS/MICRO ÔNIBUS", "CAMINHÃO PESADO", "MOTO E CARRO", "MOTO E CAMINHÃO", "MOTO E ÔNIBUS/MICRO ÔNIBUS", "MOTO E CAMINHÃO PESADO"];
    for (let i = 0; i < listaCatCNH.length; i++) {
        await modelTBA_CAT_CNH.create({
            sigla : listaSiglaCNH[i],
            descricao : listaCatCNH[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaRedeSocial = [ "Facebook", "WhatsApp", "Telegram", "TikTok"];
    for (let i = 0; i < listaRedeSocial.length; i++) {
        await modelTBA_REDE_SOCIAL.create({
            descricao : listaRedeSocial[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaTipoConta = [ "Corrente", "Poupança", "Digital", "Salário"];
    for (let i = 0; i < listaTipoConta.length; i++) {
        await modelTBA_TIPO_CONTA.create({
            descricao : listaTipoConta[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaTipoMov = [ "Venda Simples", "Cancelado", "Alterado"];
    for (let i = 0; i < listaTipoMov.length; i++) {
        await modelTBA_TIPO_MOV.create({
            descricao : listaTipoMov[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaTBA_TSOCIAL = [ "PADRE", "CAPITÃO", "SARGENTO"];
    for (let i = 0; i < listaTBA_TSOCIAL.length; i++) {
        await modelTBA_TSOCIAL.create({
            descricao : listaTBA_TSOCIAL[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaCorredorProd = [ "HIGIENE/LIMPEZA", "MERCEARIA/CONFEITARIA", "BISCOITO/BOLACHA", "UTENSÍLIOS", "BEBIDAS"];
    for (let i = 0; i < listaCorredorProd.length; i++) {
        await modelTBA_CORREDOR_PROD.create({
            descricao : listaCorredorProd[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    let listaPensamentos    = [
        "A abelha atarefada não tem tempo para a tristeza. ",
        "A água de boa qualidade é como a saúde ou a liberdade: só tem valor quando acaba. ",
        "A água é o princípio de todas as coisas. ",
        "A água que não corre forma um pântano; a mente que não pensa forma um tolo!",
        "A alegria e o sofrimento são inseparáveis como compassos diferentes da mesma música. ",
        "A alegria evita mil males e prolonga a vida. ",
        "A alegria não está nas coisas, está em nós. ",
        "A arquitetura é o jogo sábio, correto e magnífico dos volumes dispostos sob a luz. ",
        "A árvore quando está sendo cortada, observa com tristeza que o cabo do machado é de madeira. ",
        "A bandeira é o símbolo da pátria, por isso merece respeito e amor. ",
        "A beleza é a melhor carta de recomendação. ",
        "A beleza na mulher honesta é como o fogo afastado ou a espada de ponta, que nem ele queima nem ela corta a quem deles se aproxima. ",
        "A boa educação é moeda de ouro. ",
        "A cidadania não é uma atitude passiva, mas ação permanente, em favor da comunidade. ",
        "A ciência é o grande antídoto contra o veneno do entusiasmo e da superstição. ",
        "A citricultura é um cultivo essencial para a nossa alimentação. ",
        "A coisa mais indispensável a um homem é reconhecer o uso que deve fazer do seu próprio conhecimento.",
        "A cortesia é a arte de fazer crer a cada um que, em relação aos outros, é o preferido. ",
        "A cura está ligada ao tempo e às vezes também às circunstâncias. ",
        "A cura para tudo é sempre água salgada: o suor, as lágrimas ou o mar. ",
        "A deficiência não precisa ser um obstáculo para o sucesso. ",
        "A democracia é o governo do povo, pelo povo, para o povo. ",
        "A desilusão é a visita da verdade. ",
        "A diferença entre o possível e o impossível está na vontade humana. ",
        "A distância longa é aquela entre a cabeça e o coração. ",
        "A dor é inevitável, porém o sofrimento é opcional.",
        "A educação é um processo social, é desenvolvimento. Não é a preparação para a vida, é a própria vida. ",
        "A esperança é o sonho do homem acordado. ",
        "A felicidade consiste num bom saldo bancário, numa boa cozinheira e numa boa digestão. ",
        "A felicidade do corpo consiste na saúde. A do espírito, na sabedoria. ",
        "A felicidade É as pequenas coisas, é somente observar, contemplar e agradecer!",
        "A felicidade é o subproduto do esforço de fazer o próximo feliz. ",
        "A felicidade é salutar parao corpo, mas só a dor robustece o espírito. ",
        "A felicidade não é uma estação onde chegamos, mas uma maneira de viajar. ",
        "A floresta esconde todos os símbolos. ",
        "A força da maternidade é maior que as leis da Natureza. ",
        "A força de vontade deve ser mais forte do que sua habilidade. ",
        "A força faz de você um grande guerreiro. Mas a fé te transforma em um grande vencedor.",
        "A humildade é a chave que abre todas as portas. ",
        "A humildade é a única base sólida de todas as virtudes. ",
        "A humildade é o primeiro degrau para a sabedoria. ",
        "A imprensa, tal como o fogo, é um excelente servidor, mas um terrível amo. ",
        "A informática está interligada ao mundo sobre as reações intergalaxias! ",
        "A inteligência é o único meio que possuímos para dominar os nossos instintos. ",
        "A intemperança da língua não é menos funesta para os homens que a da gula. ",
        "A ironia é o lirismo da desilusão. ",
        "A jornada deve ser mais  importante que o destino, pois é na jornada que tudo acontece  e você cresce.",
        "A leitura traz ao homem plenitude, o discurso segurança e a escrita precisão. ",
        "A leviandade é um salva-vidas na correnteza da vida. ",
        "A maior conquista da vida é estar em paz.",
        "A maioria das pessoas só aprende as lições da vida depois que a mão dura do destino lhe toca o ombro.",
        "A mais honrosa das ocupações é servir o público e ser útil ao maior número de pessoas. ",
        "A Matemática não mente. Mente quem faz mau uso dela. ",
        "A maturidade tem mais a ver com os tipos de experiência que se teve e o que você aprendeu com elas do que com quantos aniversários fez.",
        "A melhor coisa sobre uma fotografia é que ela não muda mesmo quando as pessoas mudam. ",
        "A moralidade é a melhor de todas as regras para orientar a humanidade. ",
        "A mudança não assegura necessariamente progresso, mas o progresso implacavelmente requer mudança. ",
        "A música é capaz de reproduzir, em sua forma real, a dor que dilacera a alma e o sorriso que inebria. ",
        "A Natureza é sábia e justa. O vento sacode as árvores, move os galhos, para que todas as folhas tenham o seu momento de ver o sol. ",
        "A noite acendeu as estrelas porque tinha medo da própria escuridão. ",
        "A palavra foi dada ao homem para disfarçar o pensamento. ",
        "A paz mais desvantajosa é melhor do que a guerra mais justa. ",
        "A paz não pode ser mantida à força. Ela somente acontece pelo entendimento",
        "A poesia tem comunicação secreta com o sofrimento do homem. ",
        "A poesia tem comunicação secreta com o sofrimento do homem. ",
        "A qualidade começa no interior e encontra o caminho para a fera. ",
        "A sabedoria da Natureza é tal que não produz nada de supérfluo ou inútil. ",
        "A saúde é conservada pelo conhecimento e observação do próprio corpo. ",
        "A solidariedade é o sentimento que melhor expressa o respeito pela dignidade humana. ",
        "A união do rebanho obriga o leão a deitar-se com fome. ",
        "A verdade não é a estrada da riqueza. ",
        "A verdadeira amizade deixa marcas positivas que o tempo jamais poderá apagar. ",
        "A verdadeira beleza não está no rosto onde muitos procuram, mas no coração onde poucos encontram.",
        "A verdadeira caridade é o desejo de ser útil aos outros sem pensar em recompensa.",
        "A verdadeira coragem é ir atrás de seu sonho mesmo quando todos dizem que ele é impossível. ",
        "A verdadeira viagem da descoberta consiste não em buscar novas paisagens, mas em ter olhos novos. ",
        "A vida começa todos os dias. ",
        "A vida é um caminho longo, onde você é mestre e aluno. Algumas vezes você ensina, mas todos os dias você aprende.",
        "A vida é um caminho, onde você é mestre e aluno,  Algumas vezes você ensina e todos os dias você aprende.",
        "A vida é uma constante lição, onde cada erro é uma porta para uma nova sala de aula.",
        "A vida humana, sem religião, é viagem sem roteiro, é navegação sem bússola. ",
        "A vida não é o que te ronda quando você Dorme. É o que brilha quando você Acorda!",
        "A vida não é uma pergunta a ser respondida. É um mistério a ser vivido. ",
        "A violência é o último refúgio do incompetente. ",
        "A violência não é força, mas fraqueza, nem nunca poderá ser criadora de coisa alguma, apenas destruidora. ",
        "A vitória tem um sabor mais doce quando você provou do fracasso. ",
        "Absorva o que for útil, Descarte o que não for e adicione aquilo que for unicamente seu!",
        "Acalme o vento dos seus pensamentos e não haverá ondas no oceano da sua mente.",
        "Acordar é um milagre diário que muitos  recebem, mas poucos sabem agradecer.",
        "Acreditar que tudo pode dar certo é uma forma de preparar o caminho para que algo bom aconteça.",
        "Acredite e aja como se fosse impossível fracassar. ",
        "Ainda que eu falasse a língua dos homens e dos anjos, se não tiver caridade, sou como o bronze que soa, ou como o címbalo que retine. ",
        "Algumas pessoas morrem aos 25 anos de idade, mas não são enterradas até os 75.",
        "Ambiente limpo não é o que mais se limpa e sim o que menos se suja. ",
        "Ame a Vida,  a Família e os Amigos, A vida é curta, a família é única e os amigos são raros!",
        "Amor de mãe não morre, só muda de atmosfera. ",
        "Antes de falar, ouça. Antes de agir, pense, Antes de criticar, conheça. E antes de desistir, tente!",
        "Ao acender uma luz na vida de alguém, Você acaba iluminando teu próprio caminha!",
        "Ao que tem fome dá o teu pão, mas ao triste dá-lhe o coração. ",
        "Aos outros dou o direito de ser como são. A mim dou o direito de ser cada dia melhor.",
        "Apague a luz, esqueça os problemas, descanse o corpo e deixe a alma livre para sonhar. ",
        "Apaixone-se pelo agora, pelo hoje, que te fazem respirar, enxergar, sentir, VIVER ",
        "Aprendi que são os pequenos acontecimentos diários que tornam a vida espetacular",
        "Argumentar com uma pessoa que renunciou ao uso da razão é como aplicar  remédio em pessoas  mortas.",
        "As cicatrizes nos ajudam a lembrar que o passado foi real. ",
        "As circunstâncias e os ambientes tem influência sobre nós, mas nós somos responsáveis por nós mesmos.",
        "As drogas me deram asas para voar, depois me tiraram o céu. ",
        "As folhas das árvores caem para nos ensinar a cair sem alardes. ",
        "As invenções são, sobretudo, o resultado de um trabalho teimoso. ",
        "As nações são todas mistérios. Cada uma é todo o mundo a sós. ",
        "As nossas digitais não se apagam na vida das pessoas que nós tocamos. ",
        "As paixões cegam. O verdadeiro amor nos torna lúcidos. ",
        "As palavras podem curar a alma que sofre. ",
        "As palavras são os suspiros da alma. ",
        "As pessoas que espalham amor não têm tempo nem disposição para jogar pedras. ",
        "As relações humanas são vastas como desertos: elas exigem toda a ousadia. ",
        "As tempestades sempre passa e as flores sempre voltam...",
        "Às vezes deve-se simplesmente concordar com o tolo para que ele pare de falar!",
        "Às vezes o homem mais  pobre deixa a seus filhos a herança mais rira: O AMOR.",
        "Às vezes precisamos mudar certas atitudes.Só para assim enxergar a verdadeira beleza da vida",
        "Assim como os tolos, os sábios cometem erros irreparáveis.",
        "Assim como uma gota de veneno compromete um balde inteiro, também a mentira, por menor que seja, estraga toda a nossa vida. ",
        "Bendito aquele que semeia livros e faz o povo pensar. ",
        "Boa publicidade não é só circular informação. É penetrar desejos e crenças na mente do público. ",
        "Bondade é amar as pessoas mais do que elas merecem. ",
        "Bondade é o único investimento que nunca vai à falência.",
        "Cada um de nós compõe a sua própria história e cada ser em si carrega o dom de ser capaz de ser feliz.",
        "Cada um sabe das delícias e das amarguras de ter o que tem e ser o que é.",
        "Com a mesma severidade com que julgas, você será em algum momento condenado.",
        "Com organização e tempo, acha-se o segredo de fazer tudo e bem-feito. ",
        "Comece fazendo o que é necessário, depois o que é possível, e de repente você estará fazendo o impossível.",
        "Como é bom saber que é possível recomeçar tudo de novo. ",
        "Como seriam venturosos os agricultores, se conhecessem os seus bens. ",
        "Comunicação é a arte de ser entendido. ",
        "Confiança não significa que tudo vai dar certo. Confiança significa acreditar que tudo já está certo.",
        "Construir pode ser a tarefa lenta e difícil de anos. Destruir pode ser o ato impulsivo de um único dia. ",
        "Corrija um sábio e o fará mais sábio, corrija um ignorante e o fará teu inimigo.",
        "Corrija um sábio e o fará mais sábio. Corrija um ignorante e o fará teu inimigo.",
        "Crianças, ao contrário dos adultos, sabem aproveitar o presente. ",
        "Cultivar estados mentais positivos, como a generosidade e a compaixão, leva à felicidade. ",
        "Dê a quem você ama: asas para voar, raízes para voltar e motivos para ficar. ",
        "De todos os meios que conduzem à sorte, os mais seguros são a perseverança e o trabalho. ",
        "Depois de um tempo você aprende a sutil diferença entre segurar uma mão e acorrentar uma alma.",
        "Depois de um tempo você aprende que até mesmo a luz do sol queima se você a tiver demais.",
        "Depois do verbo amar, ajudar é o mais belo verbo do mundo. ",
        "Deus constrói o seu templo no nosso coração sobre as ruínas das igrejas e das religiões. ",
        "Deus é onipotente e o Universo é infinito. ",
        "Deus não escuta a voz, mas sim o coração. ",
        "Deus se deixa conquistar pelo humilde e recusa a arrogância do orgulhoso. ",
        "Deus te fez forte suficiente para atravessar qualquer tempestade. Confie! ",
        "Deus, dai-me a serenidade para aceitar as coisas que eu não posso mudar, coragem para mudar as coisas que eu possa, e sabedoria para que eu saiba a diferença:",
        "Dez minutos orando são melhores do que um ano murmurando. ",
        "Digo: o real não está na saída nem na chegada: ele se dispõe para a gente é no meio da travessia. ",
        "Diplomata é um indivíduo cuja cor predileta é o arco-íris. ",
        "Do ponto de vista do planeta , não existe jogar lixo fora: porque não existe fora. ",
        "É durante as tempestades que o verdadeiro marinheiro aprende a velejar. ",
        "É impossível um home aprender aquilo que ele julga saber.",
        "É loucura jogar fora todas as chances de ser feliz porque uma tentativa não deu certo.",
        "É melhor conquistar a si mesmo do que vencer mil batalhas.",
        "É necessário sair da ilha pra ver a ilha. Não nos vemos se não sairmos de nós.",
        "É preciso coragem para enfrentar seus inimigos, mas é preciso o dobro da coragem para enfrenta seus amigos!",
        "É preciso dar força ao espírito para que o corpo se recupere. ",
        "É preciso força pra sonhar e perceber que a estrada vai além do que se vê. ",
        "É preciso não relaxar nunca, mesmo tendo chegado tão longe. ",
        "É preciso que a escola esteja aberta à colaboração da família, para que o diálogo se construa de forma produtiva. ",
        "É proibido desistir. Respire fundo e continue de formas diferentes.",
        "É triste pensar que a Natureza fala e que o gênero humano não a ouve. ",
        "Educai as crianças para que não seja necessário punir os adultos. ",
        "Em nossas vidas, a mudança é inevitável. A perda é inevitável. A felicidade reside na nossa adaptabilidade ao ruim. ",
        "Enquanto houver fé no seu coração e força nos seus braços, ainda há motivos para acreditar. Não desista. ",
        "Ensina a criança no Caminho em que deve andar, e mesmo quando idoso, não se desviará dele.",
        "Estamos aqui para sarar e não ferir... Para amar e não odiar... Para criar e não destruir.",
        "Estamos todos matriculados na escola da vida, onde o mestre é o tempo. ",
        "Eu acredito demais na sorte. E tenho constatado que, quanto mais duro eu trabalho, mais sorte eu tenho. ",
        "Existe no silêncio uma tão profunda sabedoria que às vezes ele se transforma na mais perfeita resposta. ",
        "Existem duas alavancas para mover os homens: interesse e medo. ",
        "Existem momentos inesquecíveis, coisas inexplicáveis e pessoas incompatíveis. ",
        "Existem pessoas que nos amam, mas simplesmente não sabem como demonstrar ou viver isso.",
        "Expresse gratidão com palavras e atitudes. Sua vida mudará muito de modo positivo. ",
        "Falei vezes como um palhaço, mas jamais duvidei da sinceridade da platéia que sorria. ",
        "Fazer de si mesmo prioridade de vez em quando, não é egoísmo.  É necessidade!",
        "Fazer o bem é mais suave prazer que se pode experimentar. ",
        "Fé é acreditar no que você não vê, a recompensa desta fé é ver o que você acredita. ",
        "Felicidade é um tantinho de fofura, de doçura ali, compreensão acolá... e um tantão de amor por toda parte!  ",
        "Feliz aquele que conseguiu compreender a causa das coisas. ",
        "Feliz de quem atravessa a vida inteira tendo mil razões para viver. ",
        "Feliz é aquele que semeia a paz... Distribui sorrisos e afetos... Faz somente o bem... Compartilha Amor aonde vai.",
        "Felizes os cães, que pelo faro descobrem os amigos. ",
        "Fotografar é colocar, na mesma linha de mira, a cabeça, o olho e o coração. ",
        "Grandes realizações não são feitas por impulso, mas por uma soma de pequenas realizações. ",
        "Gratidão é saber que todas as pessoas que cruzam nosso caminho são necessárias para nossa evolução.",
        "Guardar raiva é côo segurar um carvão em brasa com intenção de atirá-lo em alguém; é você que se queima.",
        "Há homens que são como as velas: sacrificam-se, queimando-se para dar luz aos outros. ",
        "Há muitas razões para duvidar e uma só para crer. ",
        "Há muros que só a Paciência derruba. E pontes que só o carinho constrói.",
        "Há pessoas tão alegres, tão meigas, tão felizes que ao entrar numa habitação parece que lhe dão luz. ",
        "Há um único recanto do universo que podemos ter certeza de melhorar: o nosso próprio eu. ",
        "Heróis são pessoas que fizeram o que era necessário fazer, enfrentando as conseqüências.",
        "Honre seu passado e liberte seus ancestrais para que o novo entre.",
        "Imprensa é um exército de 26 soldados de chumbo com o qual se pode conquistar o mundo. ",
        "Intenção sem Ação é pura Ilusão.  Ação sem intenção é pura Distração. ",
        "Jamais estará no escuro quem carrega luz dentro de si.",
        "Jesus foi só mais um carpinteiro que prometeu voltar outro dia para terminar o serviço. ",
        "Liberdade de voar num horizonte qualquer, liberdade de pousar onde o coração quiser. ",
        "Melhore tudo dentro de você para que tudo melhore ao redor dos seus passos.",
        "Mentalize todos as manhãs que as coisas boas da vida abram seu dia e te tragam toda felicidade que você mereça",
        "Mesmo que você não possa impedir o sofrimento de outro, importando-se irá diminuí-lo. ",
        "Minha força não foi conquistada levantando pesos. A conquistei levantando a mim mesmo todas as vezes que me derrubaram.",
        "Mire na lua, mesmo que erre ainda assim poderá acertar as estrelas.",
        "Muita vezes não é a motivação que precisa mudar, e sim, o foco",
        "Muitas coisas pequenas foram transformadas em grandes pelo tipo certo de publicidade. ",
        "Muitas pessoas devem a grandeza de suas vidas aos problemas que tiveram de vencer. ",
        "Muitas rugas podem recordar momentos de alegria e não tiram a dignidade do rosto. ",
        "Muitos homens iniciaram uma nova era na sua vida a partir da leitura de um livro. ",
        "Na juventude aprendemos, com a idade compreendemos. ",
        "Nada é difícil se for dividido em pequenas partes. ",
        "Nada vai embora sem antes ter nos ensinado o que precisamos saber.",
        "Não administre um negócio nem qualquer outra coisa baseado em teoria. ",
        "Não basta que seja pura e justa a nossa causa. É necessário que a pureza e a justiça existam dentro de nós. ",
        "Não concordo com uma palavra que estás dizendo! Mas defendo até à morte o direito de pronunciá-las.",
        "Não deixe ninguém estrague o seu dia, e nem tente estragar o dia do próximo, agindo assim com certeza terás  realmente um dia na paz.",
        "Não é o mais forte que sobrevive, nem o mais inteligente, mas o que melhor se adapta às mudanças. ",
        "Não é o mais forte que sobrevive, nem o mais inteligente, mas o que melhor se adapta às mudanças. ",
        "Não esconda sua sombra. Traga-a para a luz para  que possa ser curada.",
        "Não existe Escolha sem Perda. ",
        "Não Guarde nada para uma ocasião especial. A ocasião especial é cada dia que se vive.",
        "Não há paz sem justiça, não há justiça sem perdão. Não há perdão sem amor. ",
        "Não importa a cilindrada, o que interessa é o espírito. ",
        "Não importa em quantos pedaços seu coração foi partido, o mundo não para pra que você o conserte.",
        "Não importa onde já chegou, mas onde se está indo, mas se você não sabe para onde está indo qualquer lugar serve.",
        "Não morre aquele que deixou na terra a melodia de seu cântico na música de seus versos. ",
        "Não pode haver felicidade quando as coisas nas quais acreditamos são diferentes das que fazemos. ",
        "Não posso conhecer o desconhecido se ao conhecido me agarro.",
        "Não reclame do que te falta. Agradeça pelo o que você tem!",
        "Não são as ervas más que afogam a boa semente, e sim a negligência do lavrador. ",
        "Não se ensina a Filosofia: ensina-se a filosofar. ",
        "Não se pode descobrir novas terras sem aceitar perder de vista a costa por um longo tempo. ",
        "Não se pode ensinar tudo a alguém, pode-se apenas ajudá-lo a encontrar por si mesmo. ",
        "Não se preocupe com os que te odeiam. Preocupe-se com os que fingem gostar de você.",
        "Não vencemos todas as vezes que lutamos, mas perdemos todas as vezes que deixamos de luta.",
        "Nas grandes batalhas da vida, o primeiro passo para a vitória é o desejo de vencer. ",
        "Nascemos para fazer o bem, e fazê-lo é suficiente para encher uma vida. ",
        "Nem o sacerdote, nem o soldado devem sentir as inquietações de dúvida. ",
        "Nem sempre conseguimos fazer o que queremos, mas em todas elas fazemos o que é preciso fazer.",
        "Nem sempre é suficiente ser perdoado por alguém, algumas vezes você tem que perdoar a si mesmo.",
        "Nem todos os olhos fechados dormem, nem todos os olhos abertos veem!",
        "Nenhum obstáculo será tão grande se a tua vontade de vencer for maior. ",
        "Nenhum problema poderá ser resolvido pelo mesmo estado de consciência que o criou.",
        "Nenhuma sociedade pode fazer uma Constituição perpétua, ou sequer uma lei perpétua. ",
        "No cumprimento de deveres meu guia será a Constituição, que eu este dia jurei conservar, proteger e defender. ",
        "No dia em que não houver lugar para o índio no mundo, não haverá lugar para ninguém. ",
        "Nos olhos do jovem arde a chama. Nos olhos do vento brilha a luz. ",
        "Nossa liberdade depende da liberdade da imprensa, e ela não pode ser limitada sem ser perdida. ",
        "Nossas vidas começam a terminar no dia em que permanecemos em silêncio sobre as coisas que importam.",
        "Nossos corpos são nossos jardins, cujos jardineiros são nossas vontades. ",
        "Nunca deixe de acreditar em dias melhores, porque milagres acontecem todos os dias",
        "Nunca deixe que a saudade do passado e o medo do futuro estraguem a beleza do seu presente, pois há dias que valem um momento e momentos que valem por toda uma vida.",
        "Nunca discutas com um idiota. Ele arrasta-se até o nível dele, e depois vence-te com experiência.",
        "O amor é a asa veloz que Deus deu à alma para que voe até o céu. ",
        "O Amor é a melhor resposta. Não importa a qual é a pergunta!",
        "O amor é cego, por isso os namorados nunca vêem as tolices que praticam. ",
        "O Amor-próprio é um processo de libertação e de permitir a você mesmo ser exatamente como você é.",
        "O beijo é uma estrofe que duas bocas rimam. ",
        "O bom humor é a única qualidade divina do homem. ",
        "O café é a bebida que desliza para o estômago e põe tudo em movimento. ",
        "O cara só é sinceramente ateu quando está bem de saúde. ",
        "O cinema não tem fronteiras nem limites. É um fluxo constante de sonho. ",
        "O comércio liga toda humanidade em uma fraternidade de dependência e interesses mútuos. ",
        "O conjunto dos bens da terra destina-se, antes de mais nada, a garantir a todos os homens um decente teor de vida. ",
        "O corpo precisa de Descanso, a mente de Paz e o coração de Alegrias.",
        "O dia em que pararmos com Consciência Negra, Amarela ou Branca e nos preocuparmos com a Consciência Humana, o racismo desaparece. ",
        "O dinheiro faz homens ricos, o conhecimento faz homens sábios e a humildade faz homens grandes.",
        "O engenheiro trabalha com o quilômetro, o arquiteto com o metro, o designer com o centímetro. ",
        "O essencial é invisível para os olhos. Só se vê com o coração. ",
        "O essencial faz a vida valer a pena. ",
        "O folclore cria uma identidade para a região. ",
        "O gênio é composto por 2% de talento e de 98% de perseverante aplicação. ",
        "O grande homem é aquele que não perdeu a candura de sua infância. ",
        "O home sábio irá querer estar sempre com quem é melhor que ele.",
        "O Homem é dono do que cala e escravo do que fala.",
        "O ideal é ainda a alma de todas as realizações. ",
        "O importante é motivar a criança para a leitura, para a aventura de ler. ",
        "O importante não é viver, mas viver bem. ",
        "O livro traz a vantagem de a gente poder estar só e ao mesmo tempo acompanhado. ",
        "O maior dos crimes é matar a língua de uma nação com tudo aquilo que ela encerra de esperança e de gênio. ",
        "O mais belo triunfo do escritor é fazer pensar os que podem pensar. ",
        "O meu maior tesouro são os selos, porque neles contém cada lembrança minha. ",
        "O mistério gera curiosidade e a curiosidade é a base do desejo humano para compreender. ",
        "O mundo é o meu país, todo ser humano é meu irmão e fazer o bem é minha religião. ",
        "O Natal não é um período e nem uma estação, é um estado de espírito. ",
        "O ódio e o amor são gratuitos; Cada pessoa oferece o que de melhor tem no coração.",
        "O otimismo, é a fé em ação. ",
        "O otimista é um tolo. O pessimista, um chato. Bom mesmo é ser um realista esperançoso. ",
        "O papel de um comediante é fazer a platéia rir, no mínimo, uma vez a cada quinze segundos. ",
        "O pensamento faz o homem: por isso o bom pensamento é a coisa mais importante da vida. ",
        "O poeta é como o príncipe das nuvens. As suas asas de gigantes não o deixam caminhar. ",
        "O prazer aperfeiçoa a atividade. ",
        "O prazer de fazer o bem é maior do que recebê-lo. ",
        "O prazer de uma boa ação é o único prazer sem mistura de dor. ",
        "O prazer é a energia que une e agrega. ",
        "O preconceito da raça é injusto e causa grande sofrimento às pessoas. ",
        "O presente é a sombra que se move separando o ontem do amanhã. Nela repousa a esperança. ",
        "O primeiro em pedir desculpas é o mais valente. O primeiro em perdoar, ó mais forte. O primeiro em esquecer, é o mais feliz.",
        "O problema da morte é, no fundo, o problema da vida. ",
        "O problema que a maioria de nós tem é que preferimos ser arruinados por elogios do que salvos por críticas. ",
        "O professor medíocre conta. O bom professor explica. O professor superior demonstra. O grande professor inspira. ",
        "O que faz o homem feliz não é o dinheiro: são a retidão e a prudência. ",
        "O que faz uma República é a solidez das leis. ",
        "O que há de melhor no homem somente desabrocha quando se envolve em uma comunidade. ",
        "O que mais desespera, não é o impossível, mas o possível não almejado.",
        "O que não enfrentamos em n[os mesmos encontraremos como destino ",
        "O que não for bom para a colméia também não é bom para a abelha. ",
        "O que temos dentro de nós é o essencial para a felicidade humana. ",
        "O rio atinge seus objetivos porque aprendeu a contornar os seus obstáculos. ",
        "O segredo da criatividade é saber como esconder as suas fontes. ",
        "O segredo de seguir em frente é começar a trabalhar. ",
        "O sertanejo é, antes de tudo, um forte. ",
        "O silencio é a respiração da Alma. A sabedoria é a voz do Coração.",
        "O sinal mais seguro da sabedoria é a constante serenidade. ",
        "O sol é para as flores o que os sorrisos são para a humanidade. ",
        "O sol é para as flores o que os sorrisos são para a humanidade. ",
        "O sonho é o primeiro passo da realização de um projeto. ",
        "O teatro não pode desaparecer porque é a única arte em que a a humanidade enfrenta a sai mesma. ",
        "O tempo tem duas caras, se bem aproveitado será um grande aliado, se não, será seu pior inimigo. ",
        "O trabalho é, na maioria das vezes, o pai do prazer. ",
        "O único passo entre o sonho e a realidade é a Atitude.",
        "O vaso dá forma ao vazio e a música ao silêncio. ",
        "O vento e as ondas estão sempre do lado dos melhores marinheiros. ",
        "O verdadeiro heroísmo consiste em persistir por mais um momento quando tudo parece perdido. ",
        "O verdadeiro ofício de um ser humano é encontrar o seu próprio caminho. ",
        "O vício tem somente como recompensa o arrependimento. ",
        "O vício, tal como a virtude, cresce em passos pequenos. ",
        "Olá para você que acreditou e acredita que tudo vai dar certo.",
        "Onde acaba o amor têm início o poder, a violência e o terror. ",
        "Onde havia peixes, há mercúrio. Onde havia florestas, há cinzas. ",
        "Onde mora a liberdade, ali está a minha pátria. ",
        "Os anos enrugam a pele, mas renunciar ao entusiasmo faz enrugar a alma. ",
        "Os cabelos brancos são arquivos do passado. ",
        "Os erros passam. A verdade fica. ",
        "Os espinhos da vida se transformam em flores por toda eternidade. ",
        "Os melhores médicos do mundo são: o Dr. Dieta, o Dr. Tranqüilidade e o Dr. Alegria. ",
        "Os netos colherão os frutos das tuas árvores. ",
        "Os números governam o mundo. ",
        "Os tolos ignoram, os fracos duvidam, os sábios creem!",
        "Os tristes acham que o vento geme, os alegres acham que ele canta. ",
        "Ou você controla seus atos ou eles o controlarão. Ser flexível não significa ser fraco ou não ter personalidade,",
        "Outrora, a velhice era uma dignidade: hoje, ela é um peso. ",
        "Para ser eficaz, o trabalhador do conhecimento deve, em primeiro lugar, fazer as coisas certas acontecerem. ",
        "Pela aerodinâmica a abelha não deveria conseguir voar. Mas, como ela não sabe disso, voa do mesmo jeito. ",
        "Pensamos que aves na gaiola cantam, quando na realidade choram. ",
        "Pequenos descuidos podem produzir grandes males. ",
        "Plante seu jardim e enfeite sua alma, ao invés de esperar que alguém te traga flores. ",
        "Podemos escolher o que plantar, mas somos obrigados a colher o que semeamos. ",
        "Pode-se ter saudades dos tempos bons, mas não se deve fugir do presente. ",
        "Por mais humilde que seja, um bom trabalho inspira uma sensação de vitória. ",
        "Por menor que seja uma boa ação, um dia trará bons frutos. ",
        "Pouco importam as notas na música, o que conta são as sensações produzidas por elas. ",
        "Pregue o Evangelho em todo tempo. Se necessário, usa palavras. ",
        "Preservar um Patrimônio Histórico não é apenas manter de pé um passado mumificado, é, antes de tudo, conservar a cidadania de um povo. ",
        "Primavera não é uma simples estação de flores, é muito mais, é um colorido da alma. ",
        "Procure a sabedoria e aprenda a escrever os capítulos mais importantes da sua história nos momentos mais difíceis. Da sua vida. ",
        "Qualquer coisa que encoraje o crescimento de laços emocionais tem que servir contra as guerras. ",
        "Quando a casa do vizinho está pegando fogo a minha casa está em perigo. ",
        "Quando a mente está pensando, está falando consigo mesma. ",
        "Quando a tristeza bater à sua porta , abra um sorriso e diga... Desculpa, mas a hoje a felicidade chegou primeiro!",
        "Quando estiveres com raiva, tens o direto e estar com raiva, mas isso não te dá o direito de ser cruel.",
        "Quando não comunicamos adequadamente o que nos feriu, negamos ao outro a oportunidade de reparação.",
        "Quando não se tem nada, a Fé se torna tudo. ",
        "Quando o mar está calmo, qualquer um pode ser timoneiro. ",
        "Quando Pedro me fala sobre Paulo. Sei mais sobre Pedro do que de Paulo.",
        "Quando se planta cuidado e atenção. Colhe-se gratidão.",
        "Quando você encontrar o seu lugar. Guarde embaixo do solo suas mágoas. E deixe secar as velhas lágrimas sem dó",
        " Só o tempo desfaz suas marcas.",
        "Quanto mais nos elevamos, menores parecemos aos olhos daqueles que não sabem voar. ",
        "Que comete uma injustiça é sempre mais infeliz que o injustiçado.",
        "Que fogo poderia se igualar a um raio de sol num dia de inverno? ",
        "Que não nos falte sorriso no lábios... Paz de espírito e Amor no coração! ",
        "Que o novo se inicie e venha carregado de bençãos e vitórias para você.. pra mi... e pra à nossa volta!",
        "Que o teu trabalho seja perfeito para que, mesmo depois de tua morte, ele permaneça. ",
        "Que os bons ventos renovem a hoje e tragam esperanças de boas notas.",
        "Que você consiga enxergar o mundo com os olhos da fé e encarar cada dificuldade como uma oportunidade.",
        "Quem comete uma injustiça é sempre mais infeliz que o injustiçado. ",
        "Quem esbanja conhecimento, mindinha sabedoria.",
        "Quem não consegue atacar o argumento ataca o argumentador. ",
        "Quem olha para fora sonha, quem olha para dentro desperta.",
        "Quem quer ser um líder precisa estar disposto a aceitar os riscos que caminham lado a lado com o poder.",
        "Quem quer ter saúde no corpo, procure tê-la na alma. ",
        "Quem quer vencer um obstáculo deve armar-se da força do leão e da prudência da serpente. ",
        "Quero cada vez mais aprender a ver como belo aquilo que é necessário nas coisas!",
        "Repara que o outono é mais estação da alma do que da Natureza. ",
        "Respirar é a técnica mais simples e mais poderosa para proteger sua saúde. ",
        "Sábio é aquele que conhece os limites da própria ignorância.",
        "Saiba contemplar mais do que só ver. Agradecer mais do que pedir. Sentir mais do dizer. Acreditar mais do que temer.",
        "Saudade é um sentimento que quando não cabe no coração escorre pelos olhos. ",
        "Se amanhecer é o primeiro milagre do dia, agradecer é a primeira oração.",
        "Se as viagens no tempo existissem, a nossa época estaria recheada de turistas do futuro. ",
        "Se cada um cultivar afeto, beleza e lealdade em seu ambiente, por pequeno que seja, isso há de espalhar claridade no mundo.",
        "Se custa a sua paz, então é caro demais. ",
        "Se está com medo. Faz cara de que tem coragem e vá com medo mesmo. ",
        "Se não for pra me fazer voar bem alto, nem me faça tirar os pés do chão. ",
        "Se não me interessar pelo mundo, este baterá à minha porta pedindo-me contas. ",
        "Se não tivermos bibliotecas, não temos nenhum passado e não teremos um futuro. ",
        "Se o amor é fantasia, eu me encontro ultimamente em pleno carnaval. ",
        "Se podes dar uma só coisa para o seu filho, dá-lhe o entusiasmo. ",
        "Se tens fé, acharás que o caminho da virtude e da felicidade é muito curto. ",
        "Se tens que lidar com água, consulta primeiro a experiência, depois a razão. ",
        "Se todo animal inspira ternura, o que houve, então, com os homens? ",
        "Se todos os homens recebessem exatamente o que merecem, ia sobrar muito dinheiro no mundo. ",
        "Se você acreditar que as coisas darão certo, elas darão... Isso se chama fé.",
        "Se você é capaz de ser feliz quando está sozinho você aprendeu o segredo de ser feliz.",
        "Se você não pode explicar algo de forma simples, então você não entendeu muito bem o que tem a dizer.",
        "Se você não se curar do que te feriu, você vai sempre sangrar em cima dos que não te cortaram.",
        "Seja como um selo dos correios, cole-se a uma coisa até chegar ao seu destino. ",
        "Seja cortês com todos, sociável com muitos, íntimo de poucos, amigo de um e inimigo de nenhum. ",
        "Seja forte e corajoso! Não se apavore nem desanime. Existe uma força superior que a todos provê ",
        "Seja grato até pelo tombo, pois sem cair você não aprende a importância de se levantar.",
        "Sem ambição, nada se começa. Sem esforço, nada se completa. ",
        "Sempre devemos deixar as pessoas que amamos com palavras amorosas; pode ser a última vez que as vejamos.",
        "Sempre temos algum tempinho livre para ajudar alguém, é só querer que a gente acha. ",
        "Ser bom é fácil. O difícil é ser justo. ",
        "Ser feliz sem motivos é a mais autêntica forma de felicidade. ",
        "Ser fisioterapeuta é ter duas mãos e um coração entre elas, é manter expressão serena, mesmo com a alma desesperada. ",
        "Só há uma coisa constante neste mundo: a inconstância. ",
        "Só nos recordamos verdadeiramente daquilo que nos era destinado. A memória não lê as cartas alheias. ",
        "Só os tolos sabem tudo. O sábio aprende algo novo a cada dia.",
        "Só por que alguém não o ama do jeito que você quer que ame, não significa que esse alguém não o ama com tudo que pode.",
        "Só quando a última árvore for derrubada, o último peixe for morto e o último rio poluído é que o homem perceberá que não pode comer dinheiro. ",
        "Só quem entende a beleza do perdão pode julgar seus semelhantes. ",
        "Sobre a terra, antes da escrita e da imprensa, existiu a poesia. ",
        "Somente se aproxima da perfeição quem a procura com constância, sabedoria e, sobretudo, muita humildade. ",
        "Somos carteiros: temos a missão de caprichar na entrega, evitar que a carta chegue molhada e, principalmente, gostar do que fazemos. ",
        "Somos seres inacabados e estamos em eterna jornada de recriação de nós mesmos. ",
        "Somos tudo aquilo que fazemos  e não o que dizemos ser. A sinceridade das palavras  nem sempre refletem as atitudes.",
        "Sonhe grande, mas comece pequeno. Já mais troque essa ordem! Cedo ou tarde aqueles que o fazem não alcançam seus objetivos.",
        "Sua maior recompensa pelo seu trabalho não é o que você ganha, mas sim o que você se torna com ele. ",
        "Suporta de bom grado os defeitos alheios, se queres que os outros suportem os teus. ",
        "Tem palavras que chegam como um abraço. Tem abraço que não precisa de palavras.",
        "Tente se conhecer fora da sua zona de conforto e você verá como você é incrível.",
        "Ter grande conhecimento e não o usar, é como ter uma biblioteca e não abrir um único livro. ",
        "Toda grande jornada começa com o primeiro passo.",
        "Toda hora pode ser a hora única de quem sabe viver. ",
        "Toda profissão é grande quando exercida com grandeza. ",
        "Todas as leis humanas se alimentam da lei divina. ",
        "Todo homem tem o direito de decidir seu próprio destino. ",
        "Todo mundo é uma estrela e tem o direito de brilhar. ",
        "Todos os dias, entre sonhos e esperanças, começa uma nova jornada de nossas vidas. ",
        "Todos pensam em mudar o mundo, mas ninguém pensa em mudar a si mesmo. ",
        "Transforme a pedras em que você tropeça nas pedras da escada que você costroi.",
        "Três coisas que não podemos perder jamais: a Fé, que nos impulsiona; a Esperança, que nos encoraja e o Amor, que nos fortalece!",
        "Tudo aquilo que se compartilha, se multiplica. ",
        "Tudo é passageiro, mas na hora certa as  Boas coisas chegam pra ficar!",
        "Tudo o que um sonho precisa para ser realizado é alguém que acredita que ele possa ser realizado.",
        "Tudo que é necessário para realizar um vôo suave e fácil é voar solto e despreocupado. ",
        "Um caminho de mil quilômetros começa com o primeiro passo. ",
        "Um covarde morre mil vezes, um soldado só morre de um jeito. ",
        "Um dia lindo nem sempre é um dia de sol. Mas com certeza é um dia de paz.",
        "Um dia você começa a aprender que beijos não são contratos e presentes não são promessas.",
        "Um dia você descobre que se leva muito tempo para se tornar a pessoa que quer ser, e que o tempo é curto.",
        "Um grão de poesia basta para perfumar todo um século. ",
        "Um livro é a prova de que os homens são capazes de fazer magia. ",
        "Um pássaro nunca faz seu ninho em uma árvore seca. ",
        "Um pouco de aventura liberta a alma cativa do algoz cotidiano. ",
        "Um problema desaparece se você o aceita, um problema torna-se mais complexo se você entra em conflito com ele.",
        "Um sábio pode aprender mais com uma pergunta idiota, do que um idiota com uma resposta sábia.",
        "Um sorriso é a mais bela maquiagem que uma mulher pode usar em seu rosto. ",
        "Uma boa risada é um raio de Sol numa casa. ",
        "Uma cidade não é medida pelo seu comprimento e largura, mas pela amplitude de sua visão e pelo alto de seus sonhos. ",
        "Uma coletânea de pensamentos é uma farmácia moral onde se encontram remédios para todos os males. ",
        "Uma probabilidade razoável é a única certeza. ",
        "Uma vez perguntaram a Buda, “O homem iluminado tem amigos?” Ele disse “Não, pois ele também não pode ter inimigos!”.",
        "Uma vida saudável depende de comida saudável. ",
        "Uns sapatos que ficam bem numa pessoa são pequenos para outra; não existe uma receita para a vida que sirva para todos. ",
        "Valorize as pessoas que ficam com você durante a tempestade, porque em dias de sol a praia fica cheia.",
        "Viver a vida de forma positiva é descobrir a Bênção dos dias a toda hora e em cada local",
        "Você aprende a construir todas as estradas hoje porque o terreno de amanhã é demasiado incerto para planos.",
        "Você não deve se preocupar como que é dito pelo outro. O a verdadeira preocupação deve residir no  silêncio dela.",
        "Você não é o dinheiro que tem, não é o seu diploma ou seu currículo, nem o carro das garagem. Você é o sorriso que entrega, a mão que estende, o abraço que dá, Você é o amor que espalha.",
        "Você não fecha uma venda, você abre as portas de um relacionamento se quer construir um negócio duradouro e de sucesso. ",
        "Você não pode mudar o vento, mas pode ajustar as velas do barco para chegar onde quer.",
        "Você pode ir muito mais longe, mesmo depois de pensar que não consegue ir mais além.",
        "Você pode resumir em duas palavras  tudo que aprendeu sobre a vida. ELA CONTINUA.",
        "Voe na direção dos seus sonhos e quando alcançá-los, não se esqueça do lugar de onde você saiu.",
        "É com fé, paz, amor e afeto que a vida vai dando certo!",
        "Não demore muito para perceber. É preciso pouco para fazer!",
        "A vida é como teatro, todos os dias precisamos estar preparados para contracenar. O importante é saber o texto.",
        "Nunca confie na sua língua quando o seu  coração estiver magoado.",
        "A vida é como o jogo dos sete erros, a solução na maioria das vezes está nas coisa simples.",
        "O céu é para quem Sonha Grande, Pensa Grande, Ama Grande e tem coragem de Viver Pequeno.",
        "Há duas fontes perenes de alegria pura: O bem realizado e o dever cumprido.",
        "Você nunca será criticado por alguém que faça mais que você. Você só será criticado por aqueles que fazem menos.",
        "Que você não perca sua fé mesmo que você perca uma batalha",
        "Há uma coisa que ninguém pode tirar de ti. Tua fé!",
        "Você evoluirá um pouco mais espiritualmente quando conseguir preferir estar em paz ao invés querer de ter  razão.",
        "A cada dia você tem a chance pra ser melhor que foi ontem!",
        "Que a sua fé seja infinitamente que todos os teus medos.",
        "Dialogar é dizer o que pensamos e suportar o que os outros pensam.",
        "Tudo que um sonho precisa para ser realizado é de alguém que acredite que ele possa ser realizado",
        "Quando uma criatura humana desperta para um sonho e sobre ele lança toca sua força de alma , todo o Universo conspira a seu favor",
        "Não demore muito para perceber que é preciso pouco para ser feliz.",
        "Nunca confie na língua quando o coração estiver magoado",
        "A vida é como o jogo dos sete erros, na maioria das vezes a solução  est[a nas coisas simples.",
        "Há duas fontes perenes de alegria pura: O Bem realizado e o Dever cumprido!",
        "Viver a vida de forma positiva é descobrir a benção dos dias a toda hora e em cada local!",
        "Somente após a última árvores se cortada, o último peixe ser pescado, o último rio ser envenenado, somente então o homem descobrirá que dinheiro não pode ser comido!",
        "Se você fala com os animais, eles falam com você e você conhecerão um ao outro. Se não falar com eles você não os conhecerá e o que você não conhece, você temerá. E aquilo que não conhecemos, destruímos.",
        "Não espere que a vida seja generosa com você, se você não é grato pelo que ela já está te dando",
        "Fugir pra onde: Se tudo está dentro! Cuide da sua verdadeira casa... Tua mente e teu coração!",
        "Um leão falha de 7 a 10 vezes antes de conseguir capturar uma presa. Ou seja, 85% da sua vida é um fracasso. Você sabe o que faz dele um rei? A perseverança!",
        "Há muros que só a paciência derruba. E há pontes que só o caminho constrói.",
        "Para hoje... Que sejas grato, paciente e confiante. Que tenhas fé e saiba esperar o tempo certo de todas as coisas.",
        "Feliz daquele que desistiu de mudar os outros e passou a  investir seu tempo lapidando a si mesmo.",
        "Se dificuldades ameaçarem o teu equilíbrio, utiliza-te da oração. A prece é o mecanismo eficaz para todas as doenças da alma.",
        "Talvez você não tem a vida que sonhaste. Mas provavelmente possui a vida que muitos sonham. Seja grato por isso.",
        "Por mais curto que seja o caminho, quem pisa com força, deixa sua marca!",
        "Se você quer fazer a diferença, tem que avançar. Ninguém segue alguém que está parado!",
        "Você pode não ser bom o suficiente para todo mundo, mas com certeza é perfeito para alguém.",
        "Muito perdem na vida mais por MEDO, que por TENTATIVA!",
        "O segredo de qualquer conquista é a coisa mais simples do mundo: Ser grato e saber o que fazer com ela.",
        "Não espere que avida seja generosa com você se você não for grato pelo ela já te está dando!",
        "Não permita que ninguém estrague seu dia e nem tente estragar o dia do próximo. Agindo assim com certeza sempre terás um dia de Paz.",
        "Você não deve se angustiar por não conquistar  o impossível, mas sim por não alcançar o possível. ",
        "O que vale na vida não é o ponto de partida e sim a caminhada.  Caminhando e semeando, no fim terás o que colher.",
        "Lindas são as pessoas que mudam nossos dias com pequenas delicadezas, grandes sorrisos e boas palavras! ",
        "Você tem INIMIGOS? Bob. Significa que você BRIGOU por algo, alguma vez na vida. ",
        "O melhor professor na vida é o Tempo. Mesmos em você fazer perguntas, ele sempre te dará as melhores respostas.",
        "Parece que pros nossos amigos  nós não temos  tempo, mas pros  nossos inimigos, temos todo tempo do mundo.",
        "Faça o melhor que puder. Seja o melhor que puder. O resultado virá na mesma proporção de seu esforço."
    ];
    let listaAutores        = [
        "William Blake",
        "João Guimarães Rosa",
        "Tales de Mileto",
        "Vitor Hugo",
        "Hermann Hesse",
        "William Shakespeare",
        "Johann Goethe",
        "Le Corbusier",
        "Provérbio Árabe",
        "Autor Desconhecido",
        "Aristóteles",
        "Miguel de Cervantes",
        "Padre Antônio Vieira",
        "Tancredo Neves",
        "Adam Smith",
        "Autor Desconhecido",
        "Platão",
        "Edgar Quinet",
        "Hipócrates",
        "Isak Dinesen",
        "Stephen Hawking",
        "Abraham Lincoln",
        "Chico Xavier",
        "Louis Pasteur",
        "Thomas Merton",
        "Leandro Karnal",
        "John Dewey",
        "Aritósteles",
        "Jean-Jacques Rousseau",
        "Tales de Mileto",
        "Elcimar Maia",
        "Greta Palmer",
        "Marcel Proust",
        "Margareth Lee Rimbeuk",
        "Álvaro Botelho Maia",
        "Bárbara Kingsolver",
        "Muhammad Ali",
        "Elcimar Maia",
        "São Francisco de Assis",
        "Confúncio",
        "Tomás de Aquino",
        "James Fenimore Cooper",
        "Bill Gates",
        "Sigmund Freud",
        "Marquês de Marica",
        "Godofredo de Alencar",
        "Autor desconhecido",
        "Francis Bacon",
        "Ludwig Borne",
        "Autor desconhecido",
        "Napoleon Hill",
        "Michel de Montaigne",
        "Albert Einstein",
        "Willian Shakespeare",
        "Andy Warhol",
        "Friedrich Nietzsche",
        "Henry S. Commager",
        "Ludwig van Beethoven",
        "Humberto de Campos",
        "Mário Quintana",
        "Charles Talleyrand-Périgord",
        "Erasmo de Rotterdam",
        "Albert Einstein",
        "Pablo Neruda",
        "Pablo Picasso",
        "Bob Moawad",
        "Nicolau Copérnico",
        "Cícero",
        "Franz Kafka",
        "Provérbio Africano",
        "Jean-Jacques Rousseau",
        "Érico Veríssimo",
        "Autor Desconhecido",
        "@amorsimesempre",
        "Cora Coralina",
        "Marcel Proust",
        "Érico Veríssimo",
        "Autor Desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Provérbio Português",
        "Autor desconhecido",
        "Buda",
        "Isaac Asimov",
        "Benedito Croce",
        "Malcolm Forbes",
        "Bruce Lee",
        "Autor desconhecido",
        "Autor desconhecido",
        "Inês Seibert",
        "Charles Franklin Kettering",
        "Paulo de Tarso",
        "Benjamim Fraklin",
        "Chico Xavier",
        "Autor desconhecido",
        "Ruy Barbosa",
        "Elcimar Maia",
        "@harmoniaevidaplena",
        "Provérbio Chinês",
        "Chico Xavier",
        "Autor desconhecido",
        "Sirlei Lima",
        "Willian Shakespeare",
        "Autor desconhecido",
        "Hannibal Lecter",
        "Willian Shakespeare",
        "John Lennon",
        "Manoel de Barros",
        "Santos Dumont",
        "Charles Bukowiski",
        "Robert Pattinson",
        "Autor Desconhecido",
        "Ésquilo",
        "Pitágoras",
        "Irmã Dulce",
        "Patrick White",
        "Autor desconhecido",
        "Autor desconhecido",
        "Ruth E. Renkel",
        "Autor desconhecido",
        "João Dantas",
        "Mahatma Gandhi",
        "Castro Alves",
        "Leo Burnett",
        "Joseph Joubert",
        "Autor desconhecido",
        "Almir Sattler",
        "Autor Desconhecido",
        "Willian Shakespeare",
        "Pitágoras",
        "São Francisco de Assis",
        "Inajá Martins de Almeida",
        "Vigílio",
        "Peter Ustinov",
        "Osho",
        "Winston Churchill",
        "Abrahan Lincon",
        "Autor desconhecido",
        "Jean de La Bruyére",
        "Dalai Lama",
        "Dalai Lama",
        "Louis Reybaud",
        "Willian Shakespeare",
        "Willian Shakespeare",
        "Carlos Drummond de Andrade",
        "Ralph Waldo Emerson",
        "Giordano Bruno",
        "Clarice Lispector",
        "Papa João Paulo II",
        "Autor desconhecido",
        "Autor Desconhecido",
        "Charles Spurgeon",
        "João Guimarães Rosa",
        "Millôr Fernandes",
        "Autor Desconhecido",
        "Ricardo Jordão Magalhães",
        "Autor desconhecido",
        "Autor desconhecido",
        "Buda",
        "José Saramago",
        "Alvo Dumbledore",
        "Carlos Pastore",
        "Marcelo Camelo",
        "Paulo Coelho",
        "Andrea Ramal",
        "Elcimar Maia",
        "Victor Hugo",
        "Pitágoras",
        "Buda",
        "Autor Desconhecido",
        "Provérbios – 22.6",
        "Autor desconhecido",
        "Cora Coralina",
        "Thomas Jefferson",
        "Fernando Pessoa",
        "Napoleão Bonaparte",
        "Fernando Pessoa",
        "Willian Shakespeare",
        "Masaharu Taniguchi",
        "Charles Chaplin",
        "Autor desconhecido",
        "Henri Cartier-Bresson",
        "Santo Agostinho",
        "Autor desconhecido",
        "Virgílio",
        "Dom Hélder Câmara",
        "Autor desconhecido",
        "Machado de Assis",
        "Henri Cartier-Bresson",
        "Vincent van Gogh",
        "#Codigotriunfo",
        "Buda",
        "Padre Antônio Vieira",
        "Carlos Drummond de Andrade",
        "Cora Coralina",
        "Worl Beecher",
        "Aldoux Huxley",
        "Willian Shakespeare",
        "Autor Desconhecido",
        "Johannes Gutenberg",
        "O Pequeno príncipe",
        "Autor desconhecido",
        "Zé Rodrix",
        "Cecília Meireles",
        "Autor desconhecido",
        "Elcimar Maia",
        "Frank Clark",
        "Autor desconhecido",
        "Autor desconhecido",
        "Raphael  Michel",
        "Mark Twain",
        "Robert Baden-Powell",
        "Ivo Pitanguy",
        "Henry David Thoreau",
        "Marie Von Ebner-Eschenbach",
        "Henry Ford",
        "Autor desconhecido",
        "Harold Geneen",
        "Agostinho Neto",
        "Voltaire ",
        "Autor desconhecido",
        "Charles Darwin",
        "Charles Darwin",
        "Portal Filhos da Luz",
        "Leandro Karnal",
        "Autor desconhecido",
        "Papa João Paulo II",
        "Autor Desconhecido",
        "Willian Shakespeare",
        "Willian Shakespeare",
        "Cora Coralina",
        "Freya Stark",
        "Autor Desconhecido",
        "Elcimar Maia",
        "Confúcio",
        "Immanuel Kant",
        "André Gide",
        "Galileu Galileu",
        "Elcimar Maia",
        "Autor desconhecido",
        "Mahatma Gandhi",
        "Vicente Ferrer",
        "Anatole France",
        "Elcimar Maia",
        "Willian Shakespeare",
        "Sabedoria indígena",
        "Autor desconhecido",
        "Albert Einstein",
        "Thomas Jefferson",
        "Zachary Taylor",
        "Aílton Krenak",
        "Victor Hugo",
        "Thomas Jefferson",
        "Martin Luther King",
        "William Shakespeare",
        "Autor desconhecido",
        "Autor desconhecido",
        "Mrk Tain",
        "Michelangelo",
        "Autor desconhecido",
        "William Shakespeare",
        "Autor desconhecido",
        "Coelho Neto",
        "Arthur Schopenhauer",
        "Honoré de Balzac",
        "Millôr Fernandes",
        "Orson Welles",
        "James Abram Garfield",
        "Papa João XXII",
        "Autor Desconhecido",
        "Morgan Freeman",
        "Autor desconhecido",
        "Tomas Maldonado",
        "Antoine de Saint-Exupéry",
        "Charles Chaplin",
        "Autor Desconhecido",
        "Ludwig van Beethoven",
        "Provérbio Chinês",
        "Platão",
        "Sigmund Freud",
        "Getúlio Vargas",
        "Ziraldo",
        "Platão",
        "Mário Quintana",
        "Charles Nodier",
        "Eugene Delacroix",
        "Autor Desconhecido",
        "Nell Armstrong",
        "Thomas Paine",
        "Calvin Coolidge",
        "Autor desconhecido",
        "Helen Keller",
        "Ariano Suassuna",
        "Lenny Bruce",
        "James Allen",
        "Charles Baudelaire",
        "Aristóteles",
        "Epícuro",
        "Camilo Castelo Branco",
        "Autor Desconhecido",
        "Voltaire",
        "Frank Lloyd Wright",
        "Papa Francisco",
        "Abílio Guerra Junqueiro",
        "Norman Vincent Peale",
        "William Arthur Ward",
        "Demócrito",
        "Montesquieu",
        "Albert Einstein",
        "Robert Mallet",
        "Carl G. Jung",
        "Barão de Montesquieu",
        "Arthur Schopenhauer",
        "Lao-Tsé",
        "Albert Einstein",
        "Agatha Christie",
        "Euclides da Cunha",
        "Renan Carvalho",
        "Michel de Montaigne",
        "Thomas Addison",
        "Thomas Addison",
        "Lucian Rodrigues Cardoso",
        "Arthur Miller",
        "Autor Desconhecido",
        "Voltaire",
        "Autor desconhecido",
        "Georges Braque",
        "Edward Gibbon",
        "W. F. Grenfek",
        "Viktor Frankl",
        "Chico Xavier",
        "Jean Racine",
        "Autor desconhecido",
        "Carl Jung",
        "Roberto Burle Marx",
        "Benjamin Franklin",
        "Albert Schweitzer",
        "Edgar Allan Poe",
        "Denis Diderot",
        "Dom Bosco",
        "Jonathan Swift",
        "Virgílio",
        "Pitágoras",
        "Autor desconhecido",
        "Luís Fernando Veríssimo",
        "Willian Shakespeare",
        "François René de Chateaubriand",
        "Peter Drucker",
        "Mary Kay Ash",
        "John Webster",
        "Benjamin Franklin",
        "Willian Shakespeare",
        "Provérbio Chinês",
        "Michel de Montaigne",
        "Jack Kemp",
        "Masaharu Taniguchi",
        "Leonid Pervomaisky",
        "São Francisco de Assis",
        "André Raboni",
        "Jaak Bosmans",
        "Autor desconhecido",
        "Sigmund Freud",
        "Horácio",
        "Platão",
        "Insta Quote",
        "Willian Shakespeare",
        "Autor desconhecido",
        "São Francisco de Assis",
        "Públio Siro",
        "Sigmund Freud",
        "Misticos Online",
        "Autor Desconhecido",
        "",
        "Friedrich Nietzsche",
        "Platão",
        "Henry David Thoreau",
        "Autor desconhecido",
        "Autor desconhecido",
        "Leonardo da Vinci",
        "Autor desconhecido",
        "Elcimar Maia",
        "Platão",
        "Autor Desconhecido",
        "Paul Valéry",
        "Carl Jung",
        "Autor Desconhecido",
        "Francisco Quevedo",
        "Pindaro",
        "Nietzsche",
        "Friedrich Nietzsche",
        "Andrew Well",
        "Sócrates",
        "Vanessa Hass",
        "Bob Marley",
        "Autor desconhecido",
        "Stephen Hawking",
        "O Pequeno príncipe",
        "Autor desconhecido",
        "Elcimar Maia",
        "Autor Desconhecido",
        "José Saramago",
        "Ray Bradbury",
        "Vinícius de Moraes",
        "Bruce Barton",
        "Quintiliano",
        "Leonardo da Vinci",
        "João Guimarães Rosa",
        "Millôr Fernandes",
        "Autor desconhecido",
        "Osho",
        "Albert Einstein",
        "Dyll  Roha",
        "Josh Billing",
        "Benjamin Franklin",
        "Elcimar Maia",
        "Autor desconhecido",
        "Ralph Waldo Emerson",
        "Willian Shakespeare",
        "Martha Medeiros",
        "Victor Hugo",
        "Carlos Drummond de Andrade",
        "Érica Alves Serrão",
        "Jonathan Swift",
        "Vilhelm Ekelun",
        "Autor desconhecido",
        "Willian Shakespeare",
        "Provérbio Indígena",
        "Sócrates",
        "Pablo Neruda",
        "Jigoro Kano",
        "Ronald Golias",
        "Leonardo Boff",
        "Larissa Romeika",
        "Autor desconhecido",
        "Autor desconhecido",
        "São João Bosco",
        "Autor desconhecido",
        "Mahrukh",
        "Inácio Dantas",
        "Buda",
        "Gilberto Amado",
        "J. Jofrey",
        "Heráclito",
        "Bob Marley",
        "Marilyn Monroe",
        "Elcimar Maia",
        "Leon Tolstoi",
        "Sócrates",
        "Autor desconhecido",
        "Papa Francisco",
        "Autor desconhecido",
        "Roberto Shinyashiki",
        "Richard Bach",
        "Lao-Tsé",
        "Tupac Shakur",
        "Autor desconhecido",
        "Willian Shakespeare",
        "Willian Shakespeare",
        "José Marti",
        "Carl Sagan",
        "Provérbio Japonês",
        "Clarice Lispector",
        "Osho",
        "Bruce Lee",
        "Rafael Laimer Bilibio",
        "William Makepeace Thackeray",
        "Herb Caen",
        "Voltaire",
        "Samuel Howe",
        "Autor Desconhecido",
        "Autor Desconhecido",
        "Carl Jung",
        "Autor desconhecido",
        "Autor desconhecido",
        "Willian Shakespeare",
        "Elcimar Maia",
        "Autor Desconhecido",
        "Patrícia Fripp",
        "Confúcio",
        "Willian Shakespeare",
        "Autor desconhecido",
        "#Jardimsecreto",
        "#Tudodeotimo",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Padre Leo",
        "Autor desconhecido",
        "Estive Jobs",
        "Autor desconhecido",
        "Autor desconhecido",
        "Elcimar Maia",
        "Ibrare",
        "Autor desconhecido",
        "Autor desconhecido",
        "Roberto Shinyashiki",
        "Johann Goethe",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Ditado Indígena",
        "Chefe Dan George",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Cora Coralina",
        "Elcimar Maia",
        "Autor desconhecido",
        "Joanna de Ângelis",
        "Autor desconhecido",
        "Autor desconhecido",
        "Elcimar Maia",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Autor desconhecido",
        "Robert Mallet",
        "Cora Coralina",
        "Autor desconhecido",
        "Winston Churchill",
        "Autor desconhecido",
        "Leon  Uris",
        "Autor desconhecido"
    ];
    let conjuntoDadosInsertPensamentos = [];
    for (let i = 0; i < listaPensamentos.length; i++) {
        conjuntoDadosInsertPensamentos.push({
            frase : listaPensamentos[i],
            autor : listaAutores[i]
        });
        contadorGeral++;
        if((listaPensamentos.length - 1) === i){
            await modelTBA_PENSAMENTO.bulkCreate(conjuntoDadosInsertPensamentos).then(dados=>{atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
            conjuntoDadosInsertPensamentos = [];
        }
    }

    listaCAT_PADRAO = [
        "",
        "PIZZAS",
        "BEIRUTES",
        "INGREDIENTES",
        "ESFIHAS",
        "FOGAZZAS",
        "SALGADOS",
        "REFEIÇÕES",
        "LANCHES",
        "MASSAS",
        "OUTROS",
        "PORÇÕES",
        "REFRIGERANTES",
        "SOBREMESAS",
        "SUCOS",
        "BEBIDAS QUENTES",
        "CALZONES"
    ];
    listaFISCAL_PADRAO = [
        "",
        "0 19059090 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704802",
        "0 19059090 5102 102 0 0 49 0 0 49 0 0 0 0 0 1703100",
        "0 99999999 5102 102 0 0 49 0 0 49 0 0 0 0 0 0",
        "0 19023000 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704700",
        "0 19022000 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704802",
        "0 19021900 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704903",
        "0 21069029 5102 102 0 0 49 0 0 49 0 0 0 0 0 0",
        "0 21069090 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704802",
        "0 19023000 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704800",
        "0 19021900 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704900",
        "0 21069029 5102 102 0 0 49 0 0 49 0 0 0 0 0 0",
        "0 22021000 5405 102 0 0 49 0 0 49 0 0 0 0 0 0",
        "0 21069090 5102 102 0 0 49 0 0 49 0 0 0 0 0 2300200",
        "0 20098000 5405 102 0 0 49 0 0 49 0 0 0 0 0 1701000",
        "0 22084000 5405 102 0 0 49 0 0 49 0 0 0 0 0 0200400",
        "0 19022000 5102 102 0 0 49 0 0 49 0 0 0 0 0 1704802"
    ];

    for (let i = 0; i < listaCAT_PADRAO.length; i++) {
        await modelTBT_CAT_FISCAL_PADRAO.create({
            descricao       : listaCAT_PADRAO[i],
            dados_fiscais   : listaFISCAL_PADRAO[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

    await modelTBT_INFOR_SICLOP.create({
        nome_fantasia: 'SICLOP – Serviços e Sistemas',
        CNPJ: '19.832.566/0001-08',
        num_caixa: 12345678,
        chave_ac: 'chave_aqui'
    }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});

    let listaRegTributario = ["SIMPLES NACIONAL", "LUCRO PRESUMIDO", "LUCRO REAL"];
    for (let i = 0; i < listaRegTributario.length; i++) {
        await modelTBA_REG_TRIBUTARIO.create({
            descricao: listaRegTributario[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }
    
    let listaTBA_TP_FONE = ["RESIDENCIAL", "COMERCIAL", "RECADO", "VENDAS"];
    for (let i = 0; i < listaTBA_TP_FONE.length; i++) {
        await modelTBA_TP_FONE.create({
            descricao: listaTBA_TP_FONE[i]
        }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
    }

  /*  await modelTB_LOGRADOUROS.create({
        titulo: 'RUA',
        rua: 'Teste',
        bairro: 'Bairro de Teste',
        CEP: '042233220',
        range: '10',
        frete: 15.00,
        id_tplogr: 1,
        id_uf: 1,
        id_cidade: 1
    }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});
*/
    
    await model_INFORTRM.create({
        realiza_baixa: 1,
        abre_caixa: 1,
        fecha_caixa: 1,
        nome_trm: os.hostname(),
        ip_trm: ip.address(),
        sit_trm: 1,
        id_tpterminal: 1
    }).then(dados=>{contadorGeral++; atualizaLabelCheck(labelConjTab3, 'Preenchendo dados padrão ' + contadorGeral + ' informações inseridas!');});

    focoLinhaInstalacao(linhaCheckTb3, false);
    checkTab3.checked = true;
    // swal.close();
    swal({
        text: 'Banco criado com sucesso!',
        icon: 'success',
		closeOnClickOutside:false,
		closeOnEsc: false,
        button: {
            text:'Cadastrar dados do estabelecimento'
        }
    }).then(()=>{
        fs.writeFile(path.join('c:/siclop/hibrido/cfghibrido.txt'), 'dbcriado', (err)=>{if(err) throw err});        
        window.location.assign(path.join(__dirname , '../view/html/infoSis.html'));
    })

    /*
    let listaFpgto = [ "Crédito", "Débito", "Voucher", "Vale Refeição", "Vale Alimentação", "Dinheiro", "Cheque", "Boleto", "Depósito", "Transferência" ];
    for (let i = 0; i < listaFpgto.length; i++) {
        modelFpgto.insert((i + 1), listaFpgto[i]);
    }*/

}
//endregion

async function atualizaMensagemAlert(novaMensagem){
    swal_text.innerHTML = novaMensagem;
}

async function atualizaLabelCheck(elemento, texto){
    elemento.innerHTML = texto;
}

//SE EU QUISER, POSSO COLOCAR AS VARIÁVEIS, FUNCIONA DO MESMO JEITO
function focoLinhaInstalacao(elemento, focar = true){
    if(focar){
        elemento.style.border = '1px solid var(--azulSiclop)';
        elemento.style.borderRadius = '10px';
        elemento.style.boxShadow = '0 0 10px var(--azulSiclop)';
        elemento.style.background = 'var(--azulSiclop)';
        elemento.style.color = '#ffffff';
    }else{
        elemento.style.border = '0px';
        elemento.style.borderRadius = '10px';
        elemento.style.boxShadow = 'none';
        elemento.style.background = '#ffffff';
        elemento.style.color = '#000000';
    }
}