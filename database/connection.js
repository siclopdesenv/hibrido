const Sequelize = require ("sequelize");
const fs = require('fs');
const path = require('path');
//region Configurando a conexão com o banco

var Helper = require(path.join(__dirname, '../controller/Helper'));
Helper.verificaExistencia('cfghibridobd.txt', path.join('c:/siclop/hibrido/'), 'arquivo');

const cfgHibrido = fs.readFileSync(path.join('c:/siclop/hibrido/cfghibridobd.txt'),'utf-8').split('|');
const user = cfgHibrido[0];
const pass = cfgHibrido[1];
const database = cfgHibrido[2];
const host = cfgHibrido[3];
const port = cfgHibrido[4];

const objCon = 
  {
    username : user,
    password : pass,
    database : database,
    host: host,
    dialect: 'mysql',
    //logging: false,
    pool: {
      max: 20,
      min: 0,
      acquire: 60000,
      idle: 10000
    }
  };

const connection = new Sequelize(objCon); 
console.log(connection);

// require(path.join('../../database/criaBanco')); Chamar essa linha se não existir o banco

//endregion
module.exports = connection;