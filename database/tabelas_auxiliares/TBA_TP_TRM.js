const Sequelize = require('sequelize');
const connection = require('../connection');

const tbaTPtrm = connection.define('TBA_TP_TRM',{
    id_tpterminal: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey:true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbaTPtrm.sync({force: false});

module.exports = tbaTPtrm;