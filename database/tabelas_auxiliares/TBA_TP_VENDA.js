const Sequelize = require('sequelize');
const connection = require('../connection');

const tpVenda = connection.define('TBA_TP_VENDA',{
    id_tpvenda: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tpVenda.sync({force: false});

module.exports = tpVenda;