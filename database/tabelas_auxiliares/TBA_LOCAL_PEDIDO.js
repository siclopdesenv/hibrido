const Sequelize = require('sequelize');
const connection = require('../connection');

const tbaLocalpedido = connection.define('TBA_LOCAL_PEDIDO',{
    id_local: {
        type: Sequelize.INTEGER,
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    },
    habilitado: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbaLocalpedido.sync({force: false});

module.exports = tbaLocalpedido;