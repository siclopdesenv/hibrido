const Sequelize = require('sequelize');
const connection = require('../connection');

const tpLogr = connection.define('TBA_TP_LOGR',{
    id_tplogr: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(25),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tpLogr.sync({force: false});

module.exports = tpLogr;