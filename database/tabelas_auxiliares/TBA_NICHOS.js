const Sequelize = require('sequelize');
const connection = require('../connection');

const Nichos = connection.define('TBA_NICHOS',{
    id_nicho: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Nichos.sync({force: false});

module.exports = Nichos;