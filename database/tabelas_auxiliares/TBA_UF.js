const Sequelize = require('sequelize');
const connection = require('../connection');

const UF = connection.define('TBA_UF',{
    id_uf: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    uf: {
        type: Sequelize.STRING(2),
        allowNull: false
    },
    nome_uf: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

UF.sync({force: false});

module.exports = UF;