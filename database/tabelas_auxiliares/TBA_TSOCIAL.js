const Sequelize  = require('sequelize');
const connection = require('../connection');

//Exemplo: PADRE, CAPITÃO, SARGENTO ETC

const tbaTSocial = connection.define('TBA_TSOCIAL',{
    id_tsocial: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbaTSocial.sync({force: false});

module.exports = tbaTSocial;