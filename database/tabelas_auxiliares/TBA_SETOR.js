const Sequelize = require('sequelize');
const connection = require('../connection');

const Setor = connection.define('TBA_SETOR',{
    id_setor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Setor.sync({force: false});

module.exports = Setor;