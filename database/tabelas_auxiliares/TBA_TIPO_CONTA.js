const Sequelize     = require('sequelize');
const connection    = require('../connection');

//tipo conta banco
//descricao ex: (corrente, poupança)

const tipoConta = connection.define('TBA_TIPO_CONTA',{
    id_tipo_conta: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tipoConta.sync({force: false});

module.exports = tipoConta;