const Sequelize = require('sequelize');
const connection = require('../connection');

const tpTAB = connection.define('TBA_TP_TAB',{
    id_tp_tab: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tpTAB.sync({force: false});

module.exports = tpTAB;