const Sequelize = require('sequelize');
const connection = require('../connection');

const Situacao = connection.define('TBA_SITUACAO',{
    id_situacao: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Situacao.sync({force: false});

module.exports = Situacao;