const Sequelize = require('sequelize');
const connection = require('../connection');

const unid_medida = connection.define('TBA_UNID_MEDIDA',{
    id_unid_medida: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    sigla: {
        type: Sequelize.STRING(2),
        allowNull: false
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

unid_medida.sync({force: false});

module.exports = unid_medida;