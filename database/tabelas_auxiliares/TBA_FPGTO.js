const Sequelize = require('sequelize');
const connection = require('../connection');

const FPGTO = connection.define('TBA_FPGTO',{
    id_fpgto: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    cod_fpgto: {
        type: Sequelize.STRING(2),
        allowNull: true
    },
    descricao: {
        type: Sequelize.STRING(35),
        allowNull: true
    },
    habilitado: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

FPGTO.sync({force: false});

module.exports = FPGTO;