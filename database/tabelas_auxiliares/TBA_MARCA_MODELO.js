const Sequelize = require('sequelize');
const connection = require('../connection');

//Marca ou modelo de produto
//O 'cod_original' é o código que o produto recebeu quando foi fabricado e não o código dado pela loja.

const marcaModelo = connection.define('TBA_MARCA_MODELO',{
    id_mm: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    cod_original: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

marcaModelo.sync({force: false});

module.exports = marcaModelo;