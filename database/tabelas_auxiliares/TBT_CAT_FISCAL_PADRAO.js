const Sequelize = require('sequelize');
const connection = require('../connection');

//contém os dados fiscais das categorias que vem por padrão quando instala o SISTEMA.

const catFiscalPadrao = connection.define('tbt_cat_Fiscal_Padrao',{
    id_cat_padrao: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    dados_fiscais: {
        type: Sequelize.STRING(100),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

catFiscalPadrao.sync({force: false});

module.exports = catFiscalPadrao;