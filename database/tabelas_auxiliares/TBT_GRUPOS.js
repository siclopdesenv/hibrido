const Sequelize = require('sequelize');
const connection = require('../connection');

const tbtGrupos = connection.define('TBT_GRUPOS',{
    id_tbt_grupos: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    id_nicho: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    desc_nicho: {
        type: Sequelize.STRING(60),
        allowNull: false
    },
    desc_grupo: {
        type: Sequelize.STRING(60),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbtGrupos.sync({force: false});

module.exports = tbtGrupos;