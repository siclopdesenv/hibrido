const Sequelize = require('sequelize');
const connection = require('../connection');

const tbtModulosSiclop = connection.define('TBT_MODULOS_SICLOP',{
    id_modulo: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nome: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    ativo: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbtModulosSiclop.sync({force: false});

module.exports = tbtModulosSiclop;