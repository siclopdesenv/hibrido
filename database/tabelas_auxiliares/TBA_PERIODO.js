const Sequelize = require('sequelize');
const connection = require('../connection');

const Periodo = connection.define('TBA_PERIODO',{
    id_periodo: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    },
    hora_inicio: {
        type: Sequelize.STRING(5),
        allowNull: false
    },
    hora_termino: {
        type: Sequelize.STRING(5),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Periodo.sync({force: false});

module.exports = Periodo;