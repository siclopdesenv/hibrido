const Sequelize     = require('sequelize');
const connection    = require('../connection');

const RegTributario = connection.define('TBA_REGTRIBUTARIO', {
    id_regime_tributario: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

RegTributario.sync({force: false});

module.exports = RegTributario;