const Sequelize = require('sequelize');
const connection = require('../connection');

//Prateleira de produtos

const prateProd = connection.define('TBA_PRATE_PROD',{
    id_prate: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    num: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

prateProd.sync({force: false});

module.exports = prateProd;