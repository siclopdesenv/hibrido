const Sequelize = require('sequelize');
const connection = require('../connection');

//descricao (Telegram, WhatsApp, Facebook)

const redeSocial = connection.define('TBA_REDE_SOCIAL',{
    id_rede_social: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

redeSocial.sync({force: false});

module.exports = redeSocial;