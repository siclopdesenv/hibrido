const Sequelize = require('sequelize');
const connection = require('../connection');

//descricao ex: (A, B, ETC)

const catCNH = connection.define('TBA_CAT_CNH',{
    id_cat: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    sigla: {
        type: Sequelize.STRING(2),
        allowNull: false
    },
    descricao: {
        type: Sequelize.STRING(60),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

catCNH.sync({force: false});

module.exports = catCNH;