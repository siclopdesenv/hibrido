const Sequelize     = require('sequelize');
const connection    = require('../connection');
const InforSICLOP   = require('../tabelas_auxiliares/TBT_INFOR_SICLOP');

//A fk 'id_siclop' é para pegar a chave ac

const tbtInforSAT = connection.define('TBT_INFOR_SAT',{
    id_sat: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    marca: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    modelo: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    cod_ativacao: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    cod_paginas: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    tipo_driver: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    versao_xml: {
        type: Sequelize.STRING(10),
        allowNull: false
    },
    caminho_dll: {
        type: Sequelize.STRING(60),
        allowNull: false
    },
    pasta_txt: {
        type: Sequelize.STRING(60),
        allowNull: false
    },
    gerar_sat_automatico: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    sat_autonomo: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbtInforSAT.belongsTo(InforSICLOP, {foreignKey: 'id_siclop'});

tbtInforSAT.sync({force: false});

module.exports = tbtInforSAT;