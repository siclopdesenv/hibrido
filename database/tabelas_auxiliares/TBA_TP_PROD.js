const Sequelize = require('sequelize');
const connection = require('../connection');

const tp_prod = connection.define('TBA_TP_PROD',{
    id_tpprod: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    },
    habilitado: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tp_prod.sync({force: false});

module.exports = tp_prod;