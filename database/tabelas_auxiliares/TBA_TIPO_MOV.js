const Sequelize     = require('sequelize');
const connection    = require('../connection');

// descricao ex: (Alterado, Venda Simples, Cancelado)

const tipoMov = connection.define('TBA_TIPO_MOV',{
    id_tp_mov: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tipoMov.sync({force: false});

module.exports = tipoMov;