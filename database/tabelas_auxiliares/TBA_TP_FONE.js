const Sequelize     = require('sequelize');
const connection    = require('../connection');

//Tipo de telefone: residencial, comercial, recado, vendas etc

const tbaTpFone = connection.define('TBA_TP_FONE',{
    id_tp_fone: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbaTpFone.sync({force: false});

module.exports = tbaTpFone;