const Sequelize = require('sequelize');
const connection = require('../connection');

const NivelUsu = connection.define('TBA_NIVEL_USU',{
    id_nivel: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
},{
    timestamps : false,
    freezeTableName: true
});

NivelUsu.sync({force: false});

module.exports = NivelUsu;