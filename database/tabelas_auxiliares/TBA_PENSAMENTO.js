const Sequelize = require('sequelize');
const connection = require('../connection');

const tbaPensamento = connection.define('TBA_PENSAMENTO',{
    id_pensamento: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    frase: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    autor: {
        type: Sequelize.STRING(50),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbaPensamento.sync({force: false});

module.exports = tbaPensamento;