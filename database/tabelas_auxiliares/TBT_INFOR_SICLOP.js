const Sequelize = require('sequelize');
const connection = require('../connection');

const tbtInforSICLOP = connection.define('TBT_INFOR_SICLOP',{
    id_siclop: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nome_fantasia: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    CNPJ: {
        type: Sequelize.STRING(30),
        allowNull: true
    },
    num_caixa: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    chave_ac: {
        type: Sequelize.STRING(60),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbtInforSICLOP.sync({force: false});

module.exports = tbtInforSICLOP;