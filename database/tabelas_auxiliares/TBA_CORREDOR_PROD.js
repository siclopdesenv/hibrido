const Sequelize = require('sequelize');
const connection = require('../connection');

const corredorProd = connection.define('TBA_CORREDOR_PROD',{
    id_corredor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(30),
        allowNull: false
    },
    num: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
}, {
    timestamps : false,
    freezeTableName: true
});

corredorProd.sync({force: false});

module.exports = corredorProd;