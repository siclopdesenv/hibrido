const Sequelize = require('sequelize');
const connection = require('../connection');

const Funcoes = connection.define('TBA_FUNCOES',{
    id_funcao: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

Funcoes.sync({force: false});

module.exports = Funcoes;