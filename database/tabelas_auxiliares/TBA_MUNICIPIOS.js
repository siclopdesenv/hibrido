const Sequelize = require('sequelize');
const connection = require('../connection');

const tbaMunicipios = connection.define('TBA_MUNICIPIOS',{
    id_municipio: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    descricao: {
        type: Sequelize.STRING(15),
        allowNull: false
    }
}, {
    timestamps : false,
    freezeTableName: true
});

tbaMunicipios.sync({force: false});

module.exports = tbaMunicipios;