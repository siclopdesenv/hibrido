let path = require('path');
let conn = require(path.join(__dirname, './connection'));
let model = require(path.join(__dirname, './model/principais/mProdutos'));
let modelGp = require(path.join(__dirname, './model/principais/mGrupos'));

modelGp.init(conn);

model.init(conn);

model.hasMany(modelGp, {foreignKey : 'id_grupo'});
model.belongsTo(modelGp, {foreignKey : 'id_grupo'});

model.findAll({
    include: modelGp
}).then(response => {
    let jay = JSON.parse(JSON.stringify(response));
     jay.forEach((data)=>{
         console.log(data.nome_prod);
         console.log(data.Grupos[0].descricao)
     })
})