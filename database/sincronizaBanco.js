const cfgHibrido = require('fs').readFileSync(require('path').join('c:/siclop/hibrido/cfghibridobd.txt'),'utf-8').split('|');
const user = cfgHibrido[0];
const pass = cfgHibrido[1];
const database = cfgHibrido[2];
const host = cfgHibrido[3];
const port = cfgHibrido[4];
const mysql = require('mysql2/promise');
const path = require('path');
const swal = require('sweetalert');
let connection ;

swal({
    text: 'Sincronizando Banco de Dados',
    button: {
        text:'OK',
        closeModal: false
    }
});

mysql.createConnection({
    host     :host,
    user     : user,
    password : pass,
    database : database
}).then((conn)=>{
    connection = conn;
}).catch((err)=>{
    swal({
        icon: 'error',
        text: 'Erro ao se conectar ao banco: ' + err
    });
});

async function verificaExistenciaTabela(tabelas, caminhos, connection){
    
    await connection.query('show tables;').then(retorno => {

        for (let i = 0; i < tabelas.length; i++) {
            let achou = false;

            for (let index = 0; index < retorno[0].length; index++) {
                if(retorno[0][index].Tables_in_bdsiclop === tabelas[i]){
                    console.log('achou ' + tabelas[i]);
                    achou = true;
                    break;
                }
            }

            if(achou !== true){
                console.log('Não achou ========= ' + tabelas[i]);
                require(path.join(__dirname, caminhos[i]));
            }
            
        }

    });    

    swal.close();
}

/*connection.queryInterface.addColumn('Logs','cachorroMagro', Sequelize.STRING, {
    after: 'descricao'
})*/

module.exports = verificaExistenciaTabela;